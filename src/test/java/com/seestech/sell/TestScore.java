package com.seestech.sell;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hong610.net.http.client.Client;
import com.hong610.net.http.client.NetClient;
import com.seestech.sell.common.config.ApplicationConfig;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.EncodeMD5;
import com.seestech.sell.common.utils.FileUtils;
import com.seestech.sell.common.utils.StringUtils;
import com.seestech.sell.domain.model.FileInfo;
import com.seestech.sell.domain.model.Goods;
import com.seestech.sell.domain.model.Recorder;
import com.seestech.sell.domain.model.Score;
import com.seestech.sell.service.*;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.apache.coyote.http11.Constants.a;
import static org.aspectj.weaver.tools.cache.SimpleCacheFactory.path;

/**
 * Created by idiot on 2017/7/24.
 */
public class TestScore extends AppTest {
    private static Logger logger = LoggerFactory.getLogger(TestScore.class);

    @Resource
    private RestTemplate restTemplate;
    @Resource
    private IFileService fileService;
    @Resource
    private ApplicationConfig applicationConfig;
    @Resource
    private IDirectoryService directoryService;
    @Resource
    private IAgentService agentService;
    @Resource
    private IGoodsService goodsService;
    @Resource
    private IRecorderService recorderService;

    @Test
    public void testScore(){
        //d  aae9b8a167156071418adae918756e96
        /*String a = "c8a69723dab1b0f7,89600,1500866924";

        Long asTime, aeTime;
        String b;
        asTime = System.nanoTime();
        logger.error(asTime+"------");
        b = StringUtils.revert(a);
        aeTime = System.nanoTime();
        logger.error(aeTime+"------");
        logger.error("用时=======》》》"+(aeTime-asTime));
        logger.error(b+"****************");

        asTime = System.nanoTime();
        logger.error(asTime+"------");
        b = new StringBuffer(a).reverse().toString();
        // b = new StringBuilder(a).reverse().toString();
        aeTime = System.nanoTime();
        logger.error(aeTime+"------");
        logger.error("用时=======》》》"+(aeTime-asTime));
        logger.error(b+"****************");

        String c = b+","+(1500866924-9999);
        logger.error("c==="+c);
        String d = EncodeMD5.GetMD5Code(c);
        logger.error("d==============>>>>>>>>>>"+d);*/

        /************************/
        /*String code = "c8a69723dab1b0f7,89600,1500866924,aae9b8a167156071418adae918756e96";

        String a1 = code.substring(0, code.lastIndexOf(","));
        String b1 = new StringBuffer(a1).reverse().toString();
        long time = Long.parseLong(a1.substring(a1.lastIndexOf(",")+1));
        String c1 = b1+","+(time - 9999);
        String d1 = EncodeMD5.GetMD5Code(c1);
        logger.info("a1**"+a1+"*b1*"+b1+"*c1*"+c1+"*d1*"+d1);
        if(d1.equals(code.substring(code.lastIndexOf(",")+1))){
            logger.info("************");
        }


        String[] result = code.split(",");
        //前三组
        a1 = code.substring(0, code.lastIndexOf(","));
        //对a进行反转字符串
        b1 = new StringBuffer(a1).reverse().toString();
        //获取时间戳
        time = Long.parseLong(result[2]);
        //b 加上 分隔符  加上  时间戳减去9999
        String c = b1+","+(time - 9999);
        //获取加密后的c   也就是校验码  跟code的第四组对比
        d1 = EncodeMD5.GetMD5Code(c);
        if(d1.equals(code.substring(code.lastIndexOf(",")+1))){
            logger.info("************11111111111");
        }*/
    }

    @Test
    public void test1() throws Exception {
        /*String url = "http://test-api.shewngroup.com/gateway.cgi?mod=wine.consume&v=1";
        NetClient client = new NetClient(url);
        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("transaction_type", "defray.wine"));
        params.add(new BasicNameValuePair("point", "0.00"));
        params.add(new BasicNameValuePair("card_id", "a5028adfd16b2787"));
        params.add(new BasicNameValuePair("region", "上海"));
        params.add(new BasicNameValuePair("product_number", "876738514488655872"));
        params.add(new BasicNameValuePair("machine_number", "09140222"));
        params.add(new BasicNameValuePair("product_name", "美年达"));
        String res = client.doPost(params);
        logger.error(res+"------------");


        JSONObject object = new JSONObject();
        object.put("transaction_type", "defray.wine");
        object.put("point", "0.00");
        object.put("card_id", "a5028adfd16b2787");
        object.put("region", "上海");
        object.put("product_number", "876738514488655872");
        object.put("machine_number", "09140222");
        object.put("product_name", "美年达");

        MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<>();
        paramMap.add("transaction_type", "defray.wine");
        paramMap.add("point", "0.00");
        paramMap.add("card_id", "a5028adfd16b2787");
        paramMap.add("region", "上海");
        paramMap.add("product_number", "876738514488655872");
        paramMap.add("machine_number", "09140222");
        paramMap.add("product_name", "美年达");*/

        /*Score score = new Score();
        score.setTransaction_type("defray.wine");
        score.setPoint(0.00);
        score.setCard_id("a5028adfd16b2787");
        score.setProduct_number("876738514488655872");
        score.setMachine_number("09140222");
        score.setProduct_name("红酒1");
        score.setRegion("上海");*/

        // String result  = restTemplate.postForObject(url, paramMap, String.class);
        /*ResponseEntity<String> result  = restTemplate.postForEntity(url, paramMap, String.class);
        logger.error(result.toString());*/
    }

    @Test
    public void test_replaceFilePath(){
        /*String path = new String();
        FileInfo fileInfo = fileService.getFileByFileId(894540757363326976l);
        //替换字符串
        if(fileInfo.getFilePath().indexOf(applicationConfig.getUploadUrl()) != -1){
            path = fileInfo.getFilePath().substring(applicationConfig.getUploadUrl().length());
            path = FileUtils.addPathSeparate(FileUtils.getTrulyPath(Constants.Regular.base_dir), path);
        }
        logger.info(path+"==========================");*/
    }

    @Test
    public void test_directory(){
        /*int level = directoryService.getLevelByDirectoryId(889319216363077633l);
        logger.error(level+"*************");
        level = directoryService.getLevelByDirectoryId(889319283987841025l);
        logger.error(level+"*************");
        level = directoryService.getLevelByDirectoryId(889319371309056001l);
        logger.error(level+"*************");
        level = directoryService.getLevelByDirectoryId(889319458529607681l);
        logger.error(level+"*************");
        level = directoryService.getLevelByDirectoryId(889319527089700865l);
        logger.error(level+"*************");*/
    }

    @Test
    public void test_initRecorder(){
        /*Long agentId = 879583226564182016l;
        Long secondaryShelfId = 877341559861805056l;
        Map<String, Object> map = agentService.getGoodsInfo(agentId);
        if(map != null && map.size() > 0){
            String[] stocks = String.valueOf(map.get("stocks")).split(";");
            // String[] alrams = String.valueOf(map.get("alarms")).split(";");
            String[] remains = new String[]{};
            int[] remain = StringUtils.toIntArray(remains, stocks.length);
            //获取商品编号
            String[] goodsIds = String.valueOf(map.get("products")).split(";");
            Goods goods = goodsService.getGoodsByGoodsId(Long.parseLong(goodsIds[0]));
            if(goods != null){
                Recorder recorder = new Recorder();
                recorder.setAgentId(agentId);                               //终端编号
                recorder.setShelfId(secondaryShelfId);                      //货架编号
                recorder.setBatchNumber(StringUtils.generateUUID());        //设置batchNumber
                recorder.setOperate(0);                                     //减货操作
                recorder.setGoodsId(goods.getGoodsId());                    //商品编号
                recorder.setTotal(Integer.parseInt(stocks[0]));             //总量
                recorder.setAmount(0);                                      //现存量
                recorder.setRemain(StringUtils.arrayToString(remain, Constants.CsMessageSeperator.semicolon));
                recorderService.insert(recorder);
            }
        }*/
    }
}
