package com.seestech.sell;

import com.alipay.api.AlipayApiException;
import com.seestech.sell.common.utils.FileUtils;
import com.seestech.sell.common.utils.XStreamUtils;
import com.seestech.sell.domain.mapper.IUserDao;
import com.seestech.sell.domain.model.pay.WeichatBaseResult;
import com.seestech.sell.service.*;
import com.seestech.sell.service.pay.AlipayService;
import com.seestech.sell.service.pay.WeichatPayService;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.io.xml.XppDriver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Unit test for simple App.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
/*@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)*/
public class AppTest {

    private static Logger logger = LoggerFactory.getLogger(AppTest.class);

    @Resource
    private IUserDao userDao;
    @Resource
    private WeichatPayService weichatPayService;
    @Resource
    private AlipayService alipayService;
    @Resource
    private IOrderService orderService;
    @Resource
    private IRecorderService recorderService;
    @Resource
    private IDirectoryService directoryService;
    @Resource
    private IFileService fileService;
    @Resource
    private RestTemplate restTemplate;
    @Resource
    private IStatisticsService statisticsService;

    @Test
    public void test(){
        logger.info("测试类>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        // userDao.selectOne(new User());
        /*User u = userDao.getUserByName("test");
        logger.info(u+"----");*/
        logger.error(""+System.currentTimeMillis());
    }

    @Test
    public void testBeanToMap(){
        /*Recorder recorder = new Recorder();
        recorder.setAgentId(123456L);
        recorder.setRecorderId(12414543456L);
        recorder.setData1(1);
        recorder.setData2(2);
        recorder.setData3(3);
        recorder.setData4(4);
        recorder.setData5(5);
        recorder.setData6(6);
        recorder.setAmount(5);
        Map<String, Object> map =  beanToMap(recorder);
        logger.info(map.toString());*/
        /*String res = FileUtils.replacePath("http://www.idiotalex.com:1122/people/b094d20b34c3cca0960922cdc3221cba/1501742143533/15017421442487265.mp4", "http://www.idiotalex.com:1122", Constants.Regular.base_dir);
        logger.error(res);
        res = FileUtils.getTrulyPath(res);
        logger.error(res);*/
    }

    @Test
    public void test_restock() throws Exception {
        //补货接口
        /*String url = "http://192.168.1.126:8080/machine/api/httpTask/sellTask/restock.json";
        NetClient netClient = new NetClient(url, Config.CHARSET);
        String data = "{\"products\": [\"876738514488655872\",\"878546136938315776\"],\"stocks\": [10,20],\"total\":30}";
        JSONObject jsonObject = JSON.parseObject(data);
        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("agentCode","11111"));
        params.add(new BasicNameValuePair("data",data));
        String result = netClient.doPost(params);
        logger.info(result);*/
    }

    @Test
    public void test_weichat_prepay(){
       /* WeichatOrderResult result =weichatPayService.orderResult(String.valueOf(IdGen.get().nextId()), "测试商品", 1);
        if(result.getReturn_code().equals("SUCCESS") && "SUCCESS".equals(result.getResult_code())) {
            ResultBean resultBean = ResponseUtils.initResultBean(result, Constants.ResponseCode.success, "成功", true);
            logger.info(JSON.toJSONString(resultBean));
        }*/
    }

    @Test
    public void test_alipay_prepay() throws AlipayApiException {
        /*AlipayTradePrecreateResponse result = alipayService.preOrder(payConfig.getAlipay_app_id(),
                payConfig.getAlipay_public_key(), payConfig.getAlipay_private_key(),
                payConfig.getAlipay_sign_type(), payConfig.getAlipay_notify_url(),
                String.valueOf(IdGen.get().nextId()), 0.01, "测试商品");
        if (result.getCode().equals("10000")) { //返回CODE 为成功
            ResultBean resultBean = ResponseUtils.initResultBean(result, Constants.ResponseCode.success, "成功", true);
            logger.info(JSON.toJSONString(resultBean));
        }else {
            logger.error(JSON.toJSONString(result));
        }*/
    }

    @Test
    public void test_weichat_refund(){
        /*String refundId = String.valueOf(IdGen.get().nextId());
        String orderId = "890770455818928128";
        String certPath = "C:\\Users\\Administrator\\Desktop\\cert\\apiclient_cert.p12";
        WeichatRefundResult result = weichatPayService.refundResult(refundId, orderId, 1, 1, certPath);
        logger.info(JSON.toJSONString(result));
        if("SUCCESS".equals(result.getReturn_code()) && "SUCCESS".equals(result.getResult_code())){
            logger.error("**********退款成功");
        }else {
            logger.error("----------s退款失败");
        }*/
    }

    @Test
    public void test_alipay_refund() throws AlipayApiException {
        /*AlipayTradeRefundResponse result = alipayService.refundResponse("890201790711070720", 0.01, "测试");
        ResultBean resultBean = ResponseUtils.initResultBean(result, Constants.ResponseCode.success, "成功", true);
        logger.info(JSON.toJSONString(resultBean));*/
    }

    @Test
    public void test_queryOrderByOrderId(){
        /*Order order = orderService.getOrderByOrderId(879310907711160320L);
        ResultBean resultBean = ResponseUtils.initResultBean(order, Constants.ResponseCode.success, "成功", true);
        logger.info(JSON.toJSONString(resultBean));*/
    }

    @Test
    public void test_beanToXml(){
        WeichatBaseResult weichatBaseResult = new WeichatBaseResult();
        weichatBaseResult.setReturn_code("<![CDATA[SUCCESS]]>");
        weichatBaseResult.setReturn_msg("<![CDATA[ok]]>");

        /*因为_是关键字，默认的会变为__
        $也是关键字，默认为_-,这2个参数一个改变$的显示，一个改变_的显示*/
        XStream xstream = new XStream(new XppDriver(new XmlFriendlyNameCoder("_-", "_")));
        xstream.processAnnotations(WeichatBaseResult.class);
        System.out.println(xstream.toXML(weichatBaseResult));
        logger.info(XStreamUtils.xmlToBeanWithCDATA.toXML(weichatBaseResult).replaceAll("__", "_"));
    }

    @Test
    public void test_ceil(){
        logger.info("=================================================");
        logger.info("=================================================");
        logger.info("=================================================");
        /*logger.info(Math.ceil(2617/60/24.0)+"");
        logger.info(Math.ceil(1.8)+"");*/
        /*Long s1 = new Long(1);
        Long s2 = new Long(1);
        if(s1 == s2)
            logger.info("true");
        if(s1.equals(s2))
            logger.info("**********");
        if(s1==1)
            logger.info("----------");*/
        /*if(Integer.parseInt(String.valueOf("20")) == Integer.parseInt("20")){
            logger.error("**************111111111111");
        }*/
    }


    @Test
    public void testGetRcorder(){
        //根据agentId  shelfId goodsId  获取 最新的记录信息
        /*Recorder recorder = new Recorder();
        recorder.setAgentId(879583226564182016l);    //设置终端编号
        recorder.setShelfId(877341515817418752l);    //设置货架编号
        String[] ids = new String[]{"876738514488655872", "876645613716897792", "880617779990888448"};
        recorder.setGoodsIds(ids);             //设置商品编号
        recorder.setIsNew(1);                       //最新的记录
        List<Recorder> recorders = recorderService.getRecordersByInfo(recorder);
        logger.info(recorders.size()+"==============");*/
    }

    @Test
    public void testArrayToString(){
        /*String[] ids = new String[]{"876738514488655872", "876645613716897792", "880617779990888448"};
        String[] ids2 = new String[]{""};
        String[] ids3 = new String[]{};
        String result = StringUtils.arrayToString(ids3, ";");
        logger.error(result);*/

        /*String str1 = "23;45";
        String str2 = "";
        int[] ids4 = StringUtils.stringToArray(new int[]{}, str1, ";");
        if(ids4 == null || ids4.length == 0)
            logger.error("---------");
        for(Object o : ids4){
            logger.error(o.toString());
        }*/

    }

    @Test
    public void test_restore() throws Exception {
        /*String url = "http://localhost/machine/api/httpTask/sellTask/restock.json";
        NetClient client = new NetClient(url);
        List<BasicNameValuePair> params = new ArrayList<>();
        BasicNameValuePair agentCode = new BasicNameValuePair("agentCode", "09140222");
        Map map = new HashMap<String, Object>();
        List<String> products = new ArrayList<>();
        products.add("876738514488655872");
        products.add("880617779990888448");
        products.add("876645613716897792");
        List<Integer> stocks = new ArrayList<>();
        stocks.add(30);
        stocks.add(10);
        stocks.add(10);
        map.put("products", products);
        map.put("stocks", stocks);
        map.put("total", 80);
        JSONObject jsonObject = new JSONObject(map);
        //"{\"products\": [\"876738514488655872\",\"880617779990888448\",\"876645613716897792\"],\"stocks\": [30,10,10],\"total\":80}"
        BasicNameValuePair data = new BasicNameValuePair("data", jsonObject.toString());
        params.add(agentCode);
        params.add(data);
        String result = client.doPost(params);
        logger.error("******************************************");
        logger.error(result);
        logger.error("******************************************");*/
    }

    @Test
    public void test_directory(){
        /*String path = directoryService.getDirectoryPathBydirectoryId(888003614683955200l);
        logger.error("============》》》》》》"+path+"\r\n");

        path = fileService.getFilePath(888003614683955200l);
        logger.error("============》》》》》》"+path+"\r\n");*/

        String res = FileUtils.getWindowsPath(FileUtils.addPathSeparate("/people", "apiclient.p12"));
        logger.error(res);
    }

    @Test
    public void test_image() throws IOException {
        // int[] size = ImageUtils.getImageSize("F:\\document\\photo\\跑车\\11.jpg");
        /*int[] size = ImageUtils.getImageSize("F:\\document\\photo\\13.png");
        logger.info(size[0]+"==="+size[1]);*/
    }

    @Test
    public void test_uniqueCode(){
        /*String jsonStr = "{\n" +
                "    \"limit\": 50,\n" +
                "    \"start\": 0,\n" +
                "    \"success\": true,\n" +
                "    \"totalCount\": 0,\n" +
                "    \"resultInfo\": {\n" +
                "        \"status\": 0,\n" +
                "        \"statusText\": \"成功\"\n" +
                "    },\n" +
                "    \"data\": {\n" +
                "        \"layout\": \"2*6\",\n" +
                "        \"total\": 50,\n" +
                "        \"amount\": 6,\n" +
                "        \"grids\": [\n" +
                "            {\n" +
                "                \"createTime\": 1498293898000,\n" +
                "                \"updateTime\": 1499934897000,\n" +
                "                \"createUser\": 808949520158163000,\n" +
                "                \"updateUser\": 808949520158163000,\n" +
                "                \"status\": 1,\n" +
                "                \"remark\": null,\n" +
                "                \"detailId\": 878534400088735700,\n" +
                "                \"shelfId\": 877341515817418800,\n" +
                "                \"gridId\": 878123701441134600,\n" +
                "                \"gridName\": \"美年达专格\",\n" +
                "                \"goodsName\": \"美年达\",\n" +
                "                \"image\": \"http://www.idiotalex.com:1122/people/storage/goods/美年达600.jpg\",\n" +
                "                \"gridPrice\": 0.01,\n" +
                "                \"goodsPrice\": 0.02,\n" +
                "                \"goodsId\": 876738514488655900,\n" +
                "                \"userName\": null,\n" +
                "                \"totalGoods\": 10\n" +
                "            },\n" +
                "            {\n" +
                "                \"createTime\": 1498789570000,\n" +
                "                \"updateTime\": 1499934709000,\n" +
                "                \"createUser\": 808949520158163000,\n" +
                "                \"updateUser\": 808949520158163000,\n" +
                "                \"status\": 1,\n" +
                "                \"remark\": null,\n" +
                "                \"detailId\": 880613398822256600,\n" +
                "                \"shelfId\": 877341515817418800,\n" +
                "                \"gridId\": 878120971054088200,\n" +
                "                \"gridName\": \"大瓶可乐专格\",\n" +
                "                \"goodsName\": \"可口可乐\",\n" +
                "                \"image\": \"http://www.idiotalex.com:1122/people/storage/goods/可口可乐2000.jpg\",\n" +
                "                \"gridPrice\": 0.01,\n" +
                "                \"goodsPrice\": 0.02,\n" +
                "                \"goodsId\": 880617779990888400,\n" +
                "                \"userName\": null,\n" +
                "                \"totalGoods\": 5\n" +
                "            },\n" +
                "            {\n" +
                "                \"createTime\": 1498898466000,\n" +
                "                \"updateTime\": 1499934671000,\n" +
                "                \"createUser\": 808949520158163000,\n" +
                "                \"updateUser\": 808949520158163000,\n" +
                "                \"status\": 1,\n" +
                "                \"remark\": null,\n" +
                "                \"detailId\": 881070141595451400,\n" +
                "                \"shelfId\": 877341515817418800,\n" +
                "                \"gridId\": 879585691179155500,\n" +
                "                \"gridName\": \"七喜专格3\",\n" +
                "                \"goodsName\": \"七喜（小瓶）\",\n" +
                "                \"image\": \"http://www.idiotalex.com:1122/people/storage/goods/七喜330p.jpg\",\n" +
                "                \"gridPrice\": 0.01,\n" +
                "                \"goodsPrice\": 0.05,\n" +
                "                \"goodsId\": 876645613716897800,\n" +
                "                \"userName\": null,\n" +
                "                \"totalGoods\": 10\n" +
                "            },\n" +
                "            {\n" +
                "                \"createTime\": 1498901728000,\n" +
                "                \"updateTime\": null,\n" +
                "                \"createUser\": 808949520158163000,\n" +
                "                \"updateUser\": null,\n" +
                "                \"status\": 1,\n" +
                "                \"remark\": null,\n" +
                "                \"detailId\": 881083823725477900,\n" +
                "                \"shelfId\": 877341515817418800,\n" +
                "                \"gridId\": 878123701441134600,\n" +
                "                \"gridName\": \"美年达专格\",\n" +
                "                \"goodsName\": \"美年达\",\n" +
                "                \"image\": \"http://www.idiotalex.com:1122/people/storage/goods/美年达600.jpg\",\n" +
                "                \"gridPrice\": 0.01,\n" +
                "                \"goodsPrice\": 0.02,\n" +
                "                \"goodsId\": 876738514488655900,\n" +
                "                \"userName\": null,\n" +
                "                \"totalGoods\": 10\n" +
                "            },\n" +
                "            {\n" +
                "                \"createTime\": 1499150191000,\n" +
                "                \"updateTime\": null,\n" +
                "                \"createUser\": 808949520158163000,\n" +
                "                \"updateUser\": null,\n" +
                "                \"status\": 1,\n" +
                "                \"remark\": null,\n" +
                "                \"detailId\": 882125953428357100,\n" +
                "                \"shelfId\": 877341515817418800,\n" +
                "                \"gridId\": 878120971054088200,\n" +
                "                \"gridName\": \"大瓶可乐专格\",\n" +
                "                \"goodsName\": \"可口可乐\",\n" +
                "                \"image\": \"http://www.idiotalex.com:1122/people/storage/goods/可口可乐2000.jpg\",\n" +
                "                \"gridPrice\": 0.01,\n" +
                "                \"goodsPrice\": 0.02,\n" +
                "                \"goodsId\": 880617779990888400,\n" +
                "                \"userName\": null,\n" +
                "                \"totalGoods\": 5\n" +
                "            },\n" +
                "            {\n" +
                "                \"createTime\": 1499243730000,\n" +
                "                \"updateTime\": null,\n" +
                "                \"createUser\": 808949520158163000,\n" +
                "                \"updateUser\": null,\n" +
                "                \"status\": 1,\n" +
                "                \"remark\": null,\n" +
                "                \"detailId\": 882518282513416200,\n" +
                "                \"shelfId\": 877341515817418800,\n" +
                "                \"gridId\": 878123701441134600,\n" +
                "                \"gridName\": \"美年达专格\",\n" +
                "                \"goodsName\": \"美年达\",\n" +
                "                \"image\": \"http://www.idiotalex.com:1122/people/storage/goods/美年达600.jpg\",\n" +
                "                \"gridPrice\": 0.01,\n" +
                "                \"goodsPrice\": 0.02,\n" +
                "                \"goodsId\": 876738514488655900,\n" +
                "                \"userName\": null,\n" +
                "                \"totalGoods\": 10\n" +
                "            }\n" +
                "        ],\n" +
                "        \"shelfName\": \"百货货架1\",\n" +
                "        \"products\": [\n" +
                "            \"876738514488655872\",\n" +
                "            \"880617779990888448\",\n" +
                "            \"876645613716897792\"\n" +
                "        ]\n" +
                "    },\n" +
                "    \"map\": null\n" +
                "}";

        String jsonStr1 = "{\n" +
                "    \"limit\": 50,\n" +
                "    \"start\": 0,\n" +
                "    \"success\": true,\n" +
                "    \"totalCount\": 0,\n" +
                "    \"resultInfo\": {\n" +
                "        \"status\": 0,\n" +
                "        \"statusText\": \"成功\"\n" +
                "    },\n" +
                "    \"data\": {\n" +
                "        \"layout\": \"2*6\",\n" +
                "        \"total\": 50,\n" +
                "        \"amount\": 6,\n" +
                "        \"grids\": [\n" +
                "            {\n" +
                "                \"createTime\": 1498293898000,\n" +
                "                \"updateTime\": 1499934897000,\n" +
                "                \"createUser\": 808949520158163000,\n" +
                "                \"updateUser\": 808949520158163000,\n" +
                "                \"status\": 1,\n" +
                "                \"remark\": null,\n" +
                "                \"detailId\": 878534400088735700,\n" +
                "                \"shelfId\": 877341515817418800,\n" +
                "                \"gridId\": 878123701441134600,\n" +
                "                \"gridName\": \"美年达专格\",\n" +
                "                \"goodsName\": \"美年达\",\n" +
                "                \"image\": \"http://www.idiotalex.com:1122/people/storage/goods/美年达600.jpg\",\n" +
                "                \"gridPrice\": 0.01,\n" +
                "                \"goodsPrice\": 0.02,\n" +
                "                \"goodsId\": 876738514488655900,\n" +
                "                \"userName\": null,\n" +
                "                \"totalGoods\": 10\n" +
                "            },\n" +
                "            {\n" +
                "                \"createTime\": 1498789570000,\n" +
                "                \"updateTime\": 1499934709000,\n" +
                "                \"createUser\": 808949520158163000,\n" +
                "                \"updateUser\": 808949520158163000,\n" +
                "                \"status\": 1,\n" +
                "                \"remark\": null,\n" +
                "                \"detailId\": 880613398822256600,\n" +
                "                \"shelfId\": 877341515817418800,\n" +
                "                \"gridId\": 878120971054088200,\n" +
                "                \"gridName\": \"大瓶可乐专格\",\n" +
                "                \"goodsName\": \"可口可乐\",\n" +
                "                \"image\": \"http://www.idiotalex.com:1122/people/storage/goods/可口可乐2000.jpg\",\n" +
                "                \"gridPrice\": 0.01,\n" +
                "                \"goodsPrice\": 0.02,\n" +
                "                \"goodsId\": 880617779990888400,\n" +
                "                \"userName\": null,\n" +
                "                \"totalGoods\": 5\n" +
                "            },\n" +
                "            {\n" +
                "                \"createTime\": 1498898466000,\n" +
                "                \"updateTime\": 1499934671000,\n" +
                "                \"createUser\": 808949520158163000,\n" +
                "                \"updateUser\": 808949520158163000,\n" +
                "                \"status\": 1,\n" +
                "                \"remark\": null,\n" +
                "                \"detailId\": 881070141595451400,\n" +
                "                \"shelfId\": 877341515817418800,\n" +
                "                \"gridId\": 879585691179155500,\n" +
                "                \"gridName\": \"七喜专格3\",\n" +
                "                \"goodsName\": \"七喜（小瓶）\",\n" +
                "                \"image\": \"http://www.idiotalex.com:1122/people/storage/goods/七喜330p.jpg\",\n" +
                "                \"gridPrice\": 0.01,\n" +
                "                \"goodsPrice\": 0.05,\n" +
                "                \"goodsId\": 876645613716897800,\n" +
                "                \"userName\": null,\n" +
                "                \"totalGoods\": 10\n" +
                "            },\n" +
                "            {\n" +
                "                \"createTime\": 1498901728000,\n" +
                "                \"updateTime\": null,\n" +
                "                \"createUser\": 808949520158163000,\n" +
                "                \"updateUser\": null,\n" +
                "                \"status\": 1,\n" +
                "                \"remark\": null,\n" +
                "                \"detailId\": 881083823725477900,\n" +
                "                \"shelfId\": 877341515817418800,\n" +
                "                \"gridId\": 878123701441134600,\n" +
                "                \"gridName\": \"美年达专格\",\n" +
                "                \"goodsName\": \"美年达\",\n" +
                "                \"image\": \"http://www.idiotalex.com:1122/people/storage/goods/美年达600.jpg\",\n" +
                "                \"gridPrice\": 0.01,\n" +
                "                \"goodsPrice\": 0.02,\n" +
                "                \"goodsId\": 876738514488655900,\n" +
                "                \"userName\": null,\n" +
                "                \"totalGoods\": 10\n" +
                "            },\n" +
                "            {\n" +
                "                \"createTime\": 1499150191000,\n" +
                "                \"updateTime\": null,\n" +
                "                \"createUser\": 808949520158163000,\n" +
                "                \"updateUser\": null,\n" +
                "                \"status\": 1,\n" +
                "                \"remark\": null,\n" +
                "                \"detailId\": 882125953428357100,\n" +
                "                \"shelfId\": 877341515817418800,\n" +
                "                \"gridId\": 878120971054088200,\n" +
                "                \"gridName\": \"大瓶可乐专格\",\n" +
                "                \"goodsName\": \"可口可乐\",\n" +
                "                \"image\": \"http://www.idiotalex.com:1122/people/storage/goods/可口可乐2000.jpg\",\n" +
                "                \"gridPrice\": 0.01,\n" +
                "                \"goodsPrice\": 0.02,\n" +
                "                \"goodsId\": 880617779990888400,\n" +
                "                \"userName\": null,\n" +
                "                \"totalGoods\": 5\n" +
                "            },\n" +
                "            {\n" +
                "                \"createTime\": 1499243730000,\n" +
                "                \"updateTime\": null,\n" +
                "                \"createUser\": 808949520158163000,\n" +
                "                \"updateUser\": null,\n" +
                "                \"status\": 1,\n" +
                "                \"remark\": null,\n" +
                "                \"detailId\": 882518282513416200,\n" +
                "                \"shelfId\": 877341515817418800,\n" +
                "                \"gridId\": 878123701441134600,\n" +
                "                \"gridName\": \"美年达专格\",\n" +
                "                \"goodsName\": \"美年达\",\n" +
                "                \"image\": \"http://www.idiotalex.com:1122/people/storage/goods/美年达600.jpg\",\n" +
                "                \"gridPrice\": 0.01,\n" +
                "                \"goodsPrice\": 0.02,\n" +
                "                \"goodsId\": 876738514488655900,\n" +
                "                \"userName\": null,\n" +
                "                \"totalGoods\": 10\n" +
                "            }\n" +
                "        ],\n" +
                "        \"shelfName\": \"百货货架1\",\n" +
                "        \"products\": [\n" +
                "            \"876738514488655872\",\n" +
                "            \"880617779990888449\",\n" +
                "            \"876645613716897792\"\n" +
                "        ]\n" +
                "    },\n" +
                "    \"map\": null\n" +
                "}";
        String res = EncodeMD5.GetMD5Code(jsonStr);
        String res1 = EncodeMD5.GetMD5Code(jsonStr1);
        logger.error(res+"***");
        logger.error(res1+"---");*/
    }

    @Test
    public void test_goods_remain(){
        /*MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<>();
        paramMap.add("agentId", "890104324254859264");
        String res = restTemplate.postForObject("http://192.168.1.125/machine/stock/getRemainStocks.json", paramMap, String.class);
        logger.error(res);*/
    }

    @Test
    public void test_insertBatchStatistics(){
        /*String url = "http://192.168.1.126/machine/api/httpTask/agentTask/addStatistics.json";
        String res = "{\"agentCode\": \"09140222\", \"type\": 1, \"items\": [{\"name\":\"1\",\"tapArea\":\"2\", \"time\": 3}]}";
        JSONObject data = JSON.parseObject(res);
        MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<>();
        paramMap.add("data", data);
        String result = restTemplate.postForObject(url, paramMap, String.class);
        logger.error(result);*/
    }

    @Test
    public void test_thread(){
        /*new Test0805().start();

        String b1 = new String("00 4A 00 02 17 A0 0D 0A");
        Test0805.addRequestFacade(b1);*/
    }

    @Test
    public void test_path(){
        /*logger.error(this.getClass().getResource("/").getPath()+"***");
        logger.error(Thread.currentThread().getContextClassLoader().getResource("").getPath());*/
    }

}
