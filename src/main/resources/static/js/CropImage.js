/**
 * 加载本地图片  FileReader
 */
//判断浏览器是否支持FileReader接口
if (typeof FileReader == 'undefined') {
    $("#avatar-form .avatar-wrapper").text('当前浏览器不支持FileReader接口')
    //使选择控件不可操作
    $("#avatarInput").attr("disabled", "disabled");
}

var baseurl = $("#contextPath").val()

//裁剪图片插件
var jcrop_api;
var options = {
    bgFade: true,
    bgOpacity: .2,
    boxWidth: 800,
    boxHeight: 600,
    allowResize: false, //不允许缩放选框
    minSize: [600, 300],
    maxSize: [600, 300],
    setSelect: [0, 0, 600, 300]
}

function init() {
    //初始化该图片剪裁插件
    if(jcrop_api != null)
        jcrop_api.destroy()
    $('#jcrop-image').Jcrop(options,function(){
        jcrop_api = this;
    });
    return jcrop_api
}

jcrop_api = init()

//初始化 size
var size = jcrop_api.getBounds()

//file标签  改变事件
$("#avatarInput").change(function () {
    xmTanUploadImg($(this))
})

//选择图片，马上预览
function xmTanUploadImg(obj) {
    var files = $(obj).prop('files')
    if(files.length > 1){
        alert('注意：当前预览只是第一张')
    }
    var file = files[0];

    if(!isImageFile(file)){
        layer.msg('不是图片类型,请重试')
        return
    }

     //console.log(file);
    /*console.log(obj);
     console.log("file.size = " + file.size);*/  //file.size 单位为byte

    var reader = new FileReader();
    reader.readAsDataURL(file)
    //读取文件过程方法
    reader.onload = function (e) {
       // console.log("成功读取....");

        //加载图片获取图片真实宽度和高度
        var data = e.target.result
        var image = new Image();
        image.src= data;
        image.onload=function(){
            size[0] = image.width;
            size[1] = image.height;

            //加载图片完成之后  修改图片  选项
            jcrop_api.setImage(data)     //改变图片
            jcrop_api.setOptions(options)           //设置参数

            tips()
        };
    }

}

//提示信息
function tips() {
    //console.log(size)
    if(size[0] < 600 || size[1] < 300){
        layer.msg('提示：为保证图片清晰,请选择【600,300】尺寸以上的图片')
        jcrop_api.disable() //禁用
    }else
        jcrop_api.enable()  //启用
}

var layer_index;    //弹出层的标识
//checkbox选中事件
$("#isRotation").change(function () {
    //如果复选框选中了  加载轮播图上传页面
    var value = $("#isRotation").val()
    if(value == '0') {
        $("#rotation_img").hide()
    } else{
        layer_index = layer.open({
            type: 1,
            title: '裁剪轮播图',
            closeBtn: 0,
            area: ['850px', '780px'],
            shadeClose: true,
            content: $('#avatar-form')
        })

        tips()  //提示信息
    }

})

//点击完成  裁剪事件
$("#crop").click(function () {
    var re = jcrop_api.tellScaled()
    $("#x").val(re.x)
    $("#y").val(re.y)
    $("#w").val(re.w)
    $("#h").val(re.h)
    var options = {
        data: $("#avatar-form").serialize(),
        dataType:'json',
        type: 'POST',
        url: baseurl + '/news/newsList/cutImage',
        success:  function(res){
            if(res.code == 0){
                layer.msg('裁剪图片成功')
                $("#rotation_img").removeClass("hide")
                var data = res.data
                $("#rotationImage").val(data)
                $("#rotationImage-pre").attr("src", data).show().removeClass('hide')
                layer.close(layer_index)    //关闭当前裁剪层
            }else {
                layer.msg('裁剪图片失败,请重试')
            }
        }
    };
    $("#avatar-form").ajaxSubmit(options)
})


//判断是否是图片
function isImageFile (file) {
    if (file.type) {
        return /^image\/\w+$/.test(file.type);
    } else {
        return /\.(jpg|jpeg|png|gif)$/.test(file);
    }
}