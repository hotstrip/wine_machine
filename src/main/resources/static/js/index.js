/*修改密码*/
var index;

//${rc.contextPath}
var contextPath = $("#contextPath").val();

function showEdit(){
    index = layer.open({
        type: 1,
        title: '修改密码',
        fix: false, //不固定
        maxmin: true,
        skin: 'layui-layer-rim', //加上边框
        area: ['720px', '400px'], //宽高
        content: $('#updatePwd')
    });
}

$('.Hui-aside').on('click','li',function(){
   $(this).addClass('active').siblings('li').removeClass('active');
    $(this).parent().parent().parent().addClass('active').siblings('dl').removeClass('active');
   $(this).parent().parent().parent().siblings('dl').find('li').removeClass('active');
});
$("#sub").click(function(){
    var oPwd=$("#oldPassword").val();
    var cPwd=$("#confirmPassword").val();
    var uPwd=$("#userPassword").val();
    if(oPwd==''){
        layer.open({
            title: '提示',
            content: '原密码不能为空'
        });
        return;
    }
    if(cPwd==''){
        layer.open({
            title: '提示',
            content: '确认密码不能为空'
        });
        return;
    }
    if(uPwd==''){
        layer.open({
            title: '提示',
            content: '新密码不能为空'
        });
        return;
    }
    if(oPwd!=cPwd){
        layer.open({
            title: '提示',
            content: '原密码和确认密码不同'
        });
        return;
    }
    if(oPwd==uPwd){
        layer.open({
            title: '提示',
            content: '原密码和新密码不能相同'
        });
        return;
    }
    $.post(contextPath + "/user/updatePassword",$("#updatePwd form").serialize(),function(result){
        if(result.code == 0){
            layer.msg(result.data, {icon: 6, time: 2000}, function(){
                layer.close(index);
                //调用通用方法
                common.confirm('您已经更改密码，前往重新登陆？', {dosome:function () {
                        top.location.href = contextPath +'/login.html';
                    }})
            })
        }else{
            layer.msg(result.data, {time: 2000})
        }
    },"json");
});
$(document).keydown(function(e){
    if ((e.ctrlKey && e.keyCode == 82) || e.keyCode == 116) {//F5刷新，禁止
        var activeDt=$('.Hui-aside').find('dt.selected')[0];
        if($('#min_title_list').find('li.active').text()!='我的桌面'){
            var obj,aCloseIndex=$('#min_title_list').find('li.active').index();
            if($(activeDt).has('a').length==1){ //导航
                obj=$(activeDt).find('a')[0];
            }else{
                obj=$('.Hui-aside').find('li.active a')[0];
            }
            $('#min_title_list').find('li.active').remove(); //移除 tab
            $('#iframe_box').find('.show_iframe').eq(aCloseIndex).remove(); // 移除 iframe
            Hui_admin_tab(obj); // 重新打开
            return false
        }
    }
});