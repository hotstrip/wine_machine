/**
 * 素材库中  上传文件时   加载对应的存储目录树*/
function onClick() {
    var zTree = $.fn.zTree.getZTreeObj("treeDirectory"),
        nodes = zTree.getSelectedNodes();
    var name=nodes[0].directoryName;
    var id=nodes[0].id;
    $("#directoryName").val(name);
    $("#directoryId").val(id);
    $("#menuContent").hide();
    $('#uploader').show();
}
function showMenu() {
    if($("#menuContent").is(":hidden")){
        $("#menuContent").show();
    }else{
        $("#menuContent").hide();
    }
}