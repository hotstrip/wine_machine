/**
 * @date 2017-5-19
 * @author idiot
 * @description  主题相关js
 */

var contextPath = $("#contextPath").val()

$("#sub").click(function(){
    if($("#themeName").val() == ''){
        layer.msg('请输入主题名称');
        return
    }
    if($("#fileId").val() == ''){
        layer.msg('请选择文件');
        return
    }
    //调用通用方法
    common.postRefresh(contextPath+"/release/theme/update", $("#form-update").serialize())
})

$('#form-update input').keydown(function(e){
    if(e.keyCode==13){
        e.preventDefault();
    }
})
//加载资源文件之后
function takeMaterial() {
    var fileName = $("#fileName").val()
    if(fileName != ""){
        $("#fileNameText").text('当前选择文件：'+fileName)
        layer.msg('当前选择文件：'+fileName, {icon:6, time:1000})
    }
}



