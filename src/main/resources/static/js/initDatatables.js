/***
 * @author idiot
 * @date 2017-5-15
 * @description 初始化datatables
 */
$(document).ready(function () {
    // $('.table').DataTable({
    //     "paging": false,
    //     "searching": false,
    //     "info": false,
    //     "retrieve": true,   //检索一个已经存在的Datatables实例(存在就不再重新创建)
    //     "language": {
    //         "emptyTable": "无数据"
    //     }
    // });
   $.each($('#page_data td'),function(index,item){
       if(item.innerText==''){
           if(!($(item).find('img').length!=0&&$(item).find('img').attr('src')!='')){
               item.innerText='--';
           }
       }
       if(item.innerText=='无效'){
           $(item).addClass('text-invalid')
       }
   })
});

//多选操作（全选全不选）
$("#checkAll").change(function () {
    //如果当前复选框选中  则选中全部  反之亦然
    if($(this).is(':checked')){
        $(".table tbody").find("input[type='checkbox']").prop("checked",'true');
    }else {
        $(".table tbody").find("input[type='checkbox']").removeAttr("checked");
    }
});
//全选
function btnCheckAll(btn){
    $(btn).toggleClass('hasCheck');
    if($(btn).hasClass('hasCheck')){
        $(".table tbody input").prop("checked",'true');
        $(btn).text('取消全选')
    }else {
        $(".table tbody input").removeProp("checked");
        $(btn).text('全选')
    }
}
function operate(a){
    $(a).toggleClass('hasCheck');
    if($(a).hasClass('hasCheck')){
        $(a).text('完成');
        $(".table tbody input").removeClass("hide");
        $(".btn-check").removeClass("hide");
        $(".btn-del").removeClass("hide");
        $('.commodity').addClass('hide');
    }else{
        $(a).text('编辑');
        $('.btn-check').text('全选');
        $(".btn-check").removeClass("hasCheck");
        $(".table tbody").find("input[type='checkbox']").removeProp("checked");
        $(".table tbody input").addClass("hide");
        $(".btn-check").addClass("hide");
        $(".btn-del").addClass("hide");
        $('.commodity').removeClass('hide');
    }
}

