//新建栏目  tree
var setting = {
    view: {
        selectedMulti: false, //不可同时点击多个
        addHoverDom: addHoverDom,
        removeHoverDom: removeHoverDom
    },
    data: {
        key: {
            name: "directoryName",//保存节点名称的属性名称
            children: "children" //保存子节点数据的属性名称
        },
        simpleData: {
            enable: true,// 简单数据模式
            idKey: "id",
            pIdKey: "pid",
            rootPId:0
        }
    },
    edit: {
        enable: true,
        showRemoveBtn: false,
        showRenameBtn: false
    },
    callback: {
        onClick: zTreeOnDblClick //单独现在 使用的页面
    }
};

// tree 数据
function  treeNodes(type){
    type=type||'3';
    $.post($("#contextPath").val()+'/cloudDisk/directory/getDirectories.json',{type:type}, function(res){
        if(res.success){
            $.fn.zTree.init($("#treeDirectory"), setting, res.data);
        }
    })
}
//tree 新增元素
function addHoverDom(treeId, treeNode) {
    var aObj = $("#" + treeNode.tId + "_a");    //获取该节点上的a标签
    if ($("#treeDirectory_"+ treeNode.id +"_add").length > 0)   //如果已经存在则不继续添加
        return;
    if($("#treeDirectory_"+ treeNode.id +"_del").length > 0)
        return;
    var addStr = $('<span class="button add" id="treeDirectory_'+ treeNode.id +'_add" title="添加目录" style=""></span>');
    var delStr = $('<span class="button remove" id="treeDirectory_'+ treeNode.id +'_del" title="删除目录" style=""></span>');
    aObj.append(addStr);
    aObj.append(delStr);
    //获取span标签   绑定事件
    var btn = $("#treeDirectory_"+ treeNode.id +"_add");
    if (btn) {
        btn.bind("click", function(){
            //新建子目录
            addChildDir(treeNode.id)
        });
    }
    //del
    var btn = $("#treeDirectory_"+ treeNode.id +"_del");
    if (btn) {
        btn.bind("click", function(){
            delChildDir(treeNode)//删除该目录
        });
    }
}
//移除元素
function removeHoverDom(treeId, treeNode) {
    if ($("#treeDirectory_"+ treeNode.id +"_add").length > 0)   //如果已经存在则移除
        $("#treeDirectory_"+ treeNode.id +"_add").unbind().remove();
    if ($("#treeDirectory_"+ treeNode.id +"_del").length > 0)   //如果已经存在则移除
        $("#treeDirectory_"+ treeNode.id +"_del").unbind().remove();
}
//新建子目录
function addChildDir(id) {
    var url = $("#contextPath").val()+'/cloudDisk/directory/toUpdate';
    url += '?parentDirectoryId=' + id + '&type=3&status=1';
    layer_show('新建子目录', url, '520', '420');
}
//新建 根目录
function toUpdate(title, url, width, height,type){
    url += '?parentDirectoryId=&type='+type+'&status=1';
    layer_show(title, url, width, height);
}
//删除目录
function delChildDir(treeNode) {
    console.log(treeNode)
    if(treeNode.isParent){
        layer.msg('含有子目录，不可删除')
    }else{
        common.deleteMethod('确认删除么？', $("#contextPath").val()+'/cloudDisk/directory/delete', {directoryId: treeNode.id})
    }

}