var url1=$('#url').val();

function goodsPaging(curr,pageSize,postData){
    $.post(url1+'/cloudDisk/file/loadFiles.json',postData, function(res){
        //console.log(res);
        if(res.success){
            goodsPrintHtml(res.data);
            //显示分页
            laypage({
                cont: 'goods-page', //容器。
                pages: Math.ceil(res.totalCount/pageSize), //通过后台拿到的总页数
                curr: curr || 1, //当前页
                skin: '#efefef',
                jump:function(e,first){
                    if(!first){ //点击跳页触发函数自身，并传递当前页：obj.curr
                        postData.pageNo=e.curr;
                        $.post(url1+'/cloudDisk/file/loadFiles.json',postData,function(res){
                            e.pages = e.last = Math.ceil(res.totalCount/pageSize); //重新获取总页数，
                            goodsPrintHtml(res.data);
                        })
                    }
                }
            });
        }
    });
}
var thisImg ; //0代表 商品图片 1代表 商品详情图片
//商品图片
$('.goods-form .upload-main-image .goods-tog').click(function(){
    thisImg=0;
    $(this).addClass('active').siblings('a').removeClass('active');
    if($(this).hasClass('show-colla')){
        var pageSize=12,
            postData={
                pageNo: 1, //向服务端传的参数
                pageSize:pageSize,
                maxWidth:500
            };
        goodsPaging(1,pageSize,postData);
        $('.goods-form .right-colla').removeClass('no-w');
        $('.goods-form .colla-out').removeClass('no-s');
        $('.goods-form .upload-main-image label').html('<span class="c-red">*</span>商品图片：')
    }else{
        $('.goods-form .upload-main-image label').html('<span class="c-red">*</span>商品图片：(点击图片更换)：')
    }
});
//商品详情图片
$('.goods-form .upload-detail-image .goods-tog').click(function(){
    thisImg=1;
    $(this).addClass('active').siblings('a').removeClass('active');
    if($(this).hasClass('show-colla')){
        var pageSize=12,
            postData={
                pageNo: 1, //向服务端传的参数
                pageSize:pageSize,
                minWidth:500
            };
        goodsPaging(1,pageSize,postData);
        $('.goods-form .right-colla').removeClass('no-w');//添加 detail_click
        $('.goods-form .colla-out').removeClass('no-s');
        $('.goods-form .upload-detail-image label').html('<span class="c-red">*</span>商品详情图片：')
    }else{
        $('.goods-form .upload-detail-image  label').html('<span class="c-red">*</span>商品详情图片：(点击图片更换)：')
    }
});
$('.goods-form .colla-hide').click(function(){
    $('.goods-form .right-colla').addClass('no-w');
    $('.goods-form .colla-out').addClass('no-s');
});
//修改头像
layui.use('upload', function(){
    var upload = layui.upload();
    layui.upload({
        url: url1+'/goods/uploadImage?key=file'
        ,elem: '.goods-form #update-input' //指定原始元素，默认直接查找class="layui-upload-file"
        ,method: 'post' //上传接口的http类型
        ,success: function(res){
            layer.msg('图片上传成功');
            if(res.code==0){
                $('.goods-form #update_img').attr('src',res.data);
                // update_img.src = res.data;
                $('.goods-form #upload-img').val(res.data);
            }
        }
    });
    layui.upload({
        url: url1+'/goods/uploadImage?key=detailImg'
        ,elem: '.goods-form #update-detail' //指定原始元素，默认直接查找class="layui-upload-file"
        ,method: 'post' //上传接口的http类型
        ,success: function(res){
            layer.msg('图片上传成功');
            if(res.code==0){
                $('.goods-form #update_detail').attr('src',res.data);
                // update_img.src = res.data;
                $('.goods-form #upload-detail').val(res.data);
            }
        }
    });
});
function goodsPrintHtml(res){
    common.freshPage(2);
    var ulHtml='';
    if(res.length!=0){
        $.each(res,function(index,item){
            ulHtml+= '<li> ' +
                '<a href="javascript:void(0)"> ' +
                '<div class="product-img"> ' +
                '<img src='+item.filePath+' alt=""  class="default-logo"> ' +
                '<span class="cover"></span>'+
                '</div> ' +
                '</a> ' +
                '</li>';
        })
    }else{
        ulHtml= '暂无商品。。。'
    }
    $('.goods-form .colla-body').html(ulHtml);
    $.each($('.goods-form li .default-logo'),function(index,item){ //标记出当前选择
        if(thisImg==0){ //商品图片
            if($(item).attr('src')==$('.goods-form #update_img').attr('src')){
                $(item).next().css('background','url('+url1+'"/images/cover-checked.svg")');
            }
        }else{ //详情图片
            if($(item).attr('src')==$('.goods-form #update_detail').attr('src')){
                $(item).next().css('background','url('+url1+'"/images/cover-checked.svg")');
            }
        }
    })
}
$('.goods-form .colla-main').on('click','li',function(){
    $('.goods-form .preview-img').removeClass('hide');
    $('.goods-form .right-colla').addClass('no-w');
    $('.goods-form .colla-out').addClass('no-s');
    if(thisImg==0){ //判断是商品图片
        $('.goods-form #update_img').attr('src',$(this).find('img').attr('src'));
        $('.goods-form #upload-img').val($(this).find('img').attr('src'));
    }else if(thisImg==1){   //商品详情图片
        $('.goods-form #update_detail').attr('src',$(this).find('img').attr('src'));
        $('.goods-form #upload-detail').val($(this).find('img').attr('src'));
    }
});
$('.goods-form .colla-main').on('mousedown','li',function(){
    $(this).find('.cover').css('background','url('+url1+'"/images/cover-checked.svg")');
});
$('.goods-form .colla-main').on('mouseout','li',function(){
    if(thisImg==0){ //判断是商品图片
        if($(this).find('img').attr('src')!=$('.goods-form #update_img').attr('src')){
            $(this).find('.cover').css('background','url('+url1+'"/images/cover-default.svg")');
        }
    }else{   //商品详情图片
        if($(this).find('img').attr('src')!=$('.goods-form #update_detail').attr('src')){
            $(this).find('.cover').css('background','url('+url1+'"/images/cover-default.svg")');
        }
    }

});