var url1=$('#url').val();
$('.grid-form #form-update input').keydown(function(e){
    if(e.keyCode==13){
        e.preventDefault();
    }
});
$('.grid-form .colla-hide').click(function(){
    $('.grid-form .right-colla').addClass('no-w');
    $('.grid-form .colla-out').addClass('no-s');
});
$('#choose').click(function(){
    paging(1);
    $('.grid-form .right-colla').removeClass('no-w');
    $('.grid-form .colla-out').removeClass('no-s');
});
function paging(curr){
    var pageSize=12,
        postData={
            pageNo: 1, //向服务端传的参数
            pageSize:pageSize,
            name:$('.grid-form #searchName').val()
        };
    $.post(url1+'/goods/getAllGoodses.json',postData, function(res){
        if(res.success){
            printHtml(res.data);
            //显示分页
            laypage({
                cont: 'grid-page', //容器。
                pages: Math.ceil(res.totalCount/pageSize), //通过后台拿到的总页数
                curr: curr || 1, //当前页
                skin: '#efefef',
                jump:function(e,first){
                    if(!first){
                        postData.pageNo=e.curr;
                        $.post(url1+'/goods/getAllGoodses.json',postData,function(res){
                            e.pages = e.last = Math.ceil(res.totalCount/pageSize); //重新获取总页数，
                            printHtml(res.data);
                        })
                    }//点击跳页触发函数自身，并传递当前页：obj.curr
                }
            });
        }
    });
}
$('.grid-form .colla-main').on('click','li',function(){
    $('.grid-form #goodsId').val($(this).find('input').val());
    $('.grid-form .goodsName').text($(this).find('p').text());
    $('.grid-form .update-preview').removeClass('hide');
    $('.grid-form .right-colla').addClass('no-w');
    $('.grid-form .colla-out').addClass('no-s');
    $('.grid-form #update_img').attr('src',$(this).find('img').attr('src'))
});
$('.grid-form .colla-main').on('mousedown','li',function(){
    $(this).find('.cover').css('background','url('+url1+'"/images/cover-checked.svg")');
});
$('.grid-form .colla-main').on('mouseout','li',function(){
    if($(this).find('input').val()!=$('.grid-form #goodsId').val()){
        $(this).find('.cover').css('background','url('+url1+'"/images/cover-default.svg")');
    }
});
$('#searchName').keydown(function(e){
    if(e.keyCode==13){
        paging(1)
    }
});

function printHtml(res){
    common.freshPage(2);
    var ulHtml='';
    if(res.length!=0){
        $.each(res,function(index,item){
            ulHtml+= '<li> ' +
                '<a href="javascript:void(0)"> ' +
                '<input class="hide" type="radio" value='+item.goods_id+' name="chooseId"> ' +
                '<div class="product-img"> ' +
                '<img src='+item.image+' alt=""  class="default-logo"> ' +
                '<span class="cover"></span>'+
                '<p>'+item.name+'</p> ' +
                '<p style="color: #ffd00d">('+item.specification+')</p> ' +
                '</div> ' +
                '</a> ' +
                '</li>';
        })
    }else{
        ulHtml= '暂无。。。'
    }
    $('.grid-form .colla-body').html(ulHtml);
    $.each($('li input'),function(indxe,item){
        if($(item).val()==$('.grid-form  #goodsId').val()){
            $(item).next().find('.cover').css('background','url('+url1+'"/images/cover-checked.svg")');
        }
    })
}