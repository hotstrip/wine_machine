

var contextPath = $("#contextPath").val()

//  进度导航切换
/*$(".steps .step").click(function () {
    var index = $(this).index()
    changeNav(this, index)
})*/

//  点击按钮  下一步
$(".next-step").click(function () {
    nextStep(this)
})

//  点击按钮  上一步
$(".prev-step").click(function () {
    prevStep(this)
})

//  改变导航面板
function changeNav(obj, index) {
    var val = $("#kind").val() || "";
    var type = $('#newsTypeId').val()||"";
    if(type==''){
        layer.msg('请先创建新闻类型', { time: 1000});
        return
    }
    if(index >= 1){
        if(val == ""){
            layer.msg('请选择新闻种类', { time: 1000});
            return
        }else{
            //设置投放平台
            setCheckbBox()
            //根据之前选择的类型加载内容
            initNews(val)
        }
    }
    //点击更换当前样式
    $(obj).removeClass("disabled").addClass("active").siblings().removeClass("active").addClass("disabled")
    //加载对应的div
    $(".page-container .form").find(".my-step").eq(index).show().siblings().hide()
}

//  下一步
function nextStep(obj) {
    //  获取class my-step 的索引  下一步：index加1
    var index = $(obj).parents(".my-step").index()
    var nav = $(".steps .step").eq(index+1)
    changeNav(nav, index+1)
}

//  上一步
function prevStep(obj) {
    //  获取class my-step 的索引  下一步：index减1
    var index = $(obj).parents(".my-step").index()
    var nav = $(".steps .step").eq(index-1)
    changeNav(nav, index-1)
}

//  根据种类加载页面  1 图文  2 图片  3 视频  4 直播
function initNews(kind) {
    $(".my-step .item").eq(kind-1).show().siblings(".item").hide()
}


//  设置投放平台
function setCheckbBox() {
    var val = ""
    $(".p_check").each(function (index, item) {
        if($(item).is(":checked")){
            val+= val == "" ? $(item).val() : ","+$(item).val()
        }
    })
    $("#publishPlatform").val(val)
}