/**
 * 加载本地图片  FileReader
 */
//判断浏览器是否支持FileReader接口
if (typeof FileReader == 'undefined') {
    $("#avatar-form .avatar-wrapper").text('当前浏览器不支持FileReader接口')
    //使选择控件不可操作
    $("#avatarInput").attr("disabled", "disabled");
}

//裁剪图片插件
var $image = $('#jcrop-image');

$image.cropper({
    aspectRatio: 1,
    //viewMode: 1,
    ready: function () {
    }
});



//file标签  改变事件
$("#avatarInput").change(function () {
    xmTanUploadImg($(this))
})

//选择图片，马上预览
function xmTanUploadImg(obj) {
    var files = $(obj).prop('files')
    if (files.length > 1) {
        alert('注意：当前预览只是第一张')
    }
    var file = files[0];

    if (!isImageFile(file)) {
        layer.msg('不是图片类型,请重试')
        return
    }

    //console.log(file);
    /*console.log(obj);
     console.log("file.size = " + file.size);*/  //file.size 单位为byte

    var reader = new FileReader();
    reader.readAsDataURL(file)
    //读取文件过程方法
    reader.onload = function (e) {
       // console.log("成功读取....");

        //加载图片
        var data = e.target.result
        var image = new Image();
        image.src = data;
        image.onload = function () {
            //替换图片
            $image.cropper('replace', data)

            layer.msg('提示：为保证图片清晰,请尽量选择【600,300】尺寸以上的图片')
        };
    }

}

var layer_index;    //弹出层的标识
//checkbox选中事件
$("#isRotation").change(function () {
    //如果复选框选中了  加载轮播图上传页面
    var value = $("#isRotation").val()
   // console.log('===='+value)
    if(value == '1') {
        layer_index = layer.open({
            type: 1,
            title: '裁剪轮播图',
            closeBtn: 0,
            area: ['850px', '680px'],
            shadeClose: true,
            content: $('#avatar-form')
        })

        layer.msg('提示：为保证图片清晰,请尽量选择【600,300】尺寸以上的图片')
    }

})

//点击完成  裁剪事件
$("#crop").click(function () {

    var croppedCanvas = $image.cropper('getCroppedCanvas');
    var roundedCanvas = getRoundedCanvas(croppedCanvas);
   // console.log(roundedCanvas)

    $("#result").html('<img src="' + roundedCanvas.toDataURL() + '">')

})


//判断是否是图片
function isImageFile (file) {
    if (file.type) {
        return /^image\/\w+$/.test(file.type);
    } else {
        return /\.(jpg|jpeg|png|gif)$/.test(file);
    }
}

function getRoundedCanvas(sourceCanvas) {
    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    var width = sourceCanvas.width;
    var height = sourceCanvas.height;

    canvas.width = width;
    canvas.height = height;
    context.beginPath();
    //画圆
    //context.arc(width / 2, height / 2, Math.min(width, height) / 2, 0, 2 * Math.PI);
    //画矩形
    context.rect(0, 0, width, height);
    context.strokeStyle = 'rgba(0,0,0,0)';
    context.stroke();
    context.clip();
    context.drawImage(sourceCanvas, 0, 0, width, height);

    return canvas;
}