/***
 * 封装常用  以及  模块所用的方法
 * */

/**通用方法*/
var common = {
    /**
     * @description  获取数据
     * @param _type 提交类型
     * @param _url  请求地址
     * @param _data 请求数据
     * @param _dataType 返回类型
     * @param _element html元素
     */
    getData: function (_type, _url, _data, _dataType, _element) {
        $.ajax({
            type: _type,
            url: _url,
            data: _data,
            dataType: _dataType,
            error: function () {
                return false;
            },
            success: function (data) {
                _element.html(data);
            }
        })
    },

    /**
     * @description 更换图片  验证码
     * obj html元素
     * */
    changeImg: function () {
        var contextPath = $("#contextPath").val()
        var src = contextPath + "/captcha-image"
        src += '?p=' + Math.random();
        $("#img").attr("src", src)
    },
    /**
     * @description  列表删除元素
     * msg：提示消息
     * url：请求路径
     * */
    deleteMethod: function (msg, url, data) {
        var index = layer.open({
            content: msg,
            btn: ['确认', '取消'],
            shadeClose: false,
            yes: function () {
                $.post(url, data, function (result) {
                    layer.msg(result.data, {icon: 6, time: 2000}, function () {
                        fresh();
                        layer.close(index);
                    })
                }, "json")
            }, no: function () {
                layer.close(index)
            }
        });
    },
    /**
     * @description 加载数据到指定页面位置
     * type: 提交方式
     * url: 请求路径
     * data: 数据 json格式
     * dataType: 返回数据类型
     * pageElement 页面元素 jquery对象
     * */
    dataToHtml: function (type, url, data, dataType, $pageElement) {
        $.ajax({
            type: type,
            url: url,
            data: data,
            dataType: dataType,
            error: function () {
                return false;
            },
            success: function (data) {
                $pageElement.html(data);
            }
        })
    },
    /**
     * @description post提交  返回信息
     * url: 请求路径
     * data: 提交数据
     * */
    postRefresh: function (url, data) {
        var postLoading = layer.load(2, {time: 10*1000}); //最长等待10秒
        $.post(url, data, function (result) {
            layer.msg(result.data, {icon: 6, time: 2000}, function () {
                try {
                    parent.fresh();
                } catch (err) {
                    fresh()
                }
                $('.table-btn-right ').text('编辑').removeClass('hasCheck');
                $('.btn-check').text('全选').addClass('hide').removeClass('hasCheck');
                $('.btn-del').addClass('hide');
                layer.close(postLoading);
                layer_close();
            })
        }, "json")
        .error(function (res) {
            var script = common.removeReg(res.responseText)
            eval(script)
            layer.close(postLoading);
        })
    },
    /**
     * @description post提交数据 返回json信息
     * url: 请求路径
     * data: 请求数据
     * */
    postToJson: function (url, data) {
        $.post(url, data, function (result) {
            layer.msg(result.data, {icon: 6, time: 1000});
        }, "json")
    },
    /**
     * @description 提交数据  根据返回消息  处理数据
     * @param type  提交方式
     * @param url   请求地址
     * @param data  数据
     * @param settings  json数据  里面包含数据处理方法之类的
     * */
    submitToOperate: function (type, url, data, settings) {
        $.ajax({
            type: type,
            url: url,
            data: data,
            dataType: "json",
            error: function (res) {
                settings.doError(res)
                return false;
            },
            success: function (data) {
                settings.doResponse(data)
            }
        })
    },
    /**
     * @description 确认框
     * @param msg
     * @param settings
     */
    confirm: function (msg, settings) {
        var index = layer.open({
            content: msg,
            btn: ['现在登录', '不，再等等'],
            shadeClose: false,
            yes: function () {
                settings.dosome()
            }, no: function () {
                layer.close(index)
            }
        });
    },
    /**
     * @description 确认框
     * @param msg
     * @param btns
     * @param settings
     */
    customerConfirm: function (msg, btns, settings) {
        var index = layer.open({
            content: msg,
            btn: btns,
            shadeClose: false,
            yes: function () {
                settings.doConfirm()
                layer.close(index)
            }, no: function () {
                settings.doCancel()
                layer.close(index)
            }
        });
    },
    /**
     * @description  列表删除元素
     * msg：提示消息
     * url：请求路径
     * */
    confirmPush: function (msg, url) {
        var index = layer.open({
            content: msg,
            btn: ['确认', '取消'],
            shadeClose: false,
            yes: function () {
                $.post(url, {}, function (result) {
                    layer.msg(result.data, {icon: 6, time: 2000}, function () {
                        layer.close(index);
                    })
                }, "json")
            }, no: function () {
                layer.close(index)
            }
        });
    },
    //询问框
    doConfirm: function (msg, btns, settings) {
        var index = layer.confirm(msg, {
            btn: btns //按钮
        }, function () {
            settings.doConfirm()
            layer.close(index)
        }, function () {
            settings.doCancel()
        })
    },
    //刷新
    freshPage: function (type) {
        layer.load(type, {shade: false, time: 1000});
    },
    //清空 表单  刷新
    freshAll:function(){
        $('.form')[0].reset();
        fresh();
    },
    //layer  full
    full: function (title, url, w, h) {
        if (title == null || title == '') {
            title=false;
        }
        if (url == null || url == '') {
            url="404.html";
        }
        /*if (w == null || w == '') {
            w=800;
        }
        if (h == null || h == '') {
            h=($(window).height() - 50);
        }*/
        var index = layer.open({
            type: 2,
            // area: [w+'px', h +'px'],
            fix: true, //不固定
            maxmin: false,
            shade:0.4,
            title: title,
            content: url,
            scrollbar: false
        });
        layer.full(index)
    },
    //正则获取script内容
    removeReg: function (s) {
        return s.replace(/<\/?script[^>]*>/ig, '');
    }
    ,
    //查看详细库存
    // agentId
    stockMain:function (agentId,url) {
        common.freshPage(2);
        layer.close(layer.index);
        var thisAgentId=agentId,show; //展示内容
        if($("#stock-main ."+thisAgentId).length > 0){ //存在
            show=$("#stock-main ."+thisAgentId);
            layer.open({
                //offset: '100px',
                type: 1,
                title:"详细库存",
                skin: 'layui-layer-demo', //样式类名
                // closeBtn: 0, //不显示关闭按钮
                anim: 2,
                area: ['800px', ''], //宽高
                shadeClose: true, //开启遮罩关闭
                content: show,
                scrollbar: false
            });
        }else{ //不存在
            var tbodyHtml='';
            $.post(url,{agentId:thisAgentId},function(res){
                if(res.length!=0){
                    $.each(res,function(index,item){
                        tbodyHtml+='<tr class="text-c">' +
                            '<td>'+item.name+'</td>'+
                            '<td><img src='+item.image+' alt="" class="stock-main-img"></td>' +
                            '<td class="'+common.alarm(item.remain,item.alarm)+'">'+item.remain+'</td>' +
                            '<td>'+item.alarm+'</td>'+
                            '<td>'+item.stock+'</td>' +
                            '</tr>' ;
                    })
                }else{
                    tbodyHtml='<tr class="text-c"><td colspan="5">暂无数据</td></tr>';
                }
                var agentStockHtml='<div class='+thisAgentId+'>'+
                    '<table class="table table-border table-bordered table-hover table-bg table-sort table-striped ">' +
                    '<thead>' +
                    '<tr class="text-c">' +
                    '<th width="">商品名称</th>' +
                    '<th width="">商品图片</th>'+
                    '<th width="">剩余库存</th>' +
                    '<th width="">告警数</th>'+
                    '<th width="">总容量</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    tbodyHtml+
                    '</tbody>' +
                    '</table>'+
                    '</div>';
                $('.stock-main').append(agentStockHtml);
                show=$('#stock-main .'+thisAgentId);
                layer.open({
                    //offset: '100px',
                    type: 1,
                    title:"详细库存",
                    skin: 'layui-layer-demo', //样式类名
                    anim: 2,
                    area: ['800px', ''], //宽高
                    shadeClose: true, //开启遮罩关闭
                    content: show,
                    scrollbar: false
                });
            })
        }
    },
    //库存比对
    alarm:function(remain,alarm){
        if(parseInt(remain)<parseInt(alarm)){
            return 'stock-warn'
        }else{
            return 'stock-normal'
        }
    },
    //查看终端错误信息
    getErrinfo:function (agentId,url) {
        common.freshPage(2);
        var thisAgentId=agentId,show=$("#errinfo-main ."+thisAgentId); //展示内容
        if($("#errinfo-main ."+thisAgentId).length > 0){ //存在
            layer.open({
                //offset: '100px',
                type: 1,
                title:"错误信息",
                skin: 'layui-layer-demo', //样式类名
                // closeBtn: 0, //不显示关闭按钮
                anim: 2,
                area: ['600px', ''], //宽高
                shadeClose: true, //开启遮罩关闭
                content: show,
                scrollbar: false
            });
        }else{ //不存在
            var tbodyHtml='';
            $.post(url,{agentId:thisAgentId},function(res){
                if(res.data.length!=0){
                    $.each(res.data,function(index,item){
                        tbodyHtml+='<tr class="text-c">' +
                            '<td>'+item.message+'</td>'+
                            '<td>'+common.timeStamp(item.createTime)+'</td>' +
                            '<td>'+common.isHandle(item.solve)+'</td>' +
                            '</tr>' ;
                    })
                }else{
                    tbodyHtml='<tr class="text-c"><td colspan="5">暂未读取到错误信息，请稍后再试。。。</td></tr>';
                }
                var errinfoHtml='<div class='+thisAgentId+'>'+
                    '<table class="table table-border table-bordered table-hover table-bg table-sort table-striped ">' +
                    '<thead>' +
                    '<tr class="text-c">' +
                    '<th width="">错误信息</th>' +
                    '<th width="">时间</th>'+
                    '<th width="">是否解决</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    tbodyHtml+
                    '</tbody>' +
                    '</table>'+
                    '</div>';
                $('.errinfo-main').append(errinfoHtml);
                show=$("#errinfo-main ."+thisAgentId);
                layer.open({
                    //offset: '100px',
                    type: 1,
                    title:"详细库存",
                    skin: 'layui-layer-demo', //样式类名
                    // closeBtn: 0, //不显示关闭按钮
                    anim: 2,
                    area: ['600px', ''], //宽高
                    shadeClose: true, //开启遮罩关闭
                    content: show,
                });
            })
        }
    },
    // 是否解决0未解决 1解决
    isHandle:function(item){
        return item=="0"?'未解决':'已解决'
    },
    //时间转换
    timeStamp:function(_time){
        var _data=new Date(_time);
        return _data.getFullYear() + "-" + common.judge(_data.getMonth() + 1) + "-" + common.judge(_data.getDate()) + " " + common.judge(_data.getHours()) + ":" + common.judge(_data.getMinutes()) + ":" + common.judge(_data.getSeconds());
    },
    //小于10 加 0
    judge:function(_time){
        return _time<10?'0'+_time:_time
    },
    //自定义 弹框
    mine_show:function(title,dom,id,w,h){
        var index=layer.open({
            type: 1,
            area: [w+'px', h +'px'],
            content: $(dom),
            offset:'-100px',
            shadeClose:true,
            fix: false, //不固定
            maxmin: false,
            shade:0.4,
            title: title,
            move:false,
            scrollbar: false
        });
        $(dom).find($('.agentId')).val(id);
        $(dom).find($('.idx')).text(index)
    },
    //终端详情 弹框
    agent_main:function(title,dom,w,h,offset){
        layer.open({
            type: 1,
            area: [w+'px', h +'px'],
            content: $(dom),
            offset:offset+'px',
            shadeClose:true,
            fix: false, //不固定
            maxmin: false,
            shade:0.4,
            title: title,
            move:true,
            scrollbar: false
        });
    }
};

/**角色模块*/
var roleUtils = {

};

/**菜单模块*/
var menuUtils = {

};
/**导航页面通用方法*/
var navFun={
    newAgent:function(url,data){
        $.post(url,data,function(res){
            if(res.code==0){
                layer.msg(res.data, {icon: 6, time: 2000});
            }else{
                layer.msg(res.data, {icon: 5, time: 2000});
            }
        },'json')
    }
};