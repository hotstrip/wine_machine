var contentUrl = $("#contextPath").val()

$("#sub").click(function(){
    //调用通用方法
    common.postRefresh(contentUrl + "/ad/update", $("#form-update").serialize())
})

/*init webuploader*/
var uploader = '';
if(uploader != '')
    uploader.destroy();
uploader = new WebUploader.Uploader({
    swf: 'webuploader/swf/Uploader.swf',
    server: contentUrl + '/news/newsList/upload',
    pick: '#picker',
    method: 'post',
    formData: {
        //path: new Date().getTime(),
        kind: "0"   //上传的类型  0 缩略图  1 图文  2 图片  3 视频
    },
    threads: 1,//上传并发数
    chunked: true   // 开起分片上传
    //chunkSize: 10 * 1024 * 1024 //每片大小为10mb
});

//某个文件开始上传前触发，一个文件只会触发一次
uploader.on('uploadStart', function (file) {
    //console.log(file)
})

// 当有文件被添加进队列的时候
uploader.on('fileQueued', function(file) {
    $("#filelist").append('<div id="' + file.id + '" class="item">' +
        '<h4 class="info">' + file.name + '</h4>' +
        '<p class="state">等待上传...</p>' +
        '</div>');
});

//文件上传类型
var type = 0;

//开始上传
$("#ctlBtn").click(function () {
    type = 0;   //缩略图
    uploader.upload();
})

// 某个文件的分块上传之前  更新选中的目录编号   暂时无用
uploader.on('uploadBeforeSend',function(object,data,header){
    //var num = new Date().getTime()      //当做存储目录
    //console.log(object.chunk)
    if(object.chunk == 0){
        //第一次提交时 提交formdata里面的初始化参数  直接更改全局会在下一次生效
        data = $.extend(data, {
            kind: type,
            //path: num,
        });
        //修改全局变量参数
        uploader.option('formData', {
            kind: type,      //设置文件的类型
            //path: num
        });
    }
});

// 文件上传成功
uploader.on('uploadSuccess', function(file, response) {
    if(response.code == 0){
        //返回上传之后文件的完成路径   需要根据上面的type来存放对应的位置
        var data = response.data
        appendImages(data)      //追加图片
        //修改上传完成之后的文字信息
        var li = $('#'+file.id)
        li.find('p.state').text('上传完成')
        //提示信息
        layer.msg("上传成功", {icon: 6, time: 1000})
    }else{
        layer.msg("上传失败", {time: 1000})
    }
});

//当所有文件上传结束时触发
uploader.on('uploadFinished', function () {
    $("#filelist").empty()
    layer.msg('上传文件结束')
    uploader.reset();   //重置队列
})

//验证不通过时
uploader.on('error', function (type) {
    if(type == 'Q_TYPE_DENIED'){
        layer.msg('类型错误')
    }
})

// 文件上传失败，显示上传出错。
uploader.on('uploadError', function(file, reason) {
    layer.msg('上传失败', {time: 1000})
});

// 文件上传过程中创建进度条实时显示。
uploader.on( 'uploadProgress', function( file, percentage ) {
    var li = $('#'+file.id)
    var percent = li.find('.progress .progress-bar');

    // 避免重复创建
    if (!percent.length) {
        percent = $('<div class="progress progress-striped active">' +
            '<div class="progress-bar" role="progressbar" style="background-color:green;width: 0%">' +
            '</div>' +
            '</div>').appendTo(li).find('.progress-bar');
    }
    li.find('p.state').text('上传中');
    percent.css( 'width', percentage * 100 + '%' );
});

//图片的数组
var images = []
var images_str = ""

//添加图片
function appendImages(data) {
    $("#images_div").empty()    //先清空
    //添加至数组
    images.push(data)
    var temp = ""   //临时图片字符串
    //遍历数组  显示在页面上  同时构建字符串 用以传递后台
    for(var item in images){
        temp = temp==""?images[item]:temp+"|"+images[item]
        var img = $('<img src="'+images[item]+'" alt="缩略图"/>')
        $("#images_div").append(img)
    }
    images_str = temp
    $("#images").val(images_str)    //设置图片
    $("#imagesCount").val(images.length)    //设置图片数量
}

//加载图片数据
function loadImages() {
    //图片数据不为空
    images_str = $("#images").val()
    if(images_str != ""){
        images = images_str.split("|")
        //遍历数组  显示在页面上
        for(var item in images){
            var img = $('<img src="'+images[item]+'" alt="缩略图"/>')
            $("#images_div").append(img)
        }
    }
}

loadImages()

//获取素材
function takeMaterial() {
    var fileType = $("#fileType").val()
    if(fileType != null && fileType != undefined && fileType != ""){
        //判断fileType 弹出提示框  {1:文本文件,2:图片,3:视频,4:其他}
        if(fileType == 1){
            layer.msg('暂时不支持当前选择文件类型')
        }else if(fileType == 2){
            appendImages($("#filePath").val())
        }else if(fileType == 3){
            layer.msg('暂时不支持当前选择文件类型')
        }else if(fileType == 4){
            layer.msg('暂时不支持当前选择文件类型')
        }
    }
}

