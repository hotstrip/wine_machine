

//某个文件开始上传前触发，一个文件只会触发一次
uploader.on('uploadStart', function (file) {
    //console.log(file)
})

// 当有文件被添加进队列的时候
uploader.on('fileQueued', function(file) {
    $("#thelist").append('<div id="' + file.id + '" class="item">' +
        '<h4 class="info">' + file.name + '</h4>' +
        '<p class="state">等待上传...</p>' +
        '</div>');
});

// 某个文件的分块上传之前  更新选中的目录编号
uploader.on('uploadBeforeSend',function(object,data,header){
    var num = new Date().getTime()      //当做存储目录
    if(object.chunk == 0){
        //第一次提交时 提交formdata里面的初始化参数  直接更改全局会在下一次生效
        data = $.extend(data,{
            directoryId: $(".personal-update .directoryId").val(),
            path: num
        });
        //修改全局变量参数
        uploader.option('formData', {
            directoryId: $(".personal-update .directoryId").val(),
            path: num      //设置每个文件的目录
        });
    }
});
var loadIndex;
//开始上传
$("#ctlBtn").click(function () {
    if($('.uploader-list').html()==''){
        layer.msg('请选择文件');
        return
    }
    if($(".personal-update .directoryId").val() == ""){
        layer.msg('请选择存储目录');
        return
    }
    uploader.upload();
    loadIndex = layer.load(2, {time: 30*1000}); //又换了种风格，并且设定最长等待10秒
});

// 文件上传成功，给item添加成功class, 用样式标记上传成功。
uploader.on('uploadSuccess', function(file, response) {
    if(response.code == 0){
        layer.msg(response.data, {icon: 6, time: 1000})
    }else{
        layer.msg(response.data, { time: 1000})
    }
});


//验证不通过时
uploader.on('error', function (type) {
    if(type == 'Q_TYPE_DENIED'){
        layer.msg('类型错误，请上传zip文件')
    }
})

// 文件上传失败，显示上传出错。
uploader.on('uploadError', function(file, reason) {
    layer.msg('上传失败', {time: 1000},function(){
        layer.close(loadIndex);
    })
});

// 文件上传过程中创建进度条实时显示。
uploader.on( 'uploadProgress', function( file, percentage ) {
    var li = $('#'+file.id);
    var percent = li.find('.progress .progress-bar');
    // 避免重复创建
    if (!percent.length) {
        percent = $('<div class="progress progress-striped active">' +
            '<div class="progress-bar" role="progressbar" style="background-color:green;width: 0%">' +
            '</div>' +
            '</div>' +
            '<span class="progress-num"></span>'
            ).appendTo(li).find('.progress-bar');
    }
    li.find('p.state').text('上传中');
    percent.css( 'width', percentage * 100 + '%' );
    li.find('.progress-num').text((Math.round(percentage*100))+'%')
});

//当所有文件上传结束时触发
uploader.on('uploadFinished', function () {
    setTimeout(function () {
        $(".uploader-list").empty();
        $(".directoryName").empty();
        layer.msg('上传文件结束',function(){
            layer.close(loadIndex);
        });
        uploader.reset();   //重置队列
        if(parent.getFileList() && typeof(parent.getFileList()) == "function"){
            parent.getFileList()
        }
        layer_close();
    }, 3000)   //关闭弹窗
});
