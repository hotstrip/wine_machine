

// var contextPath = $("#contextPath").val()

//多选删除
function deleteBatch(url){
    var settings = {
        doConfirm: function () {
            //删除所选的复选框
            var items = $(".table tbody").find("input[type='checkbox']:checked")
            var ids = []
            $.each(items, function (index, item) {
                ids.push($(item).val())
            })
            if(items.length < 1){
                layer.msg('请至少选择一项', {time:1000})
                return
            }
            //提交删除
            common.postRefresh(url, {"ids": ids})
        },
        doCancel: function () {

        }
    }
    common.doConfirm('亲，是否确认删除？', ['确认', '取消'], settings)
}