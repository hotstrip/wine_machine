/*news  fetch 页面js*/

var baseUrl = $("#contextPath").val()

$("#sub").click(function(){
    //调用通用方法
    common.postRefresh(baseUrl + "/news/newsList/update", $("#form-update").serialize())
})

//点击抓取文章信息
$("#fetch").click(function(){
    var url = $("#contentText").val();
    var settings = {
        doResponse: function (res) {
            if(res.code == 0){
                var data = res.data
                $("#mainTitle").val(data.mainTitle)
                if(data.thumbnails != '' && data.thumbnails != undefined){
                    $("#img_thumbnails").empty().append("<img src='"+data.thumbnails+"' width='120' height='90'>")
                    $("#thumbnails").val(data.thumbnails)
                }
                $("#summary").val(data.summary)
            }else
                layer.msg('抓取文章信息失败')
        },
        doError: function () {
            layer.msg('抓取文章信息失败')
        }
    }
    var data = {
        'url': url
    }
    common.submitToOperate('post', baseUrl + '/news/newsList/fetch', data, settings)
})

//选择种类之后加载对应的上传控件
$("#kind").change(function () {
    //如果当前选择的是图片  或者  视频  需要显示对应的文本框
    var val = $(this).val()
    if(val == '2'){
        //图片显示  视频隐藏
        $("#images").show()
        $("#video").hide()
    }else if(val == '3'){
        //视频显示  图片隐藏
        $("#images").hide()
        $("#video").show()
    }else if(val == '1'){
        //视频隐藏  图片隐藏
        $("#images").hide()
        $("#video").hide()
    }
})


/*init webuploader*/
var uploader = '';
if(uploader != '')
    uploader.destroy();
uploader = new WebUploader.Uploader({
    swf: 'webuploader/swf/Uploader.swf',
    server: baseUrl + '/news/newsList/upload',
    pick: '#picker',
    method: 'post',
    formData: {
        path: new Date().getTime(),
        type: "0"   //上传的类型  0 缩略图  1 图文  2 图片  3 视频
    },
    threads: 1,//上传并发数
    chunked: true   // 开起分片上传
    //chunkSize: 10 * 1024 * 1024 //每片大小为10mb
});

//某个文件开始上传前触发，一个文件只会触发一次
uploader.on('uploadStart', function (file) {
    //console.log(file)
})

//  统计图片数量  b true 加   false  减
function countImages(b, val) {
    //当选择图片种类时  统计图片的数量
    if(val == '2'){
        var imagesCount = $("#imagesCount").val() || 0;
        if(b)
            $("#imagesCount").val(++imagesCount)
        else{
            $("#imagesCount").val(imagesCount > 0 ? (--imagesCount) : 0)
        }
    }
}

// 当有文件被添加进队列的时候
uploader.on('fileQueued', function(file) {
    countImages(true, $("#kind").val())
    $("#filelist").append('<div id="' + file.id + '" class="item">' +
        '<h4 class="info">' + file.name + '</h4>' +
        '<p class="state">等待上传...</p>' +
        '</div>');
});

//文件上传类型
var type = 0;

//开始上传
$("#ctlBtn").click(function () {
    var index = layer.open({
        content: '请选择上传类型',
        btn: ['缩略图', '图片', '视频', '取消'],
        area: ['500px', '150px'],
        shadeClose: false,
        yes: function(){
            type = 0;   //缩略图
            uploader.upload();
        }, btn2: function(){
            type = 2;   //图片
            uploader.upload();
        }, btn3: function(){
            type = 3;   //视频
            uploader.upload();
        }, no: function(){
            layer.close(index)
        }
    });
})

// 某个文件的分块上传之前  更新选中的目录编号   暂时无用
uploader.on('uploadBeforeSend',function(object,data,header){
    var num = new Date().getTime()      //当做存储目录
    if(object.chunk == 0){
        //第一次提交时 提交formdata里面的初始化参数  直接更改全局会在下一次生效
        data=$.extend(data,{
            type: type
        });
        //修改全局变量参数
        uploader.option('formData', {
            type: type      //设置文件的类型
        });
    }
});

// 文件上传成功
uploader.on('uploadSuccess', function(file, response) {
    if(response.code == 0){
        //返回上传之后文件的完成路径   需要根据上面的type来存放对应的位置
        var data = response.data
        appendUrl(data, type)
        //修改上传完成之后的文字信息
        var li = $('#'+file.id)
        li.find('p.state').text('上传完成')
        //提示信息
        layer.msg("上传成功", {icon: 6, time: 1000})
    }else{
        countImages(false, $("#kind").val())
        layer.msg("上传失败", {icon: 5, time: 1000})
    }
});

function appendUrl(data, type){
    //缩略图
    if(type == '0'){
        $("#img_thumbnails").append('<img src="'+data+'" alt="缩略图"/>')
        var images = $("#thumbnails").val()
        $("#thumbnails").val(images=='' ? data : images+"|"+data)
    }
    else if(type == '2'){
        var url = $("#imagesUrl").val()
        $("#imagesUrl").val(url=='' ? data : url+"|"+data)
    }
    else if(type == '3'){
        $("#videoUrl").val(data)
    }
}

//当所有文件上传结束时触发
uploader.on('uploadFinished', function () {
    $("#filelist").empty()
    layer.msg('上传文件结束')
    $("#countImages").text("当前已经上传")
    uploader.reset();   //重置队列
})

//验证不通过时
uploader.on('error', function (type) {
    if(type == 'Q_TYPE_DENIED'){
        layer.msg('类型错误')
    }
})

// 文件上传失败，显示上传出错。
uploader.on('uploadError', function(file, reason) {
    countImages(false, $("#kind").val())
    layer.msg('上传失败', {icon: 5, time: 1000})
});

// 文件上传过程中创建进度条实时显示。
uploader.on( 'uploadProgress', function( file, percentage ) {
    var li = $('#'+file.id)
    var percent = li.find('.progress .progress-bar');

    // 避免重复创建
    if (!percent.length) {
        percent = $('<div class="progress progress-striped active">' +
            '<div class="progress-bar" role="progressbar" style="background-color:green;width: 0%">' +
            '</div>' +
            '</div>').appendTo(li).find('.progress-bar');
    }
    li.find('p.state').text('上传中');
    percent.css( 'width', percentage * 100 + '%' );
});