
//上传
var contentUrl = $("#contextPath").val()

$(".textarea").Huitextarealength({
    minlength:10,
    maxlength:144,
    exceed: false
});

var editor = null;
$(function () {
    editor = new wangEditor('editor-trigger');
    editor.config.uploadImgFileName = 'fileName';
    // 上传图片
    editor.config.uploadImgUrl = contentUrl + '/upload/images';
    editor.create();
});

$("#sub").click(function(){
    $("#content").val(editor.$txt.html());
    $("#contentText").val(editor.$txt.formatText());
    //主标题
    if($('#mainTitle').val()==''){
        layer.msg('请输入主标题');
        return;
    }
    //作者
    if($('#author').val()==''){
        layer.msg('请输入作者');
        return;
    }
    //缩略图
    if($('#thumbnails').val()==''){
        layer.msg('请上传缩略图');
        return;
    }
    var kind=$('#kind').val();  //1 图文 2 图片 3 视频
    if(kind==1){
        if(editor.$txt.formatText()==''){
            layer.msg('请输入新闻内容');
            return;
        }
    }
    if(kind==2){
        if($('#imagesUrl').val()==''){
            layer.msg('请上传图片');
            return;
        }
        if($('#imagesAlt').val()==''){
            layer.msg('请输入图片描述');
            return;
        }
    }
    if(kind==3){
        if($('#videoUrl').val()==''){
            layer.msg('请上传视频');
            return;
        }
    }
    //调用通用方法
    common.postRefresh(contentUrl + "/media/mediaList/update", $("#form-update").serialize())
})

//选择种类之后加载对应的上传控件
$("#kind").change(function () {
    //如果当前选择的是图片  或者  视频  需要显示对应的文本框
    var val = $(this).val()
    if(val == '2'){
        //图片显示  视频隐藏
        $("#images").show()
        $("#imagesDescription").show()
        $("#video").hide()
        $("#video_text").hide()
    }else if(val == '3'){
        //视频显示  图片隐藏
        $("#images").hide()
        $("#imagesDescription").hide()
        $("#video").show()
        $("#video_text").show()
    }else if(val == '1'){
        //视频隐藏
        $("#video").hide()
        $("#video_text").hide()
    }
})


/*init webuploader*/
var uploader = '';
if(uploader != '')
    uploader.destroy();
uploader = new WebUploader.Uploader({
    swf: 'webuploader/swf/Uploader.swf',
    server: contentUrl + '/news/newsList/upload',
    pick: '#picker',
    method: 'post',
    formData: {
        //path: new Date().getTime(),
        kind: "0"   //上传的类型  0 缩略图  1 图文  2 图片  3 视频
    },
    threads: 1,//上传并发数
    chunked: true   // 开起分片上传
    //chunkSize: 10 * 1024 * 1024 //每片大小为10mb
});

//某个文件开始上传前触发，一个文件只会触发一次
uploader.on('uploadStart', function (file) {
    //console.log(file)
})

//  统计图片数量  b true 加   false  减
function countImages(val) {
    //当选择图片种类时  统计图片的数量
    if(val == '2'){
        $("#imagesCount").val(images.length)
    }
}

// 当有文件被添加进队列的时候
uploader.on('fileQueued', function(file) {
    countImages($("#kind").val())
    $("#filelist").append('<div id="' + file.id + '" class="item">' +
        '<h4 class="info">' + file.name + '</h4>' +
        '<p class="state">等待上传...</p>' +
        '</div>');
});

//文件上传类型
var type = 0;

//开始上传
$("#ctlBtn").click(function () {
    var index = layer.open({
        content: '请选择上传类型',
        btn: ['缩略图', '图片', '视频', '取消'],
        area: ['500px', '150px'],
        shadeClose: false,
        yes: function(){
            type = 0;   //缩略图
            uploader.upload();
        }, btn2: function(){
            type = 2;   //图片
            uploader.upload();
        }, btn3: function(){
            type = 3;   //视频
            uploader.upload();
        }, no: function(){
            layer.close(index)
        }
    });
})

// 某个文件的分块上传之前  更新选中的目录编号   暂时无用
uploader.on('uploadBeforeSend',function(object,data,header){
    //var num = new Date().getTime()      //当做存储目录
    if(object.chunk == 0){
        //第一次提交时 提交formdata里面的初始化参数  直接更改全局会在下一次生效
        data = $.extend(data, {
            kind: type,
            //path: num,
        });
        //修改全局变量参数
        uploader.option('formData', {
            kind: type,      //设置文件的类型
            //path: num
        });
    }
});

//图片的数组
var images = []
var images_str = ""

//加载图片数据
function loadImages() {
    //图片数据不为空
    images_str = $("#imagesUrl").val()
    if(images_str != ""){
        images = images_str.split("|")
        /*//遍历数组  显示在页面上
        for(var item in images){
            var img = $('<img src="'+images[item]+'" alt="图片"/>')
            $("#images_div").append(img)
        }*/
    }
    countImages('2')
}

loadImages()

// 文件上传成功
uploader.on('uploadSuccess', function(file, response) {
    if(response.code == 0){
        //返回上传之后文件的完成路径   需要根据上面的type来存放对应的位置
        var data = response.data
        appendUrl(data, type)
        //修改上传完成之后的文字信息
        var li = $('#'+file.id)
        li.find('p.state').text('上传完成')
        //提示信息
        layer.msg("上传成功", {icon: 6, time: 1000})
    }else{
        countImages($("#kind").val())
        layer.msg("上传失败", {time: 1000})
    }
});

//追加文件路径
function appendUrl(data, type){
    //{0:缩略图,2:图片,3:视频}
    if(type == '0'){
        addThumbnails(data)
    }
    else if(type == '2'){
        addImages(data)
    }
    else if(type == '3'){
        addVideo(data)
    }
}

//添加缩略图
function addThumbnails(url) {
    if($("#thumbnails").val() != ""){
        var settings = {
            doConfirm: function () {
                $("#img_thumbnails").attr("src", url).css({"width": "272px", "height": "170px"})
                $("#thumbnails").val(url)
            },
            doCancel: function(){

            }
        }
        common.doConfirm('是否替换略略图? \n<img src="'+url+'" alt="缩略图" width="100%"/>', ['是', '否'], settings)
    }else{
        $("#img_thumbnails").attr("src", url).css({"width": "272px", "height": "170px"})
        $("#thumbnails").val(url)
    }
}

//添加图片
function addImages(url) {
    images.push(url)      //添加图片到数组
    if($("#imagesUrl").val() != ""){
        var settings = {
            doConfirm: function () {
                replaceImages()
                countImages('2')
            },
            doCancel: function(){
                $("#imagesUrl").val($("#imagesUrl").val() +"|"+ url)
                countImages('2')
            }
        }
        common.doConfirm('替换还是追加? \n<img src="'+url+'" alt="缩略图" width="100%"/>', ['替换', '追加'], settings)
    }else{
        replaceImages()
        countImages('2')
    }
}

//替换图片
function replaceImages() {
    images.splice(0, images.length-1)
    var temp = ""   //临时图片字符串
    //遍历数组  显示在页面上  同时构建字符串 用以传递后台
    for(var item in images){
        temp = temp == "" ? images[item]:temp+"|"+images[item]
    }
    $("#imagesUrl").val(temp)
}

//添加视频
function addVideo(url) {
    if($("#thumbnails").val() != ""){
        var settings = {
            doConfirm: function () {
                $("#videoUrl").val(url)
                layer.msg('替换视频成功')
            },
            doCancel: function(){

            }
        }
        common.doConfirm('是否替换原有的视频?', ['是', '否'], settings)
    }else{
        $("#videoUrl").val(url)
        layer.msg('添加视频成功')
    }
}

//当所有文件上传结束时触发
uploader.on('uploadFinished', function () {
    $("#filelist").empty()
    layer.msg('上传文件结束')
    if(type == '2')
        $("#countImages").text("当前已经上传")
    uploader.reset();   //重置队列
})

//验证不通过时
uploader.on('error', function (type) {
    if(type == 'Q_TYPE_DENIED'){
        layer.msg('类型错误')
    }
})

// 文件上传失败，显示上传出错。
uploader.on('uploadError', function(file, reason) {
    countImages($("#kind").val())
    layer.msg('上传失败', {time: 1000})
});

// 文件上传过程中创建进度条实时显示。
uploader.on( 'uploadProgress', function( file, percentage ) {
    var li = $('#'+file.id)
    var percent = li.find('.progress .progress-bar');

    // 避免重复创建
    if (!percent.length) {
        percent = $('<div class="progress progress-striped active">' +
            '<div class="progress-bar" role="progressbar" style="background-color:green;width: 0%">' +
            '</div>' +
            '</div>').appendTo(li).find('.progress-bar');
    }
    li.find('p.state').text('上传中');
    percent.css( 'width', percentage * 100 + '%' );
});


//抽取图片信息
/*function fetchImages() {
    var index = layer.confirm('注意：选择确认会覆盖上面图片信息', {
        btn: ['确认','取消'] //按钮
    }, function(){
        $("#imagesUrl").val('')     //清空图片信息
        $("#imagesAlt").val('')     //清空图片描述信息
        var imgs = editor.$txt.find('img');
        $("#imagesCount").val(imgs.length)  //设置图片数量
        $(imgs).each(function (index, img) {
            console.log(img)
            var image_url = $("#imagesUrl").val()
            var image_alt = $("#imagesAlt").val()
            var url = $(img).attr("src")
            var alt = $(img).attr("alt")
            console.log(alt)
            if(url != '')
                $("#imagesUrl").val(image_url==''? url : image_url+"|"+url)
            if(alt != '')
                $("#imagesAlt").val(image_alt==''? alt : image_alt+"|"+alt)
        })
        layer.close(index)  //关闭弹窗
    }, function(){
        layer.close(index)  //关闭弹窗
    });

}*/

// 修改标签  投放平台
var time1 = setInterval(function () {
    var val = $("#publishPlatform").val()
    if(val != null && val != undefined && val != ""){
        //隐藏提示文字
        $(".Hui-tags-label").hide()
        //得到数组
        var items = val.split(",")
        //移除初始值
        $(".Hui-tags .Hui-tags-editor").remove(".Hui-tags-token")
        //遍历数组  添加
        $(items).each(function (index, item) {
            if(item != "")
                $(".Hui-tags .Hui-tags-editor .Hui-tags-iptwrap").before('<span class="Hui-tags-token">'+item+'</span>')
        })
        clearInterval(time1)
    }
}, 500)


//获取素材
function takeMaterial() {
    var fileType = $("#fileType").val()
    if(fileType != null && fileType != undefined && fileType != ""){
        //判断fileType 弹出提示框  {1:文本文件,2:图片,3:视频,4:其他}
        if(fileType == 1){
            console.log('已选择文本文件，开始导入文件...')
            var fileId = $("#fileId").val()+''
            importFile(fileId)      //导入文件
        }else if(fileType == 2){
            var settings = {
                doConfirm: function () {
                    addThumbnails($("#filePath").val())
                },
                doCancel: function () {
                    addImages($("#filePath").val())
                }
            }
            common.doConfirm('请选择插入图片的类型', ['缩略图', '图片'], settings)
        }else if(fileType == 3){
            //插入视频
            addVideo($("#filePath").val())
        }else if(fileType == 4){
            layer.msg('暂时不支持当前选择文件类型')
        }
    }
}


//导入文本文件
function importFile(fileId) {
    getText(fileId)     //加载文本信息
    //获取编辑器的纯文本信息  判断是否为空
    if(editor.$txt.formatText() != ""){
        var settings = {
            doConfirm: function () {
                layer.msg('正在替换编辑器内容，稍等...', {icon:6, time:3000}, function () {
                    if($("#content").val() == ""){
                        layer.msg('文本信息为空')
                        return
                    }
                    replaceText($("#content").val())
                    layer.msg('替换编辑器内容成功')
                })
            },
            doCancel: function(){
                layer.msg('正在向编辑器追加内容，稍等...', {icon:6, time:3000}, function () {
                    if($("#content").val() == ""){
                        layer.msg('文本信息为空')
                        return
                    }
                    editor.$txt.append($("#content").val())
                    layer.msg('追加编辑器内容成功')
                })
            }
        }
        common.doConfirm('检测到编辑器已经有内容，替换还是追加?', ['替换', '追加'], settings)
    }else{
        layer.msg('正在写入编辑器，稍等...', {icon:6, time:3000}, function () {
            if($("#content").val() == ""){
                layer.msg('文本信息为空')
                return
            }
            replaceText($("#content").val())
            layer.msg('写入编辑器内容成功')
        })
    }
}

//请求接口  解析文本文件   返回数据
function getText(fileId) {
    var data = {
        "fileId": fileId
    }
    var settings = {
        doResponse: function (res) {
            if(res.code == 0){
                $("#content").val(res.data) //设置返回的文本信息
            }else
                layer.msg('解析文本失败')
        },
        doError: function () {
            layer.msg('解析文本失败')
        }
    }
    common.submitToOperate('post', contentUrl+'/cloudDisk/file/analyzeText', data, settings)
}

//替换编辑器的内容
function replaceText(result) {
    editor.$txt.html('<p>'+ result +'</p>')
}
