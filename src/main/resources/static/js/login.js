/**
 * @date 2017-5-19
 * @author idiot
 * @description  admin 登录  js
 */

//当前页面在iframe中   刷新父页面
setInterval(function () {
    if (self != top) {
        top.location.reload();
    }
}, 200)

var contextPath = $("#contextPath").val();
//登录时ajax异步提交
$("#sub").click(function () {
    submit()
})

//enter
$('input').keydown(function(e){
    if(e.keyCode==13){
        submit();
    }
});

function submit() {
    var userName = $("#userName").val()
    var userPassword = $("#userPassword").val()
    var captcha = $("#captcha").val()
    if(userName == ''){
        layer.msg('用户名不能为空', {time:1000})
        return
    }
    if(userPassword == ''){
        layer.msg('密码不能为空', {time:1000})
        return
    }
    if(captcha == ''){
        layer.msg('验证码不能为空', {time:1000})
        return
    }
    var settings = {
        doError: function () {
            layer.msg('登录失败.....', {time:1000})
        },
        doResponse: function (res) {
            if(res.code == 0){
                $("#sub").attr({"readonly":"readonly"}).removeClass("btn-success")
                layer.msg(res.data, {icon:6, time:1000}, function () {
                    self.location.href="index.html"
                })
            }else{
                //更换验证码  提示错误信息   置空验证码
                common.changeImg()
                layer.msg(res.data, {time:1000})
                $("#captcha").val('')
                $("#sub").removeAttr("readonly").addClass("btn-success")
            }
        }
    }
    common.submitToOperate('post', contextPath+'/loginIn.html', $(".form").serialize(), settings)
}