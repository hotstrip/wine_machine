//checkbox选中事件
$("#reviewStatus").change(function () {
    //如果复选框选中了  审核通过
    var flag = $("#reviewStatus").parent().hasClass("checked")
    if(flag) {
        //修改审核复选框
        $("#reviewStatus").parent().removeClass("checked")
        $("#reviewStatus-hiden").val('0')
    } else{
        $("#reviewStatus").parent().addClass("checked")
        $("#reviewStatus-hiden").val('1')
    }
})

var contextPath = $("#contextPath").val()


//  提交审核
$("#sub").click(function(){
    if($('#discription').val()==''){
        layer.msg('请输入审核说明');
        $("#discription").focus()
        return;
    }
    //调用通用方法
    common.postRefresh(contextPath + "/news/newsList/update", $("#form-update").serialize())
})