
/***
 * @author idiot
 * @date 2017-5-15
 * @description 加载素材库
 */
var contextPath = $("#contextPath").val()

function loadMaterial(w, h) {
    var current_index = parent.layer.getFrameIndex(window.name); //获取窗口索引
    parent.layer.full(current_index) //全屏

    var index = layer.open({
        content: '请选择素材库',
        btn: ['公共素材库', '个人素材库', '取消'],
        area: ['500px', '150px'],
        shadeClose: false,
        yes: function(){
            common.full('公共素材库', contextPath + '/cloudDisk/content/material', w, h)
            layer.close(index)
        }, btn2: function(){
            common.full('个人素材库', contextPath + '/cloudDisk/personal/material', w, h)
            layer.close(index)
        }, no: function(){
            layer.close(index)
        }
    });
}