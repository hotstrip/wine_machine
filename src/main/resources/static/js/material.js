/***
 * @author idiot
 * @date 2017-5-15
 * @description 素材库页面加载
 */

layui.use('element', function(){
    var element = layui.element();
    //获取hash来切换选项卡，假设当前地址的hash为lay-id对应的值
    var layid = location.hash.replace(/^#test1=/, '');
    element.tabChange('fileType', layid); //假设当前地址为：http://a.com#test1=222，那么选项卡会自动切换到“发送消息”这一项
    //监听Tab切换，以改变地址hash值
    element.on('tab(fileType)', function(){
        location.hash = 'fileType='+ this.getAttribute('lay-id');
        if(this.getAttribute('lay-id') != 0){
            $("#fileType").val(this.getAttribute('lay-id'))
            //加载文件数据
            loadData($("#directoryId").val(), $("#fileType").val())
        }else{
            $("#fileType").val(undefined)
            loadData($("#directoryId").val(), undefined)
        }
    });
});