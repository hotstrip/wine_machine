package com.seestech.sell.web.goods;

import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.seestech.sell.common.annotation.Duplicate;
import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.JsonUtils;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.Brand;
import com.seestech.sell.domain.model.Goods;
import com.seestech.sell.domain.model.ResultBean;
import com.seestech.sell.domain.model.User;
import com.seestech.sell.domain.model.enums.StatusEnums;
import com.seestech.sell.service.IBrandService;
import com.seestech.sell.service.IGoodsService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

import static org.apache.shiro.web.filter.mgt.DefaultFilter.user;

/**
 * Created by idiot on 2017/6/17.
 */
@Controller
@RequestMapping(value = "brand")
public class BrandController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(BrandController.class);

    @Resource
    private IBrandService brandService;
    @Resource
    private IGoodsService goodsService;


    //加载page页面
    @RequiresPermissions(value = "brand:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(Model model){
        logger.info("加载品牌页面信息=====》》》");
        return "brand/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Brand info, Model model){
        User user = getUser();
        if(user == null){
            logger.error("获取品牌信息======>>>>>>>>用户信息失效，请重新登录");
            return "login";
        }
        //获取这个用户所在企业相关的用户信息
        Set<Long> userIds = getUserIds(user);
        userIds.add(user.getUserId());
        info.setUserIds(userIds);
        info.setCreateUser(null);
        info.setStatus(StatusEnums.VALID.getValue());
        Page<Brand> lists = brandService.getAllBrands(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        model.addAttribute("lists", lists);
        return "brand/data";
    }

    //加载编辑页面
    @RequestMapping(value = "toUpdate", method = RequestMethod.GET)
    public String toAdd(Model model){
        return "brand/update";
    }

    //编辑信息
    @Duplicate
    @RecordLog(value = "修改品牌信息")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public void update(Brand info){
        //menuId为空/0时表示   新增操作
        if(info.getBrandId() == null){
            info.setCreateUser(getUser().getUserId());
            brandService.insert(info);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "新增品牌信息成功");
        }else {     //修改操作
            info.setUpdateUser(getUser().getUserId());
            brandService.update(info);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "修改品牌信息成功");
        }
    }


    //加载编辑页面
    @RequestMapping(value = "{brandId}/toUpdate", method = RequestMethod.GET)
    public String toUpdate(@PathVariable("brandId") Long brandId, Model model){
        //根据menuId查询菜单信息   再查询父级菜单信息
        Brand brand = brandService.getBrandByBrandId(brandId);
        model.addAttribute("brand",brand);
        return "brand/update";
    }

    //删除品牌信息
    @RecordLog(value = "删除品牌信息")
    @RequestMapping(value = "{brandId}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable("brandId") Long brandId){
        Long[] brandIds = new Long[]{brandId};
        List<Goods> list = goodsService.getGoodsesByBrandIds(brandIds);
        if(list != null && list.size() > 0){
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "该品牌下有商品信息，不能删除");
        }else {
            brandService.delete(brandId);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "删除品牌信息成功");
        }
    }

    //批量删除
    @RequestMapping(value = "deleteBatch", method = RequestMethod.POST)
    public void deleteBatch(@RequestParam("ids[]") Long[] brandIds){
        List<Goods> list = goodsService.getGoodsesByBrandIds(brandIds);
        if(list != null && list.size() > 0){
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "该品牌下有商品信息，不能删除");
        }else {
            brandService.deleteBatch(brandIds);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "批量删除品牌信息成功");
        }
    }

    /************************************************/

    /**
     * @description  获取品牌信息
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getBrands.json")
    public Object getBrands(){
        logger.info("获取品牌信息接口========>>>>>>>>>>>开始");
        ResultBean<Object> resultBean;
        Brand info = new Brand();
        info.setStatus(StatusEnums.VALID.getValue());
        List<Brand> brands = brandService.getBrands(info);
        JSONArray object = JsonUtils.parseArray(brands);
        resultBean = ResponseUtils.initResultBean(object, Constants.ResponseCode.success, "成功", true);
        resultBean.setTotalCount(brands.size());
        logger.info("获取品牌信息接口========>>>>>>>>>>>结束");
        return resultBean;
    }
}
