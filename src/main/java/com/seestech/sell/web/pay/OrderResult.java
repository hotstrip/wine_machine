package com.seestech.sell.web.pay;

/**
 * Created by idiot on 2017/7/7.
 * @description   订单返回实体信息
 */
public class OrderResult {
    //交易二维码
    private String codeUrl;
    //交易渠道  1微信   2支付宝
    private int channel;
    //是否成功
    private boolean success;
    //订单编号
    private String orderId;

    public String getCodeUrl() {
        return codeUrl;
    }

    public void setCodeUrl(String codeUrl) {
        this.codeUrl = codeUrl;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public OrderResult() {
    }

    public OrderResult(String codeUrl, int channel, boolean success, String orderId) {
        this.codeUrl = codeUrl;
        this.channel = channel;
        this.success = success;
        this.orderId = orderId;
    }
}
