package com.seestech.sell.web.user;


import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.common.utils.EncodeMD5;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.User;
import com.seestech.sell.service.IUserService;
import com.seestech.sell.web.SuperController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

/**
 * Created by idiot on 2016/12/13.
 */
@Controller
@RequestMapping(value = "user")
public class UserController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(UserController.class);
    @Resource
    private IUserService userService;


    //加载page页面
    @RecordLog(value = "加载管理员信息")
    @RequiresPermissions(value = "user:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(Model model){
        return "user/page";
    }

    //加载用户信息页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Model model){
        User user = getUser();
        if(user != null){
            model.addAttribute("user",user);
        }
        return "user/data";
    }

    //修改用户密码
    @RecordLog(value = "修改管理员密码")
    @RequestMapping(value = "updatePassword", method = RequestMethod.POST)
    public void updatePassword(String oldPassword, long userId, User info){
        User user = userService.getUserByUserId(userId);
        if(user != null){
            if(EncodeMD5.GetMD5Code(oldPassword).equals(user.getUserPassword())){
                info.setUserPassword(EncodeMD5.GetMD5Code(info.getUserPassword())); //加密密码
                userService.update(info);
                ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"修改用户密码成功");
            }
            ResponseUtils.writeErrorResponse(request_local.get(), response_local.get(),"原密码错误");
        }
        ResponseUtils.writeErrorResponse(request_local.get(), response_local.get(),"修改用户密码失败");
    }

    /**
     * 修改用户信息
     * @param info
     */
    @RecordLog(value = "修改用户信息")
    @RequestMapping(value = "updateUser", method = RequestMethod.POST)
    public void updateUser(User info){
        logger.info("修改用户信息开始====>>>>>>");
        if(info.getUserId() == null) {
            ResponseUtils.writeErrorResponse(request_local.get(), response_local.get(), "修改用户信息失败");
        }
        User user = userService.getUserByName(info.getUserName());
        if(user != null){
            if(!user.getUserId().equals(info.getUserId())){
                ResponseUtils.writeErrorResponse(request_local.get(), response_local.get(), "用户名存在,修改失败");
                return;
            }
        }
        userService.update(info);
        logger.info("修改用户信息====>>>>>>【成功】");
        ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"修改用户信息成功");
    }

}
