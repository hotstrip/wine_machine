package com.seestech.sell.web.user;

import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.seestech.sell.common.annotation.Duplicate;
import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.common.config.ApplicationConfig;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.FileUtils;
import com.seestech.sell.common.utils.JsonUtils;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.Company;
import com.seestech.sell.domain.model.CompanyPay;
import com.seestech.sell.domain.model.ResultBean;
import com.seestech.sell.domain.model.User;
import com.seestech.sell.domain.model.enums.StatusEnums;
import com.seestech.sell.service.ICompanyPayService;
import com.seestech.sell.service.ICompanyService;
import com.seestech.sell.service.IUserService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



/**
 * Created by idiot on 2017/8/7.
 */
@Controller
@RequestMapping(value = "company")
public class CompanyController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(CompanyController.class);

    @Resource
    private ICompanyService companyService;
    @Resource
    private IUserService userService;
    @Resource
    private ICompanyPayService companyPayService;
    @Resource
    private ApplicationConfig applicationConfig;

    //加载page页面
    @RecordLog("加载企业信息页面")
    @RequiresPermissions(value = "company:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(Model model){
        logger.info("加载企业页面信息=====》》》");
        return "user/company/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Company info, Model model){
        logger.info("获取用户对应的企业信息======>>>>>>>>【开始】");
        User user = getUser();
        if(user == null){
            logger.error("获取用户对应的企业信息======>>>>>>>>用户信息失效，请重新登录");
            return "login";
        }
        if(info.getCreateUser() == null) {
            info.setCreateUser(user.getUserId());
        }
        if(info.getManager() == null) {
            info.setManager(user.getUserId());
        }
        info.setIs_or(true);    //用or连接查询
        Page<Company> lists = companyService.getCompanies(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        //还有用户绑定的企业信息
        if(user.getCompanyId() != null && "".equals(info.getName())){
            //根据用户绑定的企业编号获取企业信息   判断集合中是否存在  不存在就添加
            Company item = companyService.getCompanyById(user.getCompanyId());
            if(item != null && !lists.contains(item)){
                lists.add(item);
            }
        }
        model.addAttribute("lists", lists);
        logger.info("获取用户对应的企业信息======>>>>>>>>【结束】");
        return "user/company/data";
    }

    //加载新增页面
    @RequestMapping(value = "toUpdate", method = RequestMethod.GET)
    public String toAdd(Model model){
        return "user/company/update";
    }

    //加载编辑页面
    @RequestMapping(value = "{id}/toUpdate", method = RequestMethod.GET)
    public String toUpdate(@PathVariable("id") Long id, Model model){
        //根据当前用户信息  判断即将修改的企业信息是否能修改   企业的管理员或者创建者可以修改
        User info = getUser();
        if(info == null){
            logger.error("修改企业信息======>>>>>>>>用户信息失效，请重新登录");
            return "login";
        }
        //加载企业信息
        Company company = companyService.getCompanyById(id);
        if(company != null
                && ((company.getManager() != null && company.getManager().equals(info.getUserId()))
                || (company.getCreateUser() != null && company.getCreateUser().equals(info.getUserId())))){
            //加载管理员信息
            List<Long> ids = new ArrayList<>();
            ids.add(id);
            List<User> list = userService.getUsersByCompanyIds(ids);
            //企业的创建者不为空
            if(company.getCreateUser() != null) {
                User user = userService.getUserByUserId(company.getCreateUser());
                //如果集合中不含user  就添加进去
                if(!list.contains(user))
                    list.add(user);
            }
            //企业的管理员不为空
            if(company.getManager() != null) {
                User user1 = userService.getUserByUserId(company.getManager());
                if(!list.contains(user1))
                    list.add(user1);
            }

            model.addAttribute("company", company);
            model.addAttribute("users", list);
            return "user/company/update";
        }else {
            logger.error("修改企业信息======>>>>>>>>该用户不能修改企业信息");
            model.addAttribute("msg", "不是企业管理员或者企业创建者，不能修改");
            return "error";
        }

    }

    /**
     * 前往企业支付信息页面
     * @param companyId
     * @param model
     * @return
     */
    @RequestMapping(value = "{id}/toPayDetail", method = RequestMethod.GET)
    public String toPayDetail(@PathVariable("id") Long companyId, Model model){
        //根据当前用户信息  判断即将修改的企业信息是否能修改   企业的管理员或者创建者可以修改
        User info = getUser();
        if(info == null){
            logger.error("企业支付信息======>>>>>>>>用户信息失效，请重新登录");
            return "login";
        }
        logger.info("前往企业支付信息页面....");
        CompanyPay companyPay = companyPayService.getCompanyPayByCompanyId(companyId);
        model.addAttribute("companyPay", companyPay);
        if (companyPay == null){
            model.addAttribute("companyId", companyId);
        }
        //加载企业信息
        Company company = companyService.getCompanyById(companyId);
        if(company != null
                && ((company.getManager() != null && company.getManager().equals(info.getUserId()))
                || (company.getCreateUser() != null && company.getCreateUser().equals(info.getUserId())))){
            model.addAttribute("edit", 1);              // 表示可以编辑
        }else {
            model.addAttribute("edit", 0);
        }
        return "user/company/payDetail";
    }


    /**
     * @description  上传微信退款证书接口
     * @param companyId   企业编号
     */
    @ResponseBody
    @RequestMapping(value = "uploadCert", method = RequestMethod.POST)
    public Object uploadCert(Long companyId){
        logger.info("上传微信退款证书============>>>>>>开始");
        //企业编号当做文件夹
        String path = FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), String.valueOf(companyId));
        //保证 文件夹存在
        File fileDir = FileUtils.buildFileByPath(path);
        //获取文件信息  存储
        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest)request_local.get();
        Iterator<String> iterator = multiRequest.getFileNames();
        if (iterator.hasNext()){        // 取出一个
            MultipartFile multipartFile = multiRequest.getFile(iterator.next());
            //文件名
            String fileName = multipartFile.getOriginalFilename();

            File file = new File(fileDir, fileName);
            //拷贝文件流  到上面的文件
            FileUtils.copyInputStreamToFile(multipartFile, file);

            String result = FileUtils.addPathSeparate(String.valueOf(companyId), fileName);

            return ResponseUtils.responseSuccess(result);
        }
        return ResponseUtils.responseError("文件存在");
    }


    /****************************************************/

    //修改接口
    @Duplicate
    @ResponseBody
    @RecordLog(value = "修改企业信息")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public Object update(Company info){
        String result;
        User user = getUser();
        if (user == null){
            logger.error("用户信息已经失效");
            return ResponseUtils.responseError("无效的用户信息，请重新登录");
        }
        //根据企业名称查询企业信息   判断是否存在相同的企业
        Company item = companyService.getCompanyByName(info.getName());
        //id null时表示   新增操作
        if(info.getId() == null){
            if(item != null){
                logger.error("新增企业信息======>>>>>>已经存在该企业名称");
                result = ResponseUtils.responseError("该企业已经存在");
            }else {
                info.setCreateUser(user.getUserId());       //创建用户
                info.setManager(user.getUserId());          //管理员
                companyService.insert(info);
                result = ResponseUtils.responseSuccess("新增企业信息成功");
            }
        }else {     //修改操作
            if(item != null){
                if(!item.getId().equals(info.getId())){
                    logger.error("修改企业信息======>>>>>>已经存在该企业名称");
                    return ResponseUtils.responseError("该企业已经存在");
                }
            }
            info.setUpdateUser(user.getUserId());
            companyService.update(info);
            result = ResponseUtils.responseSuccess("修改企业信息成功");
        }
        return result;
    }

    //修改企业支付信息接口
    @Duplicate
    @ResponseBody
    @RecordLog(value = "修改企业支付信息")
    @RequestMapping(value = "updateCompanyPay", method = RequestMethod.POST)
    public Object updateCompanyPay(CompanyPay info){
        String result;
        User user = getUser();
        if (user == null){
            logger.error("用户信息已经失效");
            return ResponseUtils.responseError("无效的用户信息，请重新登录");
        }
        //id null时表示   新增操作
        if(info.getId() == null){
            info.setCreateUser(user.getUserId());       //创建用户
            companyPayService.insert(info);
            result = ResponseUtils.responseSuccess("新增企业支付信息成功");

        }else {     //修改操作
            info.setUpdateUser(user.getUserId());
            companyPayService.update(info);
            result = ResponseUtils.responseSuccess("修改企业支付信息成功");
        }
        return result;
    }

    //删除接口
    @ResponseBody
    @RecordLog(value = "删除企业信息")
    @RequestMapping(value = "{id}/delete", method = RequestMethod.POST)
    public Object delete(@PathVariable("id") Long id){
        //根据当前用户信息  判断即将修改的企业信息是否能修改   企业的管理员或者创建者可以修改
        User info = getUser();
        if(info == null){
            return ResponseUtils.responseError("无效的用户信息");
        }
        //加载企业信息
        Company company = companyService.getCompanyById(id);
        if(company != null
                && ((company.getManager() != null && company.getManager().equals(info.getUserId()))
                || (company.getCreateUser() != null && company.getCreateUser().equals(info.getUserId())))) {
            companyService.delete(id);
            return ResponseUtils.responseSuccess("删除企业信息成功");
        }else {
            return ResponseUtils.responseError("不是企业管理员或者企业创建者，不能删除");
        }
    }

    /**
     * @description 加载所有的企业信息
     * @param info
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getAllCompany.json")
    public Object getAllCompany(Company info){
        logger.info("获取所有的企业信息======>>>>>>>>【开始】");
        User user = getUser();
        if(user == null){
            logger.error("获取所有的企业信息======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.initResultBean(null, Constants.ResponseCode.invalid_user, "用户信息失效，请重新登录", false);
        }
        if(info.getStatus() == null)
            info.setStatus(StatusEnums.VALID.getValue());
        if(info.getCreateUser() == null) {
            info.setCreateUser(user.getUserId());
        }
        if(info.getManager() == null) {
            info.setManager(user.getUserId());
        }
        info.setIs_or(true);    //用or连接查询
        ResultBean<Object> resultBean;
        List<Company> list = companyService.getAllCompany(info);
        //还有用户绑定的企业信息
        if(user.getCompanyId() != null && "".equals(info.getName())){
            //根据用户绑定的企业编号获取企业信息   判断集合中是否存在  不存在就添加
            Company item = companyService.getCompanyById(user.getCompanyId());
            if(item != null && !list.contains(item)){
                list.add(item);
            }
        }
        //json过滤
        JSONArray object = JsonUtils.parseArray(list);
        resultBean = ResponseUtils.initResultBean(object, Constants.ResponseCode.success, "成功", true);
        resultBean.setTotalCount(list == null ? 0 : list.size());
        logger.info("获取所有的企业信息======>>>>>>>>【结束】");
        return resultBean;
    }
}
