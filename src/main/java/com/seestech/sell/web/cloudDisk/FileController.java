package com.seestech.sell.web.cloudDisk;


import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.seestech.sell.common.config.ApplicationConfig;
import com.seestech.sell.domain.model.ResultBean;
import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.FileUtils;
import com.seestech.sell.common.utils.JsonUtils;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.Directory;
import com.seestech.sell.domain.model.FileInfo;
import com.seestech.sell.domain.model.User;
import com.seestech.sell.domain.model.enums.StatusEnums;
import com.seestech.sell.service.IDirectoryService;
import com.seestech.sell.service.IFileService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.File;
import java.util.Set;

import static org.apache.shiro.web.filter.mgt.DefaultFilter.user;


/**
 * Created by idiot on 2017/5/12.
 */
@Controller
@RequestMapping(value = "cloudDisk/file")
public class FileController extends SuperController {

    private static Logger logger = LoggerFactory.getLogger(FileController.class);

    @Resource
    private IFileService fileService;

    //删除
    @RecordLog(value = "删除文件信息")
    @RequestMapping(value = "{fileId}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable("fileId") Long fileId){
        logger.info("删除文件信息=====>>>>>");
        fileService.delete(fileId);
        ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "删除文件信息成功");
    }

    //删除目录信息
    /*@Deprecated
    @RequestMapping(value = "delDirectory", method = RequestMethod.POST)
    public void delDirectory(Long directoryId){
        Directory directory = directoryService.getDirectoryByDirectoryId(directoryId);
        if(directory != null){
            //获取目录路径
            String dir = directoryService.getDirectoryPathBydirectoryId(directoryId);
            dir = FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), dir);
            FileUtils.deleteDirectory(new File(FileUtils.getWindowsPath(dir)));
            //删除文件 和 目录信息
            fileService.deleteFilesByDirectoryId(directoryId);
            directoryService.delete(directoryId);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "删除目录信息成功");
        }else {
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "该目录信息有误");
        }
    }*/

    //预览
    @RequestMapping(value = "{fileId}/toPreview")
    public String toPreview(FileInfo info, Model model){
        //判断是否是图片
        if(info.getFileType() == 2){
            //获取图片的地址
            FileInfo fileInfo = fileService.getFileByFileId(info.getFileId());
            model.addAttribute("image", fileInfo.getFilePath());
        }else {
            model.addAttribute("result", "该类型暂时不支持预览");
        }
        return "cloudDisk/content/preview";
    }

    /**
     * 加载素材列表
     * @param info
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "loadFiles.json")
    @ResponseBody
    public Object loadFiles(FileInfo info,
                            @RequestParam(defaultValue = "1") Integer pageNo,
                            @RequestParam(defaultValue = "10")Integer pageSize){
        User user = getUser();
        if(user == null){
            logger.error("获取目录信息接口======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.initResultBean(null, Constants.ResponseCode.invalid_user, "用户信息失效，请重新登录", false);
        }
        if(info.getFileType() == null)
            info.setFileType(2);    //默认加载图片
        //获取这个用户所在企业相关的用户信息
        Set<Long> userIds = getUserIds(user);
        userIds.add(user.getUserId());
        info.setUserIds(userIds);
        info.setCreateUser(null);
        info.setStatus(StatusEnums.VALID.getValue());
        Page<FileInfo> list = fileService.getFilesByFileInfo(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        //json 过滤
        JSONArray object = JsonUtils.parseArray(list);
        ResultBean<Object> resultBean = ResponseUtils.initResultBean(object, Constants.ResponseCode.success, "成功", true);
        resultBean.setTotalCount((int) list.getTotal());
        return resultBean;
    }

}
