package com.seestech.sell.web.cloudDisk;

import com.alibaba.fastjson.JSONArray;

import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.domain.model.Directory;
import com.seestech.sell.domain.model.FileInfo;
import com.seestech.sell.domain.model.User;
import com.seestech.sell.domain.model.enums.DirectoryTypeEnums;
import com.seestech.sell.domain.model.enums.StatusEnums;
import com.seestech.sell.service.IDirectoryService;
import com.seestech.sell.service.IFileService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/**
 * Created by idiot on 2017/5/10.
 */
@Controller
@RequestMapping(value = "cloudDisk/personal")
public class PersonalController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(PersonalController.class);

    @Resource
    private IDirectoryService directoryService;
    @Resource
    private IFileService fileService;

    //加载page页面
    @RecordLog(value = "加载个人素材库页面")
    @RequiresPermissions(value = "cloudDisk:personal:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(Model model){
        logger.info("加载个人素材库页面==============>>>>>");
        User user = getUser();
        if(user == null){
            logger.error("加载个人素材库页面======>>>>>>>>用户信息失效，请重新登录");
            return "login";
        }
        return "cloudDisk/personal/page";
    }

    //加载数据
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String datafile(Model model, FileInfo info, Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize){
        User user = getUser();
        if(user == null){
            logger.error("加载个人素材库文件信息======>>>>>>>>用户信息失效，请重新登录");
            return "login";
        }
        //获取这个用户所在企业相关的用户信息
        Set<Long> userIds = getUserIds(user);
        userIds.add(user.getUserId());
        info.setUserIds(userIds);
        info.setCreateUser(null);
        info.setStatus(StatusEnums.VALID.getValue());
        //该目录下的文件信息
        List<FileInfo> fileList = fileService.getFilesByFileInfo(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        model.addAttribute("lists", fileList);
        return "cloudDisk/content/data";
    }

    @RequestMapping(value = "materialData", method = RequestMethod.POST)
    public String materialData(Model model, FileInfo info, Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize){
        //该目录下的文件信息
        List<FileInfo> fileList = fileService.getFilesByFileInfo(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        model.addAttribute("lists", fileList);
        return "cloudDisk/content/materialData";
    }

    //前往上传文件页面
    @RequestMapping(value = "toUpload", method = RequestMethod.GET)
    public String toUpload(Model model, Directory info){
        //加载个人素材目录
        info.setCreateUser(getUser().getUserId());
        List<Directory> list = directoryService.getDirectoriesByDirectory(info);
        model.addAttribute("directories", JSONArray.toJSONString(list));
        return "cloudDisk/personal/upload";
    }

    //加载公共素材库
    @RequestMapping(value = "material", method = RequestMethod.GET)
    public String material(Model model){
        //获取个人素材库所属的目录信息
        Directory info = new Directory();
        info.setType(DirectoryTypeEnums.TWO.getValue());
        info.setStatus(StatusEnums.VALID.getValue());
        info.setCreateUser(getUser().getUserId());
        List<Directory> list = directoryService.getDirectoriesByDirectory(info);
        model.addAttribute("directories", JSONArray.toJSONString(list));
        return "cloudDisk/personal/material";
    }

}
