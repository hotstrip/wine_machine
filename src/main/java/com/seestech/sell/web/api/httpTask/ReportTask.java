package com.seestech.sell.web.api.httpTask;

import com.seestech.sell.common.annotation.NotNullParam;
import com.seestech.sell.common.config.ApplicationConfig;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.FileUtils;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.Agent;
import com.seestech.sell.domain.model.Report;
import com.seestech.sell.service.IAgentService;
import com.seestech.sell.service.IReportService;
import com.seestech.sell.web.SuperController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;

import java.io.File;

/**
 * Created by idiot on 2017/8/21.
 */
@RestController
@RequestMapping(value = "api/httpTask/reportTask")
public class ReportTask extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(ReportTask.class);

    @Resource
    private IReportService reportService;
    @Resource
    private IAgentService agentService;
    @Resource
    private ApplicationConfig applicationConfig;

    /**
     * @description   添加反馈信息
     * @param agentCode     终端编号
     * @param type          类型
     * @param content       内容
     * @param remark        备注
     * @return
     */
    @NotNullParam(params = {"agentCode", "type", "content"})
    @RequestMapping(value = "addReport.json")
    public Object addReport(String agentCode, String type,
                             String content, String remark){
        logger.info("添加反馈信息接口=====>>>>>【开始】");
        Agent agent = agentService.getAgentByAgentCode(agentCode);
        if(agent == null){
            logger.error("添加反馈信息接口=====>>>>>无效的参数agentCode"+agentCode);
            return ResponseUtils.initResultBean(null, Constants.ResponseCode.invalid_param, "终端code有误", false);
        }
        Report info = new Report();
        info.setAgentId(agent.getAgentId());        //设置终端编号
        info.setType(type);
        info.setContent(content);
        info.setRemark(remark);
        reportService.insert(info);
        logger.info("添加反馈信息接口=====>>>>>【结束】");
        return ResponseUtils.initResultBean(null, Constants.ResponseCode.success, "成功", true);
    }

    /**
     * @description  上传日志文件接口
     * @param agentCode
     *          file  是文件的key
     * @return
     */
    @NotNullParam(params = {"agentCode"})
    @RequestMapping(value = "uploadLogFile.json")
    public Object uploadLogFile(String agentCode){
        logger.info("【上传日志文件接口】=======>>>>>>【开始】");
        //获取终端信息  拿到图片存储地址
        Agent agent = agentService.getAgentByAgentCode(agentCode);
        if(agent != null) {
            //监控墙文件夹  对应终端编号
            String path = FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), Constants.Regular.logs, agent.getAgentCode());
            //保证 文件夹存在
            File fileDir = FileUtils.buildFileByPath(path);
            //获取图片信息  存储
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest)request_local.get();
            MultipartFile multipartFile = multiRequest.getFile("file");

            File file = new File(fileDir, multipartFile.getOriginalFilename());
            //拷贝文件流  到上面的文件
            FileUtils.copyInputStreamToFile(multipartFile, file);
            logger.info("【上传日志文件接口】=======>>>>>>【结束】");
            return ResponseUtils.initResultBean(null, Constants.ResponseCode.success, "上传日志文件成功", true);
        }else{
            logger.error("【上传日志文件接口】=======>>>>>>无效的参数agentCode"+agentCode);
            return ResponseUtils.initResultBean(null, Constants.ResponseCode.invalid_param, "无效的参数agentCode", false);
        }
    }
}
