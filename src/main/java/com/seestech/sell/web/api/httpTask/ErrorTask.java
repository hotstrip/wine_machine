package com.seestech.sell.web.api.httpTask;

import com.alibaba.fastjson.JSONArray;
import com.seestech.sell.common.annotation.NotNullParam;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.JsonUtils;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.Agent;
import com.seestech.sell.domain.model.Error;
import com.seestech.sell.domain.model.ResultBean;
import com.seestech.sell.domain.model.enums.ErrorEnums;
import com.seestech.sell.service.IAgentService;
import com.seestech.sell.service.IErrorService;
import com.seestech.sell.web.SuperController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import java.util.List;

import static com.alibaba.fastjson.JSON.toJSON;


/**
 * Created by idiot on 2017/7/27.
 */
@RestController
@RequestMapping(value = "api/httpTask/errorTask")
public class ErrorTask extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(ErrorTask.class);

    @Resource
    private IAgentService agentService;
    @Resource
    private IErrorService errorService;

    /**
     * @description  回报终端异常信息
     * @param agentCode
     * @param code
     * @return
     */
    @NotNullParam(params = {"agentCode", "code"})
    @RequestMapping(value = "addError.json")
    public Object addError(String agentCode, String code){
        logger.error("回报终端异常信息===============>>>>>>>>开始");
        ResultBean<Object> resultBean;
        Agent agent = agentService.getAgentByAgentCode(agentCode);
        if(agent == null){
            logger.error("回报终端异常信息===============>>>>>>>>无效的agentCode"+agentCode);
            resultBean = ResponseUtils.initResultBean(null, Constants.ResponseCode.error, "无效的agentCode", false);
        }else {
            boolean flag = false;
            for (ErrorEnums enums : ErrorEnums.values()){
                if(enums.getValue().equals(code)){
                    flag = true;
                    break;
                }
            }
            if(flag){
                Error error = new Error();
                error.setAgentId(agent.getAgentId());
                error.setCode(code);
                error.setMessage(ErrorEnums.getNameByValue(code));
                errorService.insert(error);
                resultBean = ResponseUtils.initResultBean(null, Constants.ResponseCode.success, "成功", true);
            }else {
                logger.error("回报终端异常信息===============>>>>>>>>无效的code"+code);
                resultBean = ResponseUtils.initResultBean(null, Constants.ResponseCode.error, "无效的code", false);
            }
        }
        logger.error("回报终端异常信息===============>>>>>>>>结束");
        return resultBean;
    }

    /**
     * @description 获取终端异常列表
     * @param agentCode
     * @return
     */
    @NotNullParam(params = {"agentCode"})
    @RequestMapping(value = "getAgentErrors.json")
    public Object getAgentErrors(String agentCode){
        logger.error("获取终端异常列表===============>>>>>>>>开始");
        ResultBean<Object> resultBean;
        Agent agent = agentService.getAgentByAgentCode(agentCode);
        if(agent == null){
            logger.error("获取终端异常列表===============>>>>>>>>无效的agentCode"+agentCode);
            resultBean = ResponseUtils.initResultBean(null, Constants.ResponseCode.error, "无效的agentCode", false);
        }else {
            List<Error> errors = errorService.getErrorsByAgentId(agent.getAgentId());
            //处理long类型
            JSONArray object = JsonUtils.parseArray(errors);
            resultBean = ResponseUtils.initResultBean(object, Constants.ResponseCode.success, "成功", true);
        }
        logger.error("获取终端异常列表===============>>>>>>>>结束");
        return resultBean;
    }

    /**
     * @description   排除指定异常
     * @param id
     * @return
     */
    @NotNullParam(params = {"id"})
    @RequestMapping(value = "solveError.json")
    public Object solveError(Long id){
        logger.error("排除指定异常===============>>>>>>>>开始");
        ResultBean<Object> resultBean;
        Error error = errorService.getErrorById(id);
        if(error == null){
            logger.error("排除指定异常===============>>>>>>>>无效的id"+id);
            resultBean = ResponseUtils.initResultBean(null, Constants.ResponseCode.error, "无效的id", false);
        }else {
            error.setSolve(1);  //排除异常
            errorService.update(error);
            resultBean = ResponseUtils.initResultBean(null, Constants.ResponseCode.success, "排除异常成功", true);
        }
        logger.error("排除指定异常===============>>>>>>>>结束");
        return resultBean;
    }


    /**
     * @description 解除终端异常
     * @param agentCode
     * @return
     */
    @NotNullParam(params = {"agentCode"})
    @RequestMapping(value = "solveAgentError.json")
    public Object solveAgentError(String agentCode){
        logger.error("解除终端异常===============>>>>>>>>开始");
        ResultBean<Object> resultBean;
        Agent agent = agentService.getAgentByAgentCode(agentCode);
        if(agent == null){
            logger.error("解除终端异常===============>>>>>>>>无效的agentCode"+agentCode);
            resultBean = ResponseUtils.initResultBean(null, Constants.ResponseCode.error, "无效的agentCode", false);
        }else {
            Agent info = new Agent();
            info.setAgentId(agent.getAgentId());
            info.setError(0);       //排除终端异常
            agentService.update(info);
            resultBean = ResponseUtils.initResultBean(null, Constants.ResponseCode.success, "解除终端异常成功", true);
        }
        logger.error("解除终端异常===============>>>>>>>>结束");
        return resultBean;
    }
}
