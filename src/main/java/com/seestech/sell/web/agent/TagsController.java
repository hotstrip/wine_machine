package com.seestech.sell.web.agent;

import com.alibaba.fastjson.JSONArray;
import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.JsonUtils;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.ResultBean;
import com.seestech.sell.domain.model.Tags;
import com.seestech.sell.domain.model.User;
import com.seestech.sell.service.ITagsService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/**
 * Created by idiot on 2017/8/14.
 * @description 标签处理
 */
@Controller
@RequestMapping(value = "tags")
public class TagsController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(TagsController.class);

    @Resource
    private ITagsService tagsService;

    //前往标签列表页面、
    @RecordLog(value = "加载标签列表页面")
    @RequestMapping(value = "page")
    @RequiresPermissions(value = "tags:page")
    public String page(){
        logger.info("前往标签列表页面=======>>>>>>>");
        return "tags/page";
    }

    //加载数据
    @RequestMapping(value = "data")
    public String data(Tags info, Model model,
                       Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize){
        User user = getUser();
        if(user == null){
            logger.error("获取标签信息接口======>>>>>>>>用户信息失效，请重新登录");
            return "login";
        }
        //获取这个用户所在企业相关的用户信息
        Set<Long> userIds = getUserIds(user);
        userIds.add(user.getUserId());
        info.setCreateUser(null);
        info.setUserIds(userIds);
        List<Tags> list = tagsService.getTagsByTags(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        model.addAttribute("lists", list);
        return "tags/data";
    }

    //前往新增标签页面
    @RequestMapping(value = "add")
    public String toAdd(){
        logger.info("前往新增标签页面=======>>>>>>>");
        return "tags/update";
    }

    //前往修改页面
    @RequestMapping(value = "{id}/toUpdate")
    public String toUpdate(@PathVariable("id") Long id, Model model){
        logger.info("前往修改标签页面=======>>>>>>>");
        Tags tags = tagsService.getTagsById(id);
        model.addAttribute("tags", tags);
        return "tags/update";
    }

    /*************************  接口  ******************************/

    //新增标签
    @ResponseBody
    @RequestMapping(value = "update.json", method = RequestMethod.POST)
    public String update(Tags info){
        User user = getUser();
        if(user == null){
            logger.error("获取标签信息接口======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.responseError("用户信息失效，请重新登录");
        }
        if(info.getId() == null){
            //根据名称获取信息
            Tags tags = tagsService.getTagByTagName(info.getTagName());
            if(tags != null){
                logger.error("新增标签接口=======>>>>>>标签名称已经存在..........");
                return ResponseUtils.responseError("该标签名称已经存在");
            }
            info.setCreateUser(user.getUserId());
            tagsService.insert(info);
            return ResponseUtils.responseSuccess("添加标签成功");
        }else {
            List<Tags> list = tagsService.getTagsByTagName(info.getTagName());
            if(list != null && list.size() == 1){
                //获取第一个  判断主键是否相同  相同就允许修改
                Tags tags = list.get(0);
                if(!tags.getId().equals(info.getId())){
                    logger.error("新增标签接口=======>>>>>>标签名称已经存在..........");
                    return ResponseUtils.responseError("该标签名称已经存在");
                }
            }else if(list != null && list.size() > 1) {
                logger.error("新增标签接口=======>>>>>>标签名称已经存在..........集合大小"+list.size());
                return ResponseUtils.responseError("该标签名称已经存在");
            }
            info.setUpdateUser(user.getUserId());
            tagsService.update(info);
            return ResponseUtils.responseSuccess("修改标签成功");
        }
    }

    //加载多个标签
    @ResponseBody
    @RequestMapping(value = "getTags.json", method = RequestMethod.POST)
    public Object getTags(Tags info){
        ResultBean<Object> resultBean;
        User user = getUser();
        if(user == null){
            logger.error("获取标签信息接口======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.initResultBean(null, Constants.ResponseCode.invalid_user, "用户信息失效，请重新登录", false);
        }
        //获取这个用户所在企业相关的用户信息
        Set<Long> userIds = getUserIds(user);
        userIds.add(user.getUserId());
        info.setCreateUser(null);
        info.setUserIds(userIds);
        List<Tags> list = tagsService.getTagsByTags(new RowBounds(0, Integer.MAX_VALUE), info);
        //json 过滤
        JSONArray object = JsonUtils.parseArray(list);
        resultBean = ResponseUtils.initResultBean(object, Constants.ResponseCode.success, "成功", true);
        resultBean.setTotalCount(list.size());
        return resultBean;
    }

    //删除标签
    @ResponseBody
    @RequestMapping(value = "delete.json")
    public String delete(Long id){
        tagsService.delete(id);
        return ResponseUtils.responseSuccess("删除标签成功");
    }

    //删除多个标签
    @ResponseBody
    @RequestMapping(value = "deleteBatch.json")
    public String deleteBatch(@RequestParam("ids[]") Long[] ids){
        tagsService.deleteBatch(ids);
        return ResponseUtils.responseSuccess("删除标签成功");
    }
}
