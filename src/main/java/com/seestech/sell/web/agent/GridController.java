package com.seestech.sell.web.agent;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.*;
import com.seestech.sell.common.annotation.Duplicate;
import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.enums.StatusEnums;
import com.seestech.sell.service.IAgentService;
import com.seestech.sell.service.IGridService;
import com.seestech.sell.service.IShelfDetailService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

import static java.awt.SystemColor.info;

/**
 * Created by idiot on 2017/6/19.
 */
@Controller
@RequestMapping(value = "grid")
public class GridController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(GridController.class);

    @Resource
    private IGridService gridService;
    @Resource
    private IShelfDetailService shelfDetailService;
    @Resource
    private IAgentService agentService;

    //加载page页面
    @RequiresPermissions(value = "grid:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(Model model){
        logger.info("加载柜格页面信息=====》》》");
        return "grid/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Grid info, Model model){
        User user = getUser();
        if(user == null){
            logger.error("获取品牌信息======>>>>>>>>用户信息失效，请重新登录");
            return "login";
        }
        //获取这个用户所在企业相关的用户信息
        Set<Long> userIds = getUserIds(user);
        userIds.add(user.getUserId());
        info.setUserIds(userIds);
        info.setCreateUser(null);
        info.setStatus(StatusEnums.VALID.getValue());
        Page<Grid> lists = gridService.getAllGrids(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        model.addAttribute("lists", lists);
        return "grid/data";
    }

    //获取柜格信息
    @ResponseBody
    @RequestMapping(value = "getGrids.json")
    public Object getGrids(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Grid info){
        Page<Grid> lists = gridService.getAllGrids(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        ResultBean<Object> resultBean = ResponseUtils.initResultBean(lists, Constants.ResponseCode.success, "成功", true);
        resultBean.setTotalCount((int) lists.getTotal());
        return resultBean;
    }

    //加载编辑页面
    @RequestMapping(value = "toUpdate", method = RequestMethod.GET)
    public String toAdd(Model model){
        return "grid/update";
    }

    //编辑信息
    @Duplicate
    @RecordLog(value = "修改柜格信息")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public void update(Grid info){
        //menuId为空/0时表示   新增操作
        if(info.getGridId() == null){
            info.setCreateUser(getUser().getUserId());
            gridService.insert(info);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "新增柜格信息成功");
        }else {     //修改操作
            //根据gridId获取终端信息  如果存在就不能修改
            // List<Agent> agents = agentService.getAgentsByGridId(info.getGridId());
            // if(agents != null && agents.size() > 0){
            //     ResponseUtils.writeErrorResponse(request_local.get(), response_local.get(), "该柜格已经绑定终端，不能修改");
            // }else {
                info.setUpdateUser(getUser().getUserId());
                gridService.update(info);
                ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "修改柜格信息成功");
            // }
        }
    }

    //加载编辑页面
    @RequestMapping(value = "{gridId}/toUpdate", method = RequestMethod.GET)
    public String toUpdate(@PathVariable("gridId") Long gridId, Model model){
        //查询柜格信息
        Grid grid = gridService.getGridByGridId(gridId);
        model.addAttribute("grid",grid);
        //根据gridId获取终端信息  如果存在就只能修改价格
        List<Agent> agents = agentService.getAgentsByGridId(gridId);
        if(agents != null && agents.size() > 0){
            logger.info("前往修改柜格信息======>>>>柜格已经绑定终端====>>>>仅仅允许修改价格");
            model.addAttribute("updatePrice", true);
        }
        return "grid/update";
    }

    //删除品牌信息
    @RecordLog(value = "删除柜格信息")
    @RequestMapping(value = "{gridId}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable("gridId") Long gridId){
        Long[] gridIds = new Long[]{gridId};
        List<ShelfDetail> list = shelfDetailService.getShelfDetailByGridIds(gridIds);
        if(list != null && list.size() > 0){
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "该柜格已经绑定货架信息，不能删除");
        }else {
            gridService.delete(gridId);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"删除柜格信息成功");
        }
    }

    //批量删除
    @RequestMapping(value = "deleteBatch", method = RequestMethod.POST)
    public void deleteBatch(@RequestParam("ids[]") Long[] gridIds){
        List<ShelfDetail> list = shelfDetailService.getShelfDetailByGridIds(gridIds);
        if(list != null && list.size() > 0){
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "该柜格已经绑定货架信息，不能删除");
        }else {
            gridService.deleteBatch(gridIds);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "批量删除柜格信息成功");
        }
    }
}
