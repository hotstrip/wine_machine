package com.seestech.sell.web.monitor;

import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.service.IStatisticsService;
import com.seestech.sell.web.SuperController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

/**
 * Created by idiot on 2017/8/1.
 * @description  统计相关   播放统计啊  点击统计啊
 */
@Controller
@RequestMapping(value = "monitor/statistics")
public class StatisticsController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(StatisticsController.class);

    @Resource
    private IStatisticsService statisticsService;

    //加载page页面
    @RecordLog(value = "加载统计页面")
    @RequiresPermissions(value = "monitor:statistics:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(){
        logger.info("加载统计页面......");
        return "monitor/statistics/page";
    }



}
