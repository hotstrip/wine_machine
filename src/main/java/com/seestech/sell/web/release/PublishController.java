package com.seestech.sell.web.release;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.seestech.sell.common.annotation.Duplicate;
import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.common.config.ApplicationConfig;
import com.seestech.sell.common.utils.*;
import com.seestech.sell.common.utils.XMLUtils;
import com.seestech.sell.domain.model.*;
import com.seestech.sell.domain.model.enums.*;
import com.seestech.sell.service.*;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;
import com.seestech.sell.common.utils.FileUtils;

import static com.alibaba.fastjson.JSON.parseArray;


/**
 * Created by idiot on 2017/8/1.
 * @description  新版 发布模块
 */
@Controller
@RequestMapping(value = "release/publish")
public class PublishController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(PublishController.class);

    @Resource
    private IPublishService publishService;
    @Resource
    private IFileService fileService;
    @Resource
    private ApplicationConfig applicationConfig;
    @Resource
    private ITreeService treeService;
    @Resource
    private IThemeColumnService themeColumnService;
    @Resource
    private IAgentService agentService;
    @Resource
    private IAgentThemeService agentThemeService;
    @Resource
    private IAgentTaskService agentTaskService;
    @Resource
    private IPublishHistoryService publishHistoryService;
    @Resource
    private IDirectoryService directoryService;
    @Resource
    private ILogService logservice;
    @Resource
    private ICompanyService companyService;

    //加载page页面
    @RecordLog(value = "新增发布信息页面")
    @RequiresPermissions(value = "release:publish:edit")
    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public String toEdit(Model model){
        logger.info("新增发布信息页面................");
        return "release/publish/edit";
    }


    //加载发布列表页面
    @RecordLog(value = "加载发布信息列表页面")
    @RequiresPermissions(value = "release:publish:list")
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String toList(Model model){
        logger.info("加载发布信息列表页面................");
        return "release/publish/list";
    }

    //修改主题信息页面  绑定业务目录
    @RequestMapping(value = "bindDirectory", method = RequestMethod.GET)
    public String bindDirectory(){
        logger.info("加载绑定业务目录页面................");
        return "release/publish/list";
    }

    //修改字幕信息页面
    @RequestMapping(value = "{publishId}/editLed", method = RequestMethod.GET)
    public String editLed(@PathVariable(value = "publishId") Long publishId, Model model){
        logger.info("加载修改发布信息页面................");
        Publish publish = publishService.getPublishByPublishId(publishId);
        model.addAttribute("publish", publish);
        return "release/publish/editLed";
    }


    //前往发布页面
    @RecordLog(value = "加载发布页面")
    //@RequiresPermissions(value = "release:publish:toPublish")
    @RequestMapping(value = "{publishId}/toPublish", method = RequestMethod.GET)
    public String toPublish(Model model,
                            Integer isTest,
                            @PathVariable("publishId") Long publishId){
        logger.info("加载发布页面................");
        model.addAttribute("publishId", publishId);
        model.addAttribute("isTest", isTest);   //是否是测试下发
        return "release/publish/publish";
    }

    //绑定业务目录页面(主题)
    @RequestMapping(value = "{publishId}/toBindColumn", method = RequestMethod.GET)
    public String toBindColumn(Model model, @PathVariable("publishId") Long publishId){
        logger.info("前往主题绑定业务目录页面");
        User user = getUser();
        if(user == null){
            logger.error("前往主题绑定业务目录页面======>>>>>>>>用户信息失效，请重新登录");
            return "login";
        }
        //加载主题栏目信息
        List<ThemeColumn> themeColumns = themeColumnService.getThemeColumnsByThemeId(publishId);
        //加载所有有效栏目 目录信息
        Directory info = new Directory();
        info.setStatus(StatusEnums.VALID.getValue());
        //获取这个用户所在企业相关的用户信息
        Set<Long> userIds = getUserIds(user);
        userIds.add(user.getUserId());
        info.setUserIds(userIds);
        info.setCreateUser(null);
        info.setType(DirectoryTypeEnums.Three.getValue());
        List<Directory> directories = directoryService.getDirectoriesByDirectory(info);
        model.addAttribute("lists", themeColumns);
        model.addAttribute("directories", directories);
        model.addAttribute("publishId", publishId);
        return "release/publish/bindColumn";
    }

    /******************************************  接口  **********************************/

    //主题绑定栏目信息
    @RecordLog(value = "主题绑定栏目信息")
    @ResponseBody
    @RequestMapping(value = "bindColumn.json", method = RequestMethod.POST)
    public Object bindColumn( @RequestParam("columnName") String[] columnNames,
                            @RequestParam("columnCode") String[] columnCodes,
                            @RequestParam("directoryId") Long[] directoryIds,
                            @RequestParam("displayIndex") Integer[] displayIndexs,
                            @RequestParam("columnType") Integer[] columnTypes,
                            @RequestParam("publishId") Long publishId,
                            @RequestParam("themeColumnId") Long[] themeColumnIds){
        logger.info("修改主题绑定业务目录======>开始");
        String result;
        Publish info = publishService.getPublishByPublishId(publishId);
        String xmlpath = FileUtils.getWindowsPath(FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), Constants.Regular.my_themes, info.getPath()));
        //获取index.xml的绝对路径
        String completePath = FileUtils.getFilePathByFileName(xmlpath, Constants.Regular.theme_xml_name);
        if("".equals(completePath)){
            logger.error("找不到index.xml文件");
            return ResponseUtils.responseError("修改主题栏目信息失败");
        }
        List<ThemeColumn> list = new ArrayList<>();
        //遍历数组
        for (int i=0; i < columnNames.length; i++){
            //如果目录编号不为空时   获取主题信息   修改index.xml
            if(directoryIds[i] != null){
                ThemeColumn themeColumn = new ThemeColumn();
                themeColumn.setThemeColumnId(themeColumnIds[i]);
                themeColumn.setColumnCode(columnCodes[i]);
                themeColumn.setColumnName(columnNames[i]);
                themeColumn.setColumnType(columnTypes[i]);
                themeColumn.setDisplayIndex(displayIndexs[i]);
                themeColumn.setDirectoryId(directoryIds[i]);
                Directory directory = directoryService.getDirectoryByDirectoryId(directoryIds[i]);
                themeColumn.setFold("app_" + directory.getDirectoryName());

                //更新数据库
                themeColumnService.update(themeColumn);
                list.add(themeColumn);
            }
        }
        logger.info("修改主题业务目录成功======>更新xml文件......"+xmlpath);
        //修改xml
        try {
            XMLUtils.generateXML(list, completePath);
            logger.info("修改主题业务目录成功======>成功");
            //删除zip文件
            String zipFile = FileUtils.getWindowsPath(FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), Constants.Regular.my_themes, String.valueOf(info.getPublishId()))+".zip");
            FileUtils.deleteFile(new File(zipFile));
            //修改发布信息的最终文件名
            Publish item = new Publish();
            item.setPublishId(info.getPublishId());
            item.setRemark(IdGen.get().nextId()+".zip");
            publishService.update(item);
        } catch (Exception e) {
            logger.error("修改xml文件失败===>>"+e.getMessage());
            return ResponseUtils.responseError("修改主题栏目信息失败");
        }
        result = ResponseUtils.responseSuccess("修改主题栏目信息成功");
        logger.info("修改主题绑定业务目录======>结束");
        return result;
    }

    /**
     * 根据publishId 获取终端树接口
     * @param publishId
     * @param isTest  是否是测试终端{0:否,1:是}
     * @param agentName
     * @param tags      标签的字符串  用逗号分隔
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "loadAgentTree.json")
    public Object loadAgentTree(Long publishId, Integer isTest,
                                @RequestParam(value = "agentName", defaultValue = "") String agentName,
                                @RequestParam(value = "tags", defaultValue = "") String tags){
        User user = getUser();
        if(user == null){
            logger.error("获取终端树接口===============>>>用户信息失效，请重新登录");
            return ResponseUtils.responseError("用户信息失效，请重新登录");
        }
        String[] arr = new String[]{};
        if(tags != null && !"".equals(tags)) {
             arr = tags.split(",");
        }
        String result;
        List<TreeInfo> treeInfos = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        //判断发布编号是否为空  为空就加载  所有终端   为发送命令而准备
        if(publishId == null){
            treeInfos =  treeService.getAgentTree(agentName, arr, user.getUserId(), isTest);
            map.put("agentTree", treeInfos);
            return ResponseUtils.responseSuccess(map);
        }
        //根据发布信息编号获取发布信息
        Publish info = publishService.getPublishByPublishId(publishId);
        if(info != null){
            //根据发布信息类型加载终端树
            if(info.getType().equals(PublishTypeEnums.THEME.getValue())){
                //主题
                treeInfos =  treeService.getAgentTree(agentName, arr, user.getUserId(), isTest);
            }else if(info.getType().equals(PublishTypeEnums.COLUMN.getValue())){
                //栏目 根据栏目对应的业务目录信息 获取主题栏目信息   检验是否为空
                List<ThemeColumn> list = themeColumnService.getThemeColumnsDirectoryId(info.getDirectoryId());
                if(list != null && list.size() > 0){
                    //根据主题编号加载终端信息
                    List<Long> themeIds = new ArrayList<>();
                    for (ThemeColumn item : list){
                        themeIds.add(item.getThemeId());
                    }
                    treeInfos = treeService.getThemeAgentTreeByThemeIds(themeIds, agentName, arr, user.getUserId(), isTest);
                }else {
                    logger.error("获取终端树接口===============>>>无效的目录信息");
                    // return ResponseUtils.responseError("无效的目录信息");
                }
            }else if(info.getType().equals(PublishTypeEnums.VIDEO.getValue())){
                //视频
                treeInfos =  treeService.getAgentTree(agentName, arr, user.getUserId(), isTest);
            }else if(info.getType().equals(PublishTypeEnums.ZIP.getValue())){
                //zip
                treeInfos =  treeService.getAgentTree(agentName, arr, user.getUserId(), isTest);
            }else if(info.getType().equals(PublishTypeEnums.LED.getValue())){
                //led  字幕
                treeInfos =  treeService.getAgentTree(agentName, arr, user.getUserId(), isTest);
            }else {
                //无效的类型
                logger.error("获取终端树接口===============>>>无效的类型type"+info.getType());
                return ResponseUtils.responseError("无效的发布类型");
            }
            map.put("publishId", String.valueOf(publishId));
            map.put("agentTree", treeInfos);
            result = ResponseUtils.responseSuccess(map);
        }else {
            result = ResponseUtils.responseError("发布信息有误");
        }
        return result;
    }

    /**
     * @description   获取发布信息列表接口
     * @param info
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RecordLog(value = "获取发布信息列表接口")
    @ResponseBody
    @RequestMapping(value = "getPublishList.json")
    public Object getPublishList(Publish info,
                                 @RequestParam(defaultValue = "1") Integer pageNo,
                                 @RequestParam(defaultValue = "50") Integer pageSize){
        logger.info("获取发布信息列表接口===============>>>>【开始】");
        ResultBean<Object> resultBean;
        User user = getUser();
        if(user == null){
            logger.error("获取发布信息列表接口======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.initResultBean(null, Constants.ResponseCode.invalid_user, "用户信息失效，请重新登录", false);
        }
        // 用户加入的企业信息   判断是否为空  读取是否需要审核
        Company company = companyService.getCompanyById(user.getCompanyId());
        if (company == null){
            logger.error("获取发布信息列表接口======>>>>>>>>用户没有加入企业");
            return  ResponseUtils.initResultBean(null, Constants.ResponseCode.error, "用户没有加入企业", false);
        }
        //设置是否开启审核
        Map map = new HashMap();
        map.put("isReview", company.getIsReview());     //设置是否开启审核
        // 获取用户是否有审核权限
        Subject subject = SecurityUtils.getSubject();
        if(subject.isPermitted(Constants.Regular.reviewPermission)){
            map.put("hasReview", 1);     //设置是否有审核权限
        }else {
            map.put("hasReview", 0);     //设置是否有审核权限
        }
        //获取这个用户所在企业相关的用户信息
        Set<Long> userIds = getUserIds(user);
        userIds.add(user.getUserId());
        info.setCreateUser(null);
        info.setUserIds(userIds);
        //根据发布信息获取列表
        Page<Publish> list = publishService.getPublishList(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        //json 过滤
        JSONArray object = JsonUtils.parseArray(list);
        resultBean = ResponseUtils.initResultBean(object, Constants.ResponseCode.success, "成功", true);
        resultBean.setTotalCount(list == null ? 0 : (int) list.getTotal());
        resultBean.setMap(map);
        logger.info("获取发布信息列表接口===============>>>>【结束】");
        return resultBean;
    }

    /**
     * 提交审核 接口
     * @param publishId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "addReview")
    public String addReview(Long publishId){
        logger.info("发布信息=====>>>>提交审核>>>>>【开始】");
        Publish publish = publishService.getPublishByPublishId(publishId);
        if(publish == null){
            logger.error("无效的发布信息编号......");
            return ResponseUtils.responseError("无效的发布信息编号");
        }
        String result;
        // 如果审核状态是0  修改为1   未审核 -> 待审核
        if (ReviewStatusEnums.zero.getValue().equals(publish.getReviewStatus())) {
            publish.setReviewStatus(ReviewStatusEnums.one.getValue());
            publishService.update(publish);
            result = ResponseUtils.responseSuccess("提交审核成功");
        }else {
            result = ResponseUtils.responseError("无效的待审核发布信息");
        }
        logger.info("发布信息=====>>>>提交审核>>>>>【结束】");
        return result;
    }

    /**
     * 审核发布信息接口
     * @param publishId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "review")
    @RequiresPermissions(value = Constants.Regular.reviewPermission)
    public String passReview(Long publishId){
        logger.info("发布信息=====>>>>审核>>>>>【开始】");
        Publish publish = publishService.getPublishByPublishId(publishId);
        if(publish == null){
            logger.error("无效的发布信息编号......");
            return ResponseUtils.responseError("无效的发布信息编号");
        }
        String result;
        // 如果审核状态是1  修改为2   待审核 -> 已审核
        if (ReviewStatusEnums.one.getValue().equals(publish.getReviewStatus())) {
            publish.setReviewStatus(ReviewStatusEnums.two.getValue());
            publishService.update(publish);
            result = ResponseUtils.responseSuccess("审核成功");
        }else {
            result = ResponseUtils.responseError("无效的审核发布信息");
        }
        logger.info("发布信息=====>>>>审核>>>>>【结束】");
        return result;
    }



    /**
     * @description  新增发布信息接口
     * @param info
     * @return
     */
    @RecordLog(value = "新增发布信息接口")
    @ResponseBody
    @RequestMapping(value = "addPublish.json")
    @Duplicate
    public Object addPublish(Publish info){
        logger.info("新增发布信息接口===============>>>>【开始】");
        String result = new String(), completePath = new String(), indexFileName = new String();
        //设置修改用户
        User user = getUser();
        if(user != null){
            /*//检测存储目录是否为空
            if (org.springframework.util.StringUtils.isEmpty(info.getPath())){
                info.setPath(String.valueOf(System.currentTimeMillis()));
            }*/
            FileInfo fileInfo = null;
            //主题  栏目  zip  视频  需要校验文件是否存在
            if(!PublishTypeEnums.LED.getValue().equals(info.getType())){
                logger.info("新增发布信息接口====>>>>文件类型>>>>>"+info.getType()+">>>>【校验文件是否存在】");
                //根据fileId获取文件信息
                fileInfo = fileService.getFileByFileId(info.getFileId());
                if (fileInfo == null){
                    logger.info("新增发布信息接口===============>>>>该文件不存在");
                    return ResponseUtils.responseError("文件信息不存在");
                }
                //主题  栏目  zip  视频  需要设置 文件对应目录
                info.setPath(fileInfo.getDirectoryPath());
                //根据fileId获取文件完整目录    zip文件解压  拷贝到主题存档目录   先获取源文件路径
                String filePath = fileService.getFilePath(info.getFileId());
                //完整文件路径
                completePath = FileUtils.getWindowsPath(FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), filePath, fileInfo.getFileName()));

                //判断是否是zip      主题  栏目  zip
                if(!PublishTypeEnums.VIDEO.getValue().equals(info.getType())){
                    logger.info("新增发布信息接口====>>>>文件类型>>>>>"+info.getType()+">>>>【检测文件是否是zip】");
                    String extName = FileUtils.getExtName(completePath);
                    if(!FileTypeEnums.zip.getName().equals(extName)){
                        logger.info("新增发布信息接口===============>>>>文件类型后缀不匹配"+extName);
                        return ResponseUtils.responseError("文件类型不是zip");
                    }

                    //主题  栏目  需要获取压缩文件的主要文件名
                    if(!PublishTypeEnums.ZIP.getValue().equals(info.getType())){
                        logger.info("新增发布信息接口====>>>>文件类型>>>>>"+info.getType()+">>>>【获取主要文件名】");
                        //获取压缩文件里的文件名  主题  栏目
                        try {
                            indexFileName = FileUtils.readZipFileName(completePath);
                            logger.info("新增发布信息接口====>>>>主要文件名:"+indexFileName);
                        } catch (Exception e) {
                            logger.error("新增发布信息接口===============>>>>获取文件名失败"+e.getMessage());
                            return ResponseUtils.responseError("解析压缩文件失败");
                        }
                    }
                }
            }
            //新路径  存档解压  路径
            String newFilePath = new String();
            // 检测当前发布名称是否被占用    需要区分新增还是修改   顺便更改创建人|修改人   根据发布名称获取数据
            List<Publish> list = publishService.getPublishByPublishName(info.getName());
            if (info.getPublishId() == null){  //新增数据
                // 判断集合大小是否大于0
                if (list != null && list.size() > 0){
                    logger.error("新增发布信息接口===============>>>>【失败】>>>>发布名称已经被占用");
                    return ResponseUtils.responseError("新增发布信息失败，发布名称已经被占用");
                }
                info.setCreateUser(user.getUserId());
            }else {
                // 修改的时候  需要检测集合大小是否等于1
                if (list != null && list.size() == 1){
                    // 获取第一个  判断主键是否相同
                    Publish publish = list.get(0);
                    if(publish.getPublishId().equals(info.getPublishId())){
                        logger.error("修改发布信息接口===============>>>>【失败】>>>>发布名称已经被占用");
                        return ResponseUtils.responseError("修改发布信息失败，该名称已经存在");
                    }
                }else if (list != null && list.size() > 1 ){
                    logger.error("修改发布信息接口===============>>>>【失败】>>>>发布名称已经被占用");
                    return ResponseUtils.responseError("修改发布信息失败，该名称已经存在");
                }
                info.setUpdateUser(user.getUserId());
            }

            //当前发布编号不为空  且不是 字幕类型   原因 ： 字幕类型会根据publishId 去判断新增还是修改  因此不能在这里设置publishId
            if(info.getPublishId() == null && !PublishTypeEnums.LED.getValue().equals(info.getType())){
                logger.info("============>>>>>>>>操作>>>>>【新增】");
                //对主题栏目设置  remark  当做压缩文件的文件名  {publishId}.zip  当主题绑定栏目时可修改这个remark
                info.setPublishId(IdGen.get().nextId());
                if(PublishTypeEnums.THEME.getValue().equals(info.getType()) || PublishTypeEnums.COLUMN.getValue().equals(info.getType())) {
                    info.setRemark(info.getPublishId() + ".zip");
                }
            }
            //获取类型
            if(PublishTypeEnums.THEME.getValue().equals(info.getType())){
                //主题
                newFilePath = FileUtils.getWindowsPath(FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), Constants.Regular.my_themes, fileInfo.getDirectoryPath()));
                //设置属性
                info.setIndexFileName(indexFileName);   //设置主要文件名
                // info.setKind(null);                     //设置kind为null
                result = addTheme(info, result, newFilePath, completePath);
            }else if(PublishTypeEnums.COLUMN.getValue().equals(info.getType())){
                //栏目
                newFilePath = FileUtils.getWindowsPath(FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), Constants.Regular.my_columns, fileInfo.getDirectoryPath()));
                //设置属性
                info.setIndexFileName(indexFileName);   //设置主要文件名
                result = addColumn(info, result, newFilePath, indexFileName, completePath);
            }else if(PublishTypeEnums.VIDEO.getValue().equals(info.getType())){
                //视频
                result = addVideo(info, result);
            }else if(PublishTypeEnums.ZIP.getValue().equals(info.getType())){
                //zip
                result = addZip(info, result);
            }else if(PublishTypeEnums.LED.getValue().equals(info.getType())){
                //led  字幕
                result = addLed(info, result);
            }else {
                //无效的类型
                logger.error("新增发布信息接口===============>>>无效的类型type"+info.getType());
                return ResponseUtils.responseError("无效的类型type");
            }
        }else {
            logger.error("新增发布信接口===============>>>>用户信息已经失效");
            result = ResponseUtils.responseError("用户信息已经失效，请重新登录");
        }
        logger.info("新增发布信接口===============>>>>【结束】");
        return result;
    }

    /**
     * @description  发布信息接口
     * @param publishId
     * @param agents  json数组字符串  "[{"agentCode": "agentCode1", "agentId": 123456}, {....}]"
     * @param publishMode  1  3g
     * @param priority      优先级   数字越大  优先级越高
     * @return
     */
    @RecordLog(value = "发布信息接口")
    @Duplicate
    @ResponseBody
    @RequestMapping(value = "publish.json", method = RequestMethod.POST)
    public Object publish(Long publishId, String agents,
                          @RequestParam(defaultValue = "1") Integer publishMode,
                          int priority){
        logger.info("发布信息接口===============>>>【开始】");
        User user = getUser();
        if(user == null){
            logger.error("发布信息接口===============>>>无效的用户信息");
            return ResponseUtils.responseError("用户信息已经失效，请重新登录");
        }
        String result = new String();
        //根据发布信息编号加载数据
        Publish info = publishService.getPublishByPublishId(publishId);
        if(info != null){
            //解析数据
            List<Agent> array = parseArray(agents, Agent.class);
            if(array == null || array.size() < 1){
                logger.error("发布信息接口===============>>>无效的agents"+agents);
                return ResponseUtils.responseError("选择了无效的终端");
            }
            //批次号
            String batchNumber = StringUtils.generateUUID();
            Long themeColumnId = null;              //主题栏目编号
            String path = new String();             //path

            //校验数据存在   根据发布类型   发布对应的信息
            if(PublishTypeEnums.THEME.getValue().equals(info.getType())){
                //主题    更新终端主题编号   新增终端主题信息（记录）  设置 path
                List<Agent> agentList = new ArrayList<>();
                List<AgentTheme> agentThemes = new ArrayList<>();
                for (Agent agent : array){
                    //终端信息
                    Agent item = new Agent();
                    item.setAgentId(agent.getAgentId());
                    item.setThemeId(publishId);
                    agentList.add(item);
                    //终端主题信息
                    AgentTheme agentTheme = new AgentTheme();
                    agentTheme.setAgentThemeId(IdGen.get().nextId());
                    agentTheme.setCreateUser(user.getUserId());
                    agentTheme.setAgentId(agent.getAgentId());
                    agentTheme.setCreateTime(new Date());
                    agentTheme.setBatchNumber(batchNumber);
                    agentTheme.setThemeId(publishId);
                    agentTheme.setPublishMode(publishMode);
                    agentThemes.add(agentTheme);
                }
                logger.info("发布信息接口===============>>>【主题】更新终端使用中的主题");
                //更新终端使用中的主题
                agentService.updateBatch(agentList);
                logger.info("发布信息接口===============>>>【主题】更新历史终端主题");
                //更新历史终端主题
                agentThemeService.insertBatch(agentThemes);
                //压缩文件
                path = compressZip(path, info);
                logger.info("发布信息接口===============>>>【主题】压缩文件，获取path"+path);
            }else if(PublishTypeEnums.COLUMN.getValue().equals(info.getType())){
                //栏目  设置 path  themeColumnId
                path = compressZip(path, info);
                logger.info("发布信息接口===============>>>【栏目】压缩文件，获取path"+path);
            }else if(PublishTypeEnums.VIDEO.getValue().equals(info.getType())){
                //视频  设置 path
                path = doPath(path, info.getFileId());
                logger.info("发布信息接口===============>>>【视频】获取path"+path);
            }else if(PublishTypeEnums.ZIP.getValue().equals(info.getType())){
                //zip  设置 path
                path = doPath(path, info.getFileId());
                logger.info("发布信息接口===============>>>【zip】获取path"+path);
            }else if(PublishTypeEnums.LED.getValue().equals(info.getType())){
                //led  字幕   设置 path（暂时下发时  传输内容  而非 文件路径  所以  此处有无设置路径并无关系）
                path = doLed(path, info.getPath(), info.getIndexFileName());
                logger.info("发布信息接口===============>>>【led】获取path"+path);
            }else {
                //无效的类型
                logger.error("发布信息接口===============>>>无效的类型type"+info.getType());
                return ResponseUtils.responseError("无效的类型type");
            }
            //判断path是否有效
            if(path == null || "".equals(path)){
                logger.error("发布信息接口===============>>>无效的文件地址");
                return ResponseUtils.responseError("文件不存在");
            }
            //遍历集合  组装 agentTasks  histories
            List<AgentTaskInfo> agentTasks = new ArrayList<>();
            List<PublishHistory> histories = new ArrayList<>();
            for (Agent agent : array){
                //终端任务信息
                AgentTaskInfo agentTaskInfo = new AgentTaskInfo();
                agentTaskInfo.setTaskId(IdGen.get().nextId());      //主键
                agentTaskInfo.setType(info.getType());              //设置类型
                agentTaskInfo.setAgentCode(agent.getAgentCode());   //终端code
                agentTaskInfo.setCreateTime(new Date());            //创建时间
                agentTaskInfo.setCreateUser(user.getUserId());      //用户编号
                agentTaskInfo.setCode(CodeEnums.one.getValue());    //下发任务
                agentTaskInfo.setPublishId(publishId);              //发布编号
                agentTasks.add(agentTaskInfo);
                //发布历史信息
                PublishHistory publishHistory = new PublishHistory();
                publishHistory.setPublishHistoryId(IdGen.get().nextId());   //主键
                publishHistory.setPublishId(publishId);                     //发布编号
                publishHistory.setAgentId(agent.getAgentId());              //终端id
                publishHistory.setPublishName(info.getName());              //发布名称
                publishHistory.setBatchNumber(batchNumber);                 //发布批次号
                publishHistory.setCreateTime(new Date());                   //创建时间
                publishHistory.setCreateUser(user.getUserId());             //用户编号
                publishHistory.setCompanyId(user.getCompanyId());           //企业编号
                publishHistory.setPublishMode(publishMode);                 //发布模式  1 3g
                publishHistory.setType(info.getType());                     //发布类型
                publishHistory.setPath(path);                               //发布文件的路径
                publishHistory.setPriority(priority);                       //优先级
                publishHistory.setThemeColumnId(themeColumnId);             //主题栏目编号 设置默认 null
                //设置zip（软件包）的类型
                if(PublishTypeEnums.ZIP.getValue().equals(info.getType())){
                    publishHistory.setKind(info.getKind());                 //设置zip（软件包）的类型
                }
                //栏目需要设置themeColumnId
                if(PublishTypeEnums.COLUMN.getValue().equals(info.getType())){
                    Agent item = agentService.getAgentByAgentId(agent.getAgentId());
                    ThemeColumn themeColumn = themeColumnService.getThemeColumnByIds(item.getThemeId(), info.getDirectoryId());
                    publishHistory.setThemeColumnId(themeColumn.getThemeColumnId());             //主题栏目编号
                    //获取目录信息
                    if(info.getDirectoryId() != null){
                        Directory directory = directoryService.getDirectoryByDirectoryId(info.getDirectoryId());
                        publishHistory.setRemark(directory.getDirectoryName());
                    }
                }
                histories.add(publishHistory);
            }
            //新增   添加到agentTask  publishHistory
            logger.info("发布信息接口===============>>>更新终端任务信息");
            agentTaskService.insertBatch(agentTasks);
            logger.info("发布信息接口===============>>>更新发布历史信息");
            publishHistoryService.insertBatch(histories);
            result = ResponseUtils.responseSuccess("发布信息成功");
        }else {
            logger.error("发布信息接口===============>>>无效的发布信息");
            result = ResponseUtils.responseError("发布信息有误");
        }
        logger.info("发布信息接口===============>>>【结束】");
        return result;
    }


    /**
     * @description   发布命令信息接口
     * @param code
     * @param agentTasks
     * json字符串数组  {"code": 4, agentTasks:[{agentCode: "shza0001"}]}
     * @return
     */
    @Duplicate
    @ResponseBody
    @RequestMapping(value = "sendOrders.json", method = RequestMethod.POST)
    public Object sendOrders(String code, String agentTasks){
        logger.info("发布命令信息接口===============>>>【开始】");
        User user = getUser();
        if(user == null){
            logger.error("发布命令信息接口===============>>>无效的用户信息");
            return ResponseUtils.responseError("用户信息已经失效，请重新登录");
        }
        //获取数据
        List<AgentTaskInfo> list = JSON.parseArray(agentTasks, AgentTaskInfo.class);
        if(list != null && list.size() > 0){
            Log log = new Log();
            StringBuffer msg = new StringBuffer("发送命令=="+CodeEnums.getNameByValue(Integer.parseInt(code))).append(" | agentCode=");
            for (AgentTaskInfo task : list){
                task.setTaskId(IdGen.get().nextId());
                task.setCode(Integer.parseInt(code));
                task.setCreateUser(user.getUserId());
                task.setCreateTime(new Date());
                msg.append(task.getAgentCode()+" ");
            }
            logger.error("发布命令信息接口==>>【日志】==>>"+msg.toString());
            log.setLogMessage("发送命令=="+CodeEnums.getNameByValue(Integer.parseInt(code))+"|终端数量="+list.size());
            log.setCreateUser(user.getUserId());
            log.setRequestUrl(request_local.get().getRequestURI());
            log.setResponseCode(Constants.ResponseCode.success + "");
            //新增命令
            agentTaskService.insertBatch(list);
            //新增日志
            logservice.addOperateLog(log);
            logger.info("发布命令信息接口===============>>>【结束】");
            return ResponseUtils.responseSuccess("发送命令成功");
        }else {
            logger.info("发布命令信息接口===============>>>【参数有误】====>>>>集合为空");
            return ResponseUtils.responseError("发送命令失败【参数有误】");
        }
    }


    /**
     * 预览接口   目前的预览效果较差  放弃
     * @param publishId
     * @param model
     * @return
     */
    @Deprecated
    @RequestMapping(value = "{publishId}/preview")
    public String preview(@PathVariable(value = "publishId") Long publishId,
                          Model model){
        //根据发布信息编号加载数据
        Publish info = publishService.getPublishByPublishId(publishId);
        String path = new String();
        if(info != null){
            //判断发布信息的类型
            if(PublishTypeEnums.THEME.getValue().equals(info.getType())){
                //主题
                path = FileUtils.addPathSeparate(applicationConfig.getUploadUrl(), applicationConfig.getBaseDirectory(), Constants.Regular.my_themes, info.getPath(), info.getIndexFileName());
            }else if(PublishTypeEnums.COLUMN.getValue().equals(info.getType())){
                //栏目
                path = FileUtils.addPathSeparate(applicationConfig.getUploadUrl(), applicationConfig.getBaseDirectory(), Constants.Regular.my_columns, info.getPath(), info.getIndexFileName());
            }else if(PublishTypeEnums.VIDEO.getValue().equals(info.getType())){
                //视频   根据文件编号获取文件信息  拿到文件的路径
                FileInfo file = fileService.getFileByFileId(info.getFileId());
                if(file != null)
                    path = file.getFilePath();
            }else if(PublishTypeEnums.ZIP.getValue().equals(info.getType())){
                //zip   不支持预览  提供下载路径
                FileInfo file = fileService.getFileByFileId(info.getFileId());
                if(file != null)
                    path = file.getFilePath();
            }else if(PublishTypeEnums.LED.getValue().equals(info.getType())){
                //led  字幕
                model.addAttribute("text", info.getTxt());
            }else {
                //无效的类型
                logger.error("预览信息接口=======>>>>>>无效的类型");
                model.addAttribute("error", "无效的类型");
            }
            logger.info("path==============>>>>>>>"+path);
            model.addAttribute("path", path);
            model.addAttribute("type", info.getType());
        }else {
            logger.error("预览信息接口=======>>>>>>无效的发布信息编号");
            model.addAttribute("error", "无效的发布信息编号");
        }
        return "release/publish/preview";
    }


    //删除发布信息
    @ResponseBody
    @RequestMapping(value = "{publishId}/delete", method = RequestMethod.POST)
    public Object delete(@PathVariable("publishId") Long publishId){
        logger.info("删除发布信息=====>>>>>【开始】");
        Publish info = publishService.getPublishByPublishId(publishId);
        String result = publishService.remove(info, applicationConfig.getBaseDirectory());
        logger.info("删除发布信息=====>>>>>【结束】"+result);
        return result;
    }

    //批量删除发布信息接口
    @ResponseBody
    @RequestMapping(value = "deleteBatch", method = RequestMethod.POST)
    public Object deleteBatch(@RequestParam("ids[]") Long[] publishIds){
        List<Publish> publishes = publishService.getPublishByPublishIds(publishIds);
        for (Publish info : publishes){
            publishService.remove(info, applicationConfig.getBaseDirectory());
        }
        return ResponseUtils.responseSuccess("完成");
    }

    /********************************    私有方法     *************************/

    //添加主题
    private String addTheme(Publish info, String result, String newFilePath, String completePath){
        try {
            //文件解压
            ZipFileUtils.unZipFiles(new File(completePath), newFilePath);
            //获取指定xml文件的路径   读取xml的数据  写入数据库
            String path = FileUtils.getFilePathByFileName(newFilePath, Constants.Regular.theme_xml_name);
            //读取文件里的信息
            List<ThemeColumn> themeColumns = XMLUtils.readXML(path);
            //新增主题和主题栏目信息
            if(publishService.addTheme(info, themeColumns)){
                logger.info("新增发布信息接口====>>>>【主题】>>>>成功完成");
                result = ResponseUtils.responseSuccess("新增主题信息成功");
            }else {
                result = ResponseUtils.responseError("新增主题信息失败");
            }
        } catch (MalformedURLException | DocumentException e){
            logger.error("新增发布信息接口===============>>>>【主题】读取主题xml文件失败"+e.getMessage());
            result = ResponseUtils.responseError("读取主题xml文件失败");
        } catch (IOException e) {
            logger.error("新增发布信息接口===============>>>>【主题】解压文件失败"+e.getMessage());
            result = ResponseUtils.responseError("解压文件失败");
        }
        return result;
    }

    //添加栏目
    private String addColumn(Publish info, String result, String newFilePath, String indexFileName, String completePath){
        //判断是否有directoryId
        if(info.getDirectoryId() == null){
            logger.info("新增发布信息接口===============>>>>【栏目】请选择业务目录");
            return ResponseUtils.responseError("请选择业务目录");
        }
        //文件解压
        ZipFileUtils.unZipFiles(new File(completePath), newFilePath);
        //生成url.ini文件
        logger.info("新增发布信息接口===============>>>>【栏目】准备生成url.ini文件。。。。。");
        FileUtils.generateIniFile(FileUtils.addPathSeparate(newFilePath, Constants.Regular.url_ini), indexFileName, false);
        //新增栏目信息
        if(publishService.addInfo(info)){
            logger.info("新增发布信息接口====>>>>【栏目】>>>>成功完成");
            result = ResponseUtils.responseSuccess("新增栏目信息成功");
        }else {
            result = ResponseUtils.responseError("新增栏目信息失败");
        }
        return result;
    }

    //添加视频
    private String addVideo(Publish info, String result){
        if(publishService.addInfo(info)){
            logger.info("新增发布信息接口====>>>>【视频】>>>>成功完成");
            result = ResponseUtils.responseSuccess("新增视频信息成功");
        }else {
            result = ResponseUtils.responseError("新增视频信息失败");
        }
        return result;
    }

    //添加zip
    private String addZip(Publish info, String result){
        if(publishService.addInfo(info)){
            logger.info("新增发布信息接口====>>>>【zip】>>>>成功完成");
            result = ResponseUtils.responseSuccess("新增zip信息成功");
        }else {
            result = ResponseUtils.responseError("新增zip信息失败");
        }
        return result;
    }

    //添加字幕信息
    private String addLed(Publish info, String result){
        String dir = DateUtils.getTime(DateUtils.SDF_YMD_PATTERN);     //设置文件目录
        String indexFileName = DateUtils.getTime(DateUtils.SDF_YMDHMS_PATTERN)+".txt";  //设置主要文件名
        if(info.getPublishId() != null){ //修改
            Publish publish = publishService.getPublishByPublishId(info.getPublishId());
            if(publish == null){
                return ResponseUtils.responseError("修改字幕信息失败");
            }
            //获取源文件  删除
            String file = FileUtils.getWindowsPath(FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), Constants.Regular.my_leds, publish.getPath(), publish.getIndexFileName()));
            FileUtils.deleteDirectory(new File(file));
            info.setPath(dir);     //设置文件目录
            info.setIndexFileName(indexFileName);  //设置主要文件名
            if(publishService.updateInfo(info)){
                logger.info("修改发布信息接口====>>>>【字幕】>>>>成功完成");
                //生成文件  *.txt
                String path = FileUtils.getWindowsPath(FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), Constants.Regular.my_leds, info.getPath(), info.getIndexFileName()));
                FileUtils.generateIniFile(path, info.getTxt(), false);
                result = ResponseUtils.responseSuccess("修改字幕信息成功");
            }else {
                result = ResponseUtils.responseError("修改字幕信息失败");
            }
        }else {     //新增
            info.setPath(dir);     //设置文件目录
            info.setIndexFileName(indexFileName);  //设置主要文件名
            if(publishService.addInfo(info)){
                logger.info("新增发布信息接口====>>>>【字幕】>>>>成功完成");
                //生成文件  *.txt
                String path = FileUtils.getWindowsPath(FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), Constants.Regular.my_leds, info.getPath(), info.getIndexFileName()));
                FileUtils.generateIniFile(path, info.getTxt(), false);
                result = ResponseUtils.responseSuccess("新增字幕信息成功");
            }else {
                result = ResponseUtils.responseError("新增字幕信息失败");
            }
        }
        return result;
    }

    //压缩主题  栏目  zip  返回压缩文件地址
    private String compressZip(String path, Publish info){
        //主题  设置path
        String srcParentFile;
        if(PublishTypeEnums.THEME.getValue().equals(info.getType())){
            srcParentFile = FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), Constants.Regular.my_themes);
        }else if(PublishTypeEnums.COLUMN.getValue().equals(info.getType())){
            srcParentFile = FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), Constants.Regular.my_columns);
        }else {
            logger.error("无效的类型========》》》》"+info.getType());
            srcParentFile = "";
        }
        //当remark 为空  重新赋值
        if(info.getRemark() == null || "".equals(info.getRemark())){
            info.setRemark(info.getPublishId()+".zip");
        }
        //栏目  设置 path
        String srcFile = FileUtils.getWindowsPath(FileUtils.addPathSeparate(srcParentFile, info.getPath()));    //源文件
        String zipFile = FileUtils.getWindowsPath(FileUtils.addPathSeparate(srcParentFile, info.getRemark()));   //zip文件
        try {
            if(!FileUtils.existsFile(new File(zipFile))){   //如果存在zip文件就不再打包
                logger.info("zip文件不存在，开始打包中.....");
                CompressedFileUtil.compressedFile(srcFile, zipFile);
            }
            path = zipFile;
        } catch (Exception e) {
            logger.error("压缩栏目zip文件失败======》》》》》\n"+e.getMessage());
            logger.error(e.getMessage(), e);
        }
        return path;
    }

    //设置  其他类型的path  视频   zip
    private String doPath(String path, Long fileId){
        FileInfo fileInfo = fileService.getFileByFileId(fileId);
        //替换字符串
        if(fileInfo != null && fileInfo.getFilePath().indexOf(applicationConfig.getUploadUrl()) != -1) {
            path = fileInfo.getFilePath().substring(applicationConfig.getUploadUrl().length());
            if (SystemUtils.isWindows()) {
                logger.info("=======>>>>>>>windows系统>>>>>>>检测目录路径........");
                path = FileUtils.addPathSeparate(FileUtils.getTrulyPath(Constants.Regular.base_dir), path);
            }
        }
        return path;
    }

    //处理字幕信息
    private String doLed(String path, String filePath, String indexFileName) {
        path = FileUtils.getWindowsPath(FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), Constants.Regular.my_leds, filePath, indexFileName));
        logger.info("====led文件路径====>>>>"+path);
        if(!FileUtils.existsFile(new File(path))){
            logger.error("====led文件路径====>>>>不存在该文件");
            path = "";
        }
        return path;
    }

}
