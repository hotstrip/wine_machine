package com.seestech.sell.web.release;

import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.PublishHistory;
import com.seestech.sell.domain.model.User;
import com.seestech.sell.service.IPublishHistoryService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;


/**
 * Created by idiot on 2016/12/29.
 */
@Controller
@RequestMapping(value = "release/history")
public class ReleaseHistoryController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(ReleaseHistoryController.class);

    @Resource
    private IPublishHistoryService publishHistoryService;


    //加载page页面
    @RecordLog(value = "加载发布历史页面")
    @RequiresPermissions(value = "release:history:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(){
        return "release/history/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize,
                       Model model, PublishHistory info){
        User user  = getUser();
        if(user != null){
            //获取这个用户所在企业相关的用户信息
            Set<Long> userIds = getUserIds(user);
            userIds.add(user.getUserId());
            info.setUserIds(userIds);
            info.setCreateUser(null);
            // info.setUserId(user.getUserId());
            List<PublishHistory> list = publishHistoryService.getAllPublishHistories(new RowBounds((pageNo-1)*pageSize, pageSize), info);
            model.addAttribute("lists", list);
            return "release/history/data";
        }else {
            logger.error("用户信息失效，重新登录页面.....");
            return "login";
        }
    }

    /**
     * @description  前往重发页面
     * @param publishHistoryId
     * @param model
     * @return
     */
    @RequestMapping(value = "{publishHistoryId}/toRepublish", method = RequestMethod.GET)
    public String rePublish(@PathVariable("publishHistoryId") Long publishHistoryId, Model model){
        logger.info("栏目历史记录=====>前往重发页面");
        //前往栏目重发页面 加载所有终端组
        PublishHistory publishHistory = publishHistoryService.getPublishHistoryById(publishHistoryId);
        String result;
        if(publishHistory != null){
            model.addAttribute("publishId", publishHistory.getPublishId());
            result = "release/publish/publish";
        }else {
            result = "error";
            model.addAttribute("msg", "发布信息无效");
        }
        return result;
    }


    //删除
    @RequestMapping(value = "{publishHistoryId}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable("publishHistoryId") Long publishHistoryId){
        publishHistoryService.delete(publishHistoryId);
        ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "删除发布历史信息成功");
    }

    //批量删除
    @RequestMapping(value = "deleteBatch", method = RequestMethod.POST)
    public void deleteBatch(@RequestParam("ids[]") Long[] publishHistoryIds){
        publishHistoryService.deleteBatch(publishHistoryIds);
        ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "批量删除发布历史信息成功");
    }

}
