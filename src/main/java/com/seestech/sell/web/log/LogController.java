package com.seestech.sell.web.log;

import com.github.pagehelper.Page;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.Log;
import com.seestech.sell.service.ILogService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

/**
 * Created by idiot on 2016/12/17.
 */
@Controller
@RequestMapping(value = "log")
public class LogController extends SuperController {
    private static final Logger logger = LoggerFactory.getLogger(LogController.class);

    @Resource
    private ILogService logService;

    //加载page页面
    @RequiresPermissions(value = "log:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(Model model){
        logger.info("加载日志页面信息=====》》》");
        return "log/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Log info, Model model){
        Page<Log> lists = logService.getAllLogs(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        model.addAttribute("lists", lists);
        return "log/data";
    }

    //删除日志信息
    @RequestMapping(value = "{logId}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable("logId") long logId){
        logService.delete(logId);
        ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"删除日志信息成功");
    }

    @RequestMapping(value = "deleteBatch", method = RequestMethod.POST)
    public void deleteBatch(@RequestParam("ids[]") Long[] logIds){
        logService.deleteBatch(logIds);
        ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"批量删除日志信息成功");
    }
}
