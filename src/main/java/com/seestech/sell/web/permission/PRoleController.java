package com.seestech.sell.web.permission;


import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.domain.model.Menu;
import com.seestech.sell.domain.model.Role;
import com.seestech.sell.service.IMenuService;
import com.seestech.sell.service.IRoleService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description 权限管理
 * Created by idiot on 2016/12/15.
 */
@Controller
@RequestMapping(value = "permission/role")
public class PRoleController extends SuperController {
    private static final Logger logger = LoggerFactory.getLogger(PRoleController.class);

    @Resource
    private IRoleService roleService;
    @Resource
    private IMenuService menuService;

    /**
     * 权限之角色相关
     * */

    //加载page页面
    @RecordLog(value = "加载角色信息页面")
    @RequiresPermissions(value = "permission:role:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(){
        return "permission/role/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Role info, Model model){
        List<Role> lists = roleService.getAllRoles(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        model.addAttribute("lists", lists);
        return "permission/role/data";
    }

    //加载角色信息编辑页面
    @RequestMapping(value = "toUpdate", method = RequestMethod.GET)
    public String toAdd(Model model){
        return "permission/role/update";
    }

    //编辑角色信息
    @RecordLog(value = "修改角色信息")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public void update(Role info){
        //userId为空/0时表示   新增操作
        if(info.getRoleId() == null){
            roleService.insert(info);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "新增角色信息成功");
        }else {     //修改操作
            roleService.update(info);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "修改角色信息成功");
        }
    }

    //加载编辑页面
    @RequestMapping(value = "{roleId}/toUpdate", method = RequestMethod.GET)
    public String toUpdate(@PathVariable("roleId") long roleId, Model model){
        Role role = roleService.getRoleByRoleId(roleId);
        model.addAttribute("role",role);
        return "permission/role/update";
    }

    //删除角色信息
    @RecordLog(value = "删除角色信息")
    @RequestMapping(value = "{roleId}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable("roleId") long roleId){
        roleService.delete(roleId);
        ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"删除角色信息成功");
    }

    //前往授权资源页面
    @RequestMapping(value = "{roleId}/toGrantMenus", method = RequestMethod.GET)
    public String toGrantRoles(@PathVariable("roleId") Long roleId, Model model){
        //根据roleId获取角色已经拥有的资源（菜单）信息
        List<Menu> roleMenus = menuService.getMenusByRoleId(roleId);
        //获取所有的菜单（资源）信息
        List<Menu> menus = menuService.getMultiLevelMenus();

        model.addAttribute("roleId", roleId);
        model.addAttribute("roleMenus", roleMenus);
        model.addAttribute("menus", menus);
        return "permission/role/grantMenus";
    }
}
