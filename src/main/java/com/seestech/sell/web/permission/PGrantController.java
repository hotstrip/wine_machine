package com.seestech.sell.web.permission;


import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.domain.model.RoleMenu;
import com.seestech.sell.domain.model.UserAgentGroup;
import com.seestech.sell.domain.model.UserRole;
import com.seestech.sell.domain.model.enums.StatusEnums;
import com.seestech.sell.service.IRoleMenuService;
import com.seestech.sell.service.IUserAgentGroupService;
import com.seestech.sell.service.IUserRoleService;
import com.seestech.sell.web.SuperController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

/**
 * @description 权限管理
 * Created by idiot on 2016/12/15.
 */
@Controller
@RequestMapping(value = "permission/grant")
public class PGrantController extends SuperController {
    private static final Logger logger = LoggerFactory.getLogger(PGrantController.class);

    @Resource
    private IUserRoleService userRoleService;
    @Resource
    private IRoleMenuService roleMenuService;
    @Resource
    private IUserAgentGroupService userAgentGroupService;
    /**
     * 权限之授权相关
     * */

    //修改用户角色信息
    @RecordLog(value = "修改用户角色信息")
    @RequestMapping(value = "updateUserRole", method = RequestMethod.POST)
    public void updateUserRole(Model model, Long userId, Long roleId, boolean checked){
        //checked为true  表示新增
        logger.info("用户授权角色信息=====》》》》》");
        UserRole userRole = new UserRole();
        userRole.setUserId(userId);
        userRole.setRoleId(roleId);
        if(checked){
            userRole.setUserRoleId(IdGen.get().nextId());
            userRole.setStatus(StatusEnums.VALID.getValue());
            userRoleService.insert(userRole);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"新增用户角色信息成功");
        }else {
            userRoleService.remove(userRole);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"删除用户角色信息成功");
        }
    }

    //修改角色菜单信息
    @RecordLog(value = "修改角色菜单信息")
    @RequestMapping(value = "updateRoleMenu", method = RequestMethod.POST)
    public void updateRoleMenu(Model model, long menuId, long roleId, boolean checked){
        //checked为true  表示新增
        logger.info("用户角色菜单信息=====》》》》》");
        RoleMenu roleMenu= new RoleMenu();
        roleMenu.setMenuId(menuId);
        roleMenu.setRoleId(roleId);
        if(checked){
            roleMenu.setRoleMenuId(IdGen.get().nextId());
            roleMenu.setStatus(StatusEnums.VALID.getValue());
            roleMenuService.insert(roleMenu);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"新增角色菜单信息成功");
        }else {
            roleMenuService.remove(roleMenu);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"删除角色菜单信息成功");
        }
    }

    //修改用户终端信息
    @RecordLog(value = "用户分配终端分组")
    @RequestMapping(value = "updateUserAgentGroup", method = RequestMethod.POST)
    public void updateUserAgentGroup(Model model, long userId, long agentGroupId, boolean checked){
        //checked为true  表示新增
        logger.info("用户分配终端组信息=====》》》》》");
        UserAgentGroup userAgentGroup= new UserAgentGroup();
        userAgentGroup.setUserId(userId);
        userAgentGroup.setAgentGroupId(agentGroupId);
        if(checked){
            userAgentGroup.setUserAgentGroupId(IdGen.get().nextId());
            userAgentGroup.setStatus(StatusEnums.VALID.getValue());
            userAgentGroupService.insert(userAgentGroup);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"新增用户终端分组信息成功");
        }else {
            userAgentGroupService.remove(userAgentGroup);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"删除用户终端分组信息成功");
        }
    }

}
