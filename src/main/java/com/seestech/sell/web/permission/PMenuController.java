package com.seestech.sell.web.permission;

import com.github.pagehelper.Page;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.domain.model.Menu;
import com.seestech.sell.service.IMenuService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description 权限管理
 * Created by idiot on 2016/12/15.
 */
@Controller
@RequestMapping(value = "permission/menu")
public class PMenuController extends SuperController {
    private static final Logger logger = LoggerFactory.getLogger(PMenuController.class);

    @Resource
    private IMenuService menuService;

    /**
     * 权限之菜单相关
     * */
    //加载page页面
    @RecordLog(value = "加载资源信息页面")
    @RequiresPermissions(value = "permission:menu:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(){
        return "permission/menu/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Menu info, Model model){
        Page<Menu> lists = menuService.getAllPageMenus(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        model.addAttribute("lists", lists);
        return "permission/menu/data";
    }

    //加载用户信息编辑页面
    @RequestMapping(value = "toUpdate", method = RequestMethod.GET)
    public String toAdd(Model model, int kind){
        //加载所有有效的菜单  方便选择父级菜单
        List<Menu> list = menuService.getAllFirstMenus();
        model.addAttribute("menus", list);
        model.addAttribute("kind", kind);
        return "permission/menu/update";
    }

    //编辑用户信息
    @RecordLog(value = "修改资源信息")
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public void update(Menu info){
        //menuId为空/0时表示   新增操作
        if(info.getMenuId() == null){
            menuService.insert(info);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "新增菜单信息成功");
        }else {     //修改操作
            menuService.update(info);
            ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(), "修改菜单信息成功");
        }
    }

    //加载编辑页面
    @RequestMapping(value = "{menuId}/toUpdate", method = RequestMethod.GET)
    public String toUpdate(@PathVariable("menuId") Long menuId, Model model){
        //根据menuId查询菜单信息   再查询父级菜单信息
        Menu menu = menuService.getMenuByMenuId(menuId);
        if(menu.getParentId() != null){
            Menu parentMenu = menuService.getMenuByMenuId(menu.getParentId());
            menu.setParentName(parentMenu.getMenuName());
        }
        //加载所有有效的菜单  方便选择父级菜单
        List<Menu> list = menuService.getAllFirstMenus();
        model.addAttribute("menus", list);
        model.addAttribute("menu",menu);
        return "permission/menu/update";
    }

    //删除菜单信息
    @RecordLog(value = "删除资源信息")
    @RequestMapping(value = "{menuId}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable("menuId") Long menuId){
        menuService.delete(menuId);
        ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"删除菜单信息成功");
    }

}
