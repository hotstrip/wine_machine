package com.seestech.sell.web.report;

import com.alibaba.fastjson.JSONArray;
import com.seestech.sell.common.utils.JsonUtils;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.Order;
import com.seestech.sell.domain.model.User;
import com.seestech.sell.service.IOrderService;
import com.seestech.sell.web.SuperController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2017/8/14.
 * @description  图表信息接口
 */
@Controller
@RequestMapping(value = "charts")
public class ChartsController extends SuperController{
    private static Logger logger = LoggerFactory.getLogger(ChartsController.class);

    @Resource
    private IOrderService orderService;

    //前往图表页面
    @RequestMapping(value = "page")
    @RequiresPermissions(value = "charts:page")
    public String toPage(){
        logger.info("前往图表页面=========>>>>>>");
        return "charts/page";
    }

    /****************************  接口  ********************************/

    /**
     * 月销售额   接口
     * @param info
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getMonthlySales.json", method = RequestMethod.POST)
    public Object getMonthlySales(Order info){
        logger.info("=======>>>>.加载图表数据=======>>>>>>【每月销售总额】");
        User user = getUser();
        if(user == null){
            logger.error("加载图表数据【每月销售总额】======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.responseError("用户信息失效，请重新登录");
        }
        List<Long> companyIds = getCompanyIdsByUser(user);
        info.setCompanyIds(companyIds);
        //获取数据集合   每月销售总额   年-月数
        List<Order> list = orderService.getMonthlySales(info);
        //json 过滤
        JSONArray object = JsonUtils.parseArray(list);
        return ResponseUtils.responseSuccess(object);
    }


    /**
     * 各种商品销售额接口
     * @param info
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getGoodsSales.json", method = RequestMethod.POST)
    public Object getGoodsSales(Order info){
        logger.info("=======>>>>.加载图表数据=======>>>>>>【每种商品的销售总额】");
        User user = getUser();
        if(user == null){
            logger.error("加载图表数据【每种商品的销售总额】======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.responseError("用户信息失效，请重新登录");
        }
        List<Long> companyIds = getCompanyIdsByUser(user);
        info.setCompanyIds(companyIds);
        //获取数据集合   每种商品的销售总额    商品名称
        List<Order> list = orderService.getGoodsSales(info);
        //json 过滤
        JSONArray object = JsonUtils.parseArray(list);
        return ResponseUtils.responseSuccess(object);
    }


    /**
     * 近两天销售额接口
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getRecentlyDaysSales.json")
    public Object getRecentlyDaysSales(Order info){
        logger.info("=======>>>>.加载图表数据=======>>>>>>【近两天销售额】");
        User user = getUser();
        if(user == null){
            logger.error("加载图表数据【近两天销售额】======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.responseError("用户信息失效，请重新登录");
        }
        List<Long> companyIds = getCompanyIdsByUser(user);
        info.setCompanyIds(companyIds);
        String dateformat = "%Y-%m-%d %H";
        info.setDateformat(dateformat);
        List<Order> list = orderService.getSalesByDateformat(info);
        //json 过滤
        JSONArray object = JsonUtils.parseArray(list);
        return ResponseUtils.responseSuccess(object);
    }

    /**
     * 近两天订单量接口
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getRecentlyDaysOrders.json")
    public Object getRecentlyDaysOrders(Order info){
        logger.info("=======>>>>.加载图表数据=======>>>>>>【近两天订单量】");
        User user = getUser();
        if(user == null){
            logger.error("加载图表数据【近两天订单量】======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.responseError("用户信息失效，请重新登录");
        }
        List<Long> companyIds = getCompanyIdsByUser(user);
        info.setCompanyIds(companyIds);
        String dateformat = "%Y-%m-%d %H";
        info.setDateformat(dateformat);
        List<Order> list = orderService.getOrdersByDateformat(info);
        //json 过滤
        JSONArray object = JsonUtils.parseArray(list);
        return ResponseUtils.responseSuccess(object);
    }

    /**
     * 近两月销售额接口
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getRecentlyMonthsSales.json")
    public Object getRecentlyMonthsSales(Order info){
        logger.info("=======>>>>.加载图表数据=======>>>>>>【近两月销售额】");
        User user = getUser();
        if(user == null){
            logger.error("加载图表数据【近两月销售额】======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.responseError("用户信息失效，请重新登录");
        }
        List<Long> companyIds = getCompanyIdsByUser(user);
        info.setCompanyIds(companyIds);
        String dateformat = "%Y-%m-%d";
        info.setDateformat(dateformat);
        List<Order> list = orderService.getSalesByDateformat(info);
        //json 过滤
        JSONArray object = JsonUtils.parseArray(list);
        return ResponseUtils.responseSuccess(object);
    }

    /**
     * 近两月订单量接口
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getRecentlyMonthsOrders.json")
    public Object getRecentlyMonthsOrders(Order info){
        logger.info("=======>>>>.加载图表数据=======>>>>>>【近两月订单量】");
        User user = getUser();
        if(user == null){
            logger.error("加载图表数据【近两月订单量】======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.responseError("用户信息失效，请重新登录");
        }
        List<Long> companyIds = getCompanyIdsByUser(user);
        info.setCompanyIds(companyIds);
        String dateformat = "%Y-%m-%d";
        info.setDateformat(dateformat);
        List<Order> list = orderService.getOrdersByDateformat(info);
        //json 过滤
        JSONArray object = JsonUtils.parseArray(list);
        return ResponseUtils.responseSuccess(object);
    }
}
