package com.seestech.sell.web.report;

import com.github.pagehelper.Page;
import com.seestech.sell.common.config.ApplicationConfig;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.FileUtils;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.Report;
import com.seestech.sell.domain.model.User;
import com.seestech.sell.service.IReportService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2017/8/29.
 */
@Controller
@RequestMapping(value = "report")
public class ReportController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(RecorderController.class);

    @Resource
    private IReportService reportService;
    @Resource
    private ApplicationConfig applicationConfig;

    //加载page页面
    @RequiresPermissions(value = "report:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(Model model){
        logger.info("加载日志反馈页面信息=====》》》");
        return "report/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Report info, Model model){
        User user = getUser();
        if(user != null){
            info.setUserId(user.getUserId());
            Page<Report> reports = reportService.getAllReports(new RowBounds((pageNo-1) * pageSize, pageSize), info);
            model.addAttribute("lists", reports);
        }
        return "report/data";
    }

    //下载文件页面
    @RequestMapping(value = "toDownload", method = RequestMethod.GET)
    public String toDownload(){
        return "report/download";
    }

    /*********************************  接口  ******************/

    //获取文件信息
    @ResponseBody
    @RequestMapping(value = "getFiles.json", method = RequestMethod.POST)
    public String getFiles(){
        String path = FileUtils.getWindowsPath(FileUtils.addPathSeparate(applicationConfig.getBaseDirectory(), Constants.Regular.logs));
        List<String> files = FileUtils.getFilesByPath(applicationConfig.getUploadUrl(), path);
        return ResponseUtils.responseSuccess(files);
    }
}
