package com.seestech.sell.web.report;

import com.github.pagehelper.Page;
import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.Recorder;
import com.seestech.sell.domain.model.User;
import com.seestech.sell.service.IRecorderService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2017/6/22.
 */
@Controller
@RequestMapping(value = "recorder")
public class RecorderController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(RecorderController.class);

    @Resource
    private IRecorderService recorderService;

    //加载page页面
    @RequiresPermissions(value = "order:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(Model model){
        logger.info("加载补货记录页面信息=====》》》");
        return "recorder/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Recorder info, Model model){
        User user = getUser();
        if(user == null){
            logger.error("【加载补货记录页面数据】======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.responseError("用户信息失效，请重新登录");
        }
        info.setOperate(1);     //补货操作
        Page<Recorder> lists = recorderService.getRecordersGroupByBatchNumber(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        model.addAttribute("lists", lists);
        return "recorder/data";
    }

    //详情
    @RequestMapping(value = "{batchNumber}/toDetail")
    public String detail(@PathVariable("batchNumber") String batchNumber, Model model){
        List<Recorder> recorders = recorderService.getRecordersByBatchNumber(batchNumber);
        model.addAttribute("lists", recorders);
        return "recorder/detail";
    }


    //删除品牌信息
    @RecordLog(value = "删除补货记录信息")
    @RequestMapping(value = "{recorderId}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable("recorderId") Long recorderId){
        recorderService.delete(recorderId);
        ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"删除补货记录信息成功");
    }

    //批量删除
    @RequestMapping(value = "deleteBatch", method = RequestMethod.POST)
    public void deleteBatch(@RequestParam("ids[]") Long[] recorderIds){
        recorderService.deleteBatch(recorderIds);
        ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"批量删除补货记录信息成功");
    }
}
