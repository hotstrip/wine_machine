package com.seestech.sell.web.report;

import com.github.pagehelper.Page;
import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.common.utils.ExcelExport;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.Order;
import com.seestech.sell.domain.model.User;
import com.seestech.sell.domain.model.enums.ExcelTypeEnums;
import com.seestech.sell.domain.model.vo.OrderVoExcel;
import com.seestech.sell.service.IOrderService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Set;

/**
 * Created by idiot on 2017/6/22.
 */
@Controller
@RequestMapping(value = "order")
public class OrderController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(OrderController.class);

    private static String orderListFile = "order_list.xlsx";

    @Resource
    private IOrderService orderService;

    //加载page页面
    @RequiresPermissions(value = "order:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String toPage(Model model){
        logger.info("加载订单页面信息=====》》》");
        return "order/page";
    }

    //加载data页面
    @RequestMapping(value = "data", method = RequestMethod.POST)
    public String data(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Order info, Model model){
        User user = getUser();
        if(user == null){
            logger.error("加载订单数据页面======>>>>>>>>用户信息失效，请重新登录");
            return "login";
        }
        List<Long> companyIds = getCompanyIdsByUser(user);
        info.setCompanyIds(companyIds);
        Page<Order> lists = orderService.getAllOrders(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        model.addAttribute("lists", lists);
        return "order/data";
    }

    /**
     * 数据导出
     * @param info
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "exportData.json", method = RequestMethod.POST)
    public String exportData(Order info, Model model) throws FileNotFoundException {
        User user = getUser();
        if(user == null){
            logger.error("加载订单数据页面======>>>>>>>>用户信息失效，请重新登录");
            return "login";
        }
        List<Long> companyIds = getCompanyIdsByUser(user);
        info.setCompanyIds(companyIds);
        //项目访问路径
        String serverPath = request_local.get().getScheme() + "://" + request_local.get().getServerName() + ":" +
                request_local.get().getServerPort() + request_local.get().getContextPath() + "/";
        List<Order> list = orderService.getAllExportOrders(new RowBounds(), info);
        // 写入excel文件
        File file = new File(Thread.currentThread().getContextClassLoader().getResource("/templates").getPath()+ orderListFile);
        OutputStream out = new FileOutputStream(file);
        ExcelExport.exportExcel("订单数据表", OrderVoExcel.class, list, out, ExcelTypeEnums.XLSX);
        //返回下载文件地址
        return ResponseUtils.responseSuccess(serverPath + orderListFile);
    }


    //删除品牌信息
    @RecordLog(value = "删除订单信息")
    @RequestMapping(value = "{orderId}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable("orderId") Long orderId){
        orderService.delete(orderId);
        ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"删除订单信息成功");
    }

    //批量删除
    @RequestMapping(value = "deleteBatch", method = RequestMethod.POST)
    public void deleteBatch(@RequestParam("ids[]") Long[] orderIds){
        orderService.deleteBatch(orderIds);
        ResponseUtils.writeSuccessResponse(request_local.get(), response_local.get(),"批量删除订单信息成功");
    }

    /*****************/

    //退款页面
    @RequiresPermissions(value = "order:refund")
    @RequestMapping(value = "refund")
    public String refund(){
        return "order/refundPage";
    }

    //可退款页面数据
    @RequestMapping(value = "refundData")
    public String refundData(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize, Order info, Model model){
        User user = getUser();
        if(user == null){
            logger.error("【可退款页面数据】======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.responseError("用户信息失效，请重新登录");
        }
        List<Long> companyIds = getCompanyIdsByUser(user);
        info.setCompanyIds(companyIds);
        Page<Order> lists = orderService.getRefundAbleOrders(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        model.addAttribute("lists", lists);
        return "order/refundData";
    }


    //退款
    @RequiresPermissions(value = "order:toRefund")
    @RequestMapping(value = "{orderId}/toRefund")
    public String toRefund(@PathVariable("orderId") Long orderId, Model model){
        Order order = orderService.getOrderByOrderId(orderId);
        model.addAttribute("order", order);
        return "order/refund";
    }



}
