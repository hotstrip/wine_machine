package com.seestech.sell.web.report;

import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.JsonUtils;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.model.Order;
import com.seestech.sell.domain.model.ResultBean;
import com.seestech.sell.domain.model.User;
import com.seestech.sell.service.IOrderService;
import com.seestech.sell.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.apache.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2017/9/11.
 */
@Controller
@RequestMapping(value = "rank")
public class RankController extends SuperController {
    private static Logger logger = Logger.getLogger(RankController.class);

    @Resource
    private IOrderService orderService;


    // 加载排行榜页面
    @RequiresPermissions(value = "rank:page")
    @RequestMapping(value = "page", method = RequestMethod.GET)
    public String page(){
        return "rank/page";
    }


    /**
     * 商品销售排行榜
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "getGoodsRank.json")
    @ResponseBody
    public Object getGoodsRank(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize,
                               Order info){
        ResultBean<Object> resultBean;
        User user = getUser();
        if(user == null){
            logger.error("加载订单数据页面======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.initResultBean(null, Constants.ResponseCode.invalid_user, "用户信息失效，请重新登录", false);
        }
        List<Long> companyIds = getCompanyIdsByUser(user);
        info.setCompanyIds(companyIds);
        Page<Order> orders = orderService.getGoodsRank(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        JSONArray object = JsonUtils.parseArray(orders);
        resultBean = ResponseUtils.initResultBean(object, Constants.ResponseCode.success, "成功", true);
        resultBean.setTotalCount((int) orders.getTotal());
        logger.debug("获取商品销售排行榜信息成功.....");
        return resultBean;
    }

    /**
     * 终端销售排行榜
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "getAgentRank.json")
    @ResponseBody
    public Object getAgentRank(Integer pageNo, @RequestParam(defaultValue="10")Integer pageSize,
                               Order info){
        ResultBean<Object> resultBean;
        User user = getUser();
        if(user == null){
            logger.error("加载订单数据页面======>>>>>>>>用户信息失效，请重新登录");
            return ResponseUtils.initResultBean(null, Constants.ResponseCode.invalid_user, "用户信息失效，请重新登录", false);
        }
        List<Long> companyIds = getCompanyIdsByUser(user);
        info.setCompanyIds(companyIds);
        Page<Order> orders = orderService.getAgentRank(new RowBounds((pageNo-1)*pageSize, pageSize), info);
        JSONArray object = JsonUtils.parseArray(orders);
        resultBean = ResponseUtils.initResultBean(object, Constants.ResponseCode.success, "成功", true);
        resultBean.setTotalCount((int) orders.getTotal());
        logger.debug("获取终端销售排行榜信息成功.....");
        return resultBean;
    }

}
