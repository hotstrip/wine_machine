package com.seestech.sell.domain.model;

import java.util.List;

/**
 * Created by idiot on 2017/6/15.
 */
public class Menu extends BaseBean {
    //菜单编号
    private Long menuId;
    //菜单名称
    private String menuName;
    //菜单父级编号
    private Long parentId;
    //菜单链接地址
    private String menuUrl;
    //菜单图标
    private String menuIcon;
    //菜单展示顺序
    private Integer menuIndex;
    //菜单对应的权限信息
    private String permission;
    //区分是菜单还是资源(1 菜单 2 资源)
    private Integer kind;

    //显示字段
    private String parentName;
    private List<Menu> listMenus;

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public Integer getMenuIndex() {
        return menuIndex;
    }

    public void setMenuIndex(Integer menuIndex) {
        this.menuIndex = menuIndex;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Integer getKind() {
        return kind;
    }

    public void setKind(Integer kind) {
        this.kind = kind;
    }


    /*******************/
    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public List<Menu> getListMenus() {
        return listMenus;
    }

    public void setListMenus(List<Menu> listMenus) {
        this.listMenus = listMenus;
    }
}