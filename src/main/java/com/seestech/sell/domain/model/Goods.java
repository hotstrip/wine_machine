package com.seestech.sell.domain.model;

import java.util.Set;

/**
 * Created by idiot on 2017/6/15.
 */
public class Goods extends BaseBean {
    //商品编号
    private Long goodsId;
    //编号
    private String code;
    //商品名称
    private String name;
    //品牌编号
    private Long brandId;
    //规格
    private String specification;
    //价格
    private Double price;
    //图片
    private String image;
    //详情图片
    private String detailImage;

    //显示字段
    private String userName;
    private String brandName;   //品牌名称
    private String goods_id;
    private String remain;      //剩余库存
    private String stock;       //容量
    private String alarm;       //告警数量
    private Set<Long> userIds;

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImage() {
        if (this.image == null)
            return "";
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDetailImage() {
        if (this.detailImage == null)
            return "";
        return detailImage;
    }

    public void setDetailImage(String detailImage) {
        this.detailImage = detailImage;
    }

    /*************************/
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getGoods_id() {
        return String.valueOf(this.getGoodsId());
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getRemain() {
        return remain;
    }

    public void setRemain(String remain) {
        this.remain = remain;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getAlarm() {
        return alarm;
    }

    public void setAlarm(String alarm) {
        this.alarm = alarm;
    }

    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }
}