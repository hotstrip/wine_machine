package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/6/15.
 */
public class ShelfDetail extends BaseBean {
    //编号
    private Long detailId;
    //货架类型编号
    private Long shelfId;
    //货道编号
    private Long gridId;
    //序号（1开始）
    private int idx;

    //显示字段
    private String gridName;
    private String goodsName;
    private String image;           //商品图片
    private String detailImage;     //商品详情图片
    private Double gridPrice;
    private Double goodsPrice;
    private Long goodsId;
    private String userName;
    private Integer totalGoods;     //柜格总库存
    private int gridAlarm;          //柜格报警数量

    public Long getDetailId() {
        return detailId;
    }

    public void setDetailId(Long detailId) {
        this.detailId = detailId;
    }

    public Long getShelfId() {
        return shelfId;
    }

    public void setShelfId(Long shelfId) {
        this.shelfId = shelfId;
    }

    public Long getGridId() {
        return gridId;
    }

    public void setGridId(Long gridId) {
        this.gridId = gridId;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    /**************************/

    public String getGridName() {
        return gridName;
    }

    public void setGridName(String gridName) {
        this.gridName = gridName;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDetailImage() {
        return detailImage;
    }

    public void setDetailImage(String detailImage) {
        this.detailImage = detailImage;
    }

    public Double getGridPrice() {
        return gridPrice;
    }

    public void setGridPrice(Double gridPrice) {
        this.gridPrice = gridPrice;
    }

    public Double getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(Double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getTotalGoods() {
        return totalGoods;
    }

    public void setTotalGoods(Integer totalGoods) {
        this.totalGoods = totalGoods;
    }

    public int getGridAlarm() {
        return gridAlarm;
    }

    public void setGridAlarm(int gridAlarm) {
        this.gridAlarm = gridAlarm;
    }
}