package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/8/1.
 */
public class Statistics extends BaseBean {
    //主键
    private Long id;
    //项目名
    private String name;
    //点击区域
    private String tapArea;
    //点击次数
    private Long time;
    //类型  1 点击   2 播放
    private Integer type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTapArea() {
        return tapArea;
    }

    public void setTapArea(String tapArea) {
        this.tapArea = tapArea;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
