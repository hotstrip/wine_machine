package com.seestech.sell.domain.model.vo;

import com.seestech.sell.common.annotation.Excel;
import com.seestech.sell.common.utils.DateUtils;
import com.seestech.sell.domain.model.BaseBean;

import java.util.Date;

/**
 * Created by idiot on 2017/8/31.
 */
public class AgentVoExcel {
    //终端编号
    private Long agentId;
    //终端分组编号
    private Long agentGroupId;
    //终端分组名称
    @Excel(exportName = "终端分组名称", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String agentGroupName;
    //区域编号
    private Long areaId;
    //区域名称
    @Excel(exportName = "区域名称", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String areaName;
    //货架类型编号
    private Long shelfId;
    //货架类型名称
    @Excel(exportName = "货架名称", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String shelfName;
    //终端名称
    @Excel(exportName = "终端名称", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String agentName;
    //终端code
    @Excel(exportName = "终端编号", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String agentCode;
    //心跳频率
    @Excel(exportName = "心跳频率(min)", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private Integer beatFrequency;
    //开机时间
    @Excel(exportName = "开机时间", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String startTime;
    //关机时间
    @Excel(exportName = "关机时间", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String endTime;
    //音量 1-100
    @Excel(exportName = "音量(1-100)", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private Integer volume;
    //系统  1:linux 2:windows  3:安卓
    @Excel(exportName = "系统", exportFieldWidth = 18, exportConvertSign = 1, importConvertSign = 1)
    private Integer os;
    //截图时间间隔 min
    @Excel(exportName = "截图频率(min)", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private Integer screenshotFrequency;
    //操作密码
    @Excel(exportName = "操作密码", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String operatePassword;
    // 屏幕类型（1单横屏 2单竖屏 3双横屏 4双竖屏 5横竖屏）
    @Excel(exportName = "屏幕类型", exportFieldWidth = 18, exportConvertSign = 1, importConvertSign = 1)
    private Integer screenMode;

    //创建时间
    @Excel(exportName = "创建时间", exportFieldWidth = 18, exportConvertSign = 1, importConvertSign = 1)
    private Date createTime;

    //创建用户
    private Long createUser;

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getAgentGroupId() {
        return agentGroupId;
    }

    public void setAgentGroupId(Long agentGroupId) {
        this.agentGroupId = agentGroupId;
    }

    public String getAgentGroupName() {
        return agentGroupName;
    }

    public void setAgentGroupName(String agentGroupName) {
        this.agentGroupName = agentGroupName;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Long getShelfId() {
        return shelfId;
    }

    public void setShelfId(Long shelfId) {
        this.shelfId = shelfId;
    }

    public String getShelfName() {
        return shelfName;
    }

    public void setShelfName(String shelfName) {
        this.shelfName = shelfName;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Integer getBeatFrequency() {
        return beatFrequency;
    }

    public void setBeatFrequency(Integer beatFrequency) {
        this.beatFrequency = beatFrequency;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Integer getOs() {
        return os;
    }

    public void setOs(Integer os) {
        this.os = os;
    }

    public Integer getScreenshotFrequency() {
        return screenshotFrequency;
    }

    public void setScreenshotFrequency(Integer screenshotFrequency) {
        this.screenshotFrequency = screenshotFrequency;
    }

    public String getOperatePassword() {
        return operatePassword;
    }

    public void setOperatePassword(String operatePassword) {
        this.operatePassword = operatePassword;
    }

    public Integer getScreenMode() {
        return screenMode;
    }

    public void setScreenMode(Integer screenMode) {
        this.screenMode = screenMode;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    /****************需要转换的字段****************/
    //系统  1:linux 2:windows  3:安卓
    public String getOsConvert(){
        if(this.os == null)
            return "android";
        if(this.os == 1)
            return "linux";
        else if(this.os == 2)
            return "windows";
        else if(this.os == 3)
            return "android";
        else return "android";
    }

    public void setOsConvert(String os){
        if("linux".equals(os))
            this.os = 1;
        else if("windows".equals(os))
            this.os = 2;
        else if("android".equals(os))
            this.os = 3;
        else this.os = 3;
    }

    // 屏幕类型（1单横屏 2单竖屏 3双横屏 4双竖屏 5横竖屏）
    public String getScreenModeConvert() {
        if(this.screenMode == null)
            return "横竖屏";
        if(this.screenMode == 1)
            return "单横屏";
        else if(this.screenMode == 2)
            return "单竖屏";
        else if(this.screenMode == 3)
            return "双横屏";
        else if(this.screenMode == 4)
            return "双竖屏";
        else if(this.screenMode == 5)
            return "横竖屏";
        else return "横竖屏";
    }

    public void setScreenModeConvert(String screenMode) {
        if("单横屏".equals(screenMode))
            this.screenMode = 1;
        else if("单竖屏".equals(screenMode))
            this.screenMode = 2;
        else if("双横屏".equals(screenMode))
            this.screenMode = 3;
        else if("双竖屏".equals(screenMode))
            this.screenMode = 4;
        else if("横竖屏".equals(screenMode))
            this.screenMode = 5;
        else this.screenMode = 5;
    }

    public String getCreateTimeConvert() {
        return DateUtils.format(this.createTime, DateUtils.SIMPLE_DATE_HOURS_PATTERN);
    }

    public void setCreateTimeConvert(String createTime) {
        this.createTime = DateUtils.getDateByFormat(createTime, DateUtils.SIMPLE_DATE_HOURS_PATTERN);
    }
}
