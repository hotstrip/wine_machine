package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/6/15.
 */
public class ThemeColumn extends BaseBean {
    //主键
    private Long themeColumnId;
    //主题编号
    private Long themeId;
    //目录编号
    private Long directoryId;
    //栏目名称
    private String columnName;
    //栏目code
    private String columnCode;
    //展示顺序
    private Integer displayIndex;
    //栏目类型  动态   静态
    private Integer columnType;

    //显示字段
    private String fold;

    public Long getThemeColumnId() {
        return themeColumnId;
    }

    public void setThemeColumnId(Long themeColumnId) {
        this.themeColumnId = themeColumnId;
    }

    public Long getThemeId() {
        return themeId;
    }

    public void setThemeId(Long themeId) {
        this.themeId = themeId;
    }

    public Long getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(Long directoryId) {
        this.directoryId = directoryId;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnCode() {
        return columnCode;
    }

    public void setColumnCode(String columnCode) {
        this.columnCode = columnCode;
    }

    public Integer getDisplayIndex() {
        return displayIndex;
    }

    public void setDisplayIndex(Integer displayIndex) {
        this.displayIndex = displayIndex;
    }

    public Integer getColumnType() {
        return columnType;
    }

    public void setColumnType(Integer columnType) {
        this.columnType = columnType;
    }

    public String getFold() {
        return fold;
    }

    public void setFold(String fold) {
        this.fold = fold;
    }
}