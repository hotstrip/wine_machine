package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/9/9.
 */
public class CompanyPay extends BaseBean {
    // 主键
    private Long id;
    // 企业信息编号
    private Long companyId;
    // 微信支付appid
    private String wechatAppId;
    // 微信商户号
    private String wechatMchId;
    // 微信商户号对应的key
    private String wechatMchKey;
    // 微信支付回调接口地址
    private String wechatNotifyUrl;
    // 微信退款证书路径（相对路径）  相对于根路径
    private String wechatCertPath;
    // 是否开启微信支付（0关闭 1开启）
    private Integer openWechatPay;
    // 支付宝appid
    private String alipayAppId;
    // 支付宝应用公钥
    private String alipayPublicKey;
    // 支付宝应用私钥
    private String alipayPrivateKey;
    // 支付宝支付回调接口地址
    private String alipayNotifyUrl;
    // 签名加密方式（RSA | RSA2）
    private String alipaySignType;
    // 是否开启支付宝支付（0关闭 1开启）
    private Integer openAlipay;
    //积分扫码支付接口
    private String scanCodeUrl;
    //积分退款接口
    private String codeRefundUrl;
    //是否开启积分支付（0关闭 1开启）
    private Integer openCodePay;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getWechatAppId() {
        return wechatAppId;
    }

    public void setWechatAppId(String wechatAppId) {
        this.wechatAppId = wechatAppId;
    }

    public String getWechatMchId() {
        return wechatMchId;
    }

    public void setWechatMchId(String wechatMchId) {
        this.wechatMchId = wechatMchId;
    }

    public String getWechatMchKey() {
        return wechatMchKey;
    }

    public void setWechatMchKey(String wechatMchKey) {
        this.wechatMchKey = wechatMchKey;
    }

    public String getWechatNotifyUrl() {
        return wechatNotifyUrl;
    }

    public void setWechatNotifyUrl(String wechatNotifyUrl) {
        this.wechatNotifyUrl = wechatNotifyUrl;
    }

    public String getWechatCertPath() {
        return wechatCertPath;
    }

    public void setWechatCertPath(String wechatCertPath) {
        this.wechatCertPath = wechatCertPath;
    }

    public Integer getOpenWechatPay() {
        return openWechatPay;
    }

    public void setOpenWechatPay(Integer openWechatPay) {
        this.openWechatPay = openWechatPay;
    }

    public String getAlipayAppId() {
        return alipayAppId;
    }

    public void setAlipayAppId(String alipayAppId) {
        this.alipayAppId = alipayAppId;
    }

    public String getAlipayPublicKey() {
        return alipayPublicKey;
    }

    public void setAlipayPublicKey(String alipayPublicKey) {
        this.alipayPublicKey = alipayPublicKey;
    }

    public String getAlipayPrivateKey() {
        return alipayPrivateKey;
    }

    public void setAlipayPrivateKey(String alipayPrivateKey) {
        this.alipayPrivateKey = alipayPrivateKey;
    }

    public String getAlipayNotifyUrl() {
        return alipayNotifyUrl;
    }

    public void setAlipayNotifyUrl(String alipayNotifyUrl) {
        this.alipayNotifyUrl = alipayNotifyUrl;
    }

    public String getAlipaySignType() {
        return alipaySignType;
    }

    public void setAlipaySignType(String alipaySignType) {
        this.alipaySignType = alipaySignType;
    }

    public Integer getOpenAlipay() {
        return openAlipay;
    }

    public void setOpenAlipay(Integer openAlipay) {
        this.openAlipay = openAlipay;
    }

    public String getScanCodeUrl() {
        return scanCodeUrl;
    }

    public void setScanCodeUrl(String scanCodeUrl) {
        this.scanCodeUrl = scanCodeUrl;
    }

    public String getCodeRefundUrl() {
        return codeRefundUrl;
    }

    public void setCodeRefundUrl(String codeRefundUrl) {
        this.codeRefundUrl = codeRefundUrl;
    }

    public Integer getOpenCodePay() {
        return openCodePay;
    }

    public void setOpenCodePay(Integer openCodePay) {
        this.openCodePay = openCodePay;
    }
}
