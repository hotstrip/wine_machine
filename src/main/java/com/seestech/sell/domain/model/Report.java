package com.seestech.sell.domain.model;

import com.seestech.sell.common.utils.DateUtils;

/**
 * Created by idiot on 2017/8/21.
 */
public class Report extends BaseBean {
    //主键
    private Long id;
    //终端编号
    private Long agentId;
    //类型
    private String type;
    //内容
    private String content;

    //显示字段
    private String agentName;
    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    /****************************************************/
    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Report{" +
                "id=" + id +
                ", agentId=" + agentId +
                ", type='" + type + '\'' +
                ", content='" + content + '\'' +
                ", agentName='" + agentName + '\'' +
                ", userId=" + userId +
                ",createTime=" + DateUtils.formatDatetime(super.getCreateTime(), DateUtils.SIMPLE_DATE_PATTERN) +
                ", updateTime=" + DateUtils.formatDatetime(super.getUpdateTime(), DateUtils.SIMPLE_DATE_PATTERN) +
                ", createUser=" + super.getCreateUser() +
                ", updateUser=" + super.getUpdateUser() +
                ", status=" + super.getStatus() +
                ", remark='" + super.getRemark() + '\'' +
                '}'+"\r\n";
    }
}
