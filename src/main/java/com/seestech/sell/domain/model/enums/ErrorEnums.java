package com.seestech.sell.domain.model.enums;

/**
 * Created by idiot on 2017/5/27.
 * @description  异常枚举
 */
public enum ErrorEnums {
    zero("出货异常", "1000"),
    one("取货异常", "1001"),
    two("温度异常", "1002"),
    three("库存核对异常", "1003"),
    four("串口通信异常", "1004"),
    five("网络通讯异常", "1005"),
    six("支付异常", "1006"),
    seven("设备异常", "1007"),
    ;

    private String name;
    private String value;

    ErrorEnums(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    //根据 value 获取name
    public static String getNameByValue(String value){
        for (ErrorEnums item : ErrorEnums.values()){
            if(item.getValue().equals(value)){
                return item.getName();
            }
        }
        return null;
    }
}
