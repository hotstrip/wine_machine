package com.seestech.sell.domain.model;

import java.util.*;

/**
 * Created by idiot on 2017/6/15.
 */
public class PublishHistory extends BaseBean {
    //主键
    private Long publishHistoryId;
    //对应主题、栏目等信息编号
    private Long publishId;
    //终端编号
    private Long agentId;
    //用户编号
    private Long userId;
    //企业编号
    private Long companyId;
    //主题栏目编号
    private Long themeColumnId;
    //发布信息时的名字
    private String publishName;
    //发布时间
    private Date publishTime;
    //批次号
    private String batchNumber;
    //发布模式
    private Integer publishMode;
    //发布率
    private Integer publishRate;
    //发布状态
    private Integer publishStatus;
    //更新状态(0未更新1已更新2失败)
    private Integer updateStatus;
    //发布信息的类型 1:主题 2:栏目
    private Integer type;
    //发布完成时间
    private Date finishTime;
    // 文件路径(暂时用于安卓端的zip文件路径)
    private String path;
    //对应zip下发的类型
    private Integer kind;
    //优先
    private Integer priority;

    //显示字段
    private String agentCode;
    private String fileName;
    private String agentName;
    private String columnName;
    private String themeName;
    private String userName;

    //查询字段
    private Set<Long> userIds;

    public Long getPublishHistoryId() {
        return publishHistoryId;
    }

    public void setPublishHistoryId(Long publishHistoryId) {
        this.publishHistoryId = publishHistoryId;
    }

    public Long getPublishId() {
        return publishId;
    }

    public void setPublishId(Long publishId) {
        this.publishId = publishId;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getThemeColumnId() {
        return themeColumnId;
    }

    public void setThemeColumnId(Long themeColumnId) {
        this.themeColumnId = themeColumnId;
    }

    public String getPublishName() {
        return publishName;
    }

    public void setPublishName(String publishName) {
        this.publishName = publishName;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public Integer getPublishMode() {
        return publishMode;
    }

    public void setPublishMode(Integer publishMode) {
        this.publishMode = publishMode;
    }

    public Integer getPublishRate() {
        return publishRate;
    }

    public void setPublishRate(Integer publishRate) {
        this.publishRate = publishRate;
    }

    public Integer getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(Integer publishStatus) {
        this.publishStatus = publishStatus;
    }

    public Integer getUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(Integer updateStatus) {
        this.updateStatus = updateStatus;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getKind() {
        return kind;
    }

    public void setKind(Integer kind) {
        this.kind = kind;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /*******************************/
    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    /*********************************/
    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }
}