package com.seestech.sell.domain.model;


/**
 * Created by idiot on 2017/8/7.
 */
public class Company extends BaseBean {
    //主键
    private Long id;
    //企业  组织机构名称
    private String name;
    //描述信息
    private String description;
    //图标
    private String logo;
    //管理员  默认为创建的用户  修改仅限已经关联该id的用户 (以及创建者)
    private Long manager;
    // 是否开启审核0关闭 1开启
    private Integer isReview;

    /************  显示字段  *************/
    private String userName;        //用户名称

    //是否用or查询
    private boolean is_or;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Long getManager() {
        return manager;
    }

    public void setManager(Long manager) {
        this.manager = manager;
    }

    public Integer getIsReview() {
        return isReview;
    }

    public void setIsReview(Integer isReview) {
        this.isReview = isReview;
    }

    /********************************/
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean is_or() {
        return is_or;
    }

    public void setIs_or(boolean is_or) {
        this.is_or = is_or;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == this)
            return true;
        if(obj instanceof Company){
            Company info = (Company) obj;
            return id.equals(info.getId());
        }
        return false;
    }
}
