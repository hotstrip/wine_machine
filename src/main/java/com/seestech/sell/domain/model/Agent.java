package com.seestech.sell.domain.model;

import java.util.*;

/**
 * Created by idiot on 2017/6/15.
 */
public class Agent extends BaseBean {
    //终端编号
    private Long agentId;
    //终端分组编号
    private Long agentGroupId;
    //区域编号
    private Long areaId;
    //货架类型编号
    private Long shelfId;
    //备用货架编号（用于切换货架）
    private Long secondaryShelfId;
    //使用中的主题编号
    private Long themeId;
    //终端名称
    private String agentName;
    //终端code
    private String agentCode;
    //终端ip
    private String agentIp;
    //外网IP
    private String netIp;
    //心跳频率
    private Integer beatFrequency;
    //上次心跳时间
    private Date lastHeartBeatTime;
    //开机时间
    private String startTime;
    //关机时间
    private String endTime;
    //终端软件版本
    private String csNo;
    //终端硬件版本
    private String hwVer;
    //音量 1-100
    private Integer volume;
    //离线天数
    private Integer onlineStatus;
    //系统  1:linux 2:windows  3:安卓
    private Integer os;
    //磁盘总空间 mb
    private Long diskCapacity;
    //磁盘剩余量
    private Long diskRemaining;
    //截图时间间隔 min
    private Integer screenshotFrequency;
    //点位
    private String point;
    //负责人
    private String manager;
    //联系电话
    private String phone;
    //库存
    private String remain;
    //温度
    private String temperature;
    //报警数量
    private Integer alarm;
    //经纬度地址
    private String location;
    //库存不足  0否 1是
    private Integer less;
    //异常  0 否  1是
    private Integer error;
    //唯一编码
    private String uniqueCode;
    //工控板版本号
    private String firmwareVersion;
    //操作密码
    private String operatePassword;
    // 屏幕类型（1单横屏 2单竖屏 3双横屏 4双竖屏 5横竖屏 6售货机定制屏）
    private Integer screenMode;
    //许可证
    private String licenseCode;
    //标签信息
    private String tags;
    //初始化图片
    private String initPicture;
    //可运行总内存MB
    private Long ram;
    //剩余内存MB
    private Long leftRam;
    //标识是否当做测试机(0:否,1:是)
    private Integer isTest;

    //显示字段
    private Long userId;
    private String agentGroupName;
    private String imageName;
    private String imageUrl;
    private String areaName;    //区域名称
    private String shelfName;   //货架名
    private String themeName;   //正在使用的主题

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getAgentGroupId() {
        return agentGroupId;
    }

    public void setAgentGroupId(Long agentGroupId) {
        this.agentGroupId = agentGroupId;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public Long getShelfId() {
        return shelfId;
    }

    public void setShelfId(Long shelfId) {
        this.shelfId = shelfId;
    }

    public Long getSecondaryShelfId() {
        return secondaryShelfId;
    }

    public void setSecondaryShelfId(Long secondaryShelfId) {
        this.secondaryShelfId = secondaryShelfId;
    }

    public Long getThemeId() {
        return themeId;
    }

    public void setThemeId(Long themeId) {
        this.themeId = themeId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentIp() {
        return agentIp;
    }

    public void setAgentIp(String agentIp) {
        this.agentIp = agentIp;
    }

    public String getNetIp() {
        return netIp;
    }

    public void setNetIp(String netIp) {
        this.netIp = netIp;
    }

    public Integer getBeatFrequency() {
        return beatFrequency;
    }

    public void setBeatFrequency(Integer beatFrequency) {
        this.beatFrequency = beatFrequency;
    }

    public Date getLastHeartBeatTime() {
        return lastHeartBeatTime;
    }

    public void setLastHeartBeatTime(Date lastHeartBeatTime) {
        this.lastHeartBeatTime = lastHeartBeatTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCsNo() {
        return csNo;
    }

    public void setCsNo(String csNo) {
        this.csNo = csNo;
    }

    public String getHwVer() {
        return hwVer;
    }

    public void setHwVer(String hwVer) {
        this.hwVer = hwVer;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Integer getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(Integer onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public Integer getOs() {
        return os;
    }

    public void setOs(Integer os) {
        this.os = os;
    }

    public Long getDiskCapacity() {
        return diskCapacity;
    }

    public void setDiskCapacity(Long diskCapacity) {
        this.diskCapacity = diskCapacity;
    }

    public Long getDiskRemaining() {
        return diskRemaining;
    }

    public void setDiskRemaining(Long diskRemaining) {
        this.diskRemaining = diskRemaining;
    }

    public Integer getScreenshotFrequency() {
        return screenshotFrequency;
    }

    public void setScreenshotFrequency(Integer screenshotFrequency) {
        this.screenshotFrequency = screenshotFrequency;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRemain() {
        return remain;
    }

    public void setRemain(String remain) {
        this.remain = remain;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public Integer getAlarm() {
        return alarm;
    }

    public void setAlarm(Integer alarm) {
        this.alarm = alarm;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getLess() {
        return less;
    }

    public void setLess(Integer less) {
        this.less = less;
    }

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public String getOperatePassword() {
        return operatePassword;
    }

    public void setOperatePassword(String operatePassword) {
        this.operatePassword = operatePassword;
    }

    public Integer getScreenMode() {
        return screenMode;
    }

    public void setScreenMode(Integer screenMode) {
        this.screenMode = screenMode;
    }

    public String getLicenseCode() {
        return licenseCode;
    }

    public void setLicenseCode(String licenseCode) {
        this.licenseCode = licenseCode;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getInitPicture() {
        return initPicture;
    }

    public void setInitPicture(String initPicture) {
        this.initPicture = initPicture;
    }

    public Long getRam() {
        return ram;
    }

    public void setRam(Long ram) {
        this.ram = ram;
    }

    public Long getLeftRam() {
        return leftRam;
    }

    public void setLeftRam(Long leftRam) {
        this.leftRam = leftRam;
    }

    public Integer getIsTest() {
        return isTest;
    }

    public void setIsTest(Integer isTest) {
        this.isTest = isTest;
    }

    /*********************************/
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getAgentGroupName() {
        return agentGroupName;
    }

    public void setAgentGroupName(String agentGroupName) {
        this.agentGroupName = agentGroupName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getShelfName() {
        return shelfName;
    }

    public void setShelfName(String shelfName) {
        this.shelfName = shelfName;
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }
}