package com.seestech.sell.domain.model.enums;

/**
 * Created by idiot on 2016/12/17.
 */

/**
 * @description 状态枚举类
 */
public enum IsRootEnums {
    YES("yes",1), //是
    NO("no",0),//否
    ;

    private String name;
    private Integer value;

    IsRootEnums(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }
}
