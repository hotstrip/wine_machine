package com.seestech.sell.domain.model.enums;

/**
 * Created by idiot on 2017/5/27.
 */
public enum CodeEnums {
    zero("无任务", 0),
    one("下发任务", 1),
    two("立即关机", 2),
    three("立即重启", 3),
    four("立即截图", 4),
    five("终端属性更新", 5),
    six("清理终端", 6),
    seven("刷新内容", 7),
    ;

    private String name;
    private Integer value;

    CodeEnums(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }

    /******************************************************/

    public static boolean checkValue(int value){
        boolean flag = false;
        for (CodeEnums code : CodeEnums.values()){
            if(code.getValue().equals(value)){
                flag = true;
                break;
            }
        }
        return flag;
    }

    public static String getNameByValue(int value){
        String res = new String();
        for (CodeEnums code : CodeEnums.values()){
            if(code.getValue().equals(value)){
                res = code.getName();
                break;
            }
        }
        return res;
    }
}
