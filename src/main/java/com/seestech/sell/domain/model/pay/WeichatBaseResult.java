package com.seestech.sell.domain.model.pay;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.io.Serializable;

/**
 * Created by idiot on 2017/6/26.
 */
@XStreamAlias("xml")
public class WeichatBaseResult implements Serializable {
    private static final long serialVersionUID = 6909725341458045453L;
    //返回码
    @XStreamAlias("return_code")
    private String return_code;
    //返回消息
    @XStreamAlias("return_msg")
    private String return_msg;

    public String getReturn_code() {
        return return_code;
    }

    public void setReturn_code(String return_code) {
        this.return_code = return_code;
    }

    public String getReturn_msg() {
        return return_msg;
    }

    public void setReturn_msg(String return_msg) {
        this.return_msg = return_msg;
    }


}
