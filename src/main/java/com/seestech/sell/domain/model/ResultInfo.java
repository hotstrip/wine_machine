package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/1/3.
 * @description 返回状态信息
 */
public class ResultInfo {
    private Integer status;
    private String statusText;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    @Override
    public String toString() {
        return "ResultInfo{" +
                "status=" + status +
                ", statusText='" + statusText + '\'' +
                '}';
    }
}
