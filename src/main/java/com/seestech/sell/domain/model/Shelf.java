package com.seestech.sell.domain.model;

import java.util.Set;

/**
 * Created by idiot on 2017/6/15.
 */
public class Shelf extends BaseBean {
    //货架类型编号
    private Long shelfId;
    //货架名称
    private String name;
    //拥有货道数量
    private Integer amount;
    //布局
    private String layout;
    //可存放商品总数量
    private Integer total;
    //产品
    private String products;
    //商品的总库存
    private String stocks;
    //商品报警数量
    private String alarms;

    //显示字段
    private String userName;
    private Set<Long> userIds;

    public Long getShelfId() {
        return shelfId;
    }

    public void setShelfId(Long shelfId) {
        this.shelfId = shelfId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public String getStocks() {
        return stocks;
    }

    public void setStocks(String stocks) {
        this.stocks = stocks;
    }

    public String getAlarms() {
        return alarms;
    }

    public void setAlarms(String alarms) {
        this.alarms = alarms;
    }

    /***************************/
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }
}