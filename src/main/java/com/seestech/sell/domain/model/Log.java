package com.seestech.sell.domain.model;

import com.seestech.sell.common.utils.DateUtils;

import java.util.*;

/**
 * Created by idiot on 2017/6/15.
 */
public class Log extends BaseBean {
    //日志编号
    private Long logId;
    //用户编号
    private Long userId;
    //日志内容
    private String logMessage;
    //操作时间
    private Date operateTime;
    //请求路径
    private String requestUrl;
    //响应的状态码
    private String responseCode;

    //显示字段
    private String userName;

    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLogMessage() {
        return logMessage;
    }

    public void setLogMessage(String logMessage) {
        this.logMessage = logMessage;
    }

    public Date getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**********/
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "Log{" +
                "logId=" + logId +
                ", userId=" + userId +
                ", logMessage='" + logMessage + '\'' +
                ", operateTime=" + operateTime +
                ", requestUrl='" + requestUrl + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", userName='" + userName + '\'' +
                ", createTime=" + DateUtils.formatDatetime(super.getCreateTime(), DateUtils.SIMPLE_DATE_PATTERN) +
                ", updateTime=" + DateUtils.formatDatetime(super.getUpdateTime(), DateUtils.SIMPLE_DATE_PATTERN) +
                ", createUser=" + super.getCreateUser() +
                ", updateUser=" + super.getUpdateUser() +
                ", status=" + super.getStatus() +
                ", remark='" + super.getRemark() + '\'' +
                '}'+"\r\n";
    }
}