package com.seestech.sell.domain.model.vo;

import com.seestech.sell.common.annotation.Excel;
import com.seestech.sell.common.utils.DateUtils;

import java.util.Date;

/**
 * Created by idiot on 2017/9/18.
 */
public class ErrorVoExcel {
    //主键
    private Long id;
    //终端编号
    private Long agentId;
    //异常码
    @Excel(exportName = "异常码", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String code;
    //异常描述
    @Excel(exportName = "异常描述", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String message;
    //是否排除 0否 1是
    @Excel(exportName = "是否排除", exportFieldWidth = 18, exportConvertSign = 1, importConvertSign = 1)
    private Integer solve;

    /*******************/

    //终端名称
    @Excel(exportName = "终端名称", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String agentName;
    //终端编号
    @Excel(exportName = "终端编号", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String agentCode;

    //创建时间
    @Excel(exportName = "创建时间", exportFieldWidth = 18, exportConvertSign = 1, importConvertSign = 1)
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getSolve() {
        return solve;
    }

    public void setSolve(Integer solve) {
        this.solve = solve;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getSolveConvert() {
        if (this.solve == 1)
            return "是";
        else return "否";
    }

    public void setSolveConvert(String solve) {
        if ("是".equals(solve))
            this.solve = 1;
        else this.solve = 0;
    }

    public String getCreateTimeConvert() {
        return DateUtils.format(this.createTime, DateUtils.SIMPLE_DATE_HOURS_PATTERN);
    }

    public void setCreateTimeConvert(String createTime) {
        this.createTime = DateUtils.getDateByFormat(createTime, DateUtils.SIMPLE_DATE_HOURS_PATTERN);
    }

}
