package com.seestech.sell.domain.model;


import java.util.List;

/**
 * Created by idiot on 2016/12/20.
 */
public class TreeInfo {
    private String id;                  //id
    private String pid;                 //父级id
    private String name;                //名称
    private boolean open;               //是否打开节点
    private boolean checked;            //是否选中
    private boolean chkDisabled;        //是否禁用
    //private boolean isParent;           //是否是父级节点
    private boolean directory;          //是否是目录节点
    private boolean role;               //是否是角色节点
    private boolean operate;            //标识是否操作该节点
    private boolean drag;               //是否允许拖
    private boolean drop;               //是否允许放
    private List<TreeInfo> children;    //子集集合
    private String code;                //终端树时存储agentCode

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isChkDisabled() {
        return chkDisabled;
    }

    public void setChkDisabled(boolean chkDisabled) {
        this.chkDisabled = chkDisabled;
    }

    public boolean isDirectory() {
        return directory;
    }

    public void setDirectory(boolean directory) {
        this.directory = directory;
    }

    public boolean isRole() {
        return role;
    }

    public void setRole(boolean role) {
        this.role = role;
    }

    public boolean isOperate() {
        return operate;
    }

    public void setOperate(boolean operate) {
        this.operate = operate;
    }

    public boolean isDrag() {
        return drag;
    }

    public void setDrag(boolean drag) {
        this.drag = drag;
    }

    public boolean isDrop() {
        return drop;
    }

    public void setDrop(boolean drop) {
        this.drop = drop;
    }

    public List<TreeInfo> getChildren() {
        return children;
    }

    public void setChildren(List<TreeInfo> children) {
        this.children = children;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @description init初始化
     * @param id
     * @param pid
     * @param name
     * @return
     */
    public static TreeInfo initTree(String id, String pid, String name){
        TreeInfo treeInfo = new TreeInfo();
        treeInfo.setId(id);
        treeInfo.setPid(pid);
        treeInfo.setName(name);
        return treeInfo;
    }
}
