package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/6/15.
 */
public class RoleMenu extends BaseBean {
    //角色菜单编号
    private Long roleMenuId;
    //角色编号
    private Long roleId;
    //菜单编号
    private Long menuId;

    public Long getRoleMenuId() {
        return roleMenuId;
    }

    public void setRoleMenuId(Long roleMenuId) {
        this.roleMenuId = roleMenuId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }
}