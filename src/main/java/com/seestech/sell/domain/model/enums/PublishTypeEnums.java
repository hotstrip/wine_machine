package com.seestech.sell.domain.model.enums;

/**
 * Created by idiot on 2016/12/17.
 */

/**
 * @description 发布信息类型枚举类
 */
public enum PublishTypeEnums {
    THEME("主题", 1),                  //主题
    COLUMN("栏目", 2),                 //栏目
    VIDEO("视频", 3),                  //视频
    ZIP("zip", 4),                    //zip
    LED("字幕", 5),                    //字幕
    ;

    private String name;
    private Integer value;

    PublishTypeEnums(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }

}
