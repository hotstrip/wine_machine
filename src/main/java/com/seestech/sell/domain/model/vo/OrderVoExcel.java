package com.seestech.sell.domain.model.vo;

import com.seestech.sell.common.annotation.Excel;
import com.seestech.sell.common.utils.DateUtils;
import com.seestech.sell.domain.model.BaseBean;

import java.util.Date;

/**
 * Created by idiot on 2017/9/7.
 */
public class OrderVoExcel {
    // 订单编号
    private Long orderId;
    // 终端编号
    private Long agentId;
    // 商品编号
    private Long goodsId;

    // 终端名称
    @Excel(exportName = "终端名称", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String agentName;
    // 终端code
    @Excel(exportName = "终端编号", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String agentCode;
    // 商品名称
    @Excel(exportName = "商品名称", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String goodsName;
    // 商品code
    @Excel(exportName = "商品编号", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private String goodsCode;
    // 商品价格
    @Excel(exportName = "商品价格", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private Double goodsPrice;
    // 交易价格
    @Excel(exportName = "交易价格", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private Double price;
    // 交易数量
    @Excel(exportName = "交易数量", exportFieldWidth = 18, exportConvertSign = 0, importConvertSign = 0)
    private Integer amount;
    // 订单状态（0未成功，1成功 -1失败 2退款）
    @Excel(exportName = "订单状态", exportFieldWidth = 18, exportConvertSign = 1, importConvertSign = 1)
    private Integer success;
    // 支付渠道（1微信 2支付宝）
    @Excel(exportName = "支付渠道", exportFieldWidth = 18, exportConvertSign = 1, importConvertSign = 1)
    private Integer channel;
    // 售货状态（0处理中1成功-1失败）
    @Excel(exportName = "出货状态", exportFieldWidth = 18, exportConvertSign = 1, importConvertSign = 1)
    private Integer saleStatus;

    //创建时间
    @Excel(exportName = "创建时间", exportFieldWidth = 18, exportConvertSign = 1, importConvertSign = 1)
    private Date createTime;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public Double getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(Double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public Integer getSaleStatus() {
        return saleStatus;
    }

    public void setSaleStatus(Integer saleStatus) {
        this.saleStatus = saleStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /************************ 需要转换 ************/

    // 订单状态（0未成功，1成功 -1失败 2退款）
    public String getSuccessConvert() {
        if (this.success == 0)
            return "未支付";
        else if(this.success == 1)
            return "支付成功";
        else if(this.success == -1)
            return "支付失败";
        else if(this.success == 2)
            return "发起退款";
        else return "无效的状态";
    }

    public void setSuccessConvert(String success) {
        if ("未支付".equals(success))
            this.success = 0;
        else if ("支付成功".equals(success))
            this.success = 1;
        else if ("支付失败".equals(success))
            this.success = -1;
        else if ("发起退款".equals(success))
            this.success = 2;
        //  else  无效的状态
    }

    // 支付渠道（1微信 2支付宝）
    public String getChannelConvert() {
        if (this.channel == 1)
            return "微信";
        else if (this.channel == 2)
            return "支付宝";
        else return "其他";
    }

    public void setChannelConvert(String channel) {
        if ("微信".equals(channel))
            this.channel = 1;
        else if ("支付宝".equals(channel))
            this.channel = 2;
        // else  其他支付方式
    }

    // 售货状态（0处理中1成功-1失败）
    public String getSaleStatusConvert() {
        if (this.saleStatus == 0)
            return "处理中";
        else if (this.saleStatus == 1)
            return "出货成功";
        else if (this.saleStatus == -1)
            return "出货失败";
        else return "无效的状态";
    }

    public void setSaleStatusConvert(String saleStatus) {
        if ("处理中".equals(saleStatus))
            this.saleStatus = 0;
        else if ("出货成功".equals(saleStatus))
            this.saleStatus = 1;
        else if ("出货失败".equals(saleStatus))
            this.saleStatus = -1;
        // else  无效的状态
    }

    public String getCreateTimeConvert() {
        return DateUtils.format(this.createTime, DateUtils.SIMPLE_DATE_HOURS_PATTERN);
    }

    public void setCreateTimeConvert(String createTime) {
        this.createTime = DateUtils.getDateByFormat(createTime, DateUtils.SIMPLE_DATE_HOURS_PATTERN);
    }
}
