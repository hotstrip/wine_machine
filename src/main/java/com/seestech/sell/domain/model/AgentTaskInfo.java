package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/5/27.
 * @description 安卓端任务信息表
 */
public class AgentTaskInfo extends BaseBean {
    // 任务编号
    private Long taskId;
    // 终端code
    private String agentCode;
    // 执行码
    private Integer code;
    // 发布信息编号
    private Long publishId;
    // 发布信息的类型1主题2栏目
    private Integer type;

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Long getPublishId() {
        return publishId;
    }

    public void setPublishId(Long publishId) {
        this.publishId = publishId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
