package com.seestech.sell.domain.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by idiot on 2017/6/15.
 */
public class BaseBean implements Serializable {
    //创建时间
    private Date createTime;
    //修改时间
    private Date updateTime;
    //创建用户
    private Long createUser;
    //修改用户
    private Long updateUser;
    //状态   表示是否有效  0无效  1有效
    private Integer status;
    //备注信息
    private String remark;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}