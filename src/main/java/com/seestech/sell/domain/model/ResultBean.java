package com.seestech.sell.domain.model;

import java.util.Map;

/**
 * Created by idiot on 2017/1/3.
 * @description 返回的信息实体
 */
public class ResultBean<T> {
    //数据量上限
    private Integer limit = 50;
    //数据起始位置
    private Integer start = 0;
    //响应状态
    private boolean success = false;
    //实际总数
    private Integer totalCount = 0;
    //响应信息和状态
    private ResultInfo resultInfo;
    //返回数据信息
    private T data;
    //额外的信息
    private Map<String, Object> map;

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public ResultInfo getResultInfo() {
        return resultInfo;
    }

    public void setResultInfo(ResultInfo resultInfo) {
        this.resultInfo = resultInfo;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    @Override
    public String toString() {
        return "ResultBean{" +
                "limit=" + limit +
                ", start=" + start +
                ", success=" + success +
                ", totalCount=" + totalCount +
                ", resultInfo=" + resultInfo +
                ", data=" + data +
                ", map=" + map +
                '}';
    }
}
