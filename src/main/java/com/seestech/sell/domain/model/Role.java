package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/6/15.
 */
public class Role extends BaseBean {
    //角色编号
    private Long roleId;
    //角色名称
    private String roleName;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}