package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/6/15.
 */
public class UserAgentGroup extends BaseBean {
    //用户终端关系编号
    private Long userAgentGroupId;
    //终端分组编号
    private Long agentGroupId;
    //用户编号
    private Long userId;

    public Long getUserAgentGroupId() {
        return userAgentGroupId;
    }

    public void setUserAgentGroupId(Long userAgentGroupId) {
        this.userAgentGroupId = userAgentGroupId;
    }

    public Long getAgentGroupId() {
        return agentGroupId;
    }

    public void setAgentGroupId(Long agentGroupId) {
        this.agentGroupId = agentGroupId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}