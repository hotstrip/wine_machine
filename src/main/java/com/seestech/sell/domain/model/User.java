package com.seestech.sell.domain.model;


import java.util.Set;

/**
 * Created by idiot on 2017/6/15.
 */
public class User extends BaseBean {
    //用户编号
    private Long userId;
    //用户名称
    private String userName;
    //头像
    private String avatar;
    //用户密码
    private String userPassword;
    //微信所用的openid
    private String openid;
    //微博的AccessToken
    private String weiboid;
    //企业编号
    private Long companyId;
    //邮箱
    private String email;
    //用户目录
    private String userDirectory;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getWeiboid() {
        return weiboid;
    }

    public void setWeiboid(String weiboid) {
        this.weiboid = weiboid;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserDirectory() {
        return userDirectory;
    }

    public void setUserDirectory(String userDirectory) {
        this.userDirectory = userDirectory;
    }

    /******************  查询字段  *********************/


    @Override
    public boolean equals(Object obj) {
        if(obj == this)
            return true;
        if(obj instanceof User) {
            User info = (User) obj;
            return userId.equals(info.getUserId());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return userId.hashCode();
    }
}