package com.seestech.sell.domain.model;

import java.io.Serializable;

/**
 * @descritpion  积分信息对接信息表   用于第三方接口
 * Created by idiot on 2017/7/24.
 */
public class Score implements Serializable {
    private static final long serialVersionUID = 4633407161547418923L;
    /**transaction_type(交易类型：defray.wine),
     point(消费积分),
     card_id(卡号),
     region(区域),
     product_number(商品编号),
     machine_number(机器编号),
     product_name(商品名称)
     失败 :{"status":"failure","message":"错误信息"}
     成功 :{"status":"success"}
     http://test-api.shewngroup.com/gateway.cgi?mod=wine.consume&v=1
     http://test-m.shewngroup.com/member_center.html
     13504716599
     */
    private String transaction_type;
    private double point;
    private String card_id;
    private String region;
    private String product_number;
    private String machine_number;
    private String product_name;

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public double getPoint() {
        return point;
    }

    public void setPoint(double point) {
        this.point = point;
    }

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getProduct_number() {
        return product_number;
    }

    public void setProduct_number(String product_number) {
        this.product_number = product_number;
    }

    public String getMachine_number() {
        return machine_number;
    }

    public void setMachine_number(String machine_number) {
        this.machine_number = machine_number;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }
}
