package com.seestech.sell.domain.model.enums;

/**
 * Created by idiot on 2017/5/27.
 */
public enum ReviewStatusEnums {
    zero("未审核", 0),
    one("待审核", 1),
    two("已审核", 2),
    ;

    private String name;
    private Integer value;

    ReviewStatusEnums(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }

    /******************************************************/

    public static boolean checkValue(int value){
        boolean flag = false;
        for (ReviewStatusEnums code : ReviewStatusEnums.values()){
            if(code.getValue().equals(value)){
                flag = true;
                break;
            }
        }
        return flag;
    }

    public static String getNameByValue(int value){
        String res = new String();
        for (ReviewStatusEnums code : ReviewStatusEnums.values()){
            if(code.getValue().equals(value)){
                res = code.getName();
                break;
            }
        }
        return res;
    }
}
