package com.seestech.sell.domain.model.enums;


/**
 * Created by idiot on 2017/7/6.
 */
public enum FileTypeEnums {
    //文本
    txt("txt",1),
    //图片
    jpg("jpg",2),
    jpeg("jpeg",2),
    png("png",2),
    gif("gif",2),
    //视频
    mp4("mp4",3),
    //压缩包
    zip("zip",4),
    ;

    private String name;
    private int value;

    FileTypeEnums(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
}
