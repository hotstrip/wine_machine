package com.seestech.sell.domain.model;

import java.util.Set;

/**
 * Created by idiot on 2017/6/15.
 */
public class AgentGroup extends BaseBean {
    //终端分组编号
    private Long agentGroupId;
    //终端分组名称
    private String agentGroupName;
    // 企业编号
    private Long companyId;

    //显示字段
    private String companyName;
    private String userName;
    private Set<Long> userIds;

    public Long getAgentGroupId() {
        return agentGroupId;
    }

    public void setAgentGroupId(Long agentGroupId) {
        this.agentGroupId = agentGroupId;
    }

    public String getAgentGroupName() {
        return agentGroupName;
    }

    public void setAgentGroupName(String agentGroupName) {
        this.agentGroupName = agentGroupName;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    /***********************/
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }
}