package com.seestech.sell.domain.model.enums;

/**
 * Created by idiot on 2017/5/11.
 */
public enum DirectoryTypeEnums {

    One("公共素材库", 1),
    TWO("个人素材库", 2),
    Three("业务目录", 3),
    four("主题存档目录", 4),
    ;

    private String name;
    private Integer value;



    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }

    DirectoryTypeEnums(String name, Integer value) {
        this.name = name;
        this.value = value;
    }
}
