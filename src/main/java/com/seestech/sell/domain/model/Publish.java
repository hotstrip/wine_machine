package com.seestech.sell.domain.model;

import java.util.Set;

/**
 * Created by idiot on 2017/8/1.
 */
public class Publish extends BaseBean {
    //主键
    private Long publishId;
    //发布项目名称
    private String name;
    //目录编号
    private Long directoryId;
    //文件编号
    private Long fileId;
    //企业编号
    private Long companyId;
    //主要文件名
    private String indexFileName;
    //项目存储路径
    private String path;
    //发布类型
    private Integer type;
    //发布种类  目前只是当type为4（文件）才有
    private Integer kind;
    //内容  目前只有发布项目为字幕才有
    private String txt;
    //审核状态（0未审核，1待审核，2审核通过）
    private Integer reviewStatus;

    //仅仅用来查询字段
    private Set<Long> userIds;

    public Long getPublishId() {
        return publishId;
    }

    public void setPublishId(Long publishId) {
        this.publishId = publishId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(Long directoryId) {
        this.directoryId = directoryId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getIndexFileName() {
        return indexFileName;
    }

    public void setIndexFileName(String indexFileName) {
        this.indexFileName = indexFileName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getKind() {
        return kind;
    }

    public void setKind(Integer kind) {
        this.kind = kind;
    }

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

    public Integer getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(Integer reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

    /***********************************查询字段***************/
    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }
}
