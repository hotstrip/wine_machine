package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/6/15.
 */
public class Screenshots extends BaseBean {
    //主键
    private Long screenshotsId;
    //终端编号
    private Long agentId;
    //图片名称
    private String imageName;

    //显示字段
    private String url;

    public Long getScreenshotsId() {
        return screenshotsId;
    }

    public void setScreenshotsId(Long screenshotsId) {
        this.screenshotsId = screenshotsId;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    /**********************/
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}