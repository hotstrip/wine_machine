package com.seestech.sell.domain.model;

import java.util.Set;

/**
 * Created by idiot on 2017/6/15.
 */
public class Grid extends BaseBean {
    //货道编号
    private Long gridId;
    //商品编号
    private Long goodsId;
    //名称
    private String name;
    //价格
    private Double price;
    //库存
    private Integer total;
    //告警数量
    private Integer alarm;

    //显示字段
    private String goodsName;   //商品名称
    private Double goodsPrice;  //商品价格
    private String image;       //图片
    private String grid_id;
    private Set<Long> userIds;

    public Long getGridId() {
        return gridId;
    }

    public void setGridId(Long gridId) {
        this.gridId = gridId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getAlarm() {
        return alarm;
    }

    public void setAlarm(Integer alarm) {
        this.alarm = alarm;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(Double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getGrid_id() {
        return String.valueOf(this.getGridId());
    }

    public void setGrid_id(String grid_id) {
        this.grid_id = grid_id;
    }

    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }
}