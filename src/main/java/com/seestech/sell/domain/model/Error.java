package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/7/27.
 * @description   终端异常信息
 */
public class Error extends BaseBean {
    //主键
    private Long id;
    //终端编号
    private Long agentId;
    //异常码
    private String code;
    //异常描述
    private String message;
    //是否排除 0否 1是
    private Integer solve;

    //显示字段
    private String agentName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getSolve() {
        return solve;
    }

    public void setSolve(Integer solve) {
        this.solve = solve;
    }

    /****************   显示字段  ***********/
    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }
}
