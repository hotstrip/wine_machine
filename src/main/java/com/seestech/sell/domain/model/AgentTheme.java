package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/6/15.
 */
public class AgentTheme extends BaseBean {
    //主键
    private Long agentThemeId;
    //终端编号
    private Long AgentId;
    //主题编号
    private Long ThemeId;
    //用户编号
    private Long userId;
    //批次号
    private String batchNumber;
    //发布模式1:3g 2:U盘 3:广播
    private Integer publishMode;

    public Long getAgentThemeId() {
        return agentThemeId;
    }

    public void setAgentThemeId(Long agentThemeId) {
        this.agentThemeId = agentThemeId;
    }

    public Long getAgentId() {
        return AgentId;
    }

    public void setAgentId(Long agentId) {
        AgentId = agentId;
    }

    public Long getThemeId() {
        return ThemeId;
    }

    public void setThemeId(Long themeId) {
        ThemeId = themeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public Integer getPublishMode() {
        return publishMode;
    }

    public void setPublishMode(Integer publishMode) {
        this.publishMode = publishMode;
    }
}