package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/7/7.
 * @description  用户积分信息表
 */
public class Customer extends BaseBean {
    //主键
    private Long id;
    //用户支付账号
    private String customerId;
    //电话号码
    private String phone;
    //交易次数
    private int count;
    //支付渠道
    private int channel;
    //历史交易总额
    private double totalFee;
    //积分
    private Long score;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

    public double getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(double totalFee) {
        this.totalFee = totalFee;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }
}
