package com.seestech.sell.domain.model.enums;

/**
 * Created by idiot on 2016/12/17.
 */

/**
 * @description 状态枚举类
 */
public enum StatusEnums {
    VALID("valid",1), //有效
    INVALID("invalid",0),//无效
    ;

    private String name;
    private Integer value;

    StatusEnums(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }
}
