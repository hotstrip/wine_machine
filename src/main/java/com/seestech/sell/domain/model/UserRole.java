package com.seestech.sell.domain.model;

/**
 * Created by idiot on 2017/6/15.
 */
public class UserRole extends BaseBean {
    //用户角色编号
    private Long userRoleId;
    //用户编号
    private Long userId;
    //角色编号
    private Long roleId;

    public Long getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(Long userRoleId) {
        this.userRoleId = userRoleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}