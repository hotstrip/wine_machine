package com.seestech.sell.domain.model;

import java.util.Set;

/**
 * Created by idiot on 2017/8/14.
 * @description  标签信息
 */
public class Tags extends BaseBean {
    //主键
    private Long id;
    //标签名
    private String tagName;
    //类型  默认 1  为之后可能分类做准备
    private Integer kind;

    //显示字段
    private String userName;

    //仅仅用来查询字段
    private Set<Long> userIds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Integer getKind() {
        return kind;
    }

    public void setKind(Integer kind) {
        this.kind = kind;
    }

    /***********************************显示字段***************/
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    /***********************************查询字段***************/
    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }
}
