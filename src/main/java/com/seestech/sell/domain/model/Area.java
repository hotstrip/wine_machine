package com.seestech.sell.domain.model;

import java.util.Set;

/**
 * Created by idiot on 2017/6/15.
 */
public class Area extends BaseBean {
    //区域编号
    private Long areaId;
    //区域名称
    private String  name;
    //区域负责人
    private String manager;
    //联系电话
    private String phone;

    //查询字段
    private Set<Long> userIds;

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**************************查询字段*************/
    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }
}