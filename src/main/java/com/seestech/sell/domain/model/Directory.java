package com.seestech.sell.domain.model;

import java.util.Set;

/**
 * Created by idiot on 2017/6/15.
 */
public class Directory extends BaseBean {
    //目录信息编号
    private Long directoryId;
    //目录名称
    private String directoryName;
    //是否根目录 0否 1是
    private Integer isRoot;
    //目录编码
    private String pathCode;
    //父级目录编号
    private Long parentDirectoryId;
    //类型1:公共素材库2:个人素材库3:业务目录
    private Integer type;

    //显示字段
    private String id;
    private String pid;

    //查询字段
    private Set<Long> userIds;

    public Long getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(Long directoryId) {
        this.directoryId = directoryId;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }

    public Integer getIsRoot() {
        return isRoot;
    }

    public void setIsRoot(Integer isRoot) {
        this.isRoot = isRoot;
    }

    public String getPathCode() {
        return pathCode;
    }

    public void setPathCode(String pathCode) {
        this.pathCode = pathCode;
    }

    public Long getParentDirectoryId() {
        return parentDirectoryId;
    }

    public void setParentDirectoryId(Long parentDirectoryId) {
        this.parentDirectoryId = parentDirectoryId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    /*******************/
    public String getId() {
        return String.valueOf(this.directoryId);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return String.valueOf(this.parentDirectoryId);
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    /****************************/
    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }
}