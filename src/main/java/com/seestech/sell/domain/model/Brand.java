package com.seestech.sell.domain.model;

import java.util.Set;

/**
 * Created by Administrator on 2017/6/15.
 */
public class Brand extends BaseBean {
    private Long brandId;
    private String name;

    //显示字段
    private String userName;
    private Set<Long> userIds;

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /******************/
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Set<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<Long> userIds) {
        this.userIds = userIds;
    }
}