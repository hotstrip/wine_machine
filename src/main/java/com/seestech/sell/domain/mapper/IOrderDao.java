package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Order;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;

/**
 * Created by idiot on 2017/6/22.
 */
@Mapper
public interface IOrderDao {

    //新增
    int insert(Order info);

    //修改
    int update(Order info);

    //删除
    @Delete("delete from t_order where ORDER_ID = #{orderId}")
    int delete(@Param("orderId") Long orderId);

    //批量删除
    int deleteBatch(@Param("orderIds") Long[] orderIds);

    //分页获取订单信息
    Page<Order> getAllOrders(RowBounds rowBounds, Order info);

    //根据订单编号查询订单信息
    Order getOrderByOrderId(@Param("orderId") Long orderId);

    //获取订单信息
    List<Order> getOrdersByOrder(Order info);

    //根据customerId获取订单统计信息
    HashMap getOrdersByCustomerId(@Param("customerId") String customerId);

    //获取数据
    List<Order> getMonthlySales(Order info);

    //获取商品销售额度
    List<Order> getGoodsSales(Order info);

    //获取订单数据
    List<Order> getAllExportOrders(RowBounds rowBounds, Order info);

    //根据时间格式化获取销售信息
    List<Order> getSalesByDateformat(Order info);

    //根据时间格式化获取订单信息
    List<Order> getOrdersByDateformat(Order info);

    // 获取可退款订单
    Page<Order> getRefundAbleOrders(RowBounds rowBounds, Order info);

    //获取商品排行榜
    Page<Order> getGoodsRank(RowBounds rowBounds, Order info);

    //获取终端排行榜
    Page<Order> getAgentRank(RowBounds rowBounds, Order info);
}
