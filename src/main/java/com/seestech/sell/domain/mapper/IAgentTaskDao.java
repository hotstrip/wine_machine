package com.seestech.sell.domain.mapper;

import com.seestech.sell.domain.model.AgentTaskInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by idiot on 2017/5/27.
 */
@Mapper
public interface IAgentTaskDao {
    //新增
    int insert(AgentTaskInfo info);

    //修改
    int update(AgentTaskInfo info);

    //查询任务信息
    List<AgentTaskInfo> getAgentTasks(AgentTaskInfo info);

    //批量新增
    int insertBatch(@Param("agentTasks") List<AgentTaskInfo> agentTasks);

    //根据终端code获取任务信息
    @Select("select * from t_agent_task where AGENT_CODE = #{agentCode}")
    List<AgentTaskInfo> getAgentTasksByAgentCode(@Param("agentCode") String agentCode);

    //批量删除
    int deleteBatch(@Param("ids") Long[] ids);
}
