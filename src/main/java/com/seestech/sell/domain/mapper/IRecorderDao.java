package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Order;
import com.seestech.sell.domain.model.Recorder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/22.
 */
@Mapper
public interface IRecorderDao {
    //分页查询补货记录
    Page<Recorder> getAllRecorders(RowBounds rowBounds, Recorder info);

    //分页查询补货记录
    Page<Recorder> getRecorders(RowBounds rowBounds, Recorder info);

    //删除
    int delete(Long recorderId);

    //批量删除
    int deleteBatch(Long[] recorderIds);

    //获取记录信息
    List<Recorder> getRecordersByAgentId(@Param("agentId") Long agentId);

    //新增
    int insert(Recorder info);

    //修改
    int update(Recorder info);

    //获取单个
    Recorder getRecordersByRecorder(Recorder recorder);

    //查询
    @Select("select * from t_recorder where RECORDER_ID = #{recorderId}")
    Recorder getRecorderByRecorderId(@Param("recorderId") Long recorderId);

    //根据id查询
    List<Recorder> getRecordersByIds(@Param("agentId") Long agentId, @Param("shelfId") Long shelfId, @Param("goodsId") Long goodsId);

    //根据agentId  shelfId goodsId  获取 最新的记录信息
    List<Recorder> getRecordersByInfo(Recorder recorder);

    //根据batchNumber分组查询
    Page<Recorder> getRecordersGroupByBatchNumber(RowBounds rowBounds, Recorder info);

    //根据batchNumber查询
    List<Recorder> getRecordersByBatchNumber(@Param("batchNumber") String batchNumber);
}
