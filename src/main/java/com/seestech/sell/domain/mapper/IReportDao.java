package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Report;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

/**
 * Created by idiot on 2017/8/21.
 */
@Mapper
public interface IReportDao {
    //新增
    int insert(Report info);

    //修改
    int update(Report info);

    //删除
    @Delete("delete from t_report where ID = #{id}")
    int delete(@Param("id") Long id);

    //分页加载信息
    Page<Report> getAllReports(RowBounds rowBounds, Report info);

    //批量删除
    int deleteBatch(@Param("ids") Long[] ids);
}
