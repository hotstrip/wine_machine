package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Shelf;
import com.seestech.sell.domain.model.ShelfDetail;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/20.
 */
@Mapper
public interface IShelfDetailDao {
    //新增
    int insert(ShelfDetail info);

    //修改
    int update(ShelfDetail info);

    //删除
    @Delete("delete from t_shelf_detail where DETAIL_ID = #{detailId}")
    int delete(@Param("detailId") Long detailId);

    //批量删除
    int deleteBatch(@Param("detailIds") Long[] detailIds);

    //获取货架详情
    List<ShelfDetail> getShelfDetailsByShelfId(@Param("shelfId") Long shelfId);

    //分页获取货架详情
    Page<Shelf> getAllShelfDetails(RowBounds rowBounds, ShelfDetail info);

    //获取单个
    ShelfDetail getShelfDetailByDetailId(@Param("detailId") Long detailId);

    //根据柜格编号获取货架信息
    List<ShelfDetail> getShelfDetailByGridIds(@Param("gridIds") Long[] gridIds);

    //根据货架编号删除
    int deleteByShelfIds(@Param("shelfIds") Long[] shelfIds);

    //根据货架编号以及柜格编号分组
    List<ShelfDetail> getShelfDetailsByShelfIdAndGroupByGoodsId(@Param("shelfId") Long shelfId);

    //获取货架详情  不分页
    Page<Shelf> getShelfDetails(ShelfDetail info);
}
