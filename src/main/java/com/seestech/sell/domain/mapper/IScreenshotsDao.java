package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Screenshots;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;


/**
 * Created by idiot on 2017/1/17.
 */
@Mapper
public interface IScreenshotsDao {
    //新增
    int insert(Screenshots info);

    //修改
    int update(Screenshots info);

    //根据agentCode获取终端截图信息
    @Select("select s.* from t_screenshots s LEFT JOIN t_agent a on s.AGENT_ID = a.AGENT_ID where a.AGENT_CODE= #{agentCode} order by CREATE_TIME desc")
    Page<Screenshots> getScreenshotsByAgentCode(RowBounds rowBounds, @Param("agentCode") String agentCode);

    //批量删除
    int deleteBatch(@Param("screenshotsIds") Long[] screenshotsIds);

}
