package com.seestech.sell.domain.mapper;

import com.seestech.sell.domain.model.Statistics;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by idiot on 2017/8/1.
 */
@Mapper
public interface IStatisticsDao {
    //新增
    int insert(Statistics info);

    //修改
    int update(Statistics info);

    //删除
    int delete(@Param("id") Long id);

    //批量删除
    int delteBatch(@Param("ids") Long[] ids);

    //批量新增
    int insertBatch(@Param("list") List<Statistics> list);
}
