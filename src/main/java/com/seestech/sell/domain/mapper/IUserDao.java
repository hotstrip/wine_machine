package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Set;

/**
 * Created by idiot on 2017/6/15.
 */
@Mapper
public interface IUserDao {
    //根据用户名获取用户信息
    User getUserByName(@Param("userName") String userName);

    //获取所有管理员用户
    Page<User> getAllAdminUsers(RowBounds rowBounds, User user);

    //新增用户
    int insert(User info);

    //修改用户
    int update(User info);

    //根据userId获取用户信息
    @Select("select * from t_user where USER_ID = #{userId}")
    User getUserByUserId(@Param("userId") long userId);

    //删除用户信息
    @Delete("delete from t_user where USER_ID = #{userId}")
    int delete(@Param("userId") long userId);

    //获取所有有效用户
    @Select("select * from t_user where STATUS = 1")
    List<User> getAllUsers();

    //根据用户名和密码获取用户信息
    @Select("select * from t_user where USER_NAME = #{userName} and USER_PASSWORD = #{userPassword}")
    User getUserByNameAndPassword(@Param("userName") String userName, @Param("userPassword") String userPassword);

    //根据微信的openId获取用户信息
    @Select("select * from t_user where OPENID = #{openId} and STATUS = 1")
    List<User> getUserByOpenId(@Param("openId") String openId);

    //根据微博的AccessToken获取用户信息
    @Select("select * from t_user where WEIBOID = #{weiboId} and STATUS = 1")
    User getUserByWeiboId(@Param("weiboId") String access_token);

    //根据企业编号获取用户信息
    List<User> getUsersByCompanyIds(@Param("ids") List<Long> companyIds);

    //根据企业编号获取用户编号
    Set<Long> getUserIdsByCompanyIds(@Param("ids") List<Long> ids);
}