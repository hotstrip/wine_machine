package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Shelf;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/20.
 */
@Mapper
public interface IShelfDao {
    //新增
    int insert(Shelf info);

    //修改
    int update(Shelf info);

    //删除
    @Delete("delete from t_shelf where SHELF_ID = #{shelfId}")
    int delete(@Param("shelfId") Long shelfId);

    //批量删除
    int deleteBatch(@Param("shelfIds") Long[] shelfIds);

    //分页获取货架信息
    Page<Shelf> getAllShelfs(RowBounds rowBounds, Shelf info);

    //获取单个货架信息
    @Select("select * from t_shelf where SHELF_ID = #{shelfId}")
    Shelf getShelfByShelfId(Long shelfId);

    //获取货架信息
    List<Shelf> getShelfs(Shelf info);

    //根据名称获取
    @Select("select * from t_shelf where NAME = #{name}")
    List<Shelf> getShelfsByName(@Param("name") String name);
}
