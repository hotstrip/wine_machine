package com.seestech.sell.domain.mapper;

import com.seestech.sell.domain.model.RoleMenu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by idiot on 2016/12/16.
 */
@Mapper
public interface IRoleMenuDao {
    //根据角色编号删除角色菜单关系信息
    @Delete("delete from t_role_menu where ROLE_ID = #{roleId}")
    int deleteRoleMenusByRoleId(@Param("roleId") long roleId);

    //查询该角色所拥有的菜单
    @Select("select * from t_role_menu where ROLE_ID = #{roleId}")
    List<RoleMenu> getRoleMenusByRoleId(@Param("roleId") long roleId);

    //新增
    int insert(RoleMenu roleMenu);

    //根据roleId 和 menuId获取角色菜单信息 select * from t_role_menu where
    RoleMenu getRoleMenuByRoleMenu(RoleMenu roleMenu);

    //删除
    @Delete("delete from t_role_menu where ROLE_MENU_ID = #{roleMenuId}")
    int delete(@Param("roleMenuId") long roleMenuId);
}
