package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Brand;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/17.
 */
@Mapper
public interface IBrandDao {
    //分页获取品牌信息
    Page<Brand> getAllBrands(RowBounds rowBounds, Brand info);

    //新增
    int insert(Brand info);

    //修改
    int update(Brand info);

    //根据brandId获取品牌信息
    @Select("select * from t_brand where BRAND_ID = #{brandId}")
    Brand getBrandByBrandId(@Param("brandId") Long brandId);

    //删除品牌信息
    @Delete("delete from t_brand where BRAND_ID = #{brandId}")
    int delete(@Param("brandId") Long brandId);

    //批量删除
    int deleteBatch(@Param("brandIds") Long[] brandIds);

    //获取品牌信息
    List<Brand> getBrands(Brand info);
}
