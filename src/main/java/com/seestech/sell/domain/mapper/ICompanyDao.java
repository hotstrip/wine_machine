package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Company;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/8/7.
 */
@Mapper
public interface ICompanyDao {
    //新增
    int insert(Company info);

    //修改
    int update(Company info);

    //删除
    @Delete("delete from t_company where ID = #{id}")
    int delete(@Param("id") Long id);

    //根据创建者 获取 多个
    Page<Company> getCompanies(RowBounds rowBounds, Company info);

    //获取单个
    @Select("select * from t_company where ID = #{id}")
    Company getCompanyById(@Param("id") Long id);

    //获取所有企业信息
    List<Company> getAllCompany(Company info);

    //根据名称获取企业信息
    @Select("select * from t_company where NAME = #{name}")
    Company getCompanyByName(@Param("name") String name);

    //根据用户编号获取
    @Select("select ID from t_company where MANAGER = #{userId} or CREATE_USER = #{userId} and STATUS = 1")
    List<Long> getCompanyByUserId(@Param("userId") Long userId);

    //根据userId获取管理的企业
    @Select("select * from t_company where MANAGER = #{userId} and STATUS = 1")
    List<Company> getManageCompanyByUserId(@Param("userId") Long userId);
}
