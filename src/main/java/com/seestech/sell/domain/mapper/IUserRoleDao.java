package com.seestech.sell.domain.mapper;

import com.seestech.sell.domain.model.UserRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by idiot on 2017/6/16.
 */
@Mapper
public interface IUserRoleDao {
    //根据userId删除用户角色关系信息
    @Delete("delete from t_user_role where USER_ID = #{userId}")
    int deleteUserRolesByUserId(@Param("userId") long userId);

    //根据userId查询该用户拥有的角色
    @Select("select * from t_user_role where USER_ID = #{userId}")
    List<UserRole> getUserRolesByUserId(long userId);

    //新增
    int insert(UserRole userRole);

    //删除
    @Delete("delete from t_user_role where USER_ROLE_ID = #{userRoleId}")
    int delete(@Param("userRoleId") long userRoleId);

    //根据userId 和 roleId获取UserRole信息
    UserRole getUserRoleByUserRole(UserRole userRole);
}
