package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Goods;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/17.
 */
@Mapper
public interface IGoodsDao {
    //分页获取商品信息
    Page<Goods> getAllGoodses(RowBounds rowBounds, Goods info);

    //新增
    int insert(Goods info);

    //根据goodsId获取商品信息
    @Select("select * from t_goods where GOODS_ID = #{goodsId}")
    Goods getGoodsByGoodsId(@Param("goodsId") Long goodsId);

    //删除
    @Delete("delete from t_goods where GOODS_ID = #{goodsId}")
    int delete(@Param("goodsId") Long goodsId);

    //批量删除
    int deleteBatch(@Param("goodsIds") Long[] goodsIds);

    //修改
    int update(Goods info);

    //获取商品信息
    List<Goods> getGoodses(Goods info);

    //根据品牌编号获取商品信息
    List<Goods> getGoodsesByBrandIds(@Param("brandIds") Long[] brandIds);

    //根据商品编号获取商品信息
    @Select("select * from t_goods where CODE = #{code}")
    Goods getGoodsByCode(@Param("code") String code);


    //根据商品编号获取商品信息
    @Select("select * from t_goods where CODE = #{code}")
    List<Goods> getGoodsesByCode(@Param("code") String code);

    //获取商品信息
    List<Goods> getGoodsByGoodsIds(@Param("goodsIds") String[] goodsIds);

    //获取商品信息  根据商品类型
    List<Goods> getGoodsesByTypeIds(@Param("typeIds") Long[] typeIds);
}
