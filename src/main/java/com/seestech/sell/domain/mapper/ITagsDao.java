package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Tags;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/8/14.
 * @description  标签信息dao
 */
@Mapper
public interface ITagsDao {
    //新增
    int insert(Tags info);

    //修改
    int update(Tags info);

    //删除
    @Delete("delete from t_tags where ID = #{id}")
    int delete(@Param("id") Long id);

    //查询多个
    Page<Tags> getTagsByTags(RowBounds rowBounds, Tags info);

    //根据名称获取
    @Select("select * from t_tags where STATUS = 1 and TAG_NAME = #{tagName}")
    List<Tags> getTagsByTagName(@Param("tagName") String tagName);

    //获取多个
    @Select("select * from t_tags where STATUS = 1 and TAG_NAME = #{tagName}")
    Tags getTagByTagName(String tagName);

    //根据编号查询标签信息
    @Select("select * from t_tags where ID = #{id}")
    Tags getTagsById(@Param("id") Long id);

    //批量删除
    int deleteBatch(@Param("ids") Long[] ids);
}
