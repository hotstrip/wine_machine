package com.seestech.sell.domain.mapper;

import com.seestech.sell.domain.model.CompanyPay;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * Created by idiot on 2017/9/9.
 */
@Mapper
public interface ICompanyPayDao {
    // 新增
    int insert(CompanyPay info);

    // 修改
    int update(CompanyPay info);

    // 删除
    @Delete("delete from t_company_pay where ID = #{id}")
    int delete(@Param("id") Long id);

    // 查询单个
    @Select("select * from t_company_pay where ID = #{id}")
    CompanyPay getCompanyPayById(@Param("id") Long id);

    // 根据企业编号获取信息
    @Select("select * from t_company_pay where COMPANY_ID = #{companyId}")
    CompanyPay getCompanyPayByCompanyId(@Param("companyId") Long companyId);

    // 根据订单编号获取企业支付信息
    CompanyPay getCompanyPayByOrderId(@Param("orderId") Long orderId);

    // 根据终端编号获取企业支付信息
    CompanyPay getCompanyPayByAgentId(@Param("agentId") Long agentId);
}
