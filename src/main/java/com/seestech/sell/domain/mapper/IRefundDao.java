package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Refund;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

/**
 * Created by idiot on 2017/7/7.
 */
@Mapper
public interface IRefundDao {
    //新增
    int insert(Refund info);

    //修改
    int update(Refund info);

    //获取退款信息
    Page<Refund> getAllRefundes(RowBounds rowBounds, Refund info);

    //删除
    @Delete("delete from t_refund where REFUND_ID = #{refundId}")
    int delete(@Param("refundId") Long refundId);

    //批量删除
    int deleteBatch(@Param("refundIds") Long[] refundIds);
}
