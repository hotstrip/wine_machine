package com.seestech.sell.domain.mapper;

import com.seestech.sell.domain.model.UserAgentGroup;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created by idiot on 2017/1/19.
 */
@Mapper
public interface IUserAgentGroupDao {

    //新增
    int insert(UserAgentGroup info);

    //删除
    @Delete("delete from t_user_agent_group where USER_AGENT_GROUP_ID = #{userAgentGroupId}")
    int delete(@Param("userAgentGroupId") Long userAgentGroupId);

    //根据用户终端分组信息查询
    UserAgentGroup getUserAgentGroupByUserAgentGroup(UserAgentGroup userAgentGroup);

    //批量删除
    int deleteByIds(@Param("userId") Long userId, @Param("agentGroupIds") Long[] agentGroupIds);
}
