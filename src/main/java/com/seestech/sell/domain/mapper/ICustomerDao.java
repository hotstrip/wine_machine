package com.seestech.sell.domain.mapper;

import com.seestech.sell.domain.model.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by idiot on 2017/7/7.
 */
@Mapper
public interface ICustomerDao {
    //新增
    int insert(Customer info);

    //修改
    int update(Customer info);

    //获取客户信息表
    @Select("select * from t_customer where CUSTOMER_ID = #{customerId}")
    Customer getCustomerByCustomerId(@Param("customerId") String customerId);

    //根据phone查询客户信息
    List<Customer> getCustomersByPhone(@Param("phone") String phone);
}
