package com.seestech.sell.domain.mapper;


import com.seestech.sell.domain.model.AgentTheme;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by idiot on 2016/12/23.
 */
@Mapper
public interface IAgentThemeDao {
    //新增
    int insert(AgentTheme info);

    //修改
    int update(AgentTheme info);

    //根据批次号获取主题信息
    List<AgentTheme> getThemesByBatchNumber(@Param("batchNumbers") String[] batnoObj);

    //根据终端编号获取终端对应的主题信息
    @Select("select * from t_agent_theme where AGENT_ID = #{agentId} and STATUS = 1")
    AgentTheme getValidAgentThemeByAgentId(@Param("agentId") Long agentId);

    //根据主题编号和终端编号查询有效的终端主题信息
    @Select("select * from t_agent_theme where AGENT_ID = #{agentId} and THEME_ID = #{themeId}")
    List<AgentTheme> getAgentThemesByThemeIdAndAgentId(@Param("agentId") Long agentId, @Param("themeId") Long themeId);

    //根据终端编号查询有效的终端主题信息
    @Select("select * from t_agent_theme where AGENT_ID = #{agentId} and STATUS = 1")
    List<AgentTheme> getAgentThemesByAgentId(@Param("agentId") Long agentId);

    //批量新增
    int insertBatch(@Param("agentThemes") List<AgentTheme> agentThemes);
}
