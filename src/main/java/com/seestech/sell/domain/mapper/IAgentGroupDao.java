package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.AgentGroup;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/26.
 */
@Mapper
public interface IAgentGroupDao {
    //分页查询所有的终端分组信息
    @Select("select * from t_agent_group")
    List<AgentGroup> getAllAgentGroups(RowBounds rowBounds);

    //新增
    int insert(AgentGroup info);

    //修改
    int update(AgentGroup info);

    //根据agentGroupId 获取终端分组信息
    @Select("select * from t_agent_group where AGENT_GROUP_ID = #{agentGroupId}")
    AgentGroup getAgentGroupByAgentGroupId(@Param("agentGroupId") Long agentGroupId);

    //删除
    @Delete("delete from t_agent_group where AGENT_GROUP_ID = #{agentGroupId}")
    int delete(@Param("agentGroupId") Long agentGroupId);

    //获取所有有效的终端分组信息
    List<AgentGroup> getAllValidAgentGroups(AgentGroup info);

    //根据用户编号 分页获取终端分组信息
    Page<AgentGroup> getAllAgentGroupsByUserId(RowBounds rowBounds, @Param("userId") Long userId, @Param("agentGroup") AgentGroup info);

    //根据用户编号加载有效的终端分组信息
    @Select("select ag.* from t_agent_group ag " +
            "LEFT JOIN t_user_agent_group uag on ag.AGENT_GROUP_ID = uag.AGENT_GROUP_ID where uag.USER_ID = #{userId}")
    List<AgentGroup> getAllValidAgentGroupsByUserId(@Param("userId") Long userId);

    //批量删除
    int deleteBatch(@Param("agentGroupIds") Long[] agentGroupIds);

    //根据名称获取终端分组
    @Select("select * from t_agent_group where AGENT_GROUP_NAME = #{agentGroupName}")
    List<AgentGroup> getAgentGroupsByAgentGroupName(@Param("agentGroupName") String agentGroupName);
}
