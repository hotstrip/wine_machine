package com.seestech.sell.domain.mapper;

import com.seestech.sell.domain.model.PublishHistory;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/1/9.
 */
@Mapper
public interface IPublishHistoryDao {

    //根据终端编号 和 发布批次号 查询历史记录
    List<PublishHistory> getPublishHistoriesByAgentCodesAndBatchNumbers(@Param("agentCodes") String[] agentCodeObj, @Param("batchNumbers") String[] batnoObj);

    //修改
    int update(PublishHistory info);

    //根据批次号查询发布历史记录
    List<PublishHistory> getPublishHistoriesByBatchNumbers(@Param("batchNumbers") String[] batnoObj);

    //新增
    int insert(PublishHistory info);

    //分页获取所有发布历史记录
    List<PublishHistory> getAllPublishHistories(RowBounds rowBounds, PublishHistory info);

    //根据id获取信息
    PublishHistory getPublishHistoryById(@Param("publishHistoryId") Long publishHistoryId);

    //批量删除
    int deleteBatch(@Param("publishHistoryIds") Long[] publishHistoryIds);

    //删除
    @Delete("delete from t_publish_history where PUBLISH_HISTORY_ID = #{publishHistoryId}")
    int delete(@Param("publishHistoryId") Long publishHistoryId);

    //查询发布历史记录
    List<PublishHistory> getPublishHistoriesByInfo(PublishHistory info);

    //批量新增
    int insertBatch(@Param("histories") List<PublishHistory> histories);
}
