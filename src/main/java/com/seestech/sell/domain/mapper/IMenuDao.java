package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Menu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/16.
 */
@Mapper
public interface IMenuDao {
    //根据userId获取菜单
    List<Menu> getMenusByUserId(@Param("userId") long userId);

    //根据菜单parentId获取子菜单
    @Select("select * from t_menu where STATUS = '1' and PARENT_ID = #{parentId,jdbcType=BIGINT} order by MENU_INDEX")
    List<Menu> getMenusByParentId(@Param("parentId") long parentId);

    //获取该用户的一级菜单
    List<Menu> getFirstLevelMenusByUserId(@Param("userId") long userId);

    //获取所有菜单信息  分页
    Page<Menu> getAllPageMenus(RowBounds rowBounds, Menu info);

    //新增菜单信息
    int insert(Menu info);

    //修改菜单信息
    int update(Menu info);

    //根据menuId获取菜单信息
    @Select("select * from t_menu where MENU_ID = #{menuId}")
    Menu getMenuByMenuId(@Param("menuId") long menuId);

    //删除菜单信息
    @Delete("delete from t_menu where MENU_ID = #{menuId}")
    int delete(@Param("menuId") long menuId);

    //获取所有有效的菜单信息  不分页
    @Select("select * from t_menu where STATUS = 1")
    List<Menu> getAllMenus();

    //获取该roleId对应的菜单信息
    List<Menu> getMenusByRoleId(@Param("roleId") long roleId);

    //根据roleId获取一级菜单信息
    List<Menu> getFirstMenusByRoleId(@Param("roleId") long roleId);

    //获取所有一级菜单信息
    @Select("select * from t_menu where KIND = 1 and PARENT_ID is null or PARENT_ID = ''")
    List<Menu> getAllfirstMenus();

    //根据父级菜单编号和用户编号获取菜单信息
    List<Menu> getMenusByParentIdAndUserId(@Param("parentId") Long menuId, @Param("userId") Long userId);
}
