package com.seestech.sell.domain.mapper;

import com.seestech.sell.domain.model.ThemeColumn;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by idiot on 2016/12/23.
 */
@Mapper
public interface IThemeColumnDao {
    //新增
    int insert(ThemeColumn info);

    //修改
    int update(ThemeColumn info);

    //根据主题id获取主题栏目信息
    @Select("select * from t_theme_column where THEME_ID = #{themeId}")
    List<ThemeColumn> getThemeColumnsByThemeId(@Param("themeId") long themeId);

    //根据主题栏目编号获取信息
    @Select("select * from t_theme_column where THEME_COLUMN_ID = #{themeColumnId}")
    ThemeColumn getThemeColumnByThemeColumnId(@Param("themeColumnId") Long themeColumnId);

    //删除
    @Delete("delete from t_theme_column where THEME_COLUMN_ID = #{themeColumnId}")
    int delete(@Param("themeColumnId") Long themeColumnId);

    //根据目录编号获取主题栏目信息
    @Select("select DISTINCT * from t_theme_column where STATUS = 1 and DIRECTORY_ID = #{directoryId}")
    List<ThemeColumn> getThemeColumnsByDirectoryId(@Param("directoryId") Long directoryId);

    //根据主题编号和栏目目录编号查询主题栏目信息
    @Select("select * from t_theme_column where DIRECTORY_ID = #{directoryId} and THEME_ID = #{themeId}")
    ThemeColumn getThemeColumnByIds(@Param("themeId") Long themeId, @Param("directoryId") Long directoryId);

    //批量删除
    int deleteBatch(@Param("themeColumns") List<ThemeColumn> list);

    //批量新增
    int insertBatch(@Param("themeColumns") List<ThemeColumn> themeColumns);
}
