package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Publish;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/8/1.
 */
@Mapper
public interface IPublishDao {
    //新增
    int insert(Publish info);

    //修改
    int update(Publish info);

    //删除
    int delete(@Param("publishId") Long publishId);

    //批量删除
    int deleteBatch(@Param("publishIds") Long[] publishIds);

    //发布信息列表
    Page<Publish> getPublishList(RowBounds rowBounds, Publish info);

    //获取单个
    @Select("select * from t_publish where PUBLISH_ID = #{publishId}")
    Publish getPublishByPublishId(@Param("publishId") Long publishId);

    //获取多个
    List<Publish> getPublishByPublishIds(@Param("publishIds") Long[] publishIds);

    // 根据名称获取发布信息
    @Select("select * from t_publish where NAME = #{name} and STATUS = 1")
    List<Publish> getPublishByPublishName(@Param("name") String name);
}
