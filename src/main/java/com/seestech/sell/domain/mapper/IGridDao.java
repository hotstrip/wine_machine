package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Grid;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/20.
 */
@Mapper
public interface IGridDao {
    //新增
    int insert(Grid info);

    //修改
    int update(Grid info);

    //删除
    @Delete("delete from t_grid where GRID_ID = #{gridId}")
    int delete(@Param("gridId") Long gridId);

    //批量删除
    int deleteBatch(@Param("gridIds")Long[] gridIds);

    //分页获取柜格信息
    Page<Grid> getAllGrids(RowBounds rowBounds, Grid info);

    //获取单个柜格信息
    Grid getGridByGridId(@Param("gridId")Long gridId);

    //获取柜格信息
    List<Grid> getGrids(Grid info);

    //根据商品编号获取柜格信息
    List<Grid> getGridsByGoodsIds(@Param("goodsIds") Long[] goodsIds);
}
