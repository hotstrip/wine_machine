package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Error;
import com.seestech.sell.domain.model.vo.ErrorVoExcel;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/7/27.
 * @description  异常dao
 */
@Mapper
public interface IErrorDao {
    //新增
    int insert(Error info);

    //修改
    int update(Error info);

    //删除
    @Delete("delete from t_error where ID = #{id}")
    int delete(@Param("id") Long id);

    //获取单个
    @Select("select * from t_error where ID = #{id}")
    Error getErrorById(@Param("id") Long id);

    //根据终端编号获取异常信息
    @Select("select * from t_error where AGENT_ID = #{agentId}")
    List<Error> getErrorsByAgentId(@Param("agentId") Long agentId);

    //分页获取异常信息
    Page<Error> getAllErrors(RowBounds rowBounds, Error info);

    //导出数据
    List<ErrorVoExcel> getAllExportErrors(RowBounds rowBounds, Error info);
}
