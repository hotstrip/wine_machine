package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Log;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

/**
 * Created by idiot on 2016/12/14.
 */
@Mapper
public interface ILogDao {
    //新增日志信息
    int insert(Log log);

    //分页查询所有日志信息
    Page<Log> getAllLogs(RowBounds rowBounds, Log info);

    //删除日志信息
    @Delete("delete from t_log where LOG_ID = #{logId}")
    int delete(@Param("logId") long logId);

    //批量删除
    int deleteBatch(@Param("logIds") Long[] logIds);
}
