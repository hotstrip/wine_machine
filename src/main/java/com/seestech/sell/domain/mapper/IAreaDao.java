package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Area;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/19.
 */
@Mapper
public interface IAreaDao {
    //新增
    int insert(Area info);

    //修改
    int update(Area info);

    //分页获取区域信息
    Page<Area> getAllAreas(RowBounds rowBounds, Area info);

    //根据areaId获取区域信息
    @Select("select * from t_area where AREA_ID = #{areaId}")
    Area getAreaByAreaId(@Param("areaId") Long areaId);

    //删除
    @Delete("delete from t_area where AREA_ID = #{areaId}")
    int delete(@Param("areaId") Long areaId);

    //批量删除
    int deleteBatch(@Param("areaIds") Long[] areaIds);

    //获取区域信息
    List<Area> getAreas(Area area);

    //根据名称获取
    @Select("select * from t_area where NAME = #{name}")
    List<Area> getAreasByName(@Param("name") String name);
}
