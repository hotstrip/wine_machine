package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Agent;
import com.seestech.sell.domain.model.vo.AgentVoExcel;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * Created by idiot on 2017/6/19.
 */
@Mapper
public interface IAgentDao {
    //新增
    int insert(Agent info);

    //修改
    int update(Agent info);

    //根据agentId获取终端信息
    Agent getAgentByAgentId(@Param("agentId") long agentId);

    //删除
    @Delete("delete from t_agent where AGENT_ID = #{agentId}")
    int delete(@Param("agentId")long agentId);

    //根据用户信息  分页加载终端信息
    Page<Agent> getAllAgentsByUserId(RowBounds rowBounds, Agent info);

    //批量删除
    int deleteBatch(@Param("agentIds") Long[] agentIds);

    //根据终端分组id获取终端信息  分页
    @Select("select * from t_agent where AGENT_GROUP_ID = #{agentGroupId}")
    List<Agent> getAgentsByAgentGroupId(@Param("agentGroupId") Long agentGroupId, RowBounds rowBounds);

    //根据终端分组编号获取终端
    List<Agent> getAgentsByAgentGroupIds(@Param("agentGroupIds") Long[] agentGroupIds);

    //分页加载所有终端的截图信息
    Page<Agent> getAllAgentsScreenshots(RowBounds rowBounds, Agent agent);

    //查询所有终端信息
    List<Agent> getAllAgents(Agent info);

    //根据终端分组编号获取终端信息
    @Select("select * from t_agent where AGENT_GROUP_ID = #{agentGroupId}")
    List<Agent> getAllValidAgentsByGroupId(@Param("agentGroupId")Long agentGroupId);

    //根据主题编号查询终端信息
    List<Agent> getValidAgentsByThemeId(@Param("themeId") Long themeId);

    //根据agentCode获取agent信息
    @Select("select * from t_agent where AGENT_CODE = #{agentCode}")
    Agent getAgentByAgentCode(@Param("agentCode") String agentCode);

    //根据agentCode获取agent信息
    @Select("select * from t_agent where AGENT_CODE = #{agentCode}")
    List<Agent> getAgentsByAgentCode(@Param("agentCode") String agentCode);

    //根据货架编号获取终端信息
    List<Agent> getAgentsByShelfIds(@Param("shelfIds") Long[] shelfIds);

    //获取所有有效的终端信息
    @Select("select * from t_agent where status = 1")
    List<Agent> getAllValidAgents();

    //根据gridId获取终端信息
    List<Agent> getAgentsByGridId(@Param("gridId") Long gridId);

    //根据终端id获取终端的库存信息
    Map<String,Object> getGoodsInfo(@Param("agentId") Long agentId);

    //根据主题编号获取终端信息
    List<Agent> getAgentsByThemeIds(@Param("themeIds") List<Long> themeIds, @Param("userId") Long userId);

    //批量修改
    int updateBatch(@Param("agentList") List<Agent> agentList);

    //根据终端编号获取商品信息
    List<Map<String,Object>> getGoodsInfoByIds(@Param("agentIds") List<Long> ids);

    // 批量新增
    int insertBatch(@Param("agents") List<AgentVoExcel> list);

    //导出数据查询
    List<AgentVoExcel> getAllExportAgentsByUserId(RowBounds rowBounds, Agent info);

    //根据终端类型编号查询终端信息
    List<Agent> getAgentsByAgentTypeIds(@Param("agentTypeIds") Long[] agentTypeIds);
}
