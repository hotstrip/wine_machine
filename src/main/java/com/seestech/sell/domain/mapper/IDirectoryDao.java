package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Directory;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/27.
 */
@Mapper
public interface IDirectoryDao {
    //新增
    int insert(Directory info);

    //修改
    int update(Directory info);

    //根据目录 加载目录信息
    Directory getDirectoryByDirectoryId(@Param("directoryId") Long directoryId);

    //分页获取目录信息
    @Select("select * from t_directory where status = 1")
    List<Directory> getAllDirectories(RowBounds rowBounds);

    //删除目录信息
    @Delete("delete from t_directory where DIRECTORY_ID = #{directoryId}")
    int delete(@Param("directoryId") Long directoryId);

    //获取所有有效业务目录信息
    @Select("select * from t_directory where DIRECTORY_TYPE_ID = #{directoryTypeId}")
    List<Directory> getAllValidBusinessDirectories(@Param("directoryTypeId") Long directoryTypeId);

    //获取所有有效的父级目录信息
    @Select("select * from t_directory where STATUS = 1 and PARENT_DIRECTORY_ID is not null")
    List<Directory> getValidParentDiretories();

    //获取顶级目录信息 无父级目录编号
    @Select("select * from t_directory where STATUS = 1 and PARENT_DIRECTORY_ID is null")
    List<Directory> getAllValidFirstLevelDirectories();

    //根据parentDirectoryId获取有效目录信息
    @Select("select * from t_directory where STATUS = 1 and PARENT_DIRECTORY_ID = #{parentDirectoryId}")
    List<Directory> getValidDirectoryByParentDirectoryId(@Param("parentDirectoryId") Long parentDirectoryId);

    @Select("select * from t_directory where STATUS = 1 and TYPE = #{type}")
    List<Directory> getDirectoriesByType(@Param("type") Integer value);

    //获取目录信息
    Page<Directory> getDirectoriesByDirectory(Directory info);
}
