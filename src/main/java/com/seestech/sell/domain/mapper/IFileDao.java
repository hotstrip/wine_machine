package com.seestech.sell.domain.mapper;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.FileInfo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/28.
 */
@Mapper
public interface IFileDao {
    //分页获取文件信息
    Page<FileInfo> getFilesByFileInfo(RowBounds rowBounds, FileInfo info);

    //新增
    int insert(FileInfo fileInfo);

    //修改
    int update(FileInfo fileInfo);

    //分页获取所有有效文件信息
    @Select("select * from t_file where status = 1")
    List<FileInfo> getAllValidFies(RowBounds rowBounds);

    //根据fileId获取文件信息
    @Select("select * from t_file where FILE_ID = #{fileId}")
    FileInfo getFileByFileId(@Param("fileId") Long fileId);

    //根据目录编号获取有效文件信息
    @Select("select * from t_file where DIRECTORY_ID = #{directoryId} and status = 1")
    List<FileInfo> getAllValidFiesByDirectoryId(@Param("directoryId") Long directoryId);

    //删除
    @Delete("delete from t_file where FILE_ID = #{fileId}")
    int delete(@Param("fileId") Long fileId);

    int insertBatch(@Param("files") List<FileInfo> list);

    //批量删除
    int deleteBatch(@Param("fileIds") Long[] fileIds);

    //根据目录编号删除文件信息
    @Delete("delete from t_file where DIRECTORY_ID = #{directoryId}")
    int deleteFilesByDirectoryId(@Param("directoryId") Long directoryId);
}
