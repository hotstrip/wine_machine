package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Shelf;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/20.
 */
public interface IShelfService {
    //新增
    void insert(Shelf info);

    //修改
    void update(Shelf info);

    //删除
    void delete(Long shelfId);

    //批量删除
    void deleteBatch(Long[] shelfIds);

    //分页获取货架信息
    Page<Shelf> getAllShelfs(RowBounds rowBounds, Shelf info);

    //查询单个货架信息
    Shelf getShelfByShelfId(Long shelfId);

    //获取货架信息
    List<Shelf> getShelfs(Shelf info);

    //修改货架信息
    boolean modifyShelf(Long shelfId);

    //根据名称获取
    List<Shelf> getShelfsByName(String name);
}
