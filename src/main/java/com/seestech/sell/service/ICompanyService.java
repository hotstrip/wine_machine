package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Company;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/8/7.
 */
public interface ICompanyService {
    //新增
    void insert(Company info);

    //修改
    void update(Company info);

    //删除
    void delete(Long id);

    //根据创建者 获取 多个
    Page<Company> getCompanies(RowBounds rowBounds, Company info);

    //获取单个
    Company getCompanyById(Long id);

    //获取所有企业信息
    List<Company> getAllCompany(Company info);

    //根据名称获取
    Company getCompanyByName(String name);

    //根据用户编号获取
    List<Long> getCompanyByUserId(Long userId);

    //根据userId获取管理的企业
    List<Company> getManageCompanyByUserId(Long userId);
}
