package com.seestech.sell.service;

import com.seestech.sell.domain.model.AgentTheme;

import java.util.List;

/**
 * Created by idiot on 2017/1/13.
 */
public interface IAgentThemeService {
    //根据终端编号获取终端对应的主题信息
    AgentTheme getValidAgentThemeByAgentId(Long agentId);

    //新增
    void insert(AgentTheme agentTheme);

    //修改
    void update(AgentTheme agentTheme);

    //根据主题编号和终端编号查询有效的终端主题信息
    List<AgentTheme> getAgentThemesByThemeIdAndAgentId(Long agentId, Long themeId);

    //根据终端编号查询有效的终端主题信息
    List<AgentTheme> getAgentThemesByAgentId(Long agentId);

    //批量新增
    void insertBatch(List<AgentTheme> agentThemes);
}
