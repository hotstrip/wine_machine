package com.seestech.sell.service;


import com.seestech.sell.domain.model.UserRole;

/**
 * Created by idiot on 2016/12/19.
 */
public interface IUserRoleService {
    //新增
    void insert(UserRole userRole);

    //移除userRole
    void remove(UserRole userRole);
}
