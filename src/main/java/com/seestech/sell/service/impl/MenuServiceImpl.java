package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.domain.mapper.IMenuDao;
import com.seestech.sell.domain.model.Menu;
import com.seestech.sell.service.IMenuService;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by idiot on 2016/12/13.
 */
@Service
public class MenuServiceImpl implements IMenuService {
    @Resource
    private IMenuDao menuDao;


    @Override
    public List<String> getPermissionsByUserId(long userId) {
        List<Menu> list = menuDao.getMenusByUserId(userId);
        List<String> permissions = new ArrayList<String>();
        if(list != null && !list.isEmpty()){
            for (Menu menu : list) {
                if(StringUtils.isNotBlank(menu.getPermission()))
                    permissions.add(menu.getPermission());
            }
        }
        return permissions;
    }

    @Override
    public List<Menu> getMenusByParentId(long parentId) {
        return menuDao.getMenusByParentId(parentId);
    }

    @Override
    public List<Menu> getFirstLevelMenusByUserId(long userId) {
        return menuDao.getFirstLevelMenusByUserId(userId);
    }

    @Override
    public Page<Menu> getAllPageMenus(RowBounds rowBounds, Menu info) {
        Page<Menu> list = menuDao.getAllPageMenus(rowBounds, info);
        //遍历集合  获取父级菜单名称
        for(Menu menu : list){
            if(menu.getParentId() != null){
                Menu parentMenu = menuDao.getMenuByMenuId(menu.getParentId());
                menu.setParentName(parentMenu.getMenuName());
            }
        }
        return list;
    }

    @Override
    public void insert(Menu info) {
        if (info.getMenuId() == null)
            info.setMenuId(IdGen.get().nextId());   //设置menuId
        if (info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(menuDao.insert(info) < 1){
            throw new MyException("新增菜单信息失败");
        }
    }

    @Override
    public void update(Menu info) {
        if (info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(menuDao.update(info) < 1){
            throw new MyException("修改菜单信息失败");
        }
    }

    @Override
    public Menu getMenuByMenuId(long menuId) {
        return menuDao.getMenuByMenuId(menuId);
    }

    @Override
    public void delete(long menuId) {
        if(menuDao.delete(menuId) < 1){
            throw new MyException("删除菜单信息失败");
        }
    }

    @Override
    public List<Menu> getAllMenus() {
        return menuDao.getAllMenus();
    }

    @Override
    public List<Menu> getAllFirstMenus() {
        return menuDao.getAllfirstMenus();
    }

    @Override
    public List<Menu> getMenusByRoleId(Long roleId) {
        return menuDao.getMenusByRoleId(roleId);
    }

    @Override
    public List<Menu> getMultiLevelMenus() {
        //获取一级资源信息
        List<Menu> firstMenus = menuDao.getAllfirstMenus();
        for (Menu first : firstMenus){
            List<Menu> menus = menuDao.getMenusByParentId(first.getMenuId());
            first.setListMenus(menus);
        }
        return firstMenus;
    }

    @Override
    public List<Menu> getMenusByParentIdAndUserId(Long menuId, Long userId) {
        return menuDao.getMenusByParentIdAndUserId(menuId, userId);
    }
}
