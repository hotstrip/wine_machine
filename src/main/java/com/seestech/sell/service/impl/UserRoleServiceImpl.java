package com.seestech.sell.service.impl;


import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IUserRoleDao;
import com.seestech.sell.domain.model.UserRole;
import com.seestech.sell.service.IUserRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by idiot on 2016/12/19.
 */
@Service
public class UserRoleServiceImpl implements IUserRoleService {
    @Resource
    private IUserRoleDao userRoleDao;

    @Override
    public void insert(UserRole info) {
        if (info.getUserRoleId() == null)
            info.setUserRoleId(IdGen.get().nextId());
        if (info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(userRoleDao.insert(info) < 1){
            throw new MyException("新增用户角色关系失败");
        }
    }


    @Override
    public void remove(UserRole userRole) {
        UserRole info = userRoleDao.getUserRoleByUserRole(userRole);
        if(info != null){
            if(userRoleDao.delete(info.getUserRoleId()) < 1){
                throw new MyException("删除用户角色信息失败");
            }
        }else
            throw new MyException("该用户角色信息不存在");
    }
}
