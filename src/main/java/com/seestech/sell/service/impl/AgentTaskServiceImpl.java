package com.seestech.sell.service.impl;

import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IAgentTaskDao;
import com.seestech.sell.domain.model.AgentTaskInfo;
import com.seestech.sell.service.IAgentTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by idiot on 2017/5/27.
 */
@Service
public class AgentTaskServiceImpl implements IAgentTaskService {
    private static Logger logger = LoggerFactory.getLogger(AgentTaskServiceImpl.class);

    @Resource
    private IAgentTaskDao agentTaskDao;

    @Override
    public void insert(AgentTaskInfo info) {
        if(info.getTaskId() == null)
            info.setTaskId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(agentTaskDao.insert(info) < 1){
            logger.error("新增任务信息失败");
            throw new MyException("新增任务信息失败");
        }
    }

    @Override
    public void update(AgentTaskInfo info) {
        if(agentTaskDao.update(info) < 1){
            logger.error("修改任务信息失败");
            throw new MyException("修改任务信息失败");
        }
    }

    @Override
    public List<AgentTaskInfo> getAgentTasks(AgentTaskInfo info) {
        return agentTaskDao.getAgentTasks(info);
    }

    @Override
    public void insertBatch(List<AgentTaskInfo> agentTasks) {
        if(agentTaskDao.insertBatch(agentTasks) < 1){
            logger.error("批量新增终端任务信息失败.....");
            throw new MyException("批量新增终端任务信息失败");
        }
    }

}
