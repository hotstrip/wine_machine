package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.common.utils.StringUtils;
import com.seestech.sell.domain.mapper.IAgentDao;
import com.seestech.sell.domain.mapper.IRecorderDao;
import com.seestech.sell.domain.model.Agent;
import com.seestech.sell.domain.model.Recorder;
import com.seestech.sell.service.IRecorderService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by idiot on 2017/6/22.
 */
@Service
public class RecorderServiceImpl implements IRecorderService {
    private Logger logger = LoggerFactory.getLogger(RecorderServiceImpl.class);

    @Resource
    private IRecorderDao recorderDao;

    @Override
    public Page<Recorder> getAllRecorders(RowBounds rowBounds, Recorder info) {
        return recorderDao.getAllRecorders(rowBounds, info);
    }

    @Override
    public Page<Recorder> getRecorders(RowBounds rowBounds, Recorder info) {
        return recorderDao.getRecorders(rowBounds, info);
    }

    @Override
    public void delete(Long recorderId) {
        if(recorderDao.delete(recorderId) < 1){
            logger.error("删除记录信息失败");
            throw new MyException("删除记录信息失败");
        }
    }

    @Override
    public void deleteBatch(Long[] recorderIds) {
        if(recorderDao.deleteBatch(recorderIds) < 1){
            logger.error("批量删除记录信息失败");
            throw new MyException("批量删除记录信息失败");
        }
    }

    @Override
    public List<Recorder> getRecordersByAgentId(Long agentId) {
        return recorderDao.getRecordersByAgentId(agentId);
    }

    @Override
    public void insert(Recorder info) {
        if(info.getRecorderId() == null){
            info.setRecorderId(IdGen.get().nextId());
        }
        if(info.getCreateTime() == null){
            info.setCreateTime(new Date());
        }
        if(recorderDao.insert(info) < 1){
            logger.error("新增库存记录信息失败");
            throw new MyException("新增库存记录信息失败");
        }
    }

    @Override
    public void update(Recorder info) {
        if(info.getUpdateTime() == null){
            info.setUpdateTime(new Date());
        }
        if(recorderDao.update(info) < 1){
            logger.error("修改库存记录信息失败");
            throw new MyException("修改库存记录信息失败");
        }
    }

    @Override
    public List<Recorder> getRecordersByIds(Long agentId, Long shelfId, Long goodsId) {
        return recorderDao.getRecordersByIds(agentId, shelfId, goodsId);
    }

    @Override
    public Recorder getRecorderByRecorderId(Long recorderId) {
        return recorderDao.getRecorderByRecorderId(recorderId);
    }

    @Override
    public List<Recorder> getRecordersByInfo(Recorder recorder) {
        return recorderDao.getRecordersByInfo(recorder);
    }

    @Override
    public Recorder getRecordersByRecorder(Recorder recorder) {
        return recorderDao.getRecordersByRecorder(recorder);
    }

    @Override
    public boolean modify(String[] products, Recorder recorder, Object[] stockses, String[] stocks) {
        //遍历商品编号   添加商品记录信息
        logger.info("修改库存信息========>>>>>>>>>>>开始");
        //数组转字符串    此时的各种商品库存
        String remain = StringUtils.arrayToString(stockses, Constants.CsMessageSeperator.semicolon);
        try {
            for (int i = 0; i < products.length; i++) {
                recorder.setRecorderId(IdGen.get().nextId());   //设置主键
                recorder.setGoodsId(Long.parseLong(products[i]));        //设置商品编号
                recorder.setAmount(Integer.parseInt(String.valueOf(stockses[i])));      //设置当前商品库存
                recorder.setTotal(Integer.parseInt(stocks[i]));         //设置该商品在货架中的总库存
                recorder.setCreateTime(new Date());
                if(Integer.parseInt(String.valueOf(stockses[i])) == Integer.parseInt(stocks[i])){
                    recorder.setFull(1);
                }
                recorder.setRemain(remain);     //剩余库存
                if(recorderDao.insert(recorder) < 1){
                    logger.error("添加商品记录信息失败");
                }
            }
            return true;
        } catch (Exception e){
            logger.info("修改库存信息========>>>>>>>>>>>出错：【原因】"+e.getMessage());
            return false;
        }
    }

    @Override
    public Page<Recorder> getRecordersGroupByBatchNumber(RowBounds rowBounds, Recorder info) {
        return recorderDao.getRecordersGroupByBatchNumber(rowBounds, info);
    }

    @Override
    public List<Recorder> getRecordersByBatchNumber(String batchNumber) {
        return recorderDao.getRecordersByBatchNumber(batchNumber);
    }
}
