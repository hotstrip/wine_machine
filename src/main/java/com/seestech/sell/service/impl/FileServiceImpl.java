package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.DateUtils;
import com.seestech.sell.common.utils.FileUtils;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IFileDao;
import com.seestech.sell.domain.model.FileInfo;
import com.seestech.sell.domain.model.enums.StatusEnums;
import com.seestech.sell.service.IDirectoryService;
import com.seestech.sell.service.IFileService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2016/12/22.
 */
@Service
public class FileServiceImpl implements IFileService {
    private static Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);
    @Resource
    private IFileDao fileDao;
    @Resource
    private IDirectoryService directoryService;

    @Override
    public Page<FileInfo> getFilesByFileInfo(RowBounds rowBounds, FileInfo info) {
        return fileDao.getFilesByFileInfo(rowBounds, info);
    }

    @Override
    public void upload(FileInfo fileInfo) {
        if(fileInfo.getFileId() == null)
            fileInfo.setFileId(IdGen.get().nextId());   //设置文件编号
        if(fileInfo.getCreateTime() == null)
            fileInfo.setCreateTime(DateUtils.currentDateTime());
        fileInfo.setStatus(StatusEnums.VALID.getValue());
        if(fileDao.insert(fileInfo) < 1){
            throw new MyException("上传文件出错");
        }
    }

    @Override
    public List<FileInfo> getAllValidFiles(RowBounds rowBounds) {
        return fileDao.getAllValidFies(rowBounds);
    }

    @Override
    public String insert(FileInfo fileInfo) {
        Long fileId = IdGen.get().nextId();
        if(fileInfo.getFileId() == null)
            fileInfo.setFileId(fileId);   //设置id
        else
            fileId = fileInfo.getFileId();
        if(fileInfo.getCreateTime() == null)
            fileInfo.setCreateTime(DateUtils.currentDateTime());
        if(fileDao.insert(fileInfo) < 1){
            logger.error("新增文件信息失败");
            return new String();
        }
        return fileId.toString();
    }

    @Override
    public boolean update(FileInfo fileInfo) {
        if(fileInfo.getUpdateTime() == null)
            fileInfo.setUpdateTime(DateUtils.currentDateTime());
        if(fileDao.update(fileInfo) < 1){
            logger.error("修改文件信息失败");
            return false;
        }
        return true;
    }

    @Override
    public FileInfo getFileByFileId(Long fileId) {
        return fileDao.getFileByFileId(fileId);
    }

    @Override
    public String getFilePath(Long fileId) {
        String path = new String();
        //获取文件信息
        FileInfo fileInfo = fileDao.getFileByFileId(fileId);
        if(fileInfo != null){
            //获取目录路径
            String directoryPath = directoryService.getDirectoryPathBydirectoryId(fileInfo.getDirectoryId());
            path = FileUtils.addPathSeparate(directoryPath) + fileInfo.getDirectoryPath();
        }
        return path;
    }

    @Override
    public List<FileInfo> getAllValidFilesByDirectoryId(Long directoryId) {
        return fileDao.getAllValidFiesByDirectoryId(directoryId);
    }

    @Override
    public void delete(Long fileId) {
        if(fileDao.delete(fileId) < 1){
            logger.error("删除文件信息失败");
            throw new MyException("删除文件信息失败");
        }
    }

    @Override
    public int insertBatch(List<FileInfo> list) {
        return fileDao.insertBatch(list);
    }

    @Override
    public void deleteBatch(Long[] fileIds) {
        if(fileDao.deleteBatch(fileIds) < 1){
            logger.error("批量删除文件信息失败");
            throw new MyException("批量删除文件信息失败");
        }
    }

    @Override
    public void deleteFilesByDirectoryId(Long directoryId) {
        List<FileInfo> list = fileDao.getAllValidFiesByDirectoryId(directoryId);
        if(list != null && list.size() > 0) {
            if (fileDao.deleteFilesByDirectoryId(directoryId) < 1) {
                logger.error("根据目录删除文件信息失败");
                throw new MyException("根据目录删除文件信息失败");
            }
        }
    }

}
