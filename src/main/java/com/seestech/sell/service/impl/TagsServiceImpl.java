package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.ITagsDao;
import com.seestech.sell.domain.model.Tags;
import com.seestech.sell.service.ITagsService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by idiot on 2017/8/14.
 * @description  标签处理
 */
@Service
public class TagsServiceImpl implements ITagsService {
    private static Logger logger = LoggerFactory.getLogger(TagsServiceImpl.class);

    @Resource
    private ITagsDao tagsDao;

    @Override
    public void insert(Tags info) {
        if(info.getId() == null)
            info.setId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(tagsDao.insert(info) < 1){
            logger.error("新增标签信息失败....");
            throw new MyException("新增标签信息失败....");
        }
    }

    @Override
    public void update(Tags info) {
        if(info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(tagsDao.update(info) < 1){
            logger.error("修改标签信息失败....");
            throw new MyException("修改标签信息失败....");
        }
    }

    @Override
    public void delete(Long id) {
        if(tagsDao.delete(id) < 1){
            logger.error("删除标签信息失败....");
            throw new MyException("删除标签信息失败....");
        }
    }

    @Override
    public Page<Tags> getTagsByTags(RowBounds rowBounds, Tags info) {
        return tagsDao.getTagsByTags(rowBounds, info);
    }

    @Override
    public Tags getTagByTagName(String tagName) {
        return tagsDao.getTagByTagName(tagName);
    }

    @Override
    public List<Tags> getTagsByTagName(String tagName) {
        return tagsDao.getTagsByTagName(tagName);
    }

    @Override
    public Tags getTagsById(Long id) {
        return tagsDao.getTagsById(id);
    }

    @Override
    public void deleteBatch(Long[] ids) {
        if(tagsDao.deleteBatch(ids) < 1){
            logger.error("批量删除标签信息失败....");
            throw new MyException("批量删除标签信息失败....");
        }
    }
}
