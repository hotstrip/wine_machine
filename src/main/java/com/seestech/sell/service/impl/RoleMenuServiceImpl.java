package com.seestech.sell.service.impl;


import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IRoleMenuDao;
import com.seestech.sell.domain.model.RoleMenu;
import com.seestech.sell.service.IRoleMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by idiot on 2016/12/19.
 */
@Service
public class RoleMenuServiceImpl implements IRoleMenuService {
    @Resource
    private IRoleMenuDao roleMenuDao;

    @Override
    public void insert(RoleMenu info) {
        if (info.getRoleMenuId() == null)
            info.setRoleMenuId(IdGen.get().nextId());
        if (info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(roleMenuDao.insert(info) < 1){
            throw new MyException("新增角色菜单信息失败");
        }
    }

    @Override
    public void remove(RoleMenu roleMenu) {
        RoleMenu info = roleMenuDao.getRoleMenuByRoleMenu(roleMenu);
        if(info != null){
            if(roleMenuDao.delete(info.getRoleMenuId()) < 1){
                throw new MyException("删除角色菜单信息失败");
            }
        }else
            throw new MyException("删除角色菜单信息失败");
    }
}
