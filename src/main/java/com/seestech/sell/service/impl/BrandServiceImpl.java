package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IBrandDao;
import com.seestech.sell.domain.model.Brand;
import com.seestech.sell.service.IBrandService;
import org.apache.ibatis.executor.ErrorContext;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by idiot on 2017/6/17.
 */
@Service
public class BrandServiceImpl implements IBrandService {
    private static Logger logger = LoggerFactory.getLogger(BrandServiceImpl.class);

    @Resource
    private IBrandDao brandDao;

    @Override
    public Page<Brand> getAllBrands(RowBounds rowBounds, Brand info) {
        return brandDao.getAllBrands(rowBounds, info);
    }

    @Override
    public void insert(Brand info) {
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(info.getBrandId() == null)
            info.setBrandId(IdGen.get().nextId());
        if(brandDao.insert(info) < 1){
            logger.error("新增品牌信息失败");
            throw new MyException("新增品牌信息失败");
        }
    }

    @Override
    public void update(Brand info) {
        if(info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(brandDao.update(info) < 1){
            logger.error("修改品牌信息失败");
            throw new MyException("修改品牌信息失败");
        }
    }

    @Override
    public Brand getBrandByBrandId(Long brandId) {
        return brandDao.getBrandByBrandId(brandId);
    }

    @Override
    public void delete(Long brandId) {
        if(brandDao.delete(brandId) < 1){
            logger.error("删除品牌信息失败");
            throw new MyException("删除品牌信息失败");
        }
    }

    @Override
    public void deleteBatch(Long[] brandIds) {
        if(brandDao.deleteBatch(brandIds) < 1){
            logger.error("批量删除品牌信息失败");
            throw new MyException("批量删除品牌信息失败");
        }
    }

    @Override
    public List<Brand> getBrands(Brand info) {
        return brandDao.getBrands(info);
    }
}
