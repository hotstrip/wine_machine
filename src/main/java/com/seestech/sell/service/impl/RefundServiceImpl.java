package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IRefundDao;
import com.seestech.sell.domain.model.Refund;
import com.seestech.sell.service.IRefundService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by idiot on 2017/7/7.
 */
@Service
public class RefundServiceImpl implements IRefundService {
    private static Logger logger = LoggerFactory.getLogger(RefundServiceImpl.class);

    @Resource
    private IRefundDao refundDao;

    @Override
    public void insert(Refund info) {
        if(info.getRefundId() == null)
            info.setRefundId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(refundDao.insert(info) < 1){
            logger.error("新增退款信息失败");
            throw new MyException("新增退款信息失败");
        }
    }

    @Override
    public void update(Refund info) {
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(refundDao.insert(info) < 1){
            logger.error("修改退款信息失败");
            throw new MyException("修改退款信息失败");
        }
    }

    @Override
    public Page<Refund> getAllRefundes(RowBounds rowBounds, Refund info) {
        return refundDao.getAllRefundes(rowBounds, info);
    }

    @Override
    public void delete(Long refundId) {
        if(refundDao.delete(refundId) < 1){
            logger.error("删除退款信息失败");
            throw new MyException("删除退款信息失败");
        }
    }

    @Override
    public void deleteBatch(Long[] refundIds) {
        if(refundDao.deleteBatch(refundIds) < 1){
            logger.error("批量删除退款信息失败");
            throw new MyException("批量删除退款信息失败");
        }
    }
}
