package com.seestech.sell.service.impl;

import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.ICompanyPayDao;
import com.seestech.sell.domain.model.CompanyPay;
import com.seestech.sell.service.ICompanyPayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by idiot on 2017/9/9.
 */
@Service
public class CompanyPayServiceImpl implements ICompanyPayService {
    private static Logger logger = LoggerFactory.getLogger(CompanyPayServiceImpl.class);

    @Resource
    private ICompanyPayDao companyPayDao;

    @Override
    public void insert(CompanyPay info) {
        if (info.getId() == null)
            info.setId(IdGen.get().nextId());
        if (info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if (companyPayDao.insert(info) < 1){
            logger.error("新增企业支付信息失败....");
            throw new MyException("新增企业支付信息失败....");
        }
    }

    @Override
    public void update(CompanyPay info) {
        if (info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if (companyPayDao.update(info) < 1){
            logger.error("修改企业支付信息失败....");
            throw new MyException("修改企业支付信息失败....");
        }
    }

    @Override
    public void delete(Long id) {
        if (companyPayDao.delete(id) < 1){
            logger.error("删除企业支付信息失败....");
            throw new MyException("删除企业支付信息失败....");
        }
    }

    @Override
    public CompanyPay getCompanyPayById(Long id) {
        return companyPayDao.getCompanyPayById(id);
    }

    @Override
    public CompanyPay getCompanyPayByCompanyId(Long companyId) {
        return companyPayDao.getCompanyPayByCompanyId(companyId);
    }

    @Override
    public CompanyPay getCompanyPayByOrderId(Long orderId) {
        return companyPayDao.getCompanyPayByOrderId(orderId);
    }

    @Override
    public CompanyPay getCompanyPayByAgentId(Long agentId) {
        return companyPayDao.getCompanyPayByAgentId(agentId);
    }
}
