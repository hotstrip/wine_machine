package com.seestech.sell.service.impl;


import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.DateUtils;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IPublishHistoryDao;
import com.seestech.sell.domain.model.PublishHistory;
import com.seestech.sell.domain.model.enums.StatusEnums;
import com.seestech.sell.service.IPublishHistoryService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by idiot on 2017/1/10.
 * @description 发布历史
 */
@Service
public class PublishHistoryServiceImpl implements IPublishHistoryService {
    private static Logger logger = LoggerFactory.getLogger(PublishHistoryServiceImpl.class);

    @Resource
    private IPublishHistoryDao publishHistoryDao;

    @Override
    public void insert(PublishHistory info) {
        if(info.getPublishHistoryId() == null)
            info.setPublishHistoryId(IdGen.get().nextId());
        if(info.getPublishTime() == null)
            info.setPublishTime(DateUtils.currentDateTime());
        if(info.getStatus() == null)
            info.setStatus(StatusEnums.VALID.getValue());
        if(publishHistoryDao.insert(info) < 1){
            logger.error("新增发布历史记录失败");
        }
    }

    @Override
    public List<PublishHistory> getAllPublishHistories(RowBounds rowBounds, PublishHistory info) {
        return publishHistoryDao.getAllPublishHistories(rowBounds, info);
    }

    @Override
    public PublishHistory getPublishHistoryById(Long publishHistoryId) {
        return publishHistoryDao.getPublishHistoryById(publishHistoryId);
    }

    @Override
    public void deleteBatch(Long[] publishHistoryIds) {
        if(publishHistoryDao.deleteBatch(publishHistoryIds) < 1){
            logger.error("批量删除发布历史信息失败");
            throw new MyException("批量删除发布历史信息失败");
        }
    }

    @Override
    public void delete(Long publishHistoryId) {
        if(publishHistoryDao.delete(publishHistoryId) < 1){
            logger.error("删除发布历史信息失败");
            throw new MyException("删除发布历史信息失败");
        }
    }

    @Override
    public List<PublishHistory> getPublishHistoriesByInfo(PublishHistory info) {
        return publishHistoryDao.getPublishHistoriesByInfo(info);
    }

    @Override
    public void insertBatch(List<PublishHistory> histories) {
        if(publishHistoryDao.insertBatch(histories) < 1){
            logger.error("批量新增发布历史信息失败.....");
            throw new MyException("批量新增发布历史信息失败");
        }
    }
}
