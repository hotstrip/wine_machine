package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.FileUtils;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.common.utils.ResponseUtils;
import com.seestech.sell.domain.mapper.IPublishDao;
import com.seestech.sell.domain.model.Publish;
import com.seestech.sell.domain.model.ThemeColumn;
import com.seestech.sell.domain.model.enums.PublishTypeEnums;
import com.seestech.sell.service.IPublishService;
import com.seestech.sell.service.IThemeColumnService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Created by idiot on 2017/8/1.
 */
@Service
public class PublishServiceImpl implements IPublishService {
    private static Logger logger = LoggerFactory.getLogger(PublishServiceImpl.class);

    @Resource
    private IPublishDao publishDao;
    @Resource
    private IThemeColumnService themeColumnService;

    @Override
    public void insert(Publish info) {
        if(info.getPublishId() == null)
            info.setPublishId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(publishDao.insert(info) < 1){
            logger.error("新增发布信息失败.......");
            throw new MyException("新增发布信息失败.......");
        }
    }

    @Override
    public void update(Publish info) {
        if(info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(publishDao.update(info) < 1){
            logger.error("修改发布信息失败.......");
            throw new MyException("修改发布信息失败.......");
        }
    }

    @Override
    public void delete(Long publishId) {
        if(publishDao.delete(publishId) < 1){
            logger.error("删除发布信息失败.......");
            throw new MyException("删除发布信息失败.......");
        }
    }

    @Override
    public void deleteBatch(Long[] publishIds) {
        if(publishDao.deleteBatch(publishIds) < 1){
            logger.error("批量删除发布信息失败.......");
            throw new MyException("批量删除发布信息失败.......");
        }
    }

    @Override
    public Page<Publish> getPublishList(RowBounds rowBounds, Publish info) {
        return publishDao.getPublishList(rowBounds, info);
    }

    @Transactional
    @Override
    public boolean addTheme(Publish info, List<ThemeColumn> themeColumns) {
        //设置主键
        if(info.getPublishId() == null)
            info.setPublishId(IdGen.get().nextId());
        insert(info);
        //根据主键获取信息  判断是否存在
        Publish publish = getPublishByPublishId(info.getPublishId());
        if(publish != null){
            //新增主题栏目信息
            for (ThemeColumn item : themeColumns){
                item.setThemeColumnId(IdGen.get().nextId());    //主键
                item.setThemeId(info.getPublishId());           //主题编号
                item.setCreateTime(new Date());
            }
            if(themeColumnService.insertBatch(themeColumns))
                return true;
        }
        return false;
    }

    @Override
    public Publish getPublishByPublishId(Long publishId) {
        return publishDao.getPublishByPublishId(publishId);
    }

    @Override
    public boolean addInfo(Publish info) {
        if(info.getPublishId() == null)
            info.setPublishId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(publishDao.insert(info) < 1){
            return false;
        }
        return true;
    }

    @Override
    public boolean updateInfo(Publish info) {
        if(info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(publishDao.update(info) < 1){
            return false;
        }
        return true;
    }

    @Override
    public String remove(Publish info, String baseDirectory) {
        String path, result;
        if(info != null){
            if(PublishTypeEnums.THEME.getValue().equals(info.getType())){
                //删除主题   删除  主题绑定的栏目  以及  文件
                path = FileUtils.getWindowsPath(FileUtils.addPathSeparate(baseDirectory, Constants.Regular.my_themes, info.getPath()));
                FileUtils.deleteDirectory(new File(path));
                //删除发布信息
                delete(info.getPublishId());
                List<ThemeColumn> list = themeColumnService.getThemeColumnsByThemeId(info.getPublishId());
                //删除主题对应的主题栏目信息
                if(list != null && list.size() > 0){
                    themeColumnService.deleteBatch(list);
                }
                result = ResponseUtils.responseSuccess("删除主题信息成功");
            }else if(PublishTypeEnums.COLUMN.getValue().equals(info.getType())){
                //删除栏目   删除 文件
                path = FileUtils.getWindowsPath(FileUtils.addPathSeparate(baseDirectory, Constants.Regular.my_columns, info.getPath()));
                FileUtils.deleteDirectory(new File(path));
                //删除发布信息
                delete(info.getPublishId());
                result = ResponseUtils.responseSuccess("删除栏目信息成功");
            }else if(PublishTypeEnums.ZIP.getValue().equals(info.getType())){
                //删除zip
                delete(info.getPublishId());
                result = ResponseUtils.responseSuccess("删除zip信息成功");
            }else if(PublishTypeEnums.VIDEO.getValue().equals(info.getType())){
                //删除video
                delete(info.getPublishId());
                result = ResponseUtils.responseSuccess("删除视频信息成功");
            }else if(PublishTypeEnums.LED.getValue().equals(info.getType())){
                //删除字幕
                delete(info.getPublishId());
                result = ResponseUtils.responseSuccess("删除字幕信息成功");
            }else {
                logger.error("===========>>>>>>无效的发布类型"+info.getType());
                result = ResponseUtils.responseError("无效的发布类型");
            }
        }else {
            logger.error("===========>>>>>>无效的发布信息");
            result = ResponseUtils.responseError("无效的发布信息");
        }
        return result;
    }

    @Override
    public List<Publish> getPublishByPublishIds(Long[] publishIds) {
        return publishDao.getPublishByPublishIds(publishIds);
    }

    @Override
    public List<Publish> getPublishByPublishName(String name) {
        return publishDao.getPublishByPublishName(name);
    }
}
