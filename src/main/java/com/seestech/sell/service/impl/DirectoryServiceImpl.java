package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.FileUtils;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IDirectoryDao;
import com.seestech.sell.domain.model.Directory;
import com.seestech.sell.domain.model.enums.IsRootEnums;
import com.seestech.sell.service.IDirectoryService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.awt.SystemColor.info;

/**
 * Created by idiot on 2016/12/22.
 */
@Service
public class DirectoryServiceImpl implements IDirectoryService {
    private static Logger logger = LoggerFactory.getLogger(DirectoryServiceImpl.class);
    @Resource
    private IDirectoryDao directoryDao;

    @Override
    public String insert(Directory info) {
        Long directoryId = IdGen.get().nextId();
        info.setDirectoryId(directoryId);      //设置目录编号
        info.setCreateTime(new Date());        //设置创建时间
        if(info.getParentDirectoryId() != null){
            info.setIsRoot(IsRootEnums.NO.getValue());
        }else {
            info.setIsRoot(IsRootEnums.YES.getValue());
        }
        if(directoryDao.insert(info) < 1){
            logger.error("新增目录信息失败");
            return new String();
        }
        return directoryId.toString();
    }

    @Override
    public void update(Directory info) {
        if(directoryDao.update(info) < 1){
            throw new MyException("修改目录信息失败");
        }
    }


    @Override
    public Directory getDirectoryByDirectoryId(Long directoryId) {
        return directoryDao.getDirectoryByDirectoryId(directoryId);
    }

    @Override
    public List<Directory> getAllDirectories(RowBounds rowBounds) {
        return directoryDao.getAllDirectories(rowBounds);
    }


    @Override
    public String getDirectoryPathBydirectoryId(Long directoryId) {
        //根据目录编号获取目录完整路径  递归
        StringBuffer buffer = new StringBuffer();
        /************************************/
        //获取目录信息
        Directory directory = directoryDao.getDirectoryByDirectoryId(directoryId);
        if(directory != null){
            //如果该目录是子目录
            if(directory.getParentDirectoryId() != null){
                buffer.append(directory.getPathCode());
                buffer.append(Constants.CsMessageSeperator.semicolon);
                String parentPath = getDirectoryPathBydirectoryId(directory.getParentDirectoryId());
                buffer.append(parentPath);
            }else {
                //如果该目录是根目录
                buffer.append(directory.getPathCode());
            }
        }
        String[] array = buffer.toString().split(Constants.CsMessageSeperator.semicolon);
        return FileUtils.reverseArray(array);
    }

    @Override
    public void delete(Long directoryId) {
        if(directoryDao.delete(directoryId) < 1){
            logger.error("删除目录信息失败");
        }
    }

    @Override
    public List<Directory> getAllValidBusinessDirectories(Long directoryTypeId) {
        return directoryDao.getAllValidBusinessDirectories(directoryTypeId);
    }

    @Override
    public List<Directory> getValidParentDiretories() {
        return directoryDao.getValidParentDiretories();
    }

    @Override
    public List<Directory> recursionDirectoryByList(List<Directory> firstLevelDirectories) {
        List<Directory> list = new ArrayList<>();
        for (Directory directory : firstLevelDirectories){
            list.add(directory);    //添加到集合
            //递归
            List<Directory> nextLevel = directoryDao.getValidDirectoryByParentDirectoryId(directory.getDirectoryId());
            list.addAll(recursionDirectoryByList(nextLevel));   //添加到集合
        }
        return list;
    }

    @Override
    public List<Directory> getValidDirectoryByParentDirectoryId(Long parentDirectoryId) {
        return directoryDao.getValidDirectoryByParentDirectoryId(parentDirectoryId);
    }

    @Override
    public List<Directory> getDirectoriesByType(Integer value) {
        return directoryDao.getDirectoriesByType(value);
    }

    @Override
    public Page<Directory> getDirectoriesByDirectory(Directory info) {
        return directoryDao.getDirectoriesByDirectory(info);
    }

    @Override
    public int getLevelByDirectoryId(Long directoryId) {
        int level = 1;
        //获取当前目录信息
        Directory info = directoryDao.getDirectoryByDirectoryId(directoryId);
        if(info != null && info.getParentDirectoryId() != null){
            level += getLevelByDirectoryId(info.getParentDirectoryId());
        }
        return level;
    }

}
