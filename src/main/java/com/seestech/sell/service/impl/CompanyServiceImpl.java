package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.ICompanyDao;
import com.seestech.sell.domain.model.Company;
import com.seestech.sell.service.ICompanyService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by idiot on 2017/8/7.
 */
@Service
public class CompanyServiceImpl implements ICompanyService {
    private static Logger logger = LoggerFactory.getLogger(CompanyServiceImpl.class);

    @Resource
    private ICompanyDao companyDao;

    @Override
    public void insert(Company info) {
        if(info.getId() == null)
            info.setId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(companyDao.insert(info) < 1){
            logger.error("新增组织机构信息失败...");
            throw new MyException("新增组织机构信息失败...");
        }
    }

    @Override
    public void update(Company info) {
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(companyDao.update(info) < 1){
            logger.error("修改组织机构信息失败...");
            throw new MyException("修改组织机构信息失败...");
        }
    }

    @Override
    public void delete(Long id) {
        if(companyDao.delete(id) < 1){
            logger.error("删除组织机构信息失败...");
            throw new MyException("删除组织机构信息失败...");
        }
    }

    @Override
    public Page<Company> getCompanies(RowBounds rowBounds, Company info) {
        return companyDao.getCompanies(rowBounds, info);
    }

    @Override
    public Company getCompanyById(Long id) {
        return companyDao.getCompanyById(id);
    }

    @Override
    public List<Company> getAllCompany(Company info) {
        return companyDao.getAllCompany(info);
    }

    @Override
    public Company getCompanyByName(String name) {
        return companyDao.getCompanyByName(name);
    }

    @Override
    public List<Long> getCompanyByUserId(Long userId) {
        return companyDao.getCompanyByUserId(userId);
    }

    @Override
    public List<Company> getManageCompanyByUserId(Long userId) {
        return companyDao.getManageCompanyByUserId(userId);
    }
}
