package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.domain.mapper.IScreenshotsDao;
import com.seestech.sell.domain.model.Screenshots;
import com.seestech.sell.service.IScreenShotsService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017/2/8.
 */
@Service
public class ScreenshotsServiceImpl implements IScreenShotsService {
    private static Logger logger = LoggerFactory.getLogger(ScreenshotsServiceImpl.class);

    @Resource
    private IScreenshotsDao screenshotsDao;


    @Override
    public Page<Screenshots> getScreenshotsByAgentCode(RowBounds rowBounds, String agentCode) {
        return screenshotsDao.getScreenshotsByAgentCode(rowBounds, agentCode);
    }

    @Override
    public void deleteBatch(Long[] screenshotsIds) {
        if(screenshotsDao.deleteBatch(screenshotsIds) < 1){
            logger.info("批量删除截屏信息失败");
            throw new MyException("批量删除截屏信息失败");
        }
    }
}
