package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IAgentDao;
import com.seestech.sell.domain.model.Agent;
import com.seestech.sell.domain.model.vo.AgentVoExcel;
import com.seestech.sell.service.IAgentService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by idiot on 2017/6/21.
 */
@Service
public class AgentServiceImpl implements IAgentService {
    private static Logger logger = LoggerFactory.getLogger(AgentServiceImpl.class);

    @Resource
    private IAgentDao agentDao;

    @Override
    public void insert(Agent info) {
        if(info.getAgentId() == null)
            info.setAgentId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(agentDao.insert(info) < 1){
            logger.error("新增终端信息失败");
            throw new MyException("新增终端信息失败");
        }
    }

    @Override
    public void update(Agent info) {
        if(info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(agentDao.update(info) < 1){
            logger.error("修改终端信息失败");
            throw new MyException("修改终端信息失败");
        }
    }

    @Override
    public Agent getAgentByAgentId(long agentId) {
        return agentDao.getAgentByAgentId(agentId);
    }

    @Override
    public void delete(long agentId) {
        if(agentDao.delete(agentId) < 1){
            logger.error("删除终端信息失败");
            throw new MyException("删除终端信息失败");
        }
    }

    @Override
    public Page<Agent> getAllAgentsByUserId(RowBounds rowBounds, Agent info) {
        return agentDao.getAllAgentsByUserId(rowBounds, info);
    }

    @Override
    public void deleteBatch(Long[] agentIds) {
        if(agentDao.deleteBatch(agentIds) < 1){
            logger.error("批量删除终端信息成功");
            throw new MyException("批量删除终端信息成功");
        }
    }

    @Override
    public List<Agent> getAgentsByAgentGroupId(long agentGroupId, RowBounds rowBounds) {
        return agentDao.getAgentsByAgentGroupId(agentGroupId, rowBounds);
    }

    @Override
    public List<Agent> getAgentsByAgentGroupIds(Long[] agentGroupIds) {
        return agentDao.getAgentsByAgentGroupIds(agentGroupIds);
    }

    @Override
    public Page<Agent> getAllAgentsScreenshots(RowBounds rowBounds, Agent agent) {
        return agentDao.getAllAgentsScreenshots(rowBounds, agent);
    }

    @Override
    public List<Agent> getAllAgents(Agent info) {
        return agentDao.getAllAgents(info);
    }

    @Override
    public Agent getAgentByAgentCode(String agentCode) {
        return agentDao.getAgentByAgentCode(agentCode);
    }

    @Override
    public List<Agent> getAgentsByShelfIds(Long[] shelfIds) {
        return agentDao.getAgentsByShelfIds(shelfIds);
    }

    @Override
    public List<Agent> getAgentsByAgentCode(String agentCode) {
        return agentDao.getAgentsByAgentCode(agentCode);
    }

    @Override
    public List<Agent> getAgentsByGridId(Long gridId) {
        return agentDao.getAgentsByGridId(gridId);
    }

    @Override
    public Map<String, Object> getGoodsInfo(Long agentId) {
        return agentDao.getGoodsInfo(agentId);
    }

    @Override
    public void updateBatch(List<Agent> agentList) {
        if(agentDao.updateBatch(agentList) < 1){
            logger.error("批量修改终端失败");
            throw new MyException("批量修改终端失败");
        }
    }

    @Override
    public List<Map<String, Object>> getGoodsInfoByIds(List<Long> ids) {
        return agentDao.getGoodsInfoByIds(ids);
    }

    @Override
    public void insertBatch(List<AgentVoExcel> list) {
        if (list.size() > 0 && agentDao.insertBatch(list) < 1){
            logger.error("批量新增终端信息失败");
        }
    }

    @Override
    public List<AgentVoExcel> getAllExportAgentsByUserId(RowBounds rowBounds, Agent info) {
        return agentDao.getAllExportAgentsByUserId(rowBounds, info);
    }

    @Override
    public List<Agent> getAgentsByAgentTypeIds(Long[] agentTypeIds) {
        return agentDao.getAgentsByAgentTypeIds(agentTypeIds);
    }
}
