package com.seestech.sell.service.impl;


import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.domain.mapper.IUserAgentGroupDao;
import com.seestech.sell.domain.model.UserAgentGroup;
import com.seestech.sell.service.IUserAgentGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by idiot on 2017/1/19.
 */
@Service
public class UserAgentGroupServiceImpl implements IUserAgentGroupService {
    private static Logger logger = LoggerFactory.getLogger(UserAgentGroupServiceImpl.class);

    @Resource
    private IUserAgentGroupDao userAgentGroupDao;

    @Override
    public void insert(UserAgentGroup info) {
        if (info.getUserAgentGroupId() == null)
            info.setUserAgentGroupId(IdGen.get().nextId()); //设置主键
        if (info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(userAgentGroupDao.insert(info) < 1){
            logger.error("新增用户终端分组信息失败");
        }
    }

    @Override
    public void delete(Long userAgentGroupId) {
        if(userAgentGroupDao.delete(userAgentGroupId) < 1){
            logger.error("删除用户终端分组信息失败");
        }
    }

    @Override
    public void remove(UserAgentGroup userAgentGroup) {
        UserAgentGroup info = userAgentGroupDao.getUserAgentGroupByUserAgentGroup(userAgentGroup);
        if(info != null){
            if(userAgentGroupDao.delete(info.getUserAgentGroupId()) < 1){
                throw new MyException("删除用户终端分组信息失败");
            }
        }else
            throw new MyException("删除用户终端分组信息失败");
    }

    @Override
    public void deleteByIds(Long userId, Long[] agentGroupIds) {
        if(userAgentGroupDao.deleteByIds(userId, agentGroupIds) < 1){
            logger.error("批量删除用户终端分组信息失败");
            throw new MyException("批量删除用户终端分组信息失败");
        }
    }
}
