package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IOrderDao;
import com.seestech.sell.domain.model.Order;
import com.seestech.sell.service.IOrderService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Date;
import java.util.List;

/**
 * Created by idiot on 2017/6/22.
 */
@Service
public class OrderServiceImpl implements IOrderService {
    private static Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Resource
    private IOrderDao orderDao;

    @Override
    public void insert(Order info) {
        if(info.getOrderId() == null)
            info.setOrderId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(orderDao.insert(info) < 1){
            logger.error("新增订单信息失败");
            throw new MyException("新增订单信息失败");
        }
    }

    @Override
    public void update(Order info) {
        if(info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(orderDao.update(info) < 1){
            logger.error("修改订单信息失败");
            throw new MyException("修改订单信息失败");
        }
    }

    @Override
    public void delete(Long orderId) {
        if(orderDao.delete(orderId) < 1){
            logger.error("删除订单信息失败");
            throw new MyException("删除订单信息失败");
        }
    }

    @Override
    public void deleteBatch(Long[] orderIds) {
        if(orderDao.deleteBatch(orderIds) < 1){
            logger.error("删除订单信息失败");
            throw new MyException("删除订单信息失败");
        }
    }

    @Override
    public Page<Order> getAllOrders(RowBounds rowBounds, Order info) {
        return orderDao.getAllOrders(rowBounds, info);
    }

    @Override
    public Order getOrderByOrderId(Long orderId) {
        return orderDao.getOrderByOrderId(orderId);
    }

    @Override
    public HashMap getOrdersByCustomerId(String customerId) {
        return orderDao.getOrdersByCustomerId(customerId);
    }

    @Override
    public List<Order> getMonthlySales(Order info) {
        return orderDao.getMonthlySales(info);
    }

    @Override
    public List<Order> getGoodsSales(Order info) {
        return orderDao.getGoodsSales(info);
    }

    @Override
    public List<Order> getAllExportOrders(RowBounds rowBounds, Order info) {
        return orderDao.getAllExportOrders(rowBounds, info);
    }

    @Override
    public List<Order> getSalesByDateformat(Order info) {
        return orderDao.getSalesByDateformat(info);
    }

    @Override
    public List<Order> getOrdersByDateformat(Order info) {
        return orderDao.getOrdersByDateformat(info);
    }

    @Override
    public Page<Order> getRefundAbleOrders(RowBounds rowBounds, Order info) {
        return orderDao.getRefundAbleOrders(rowBounds, info);
    }

    @Override
    public Page<Order> getGoodsRank(RowBounds rowBounds, Order info) {
        return orderDao.getGoodsRank(rowBounds, info);
    }

    @Override
    public Page<Order> getAgentRank(RowBounds rowBounds, Order info) {
        return orderDao.getAgentRank(rowBounds, info);
    }
}
