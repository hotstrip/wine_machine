package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IShelfDetailDao;
import com.seestech.sell.domain.model.Shelf;
import com.seestech.sell.domain.model.ShelfDetail;
import com.seestech.sell.service.IShelfDetailService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by idiot on 2017/6/20.
 */
@Service
public class ShelfDetailServiceImpl implements IShelfDetailService {
    private static Logger logger = LoggerFactory.getLogger(ShelfDetailServiceImpl.class);

    @Resource
    private IShelfDetailDao shelfDetailDao;

    @Override
    public void insert(ShelfDetail info) {
        if(info.getDetailId() == null)
            info.setDetailId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(shelfDetailDao.insert(info) < 1){
            logger.error("新增货架详细信息失败");
            throw new MyException("新增货架详细信息失败");
        }
    }

    @Override
    public void update(ShelfDetail info) {
        if(info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(shelfDetailDao.update(info) < 1){
            logger.error("修改货架详细信息失败");
            throw new MyException("修改货架详细信息失败");
        }
    }

    @Override
    public void delete(Long detailId) {
        if(shelfDetailDao.delete(detailId) < 1){
            logger.error("删除货架详细信息失败");
            throw new MyException("删除货架详细信息失败");
        }
    }

    @Override
    public void deleteBatch(Long[] detailIds) {
        if(shelfDetailDao.deleteBatch(detailIds) < 1){
            logger.error("批量删除货架详细信息失败");
            throw new MyException("批量删除货架详细信息失败");
        }
    }

    @Override
    public List<ShelfDetail> getShelfDetailsByShelfId(Long shelfId) {
        return shelfDetailDao.getShelfDetailsByShelfId(shelfId);
    }

    @Override
    public Page<Shelf> getAllShelfDetails(RowBounds rowBounds, ShelfDetail info) {
        return shelfDetailDao.getAllShelfDetails(rowBounds, info);
    }

    @Override
    public ShelfDetail getShelfDetailByDetailId(Long detailId) {
        return shelfDetailDao.getShelfDetailByDetailId(detailId);
    }

    @Override
    public List<ShelfDetail> getShelfDetailByGridIds(Long[] gridIds) {
        return shelfDetailDao.getShelfDetailByGridIds(gridIds);
    }

    @Override
    public Page<Shelf> getShelfDetails(ShelfDetail info) {
        return shelfDetailDao.getShelfDetails(info);
    }
}
