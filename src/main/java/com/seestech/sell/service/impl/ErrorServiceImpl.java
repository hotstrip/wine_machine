package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IErrorDao;
import com.seestech.sell.domain.model.Error;
import com.seestech.sell.domain.model.vo.ErrorVoExcel;
import com.seestech.sell.service.IErrorService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by idiot on 2017/7/27.
 */
@Service
public class ErrorServiceImpl implements IErrorService {
    private static Logger logger = LoggerFactory.getLogger(ErrorServiceImpl.class);

    @Resource
    private IErrorDao errorDao;

    @Override
    public void insert(Error info) {
        if(info.getId() == null)
            info.setId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(errorDao.insert(info) < 1){
            logger.error("新增异常信息失败......");
            throw new MyException("新增异常信息失败......");
        }
    }

    @Override
    public void update(Error info) {
        if(info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(errorDao.update(info) < 1){
            logger.error("修改异常信息失败......");
            throw new MyException("修改异常信息失败......");
        }
    }

    @Override
    public void delete(Long id) {
        if(errorDao.delete(id) < 1){
            logger.error("删除异常信息失败......");
            throw new MyException("删除异常信息失败......");
        }
    }

    @Override
    public Error getErrorById(Long id) {
        return errorDao.getErrorById(id);
    }

    @Override
    public List<Error> getErrorsByAgentId(Long agentId) {
        return errorDao.getErrorsByAgentId(agentId);
    }

    @Override
    public Page<Error> getAllErrors(RowBounds rowBounds, Error info) {
        return errorDao.getAllErrors(rowBounds, info);
    }

    @Override
    public List<ErrorVoExcel> getAllExportErrors(RowBounds rowBounds, Error info) {
        return errorDao.getAllExportErrors(rowBounds, info);
    }
}
