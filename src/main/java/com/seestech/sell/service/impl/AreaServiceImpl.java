package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IAreaDao;
import com.seestech.sell.domain.model.Area;
import com.seestech.sell.service.IAreaService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * Created by idiot on 2017/6/19.
 */
@Service
public class AreaServiceImpl implements IAreaService {
    private static Logger logger = LoggerFactory.getLogger(AreaServiceImpl.class);

    @Resource
    private IAreaDao areaDao;

    @Override
    public Page<Area> getAllAreas(RowBounds rowBounds, Area info) {
        return areaDao.getAllAreas(rowBounds, info);
    }

    @Override
    public void insert(Area info) {
        if(info.getAreaId() == null)
            info.setAreaId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(areaDao.insert(info) < 1){
            logger.error("新增区域信息失败");
            throw new MyException("新增区域信息失败");
        }
    }

    @Override
    public void update(Area info) {
        if(info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(areaDao.update(info) < 1){
            logger.error("修改区域信息失败");
            throw new MyException("修改区域信息失败");
        }
    }

    @Override
    public Area getAreaByAreaId(Long areaId) {
        return areaDao.getAreaByAreaId(areaId);
    }

    @Override
    public void delete(Long areaId) {
        if(areaDao.delete(areaId) < 1){
            logger.error("删除区域信息失败");
            throw new MyException("删除区域信息失败");
        }
    }

    @Override
    public void deleteBatch(Long[] areaIds) {
        if(areaDao.deleteBatch(areaIds) < 1){
            logger.error("批量删除区域信息失败");
            throw new MyException("批量删除区域信息失败");
        }
    }

    @Override
    public List<Area> getAreas(Area area) {
        return areaDao.getAreas(area);
    }

    @Override
    public List<Area> getAreasByName(String name) {
        return areaDao.getAreasByName(name);
    }
}
