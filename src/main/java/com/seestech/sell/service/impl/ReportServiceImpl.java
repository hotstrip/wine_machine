package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IReportDao;
import com.seestech.sell.domain.model.Report;
import com.seestech.sell.service.IReportService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

import static java.awt.SystemColor.info;

/**
 * Created by idiot on 2017/8/21.
 */
@Service
public class ReportServiceImpl implements IReportService {
    private static Logger logger = LoggerFactory.getLogger(ReportServiceImpl.class);

    @Resource
    private IReportDao reportDao;

    @Override
    public void insert(Report info) {
        if (info.getId() == null)
            info.setId(IdGen.get().nextId());
        if (info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if (reportDao.insert(info) < 1){
            logger.error("新增报告信息失败");
            throw new MyException("新增报告信息失败");
        }
    }

    @Override
    public void update(Report info) {
        if (info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if (reportDao.update(info) < 1){
            logger.error("修改报告信息失败");
            throw new MyException("修改报告信息失败");
        }
    }

    @Override
    public void delete(Long id) {
        if (reportDao.delete(id) < 1){
            logger.error("删除报告信息失败");
            throw new MyException("删除报告信息失败");
        }
    }

    @Override
    public Page<Report> getAllReports(RowBounds rowBounds, Report info) {
        return reportDao.getAllReports(rowBounds, info);
    }
}
