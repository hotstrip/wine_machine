package com.seestech.sell.service.impl;


import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IAgentThemeDao;
import com.seestech.sell.domain.model.AgentTheme;
import com.seestech.sell.service.IAgentThemeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2017/1/13.
 */
@Service
public class AgentThemeServiceImpl implements IAgentThemeService {
    private static Logger logger = LoggerFactory.getLogger(AgentThemeServiceImpl.class);
    @Resource
    private IAgentThemeDao agentThemeDao;

    @Override
    public AgentTheme getValidAgentThemeByAgentId(Long agentId) {
        return agentThemeDao.getValidAgentThemeByAgentId(agentId);
    }

    @Override
    public void insert(AgentTheme agentTheme) {
        agentTheme.setAgentThemeId(IdGen.get().nextId());   //设置主键
        if(agentThemeDao.insert(agentTheme) < 1){
            logger.error("新增终端主题信息失败");
        }
    }

    @Override
    public void update(AgentTheme agentTheme) {
        if(agentThemeDao.update(agentTheme) < 1){
            logger.error("新增终端主题信息失败");
        }
    }

    @Override
    public List<AgentTheme> getAgentThemesByThemeIdAndAgentId(Long agentId, Long themeId) {
        return agentThemeDao.getAgentThemesByThemeIdAndAgentId(agentId, themeId);
    }

    @Override
    public List<AgentTheme> getAgentThemesByAgentId(Long agentId) {
        return agentThemeDao.getAgentThemesByAgentId(agentId);
    }

    @Override
    public void insertBatch(List<AgentTheme> agentThemes) {
        if(agentThemeDao.insertBatch(agentThemes) < 1){
            logger.error("批量新增终端主题失败......");
            throw new MyException("批量新增终端主题失败");
        }
    }
}
