package com.seestech.sell.service.impl;

import com.seestech.sell.domain.mapper.IAgentDao;
import com.seestech.sell.domain.mapper.IAgentGroupDao;
import com.seestech.sell.domain.mapper.IDirectoryDao;
import com.seestech.sell.domain.model.*;
import com.seestech.sell.service.IDirectoryService;
import com.seestech.sell.service.IPublishService;
import com.seestech.sell.service.ITreeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by idiot on 2016/12/20.
 */
@Service
public class TreeServiceImpl implements ITreeService {
    private static Logger logger = LoggerFactory.getLogger(TreeServiceImpl.class);

    @Resource
    private IDirectoryDao directoryDao;
    @Resource
    private IAgentGroupDao agentGroupDao;
    @Resource
    private IAgentDao agentDao;
    @Resource
    private IDirectoryService directoryService;
    @Resource
    private IPublishService publishService;

    @Override
    public List<TreeInfo> getDirectoryTree() {
        List<TreeInfo> list = new ArrayList<TreeInfo>();
        //获取顶级目录信息 无父级目录编号
        List<Directory> firstLevelDirectories = directoryDao.getAllValidFirstLevelDirectories();
        List<Directory> directories = directoryService.recursionDirectoryByList(firstLevelDirectories);
        //遍历目录信息  获取其子目录信息
        for (Directory directory : directories){
            String pId = directory.getParentDirectoryId() == null ? "" : Long.toString(directory.getParentDirectoryId());
            //树信息
            TreeInfo directoryTree = TreeInfo.initTree(Long.toString(directory.getDirectoryId()), pId, directory.getDirectoryName());
            if(pId == null)
                directoryTree.setOpen(true);
            list.add(directoryTree);
        }
        return list;
    }

    @Override
    public List<TreeInfo> getAgentTree(String agentName, String[] arr, Long userId, Integer isTest) {
        //终端分组
        List<TreeInfo> list = new ArrayList<TreeInfo>();
        List<AgentGroup> agentGroups = agentGroupDao.getAllValidAgentGroupsByUserId(userId);
        for (AgentGroup agentGroup : agentGroups){
            TreeInfo agentGroupTree = TreeInfo.initTree(Long.toString(agentGroup.getAgentGroupId()), null, agentGroup.getAgentGroupName());
            //根据终端分组编号获取终端信息
            List<Agent> agents = agentDao.getAllValidAgentsByGroupId(agentGroup.getAgentGroupId());
            //遍历终端信息  添加到子集合
            List<TreeInfo> childTree = new ArrayList<TreeInfo>();
            boolean flag1 = agentName != null && !"".equals(agentName);
            boolean flag2 = arr != null & arr.length > 0;
            for (Agent agent : agents){
                //判断参数是否有效
                if(flag1 || flag2){
                    //当筛选终端名称不为空  匹配合适的数据
                    if(flag1 && !flag2){
                        if(agent.getAgentName().contains(agentName) || agent.getAgentCode().contains(agentName)){
                            initAgentGroupTree(agent, agentGroup, isTest, childTree);
                        }
                    } else if(flag2 && !flag1){   //遍历集合
                        for (String tag : arr){
                            if(agent.getTags() != null && agent.getTags().contains(tag)){
                                //匹配标签
                                initAgentGroupTree(agent, agentGroup, isTest, childTree);
                                break;
                            }
                        }
                    } else {
                        for (String tag : arr){
                            if((agent.getTags() != null && agent.getTags().contains(tag)) && (agent.getAgentName().contains(agentName) || agent.getAgentCode().contains(agentName))){
                                //匹配标签
                                initAgentGroupTree(agent, agentGroup, isTest, childTree);
                                break;
                            }
                        }
                    }
                } else {
                    initAgentGroupTree(agent, agentGroup, isTest, childTree);
                }
            }
            //当终端树集合大于0 时  添加到终端分组集合中
            if(childTree.size() > 0){
                agentGroupTree.setChildren(childTree);
                list.add(agentGroupTree);
            }
        }
        return list;
    }


    @Override
    public List<TreeInfo> getDirectoryTreeByDirectoryId(Long directoryId) {
        List<TreeInfo> list = new ArrayList<TreeInfo>();
        //获取目录信息
        Directory parentDirectory = directoryDao.getDirectoryByDirectoryId(directoryId);
        if(parentDirectory != null){
            TreeInfo directoryTree = TreeInfo.initTree(Long.toString(parentDirectory.getDirectoryId()), null, parentDirectory.getDirectoryName());
            directoryTree.setOpen(true);    //打开tree
            list.add(directoryTree);
            //获取该目录下的子目录信息
            List<Directory> directories = directoryDao.getValidDirectoryByParentDirectoryId(directoryId);
            for (Directory directory : directories){
                //目录树
                String pId = directory.getParentDirectoryId() == null ? "" : Long.toString(directory.getParentDirectoryId());
                TreeInfo childTree = TreeInfo.initTree(Long.toString(directory.getDirectoryId()), pId, directory.getDirectoryName());
                childTree.setOperate(true);     //设置操作节点
                list.add(childTree);
            }
        }
        return list;
    }

    @Override
    public List<TreeInfo> getThemeAgentTreeByThemeIds(List<Long> themeIds, String agentName, String[] arr, Long userId, Integer isTest) {
        List<TreeInfo> list = new ArrayList<TreeInfo>();
        List<Agent> agents = agentDao.getAgentsByThemeIds(themeIds, userId);
        boolean flag1 = agentName != null && !"".equals(agentName);
        boolean flag2 = arr != null & arr.length > 0;
        //遍历主题编号
        for (Long themeId : themeIds){
            Publish publish = publishService.getPublishByPublishId(themeId);
            if(publish == null)
                continue;
            TreeInfo themeTree = TreeInfo.initTree(String.valueOf(themeId), null, publish.getName());
            //子树集合
            List<TreeInfo> childTree = new ArrayList<TreeInfo>();
            //遍历终端  组装tree
            for (Agent agent : agents){
                //判断当前的主题编号是否是正在使用
                if(agent.getThemeId() != null && !agent.getThemeId().equals(themeId)){
                    logger.info("====>>>>>加载终端树====>>当前主题【"+agent.getThemeName()+"】没有被【"+agent.getAgentName()+"】使用");
                    continue;
                }
                //判断参数是否有效
                if(flag1 || flag2){
                    //当筛选终端名称不为空  匹配合适的数据
                    if(flag1 && !flag2){
                        if(agent.getAgentName().contains(agentName) || agent.getAgentCode().contains(agentName)){
                            initAgentThemeTree(agent, isTest, childTree);
                        }
                    } else if(flag2 && !flag1){   //匹配标签
                        for (String tag : arr){
                            if(agent.getTags() != null && agent.getTags().contains(tag)){
                                initAgentThemeTree(agent, isTest, childTree);
                                break;
                            }
                        }
                    } else {
                        for (String tag : arr){
                            if((agent.getTags() != null && agent.getTags().contains(tag)) && (agent.getAgentName().contains(agentName) || agent.getAgentCode().contains(agentName))){
                                initAgentThemeTree(agent, isTest, childTree);
                                break;
                            }
                        }
                    }
                } else {
                    initAgentThemeTree(agent, isTest, childTree);
                }
            }
            //当终端树集合大于0 时  添加到主题树集合中
            if(childTree.size() > 0){
                themeTree.setChildren(childTree);
                list.add(themeTree);
            }
        }
        return list;
    }

    //终端分组树
    private void initAgentGroupTree(Agent agent, AgentGroup agentGroup, Integer isTest, List<TreeInfo> childTree){
        TreeInfo agentTree = TreeInfo.initTree(Long.toString(agent.getAgentId()), Long.toString(agentGroup.getAgentGroupId()), agent.getAgentCode()+"->"+agent.getAgentName());
        agentTree.setCode(agent.getAgentCode());
        agentTree.setOperate(true);     //设置操作节点
        //如果isTest 不为null 根据其值筛选
        if (isTest != null){
            if (isTest.equals(agent.getIsTest())){
                childTree.add(agentTree);
            }
        }else
            childTree.add(agentTree);
    }

    //主题分组树
    private void initAgentThemeTree(Agent agent, Integer isTest, List<TreeInfo> childTree){
        TreeInfo agentTree = TreeInfo.initTree(Long.toString(agent.getAgentId()), null, agent.getAgentCode()+"->"+agent.getAgentName());
        agentTree.setCode(agent.getAgentCode());
        agentTree.setOperate(true);     //设置操作节点
        //如果isTest 不为null 根据其值筛选
        if (isTest != null){
            if (isTest.equals(agent.getIsTest())){
                childTree.add(agentTree);
            }
        }else
            childTree.add(agentTree);
    }
}
