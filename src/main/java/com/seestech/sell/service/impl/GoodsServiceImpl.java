package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IGoodsDao;
import com.seestech.sell.domain.model.Goods;
import com.seestech.sell.service.IGoodsService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by idiot on 2017/6/17.
 */
@Service
public class GoodsServiceImpl implements IGoodsService {
    private static Logger logger = LoggerFactory.getLogger(GoodsServiceImpl.class);

    @Resource
    private IGoodsDao goodsDao;

    @Override
    public Page<Goods> getAllGoodses(RowBounds rowBounds, Goods info) {
        return goodsDao.getAllGoodses(rowBounds, info);
    }

    @Override
    public void insert(Goods info) {
        if(info.getGoodsId() == null)
            info.setGoodsId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(goodsDao.insert(info) < 1){
            logger.error("新增商品信息失败");
            throw new MyException("新增商品信息失败");
        }
    }

    @Override
    public void update(Goods info) {
        if(info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(goodsDao.update(info) < 1){
            logger.error("修改商品信息失败");
            throw new MyException("修改商品信息失败");
        }
    }

    @Override
    public Goods getGoodsByGoodsId(Long goodsId) {
        return goodsDao.getGoodsByGoodsId(goodsId);
    }

    @Override
    public void delete(Long goodsId) {
        if(goodsDao.delete(goodsId) < 1){
            logger.error("删除商品信息失败");
            throw new MyException("删除商品信息失败");
        }
    }

    @Override
    public void deleteBatch(Long[] goodsIds) {
        if(goodsDao.deleteBatch(goodsIds) < 1){
            logger.error("批量删除商品信息失败");
            throw new MyException("批量删除商品信息失败");
        }
    }

    @Override
    public List<Goods> getGoodses(Goods info) {
        return goodsDao.getGoodses(info);
    }

    @Override
    public List<Goods> getGoodsesByBrandIds(Long[] brandIds) {
        return goodsDao.getGoodsesByBrandIds(brandIds);
    }

    @Override
    public Goods getGoodsByCode(String code) {
        return goodsDao.getGoodsByCode(code);
    }

    @Override
    public List<Goods> getGoodsesByCode(String code) {
        return goodsDao.getGoodsesByCode(code);
    }

    @Override
    public List<Goods> getGoodsByGoodsIds(String[] goodsIds) {
        return goodsDao.getGoodsByGoodsIds(goodsIds);
    }

    @Override
    public List<Goods> getGoodsesByTypeIds(Long[] typeIds) {
        return goodsDao.getGoodsesByTypeIds(typeIds);
    }
}
