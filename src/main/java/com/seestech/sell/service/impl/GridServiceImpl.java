package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IGridDao;
import com.seestech.sell.domain.model.Grid;
import com.seestech.sell.service.IGridService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

import static java.awt.SystemColor.info;

/**
 * Created by idiot on 2017/6/20.
 */
@Service
public class GridServiceImpl implements IGridService {
    private static Logger logger = LoggerFactory.getLogger(GoodsServiceImpl.class);

    @Resource
    private IGridDao gridDao;

    @Override
    public void insert(Grid info) {
        if(info.getGridId() == null)
            info.setGridId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(gridDao.insert(info) < 1){
            logger.error("新增柜格信息失败");
            throw new MyException("新增柜格信息失败");
        }
    }

    @Override
    public void update(Grid info) {
        if(info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(gridDao.update(info) < 1){
            logger.error("修改柜格信息失败");
            throw new MyException("修改柜格信息失败");
        }
    }

    @Override
    public void delete(Long gridId) {
        if(gridDao.delete(gridId) < 1){
            logger.error("删除柜格信息失败");
            throw new MyException("删除柜格信息失败");
        }
    }

    @Override
    public void deleteBatch(Long[] gridIds) {
        if(gridDao.deleteBatch(gridIds) < 1){
            logger.error("批量删除柜格信息失败");
            throw new MyException("批量删除柜格信息失败");
        }
    }

    @Override
    public Page<Grid> getAllGrids(RowBounds rowBounds, Grid info) {
        return gridDao.getAllGrids(rowBounds, info);
    }

    @Override
    public Grid getGridByGridId(Long gridId) {
        return gridDao.getGridByGridId(gridId);
    }

    @Override
    public List<Grid> getGrids(Grid info) {
        return gridDao.getGrids(info);
    }

    @Override
    public List<Grid> getGridsByGoodsIds(Long[] goodsIds) {
        return gridDao.getGridsByGoodsIds(goodsIds);
    }
}
