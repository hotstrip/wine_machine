package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IShelfDao;
import com.seestech.sell.domain.mapper.IShelfDetailDao;
import com.seestech.sell.domain.model.Shelf;
import com.seestech.sell.domain.model.ShelfDetail;
import com.seestech.sell.service.IShelfService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

import static com.sun.tools.doclets.formats.html.markup.HtmlStyle.details;

/**
 * Created by idiot on 2017/6/20.
 */
@Service
public class ShelfServiceImpl implements IShelfService {
    private static Logger logger = LoggerFactory.getLogger(ShelfServiceImpl.class);

    @Resource
    private IShelfDao shelfDao;
    @Resource
    private IShelfDetailDao shelfDetailDao;

    @Override
    public void insert(Shelf info) {
        if(info.getShelfId() == null)
            info.setShelfId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(shelfDao.insert(info) < 1){
            logger.error("新增货架信息失败");
            throw new MyException("新增货架信息失败");
        }
    }

    @Override
    public void update(Shelf info) {
        if(info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(shelfDao.update(info) < 1){
            logger.error("修改货架信息失败");
            throw new MyException("修改货架信息失败");
        }
    }

    @Override
    @Transactional
    public void delete(Long shelfId) {
        if(shelfDao.delete(shelfId) < 1){
            logger.error("删除货架信息失败");
            throw new MyException("删除货架信息失败");
        }
        List<ShelfDetail> details = shelfDetailDao.getShelfDetailsByShelfId(shelfId);
        if(details.size() > 0){
            Long[] shelfIds = new Long[]{shelfId};
            if(shelfDetailDao.deleteByShelfIds(shelfIds) < 1){
                logger.error("删除货架详细信息失败");
                throw new MyException("删除货架详细信息失败");
            }
        }
    }

    @Override
    @Transactional
    public void deleteBatch(Long[] shelfIds) {
        if(shelfDao.deleteBatch(shelfIds) < 1){
            logger.error("批量删除货架信息失败");
            throw new MyException("批量删除货架信息失败");
        }
        if(shelfDetailDao.deleteByShelfIds(shelfIds) < 1){
            logger.error("删除货架详细信息失败");
            throw new MyException("删除货架详细信息失败");
        }
    }

    @Override
    public Page<Shelf> getAllShelfs(RowBounds rowBounds, Shelf info) {
        return shelfDao.getAllShelfs(rowBounds, info);
    }

    @Override
    public Shelf getShelfByShelfId(Long shelfId) {
        return shelfDao.getShelfByShelfId(shelfId);
    }

    @Override
    public List<Shelf> getShelfs(Shelf info) {
        return shelfDao.getShelfs(info);
    }

    @Override
    public boolean modifyShelf(Long shelfId){
        //修改货架信息
        List<ShelfDetail> shelfDetails = shelfDetailDao.getShelfDetailsByShelfId(shelfId);
        Shelf shelf = new Shelf();
        shelf.setShelfId(shelfId);
        shelf.setAmount(shelfDetails.size());   //占用柜格数量
        int total = 0;

        //获取该货架对应的商品信息 goodsId
        StringBuffer products = new StringBuffer();
        for (ShelfDetail shelfDetail : shelfDetails){
            total += shelfDetail.getTotalGoods();
            //相同的goodsId只取一个
            if(products.indexOf(String.valueOf(shelfDetail.getGoodsId())) < 0){
                products.append(shelfDetail.getGoodsId()).append(";");
            }
        }
        shelf.setTotal(total);

        int index = products.lastIndexOf(";");
        shelf.setProducts(index < 0? products.toString() : products.substring(0, index));
        StringBuffer stocks = new StringBuffer();       //库存容量
        StringBuffer countAlarm = new StringBuffer();   //报警数量
        //获取商品对应的总数   根据货架编号获取货架详细信息
        List<ShelfDetail> details = shelfDetailDao.getShelfDetailsByShelfIdAndGroupByGoodsId(shelfId);
        for (String product : products.toString().split(";")){
            for (ShelfDetail item : details){
                //如果goodsId 和 货架详情集合中的 goodsId 相同  而且stocks中没有这个goodsId
                if(String.valueOf(item.getGoodsId()).equals(product) && stocks.indexOf(product) < 0){
                    stocks.append(item.getTotalGoods()).append(";");
                    countAlarm.append(item.getGridAlarm()).append(";");
                    break;
                }
            }
        }
        shelf.setStocks(stocks.lastIndexOf(";") < 0 ? stocks.toString() : stocks.substring(0, stocks.lastIndexOf(";")));
        shelf.setAlarms(countAlarm.lastIndexOf(";") < 0 ? countAlarm.toString() : countAlarm.substring(0, countAlarm.lastIndexOf(";")));
        if(shelfDao.update(shelf) < 1) {
            logger.error("修改货架信息失败");
            return false;
        }else
            return true;
    }

    @Override
    public List<Shelf> getShelfsByName(String name) {
        return shelfDao.getShelfsByName(name);
    }
}
