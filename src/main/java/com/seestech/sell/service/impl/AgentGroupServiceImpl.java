package com.seestech.sell.service.impl;


import com.github.pagehelper.Page;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.domain.mapper.IAgentGroupDao;
import com.seestech.sell.domain.model.AgentGroup;
import com.seestech.sell.service.IAgentGroupService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by idiot on 2016/12/26.
 */
@Service
public class AgentGroupServiceImpl implements IAgentGroupService {
    private static Logger logger = LoggerFactory.getLogger(AgentGroupServiceImpl.class);
    @Resource
    private IAgentGroupDao agentGroupDao;

    @Override
    public List<AgentGroup> getAllAgentGroups(RowBounds rowBounds) {
        return agentGroupDao.getAllAgentGroups(rowBounds);
    }

    @Override
    public void insert(AgentGroup info) {
        if (info.getAgentGroupId() == null)
            info.setAgentGroupId(IdGen.get().nextId());  //设置agentGroupId
        if (info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(agentGroupDao.insert(info) < 1){
            throw new MyException("新增终端分组信息失败");
        }
    }

    @Override
    public void update(AgentGroup info) {
        if (info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(agentGroupDao.update(info) < 1){
            throw new MyException("修改终端分组信息失败");
        }
    }

    @Override
    public AgentGroup getAgentGroupByAgentGroupId(long agentGroupId) {
        return agentGroupDao.getAgentGroupByAgentGroupId(agentGroupId);
    }

    @Override
    public void delete(long agentGroupId) {
        if(agentGroupDao.delete(agentGroupId) < 1){
            throw new MyException("删除终端分组信息失败");
        }
    }

    @Override
    public List<AgentGroup> getAllValidAgentGroups(AgentGroup info) {
        return agentGroupDao.getAllValidAgentGroups(info);
    }

    @Override
    public Page<AgentGroup> getAllAgentGroupsByUserId(RowBounds rowBounds, Long userId, AgentGroup info) {
        return agentGroupDao.getAllAgentGroupsByUserId(rowBounds, userId, info);
    }

    @Override
    public List<AgentGroup> getAllValidAgentGroupsByUserId(Long userId) {
        return agentGroupDao.getAllValidAgentGroupsByUserId(userId);
    }

    @Override
    public void deleteBatch(Long[] agentGroupIds) {
        if(agentGroupDao.deleteBatch(agentGroupIds) < 1){
            logger.error("批量删除终端分组信息失败");
            throw new MyException("批量删除终端分组信息失败");
        }
    }


    @Override
    public List<AgentGroup> getAgentGroupsByAgentGroupName(String agentGroupName) {
        return agentGroupDao.getAgentGroupsByAgentGroupName(agentGroupName);
    }

}
