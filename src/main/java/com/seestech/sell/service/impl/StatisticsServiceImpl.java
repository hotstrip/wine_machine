package com.seestech.sell.service.impl;

import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IStatisticsDao;
import com.seestech.sell.domain.model.Statistics;
import com.seestech.sell.service.IStatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * Created by idiot on 2017/8/2.
 */
@Service
public class StatisticsServiceImpl implements IStatisticsService {
    private static Logger logger = LoggerFactory.getLogger(StatisticsServiceImpl.class);

    @Resource
    private IStatisticsDao statisticsDao;

    @Override
    public void insert(Statistics info) {
        if(info.getId() == null)
            info.setId(IdGen.get().nextId());
        if(info.getCreateTime() == null)
            info.setCreateTime(new Date());
        if(statisticsDao.insert(info) < 1){
            logger.error("新增统计信息失败.....");
            throw new MyException("新增统计信息失败.....");
        }
    }

    @Override
    public void update(Statistics info) {
        if(info.getUpdateTime() == null)
            info.setUpdateTime(new Date());
        if(statisticsDao.update(info) < 1){
            logger.error("修改统计信息失败.....");
            throw new MyException("修改统计信息失败.....");
        }
    }

    @Override
    public void delete(Long id) {
        if(statisticsDao.delete(id) < 1){
            logger.error("删除统计信息失败.....");
            throw new MyException("删除统计信息失败.....");
        }
    }

    @Override
    public void deleteBatch(Long[] ids) {
        if(statisticsDao.delteBatch(ids) < 1){
            logger.error("批量删除统计信息失败.....");
            throw new MyException("批量删除统计信息失败.....");
        }
    }

    @Override
    public void insertBatch(List<Statistics> list) {
        if(statisticsDao.insertBatch(list) < 1){
            logger.error("批量新增统计信息失败.....");
            throw new MyException("批量新增统计信息失败.....");
        }
    }
}
