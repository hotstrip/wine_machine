package com.seestech.sell.service.impl;

import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.domain.mapper.IThemeColumnDao;
import com.seestech.sell.domain.model.ThemeColumn;
import com.seestech.sell.service.IThemeColumnService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by idiot on 2017/1/6.
 */
@Service
public class ThemeColumnServiceImpl implements IThemeColumnService {
    private Logger logger = LoggerFactory.getLogger(ThemeColumnServiceImpl.class);
    @Resource
    private IThemeColumnDao themeColumnDao;

    @Override
    public List<ThemeColumn> getThemeColumnsByThemeId(long themeId) {
        return themeColumnDao.getThemeColumnsByThemeId(themeId);
    }

    @Override
    public ThemeColumn getThemeColumnByThemeColumnId(Long themeColumnId) {
        return themeColumnDao.getThemeColumnByThemeColumnId(themeColumnId);
    }

    @Override
    public void insert(ThemeColumn themeColumn) {
        if(themeColumn.getThemeColumnId() == null)
            themeColumn.setThemeColumnId(IdGen.get().nextId());
        if(themeColumn.getCreateTime() == null)
            themeColumn.setCreateTime(new Date());
        if(themeColumnDao.insert(themeColumn) < 1){
            logger.error("新增主题栏目信息失败");
        }
    }

    @Override
    public void update(ThemeColumn themeColumn) {
        if(themeColumn.getUpdateTime() == null)
            themeColumn.setUpdateTime(new Date());
        if(themeColumnDao.update(themeColumn) < 1){
            logger.error("修改主题栏目信息失败");
        }
    }

    @Override
    public void delete(Long themeColumnId) {
        if(themeColumnDao.delete(themeColumnId) < 1){
            logger.error("删除主题栏目信息失败");
        }
    }

    @Override
    public List<ThemeColumn> getThemeColumnsDirectoryId(Long directoryId) {
        return themeColumnDao.getThemeColumnsByDirectoryId(directoryId);
    }

    @Override
    public ThemeColumn getThemeColumnByIds(Long themeId, Long directoryId) {
        return themeColumnDao.getThemeColumnByIds(themeId, directoryId);
    }

    @Override
    public void deleteBatch(List<ThemeColumn> list) {
        if(themeColumnDao.deleteBatch(list) < 1){
            logger.error("删除主题栏目信息失败");
            throw new MyException("删除主题栏目信息失败");
        }
    }

    @Override
    public boolean insertBatch(List<ThemeColumn> themeColumns) {
        if(themeColumnDao.insertBatch(themeColumns) < 1){
            logger.error("批量新增主题栏目信息失败");
            return false;
        }
        return true;
    }

}
