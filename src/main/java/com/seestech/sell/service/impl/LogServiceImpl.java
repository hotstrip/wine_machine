package com.seestech.sell.service.impl;

import com.github.pagehelper.Page;
import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.DateUtils;
import com.seestech.sell.common.utils.IdGen;
import com.seestech.sell.common.exceptions.MyException;
import com.seestech.sell.domain.mapper.ILogDao;
import com.seestech.sell.domain.model.Log;
import com.seestech.sell.domain.model.enums.StatusEnums;
import com.seestech.sell.service.ILogService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by idiot on 2016/12/14.
 */
@Service
public class LogServiceImpl implements ILogService {
    private Logger logger = LoggerFactory.getLogger(LogServiceImpl.class);
    @Resource
    private ILogDao logDao;

    @Override
    public void addloginLog(Log log) {
        if (log.getLogId() == null)
            log.setLogId(IdGen.get().nextId());
        if (log.getCreateTime() == null)
            log.setCreateTime(DateUtils.currentDateTime());   //设置操作时间
        log.setResponseCode(Constants.ResponseCode.success + "");
        if(logDao.insert(log) < 1){
            throw new MyException("写入登录日志失败");
        }
    }

    @Override
    public Page<Log> getAllLogs(RowBounds rowBounds, Log info) {
        return logDao.getAllLogs(rowBounds, info);
    }

    @Override
    public void addOperateLog(Log log) {
        if (log.getLogId() == null)
            log.setLogId(IdGen.get().nextId());
        if (log.getCreateTime() == null)
            log.setCreateTime(DateUtils.currentDateTime());   //设置操作时间
        if(logDao.insert(log) < 1){
            throw new MyException("写入操作日志失败");
        }
    }

    @Override
    public void delete(long logId) {
        if(logDao.delete(logId) < 1){
            throw new MyException("删除日志信息失败");
        }
    }

    @Override
    @Transactional
    public void deleteBatch(Long[] logIds) {
        if(logDao.deleteBatch(logIds) < 1){
            logger.error("批量删除日志信息失败");
            throw new MyException("批量删除日志信息失败");
        }
    }
}
