package com.seestech.sell.service;


import com.seestech.sell.domain.model.TreeInfo;

import java.util.List;

/**
 * Created by idiot on 2016/12/20.
 */
public interface ITreeService {

    //获取目录树信息
    List<TreeInfo> getDirectoryTree();

    //获取终端树
    List<TreeInfo> getAgentTree(String agentName, String[] arr, Long userId, Integer isTest);


    //根据目录编号加载目录信息树
    List<TreeInfo> getDirectoryTreeByDirectoryId(Long value);

    List<TreeInfo> getThemeAgentTreeByThemeIds(List<Long> themeIds, String agentName, String[] arr, Long userId, Integer isTest);
}
