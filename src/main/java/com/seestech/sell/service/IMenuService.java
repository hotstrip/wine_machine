package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Menu;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/16.
 */
public interface IMenuService {
    //根据userId获取权限
    List<String> getPermissionsByUserId(long userId);

    //根据菜单parentId获取子菜单
    List<Menu> getMenusByParentId(long parentId);

    //获取该用户的一级菜单
    List<Menu> getFirstLevelMenusByUserId(long userId);

    //获取所有菜单信息 分页
    Page<Menu> getAllPageMenus(RowBounds rowBounds, Menu menu);

    //新增菜单信息
    void insert(Menu info);

    //修改菜单信息
    void update(Menu info);

    //根据menuId获取菜单信息
    Menu getMenuByMenuId(long menuId);

    //删除菜单信息
    void delete(long menuId);

    //获取所有有效的菜单信息  不分页
    List<Menu> getAllMenus();

    //获取所有的一级菜单信息
    List<Menu> getAllFirstMenus();

    //根据roleId获取角色已经拥有的资源（菜单）信息
    List<Menu> getMenusByRoleId(Long roleId);

    //获取多级菜单信息
    List<Menu> getMultiLevelMenus();

    //根据父级菜单编号和用户编号获取菜单信息
    List<Menu> getMenusByParentIdAndUserId(Long menuId, Long userId);
}
