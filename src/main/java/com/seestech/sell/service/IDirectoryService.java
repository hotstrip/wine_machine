package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Directory;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/22.
 */
public interface IDirectoryService {

    //新增
    String insert(Directory info);

    //修改
    void update(Directory info);

    //根据目录 加载目录信息
    Directory getDirectoryByDirectoryId(Long directoryId);

    //分页获取所有目录信息
    List<Directory> getAllDirectories(RowBounds rowBounds);

    //根据目录编号获取路径
    String getDirectoryPathBydirectoryId(Long directoryId);

    //删除目录信息
    void delete(Long directoryId);

    //获取所有有效业务目录信息
    List<Directory> getAllValidBusinessDirectories(Long directoryTypeId);

    //获取所有有效的父级目录信息
    List<Directory> getValidParentDiretories();

    //递归获取目录信息
    List<Directory> recursionDirectoryByList(List<Directory> firstLevelDirectories);

    //根据父级目录编号查询目录信息
    List<Directory> getValidDirectoryByParentDirectoryId(Long value);

    //根据目录的Type获取目录信息
    List<Directory> getDirectoriesByType(Integer value);

    //获取目录信息
    Page<Directory> getDirectoriesByDirectory(Directory info);

    //根据目录编号获取目录等级
    int getLevelByDirectoryId(Long directoryId);
}
