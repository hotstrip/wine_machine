package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Shelf;
import com.seestech.sell.domain.model.ShelfDetail;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/20.
 */
public interface IShelfDetailService {
    //新增
    void insert(ShelfDetail info);

    //修改
    void update(ShelfDetail info);

    //删除
    void delete(Long detailId);

    //批量删除
    void deleteBatch(Long[] detailIds);

    //获取货架详情
    List<ShelfDetail> getShelfDetailsByShelfId(Long shelfId);

    //分页获取货架详情
    Page<Shelf> getAllShelfDetails(RowBounds rowBounds, ShelfDetail info);

    //获取单个货架详情
    ShelfDetail getShelfDetailByDetailId(Long detaild);

    //根据柜格编号获取货架信息
    List<ShelfDetail> getShelfDetailByGridIds(Long[] gridIds);

    Page<Shelf> getShelfDetails(ShelfDetail info);
}
