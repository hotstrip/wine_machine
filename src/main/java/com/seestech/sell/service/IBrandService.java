package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Brand;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/17.
 */
public interface IBrandService {
    //分页获取品牌信息
    Page<Brand> getAllBrands(RowBounds rowBounds, Brand info);

    //新增
    void insert(Brand info);

    //修改
    void update(Brand info);

    //根据brandId获取品牌信息
    Brand getBrandByBrandId(Long brandId);

    //删除品牌信息
    void delete(Long brandId);

    //批量删除
    void deleteBatch(Long[] brandIds);

    //获取品牌信息
    List<Brand> getBrands(Brand info);
}
