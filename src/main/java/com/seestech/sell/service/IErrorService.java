package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Error;
import com.seestech.sell.domain.model.vo.ErrorVoExcel;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/7/27.
 */
public interface IErrorService {
    //新增
    void insert(Error info);

    //修改
    void update(Error info);

    //删除
    void delete(Long id);

    //获取单个
    Error getErrorById(Long id);

    //根据终端编号获取异常信息
    List<Error> getErrorsByAgentId(Long agentId);

    //分页获取异常信息
    Page<Error> getAllErrors(RowBounds rowBounds, Error info);

    //导出数据
    List<ErrorVoExcel> getAllExportErrors(RowBounds rowBounds, Error info);
}
