package com.seestech.sell.service;


import com.seestech.sell.domain.model.AgentTaskInfo;

import java.util.List;

/**
 * Created by idiot on 2017/5/27.
 */
public interface IAgentTaskService {
    //新增
    void insert(AgentTaskInfo info);

    //修改
    void update(AgentTaskInfo info);

    //获取任务信息
    List<AgentTaskInfo> getAgentTasks(AgentTaskInfo info);

    //批量新增
    void insertBatch(List<AgentTaskInfo> agentTasks);
}
