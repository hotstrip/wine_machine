package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Order;
import com.seestech.sell.domain.model.Recorder;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/22.
 */
public interface IRecorderService {
    //分页查询补货记录
    Page<Recorder> getAllRecorders(RowBounds rowBounds, Recorder info);

    //分页查询补货记录
    Page<Recorder> getRecorders(RowBounds rowBounds, Recorder info);

    //删除
    void delete(Long recorderId);

    //批量删除
    void deleteBatch(Long[] recorderIds);

    //获取记录信息
    List<Recorder> getRecordersByAgentId(Long agentId);

    //新增
    void insert(Recorder info);

    //修改
    void update(Recorder info);

    //根据id查询
    List<Recorder> getRecordersByIds(Long agentId, Long shelfId, Long goodsId);

    //查询单个
    Recorder getRecorderByRecorderId(Long recorderId);

    //根据agentId  shelfId goodsId  获取 最新的记录信息
    List<Recorder> getRecordersByInfo(Recorder recorder);

    //获取记录信息
    Recorder getRecordersByRecorder(Recorder recorder);

    boolean modify(String[] products, Recorder recorder, Object[] stockses, String[] stocks);

    //根据batchNumber分组查询
    Page<Recorder> getRecordersGroupByBatchNumber(RowBounds rowBounds, Recorder info);

    //根据batchNumber查询
    List<Recorder> getRecordersByBatchNumber(String batchNumber);
}
