package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Report;
import org.apache.ibatis.session.RowBounds;

/**
 * Created by idiot on 2017/8/21.
 */
public interface IReportService {
    //新增
    void insert(Report info);

    //修改
    void update(Report info);

    //删除
    void delete(Long id);

    //分页加载信息
    Page<Report> getAllReports(RowBounds rowBounds, Report info);
}
