package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Screenshots;
import org.apache.ibatis.session.RowBounds;

/**
 * Created by idiot on 2017/2/8.
 */
public interface IScreenShotsService {

    //根据agentCode获取终端截图信息
    Page<Screenshots> getScreenshotsByAgentCode(RowBounds rowBounds, String agentCode);

    //批量删除
    void deleteBatch(Long[] screenshotsIds);
}
