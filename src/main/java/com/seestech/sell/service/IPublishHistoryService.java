package com.seestech.sell.service;

import com.seestech.sell.domain.model.PublishHistory;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/1/10.
 */
public interface IPublishHistoryService {
    //新增
    void insert(PublishHistory publishHistory);

    //分页获取所有发布历史记录
    List<PublishHistory> getAllPublishHistories(RowBounds rowBounds, PublishHistory info);

    //根据历史编号获取信息
    PublishHistory getPublishHistoryById(Long publishHistoryId);

    //批量删除
    void deleteBatch(Long[] publishHistoryIds);

    //删除
    void delete(Long publishHistoryId);

    //查询发布历史记录
    List<PublishHistory> getPublishHistoriesByInfo(PublishHistory info);

    //批量新增
    void insertBatch(List<PublishHistory> histories);
}
