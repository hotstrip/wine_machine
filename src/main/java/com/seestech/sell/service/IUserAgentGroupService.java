package com.seestech.sell.service;


import com.seestech.sell.domain.model.UserAgentGroup;

/**
 * Created by idiot on 2017/1/19.
 * @description  用户终端分组信息
 */
public interface IUserAgentGroupService {
    //新增
    void insert(UserAgentGroup info);

    //删除
    void delete(Long userAgentGroupId);

    //删除
    void remove(UserAgentGroup userAgentGroup);

    //批量删除
    void deleteByIds(Long userId, Long[] agentGroupIds);
}
