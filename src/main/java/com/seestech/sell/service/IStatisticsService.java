package com.seestech.sell.service;

import com.seestech.sell.domain.model.Statistics;

import java.util.List;

/**
 * Created by idiot on 2017/8/2.
 */
public interface IStatisticsService {
    //新增
    void insert(Statistics info);

    //修改
    void update(Statistics info);

    //删除
    void delete(Long id);

    //批量删除
    void deleteBatch(Long[] ids);

    //批量新增
    void insertBatch(List<Statistics> list);
}
