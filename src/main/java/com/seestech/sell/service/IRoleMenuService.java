package com.seestech.sell.service;


import com.seestech.sell.domain.model.RoleMenu;

/**
 * Created by idiot on 2016/12/19.
 */
public interface IRoleMenuService {
    //新增
    void insert(RoleMenu roleMenu);

    //删除
    void remove(RoleMenu roleMenu);
}
