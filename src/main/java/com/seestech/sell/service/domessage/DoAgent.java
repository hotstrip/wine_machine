package com.seestech.sell.service.domessage;

import com.seestech.sell.domain.model.ResultBean;
import com.seestech.sell.common.utils.*;
import com.seestech.sell.domain.mapper.IAgentDao;
import com.seestech.sell.domain.mapper.IScreenshotsDao;
import com.seestech.sell.domain.model.Agent;
import com.seestech.sell.domain.model.Screenshots;
import com.seestech.sell.domain.model.enums.StatusEnums;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import sun.management.resources.agent;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

import static com.seestech.sell.common.utils.DateUtils.diffMinutes;


/**
 * Created by idiot on 2017/1/3.
 */
@Service
public class DoAgent {
    private static Logger logger = LoggerFactory.getLogger(DoAgent.class);

    @Resource
    private IAgentDao agentDao;

    @Resource
    private IScreenshotsDao screeshotsDao;

    /**
     * @description 更新终端属性
     * @param agentCodes  code
     * @param agentIps     ip
     * @param agentCsNos   cs软件版本
     * @param agentHwversions   硬件版本
     */
    /*public void setAgentProperties(String agentCodes, String agentIps, String agentCsNos, String agentHwversions){
        logger.info("【处理消息】====更新终端属性==>开始");
        String[] agentCodeObj = agentCodes.split(";");
        String[] agentIpObj = agentIps.split(";");
        String[] agentCsObj = agentCsNos.split(";");
        String[] agentHwObj = agentHwversions.split(";");
        //获取所有有效的终端信息   遍历匹配agentCode  更新终端属性  以及  终端在线状态
        List<Agent> list = agentDao.getAgentsByAgentCodes(agentCodeObj);
        if(list != null && list.size() > 0){
            for(Agent agent : list){
                for(int i = 0; i < agentCodeObj.length; i++){
                    if(agent.getAgentCode().equals(agentCodeObj[i])){
                        agent.setAgentIp(agentIpObj[i]);
                        agent.setCsNo(agentCsObj[i]);
                        agent.setHwVer(agentHwObj[i]);
                        //根据获取到的agentId修改对应的agent信息，添加ip,csNo,hwVer
                        if(agentDao.update(agent) < 1){
                            logger.error("更新终端属性【ip 软件版本号  硬件版本号】失败");
                        }
                    }
                }
            }
        }
        logger.info("【处理消息】====更新终端属性==>结束");
    }*/

    /**
     * @description 心跳包  更新终端最新心跳时间  以及  心跳频率  和   在线状态
     * @param agentCodes
     */
    public void heartBeat(String agentCodes) {
        logger.info("【处理消息】====心跳包==>开始");
        String[] agentCodeObj = agentCodes.split(";");
        logger.info("======>agentCode：" + agentCodes);
        long minutes = -1;
        int day = 0;
        //获取所有有效的终端信息   遍历匹配agentCode  更新终端属性  以及  终端在线状态
        List<Agent> list = agentDao.getAllValidAgents();
        if(list != null && list.size() > 0){
            for(Agent agent : list){
                //先更新心跳时间  避免新终端没有上次心跳时间时  首次心跳导致终端状态为断线1天
                for(int i = 0; i < agentCodeObj.length; i++){
                    if(agent.getAgentCode().equals(agentCodeObj[i])){
                        Agent item = new Agent();
                        item.setAgentId(agent.getAgentId());
                        //更新终端心跳时间
                        item.setLastHeartBeatTime(DateUtils.currentDateTime());
                        //获取系统当前时间  对比终端上一次心跳时间  转换为分钟数  更新终端心跳频率
                        if(item.getLastHeartBeatTime() != null){
                            minutes = diffMinutes(new Date(), item.getLastHeartBeatTime());
                        }else {
                            minutes = diffMinutes(new Date(), agent.getCreateTime());
                        }
                        //把时间 转换为天数   更新终端在线状态  如果分钟数 大于5分钟  小于24小时  则断线1天
                        if (minutes > Constants.Regular.agent_live_minutes){
                            day = minutes < (60*24) ? 1 :(int) (minutes / 60f / 24.0);
                        }
                        logger.info("终端【"+ agent.getAgentName() +"】--断线天数：" + day);
                        item.setOnlineStatus(day);
                        if(agentDao.update(item) < 1){
                            logger.error("更新终端【"+ agent.getAgentName() +"】属性【心跳时间|终端在线状态】失败");
                        }
                    }
                }
            }
        }
        logger.info("【处理消息】====心跳包==>结束");
    }

    /**
     * @description  回传终端截图消息处理
     * @param agentCodes    终端编号
     * @param images        图片名称
     */
    public void screenshots(String agentCodes, String images) {
        logger.info("【处理消息】====回传终端截图消息==>开始");
        String[] agentCodeObj = agentCodes.split(";");
        String[] imageObj = images.split(";");
        //根据agentCode获取agentId  新增终端截图信息
        for(int i = 0; i < agentCodeObj.length; i++){
            Agent agent =  agentDao.getAgentByAgentCode(agentCodeObj[i]);
            Screenshots info = new Screenshots();
            info.setAgentId(agent.getAgentId());                //终端编号
            info.setCreateTime(DateUtils.currentDateTime());    //时间
            info.setImageName(imageObj[i]);                 //图片名称
            info.setScreenshotsId(IdGen.get().nextId());    //设置主键
            info.setStatus(StatusEnums.VALID.getValue());   //有效
            if(screeshotsDao.insert(info) < 1){
                logger.error("更新终端截图消息失败");
            }
        }
        logger.info("【处理消息】====回传终端截图消息==>结束");
    }

    /**
     * @description     安卓端通讯设置属性
     * @param agentId
     * @param ip
     * @param csNo
     * @param hwVer
     * @param diskCapacity
     * @param diskRemaining
     * @param error
     * @param location
     * @param licenseCode
     * @param ram       运行总内存
     * @param leftRam   剩余内存
     */
    public ResultBean<Object> setAgentProperties(Long agentId, String ip, String netIp, String csNo, String hwVer, Integer os,
                                                 Long diskCapacity, Long diskRemaining, int error, String location,
                                                 String licenseCode, Long ram, Long leftRam) {
        logger.info("【处理安卓端消息】====更新终端属性==>开始");
        ResultBean<Object> resultBean;
        Agent agent = new Agent();
        agent.setAgentId(agentId);
        agent.setAgentIp(ip);       //设置内网IP
        agent.setNetIp(netIp);      //设置外网IP
        agent.setCsNo(csNo);
        agent.setHwVer(hwVer);
        agent.setDiskCapacity(diskCapacity);
        agent.setDiskRemaining(diskRemaining);
        agent.setOs(os);
        agent.setError(error);          //终端异常
        agent.setLocation(location);    //终端gps  经纬度地址
        agent.setLicenseCode(licenseCode);      //许可证
        agent.setRam(ram);
        agent.setLeftRam(leftRam);
        //根据获取到的agentId修改对应的agent信息，添加ip,csNo,hwVer
        if(agentDao.update(agent) < 1){
            resultBean = ResponseUtils.initResultBean(null, Constants.ResponseCode.error, "更新终端属性失败", false);
            logger.error("更新终端属性【ip 软件版本号  硬件版本号】失败");
        }else
            resultBean = ResponseUtils.initResultBean(null, Constants.ResponseCode.success, "更新终端属性成功", true);
        logger.info("【处理安卓端消息】====更新终端属性==>结束");
        return resultBean;
    }
}
