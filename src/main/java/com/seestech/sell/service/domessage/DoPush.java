package com.seestech.sell.service.domessage;

import com.seestech.sell.common.utils.DateUtils;
import com.seestech.sell.domain.mapper.IPublishHistoryDao;
import com.seestech.sell.domain.model.PublishHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2017/1/9.
 * @description 处理下发相关
 */
@Service
public class DoPush {
    private static Logger logger = LoggerFactory.getLogger(DoPush.class);

    @Resource
    private IPublishHistoryDao publishHistoryDao;

    /**
     * @description 处理下发  任务接受完成
     * @param batno 批次号
     * @param agentCode 终端编号
     * @param fileName 文件名
     * @param status    状态
     * @param date  时间
     * @param kind  类型
     */
    public void process(String batno, String agentCode, String fileName, String status, String date, String kind) {
        logger.info("【处理消息】====任务接受完成消息==>开始");
        String[] batnoObj = batno.split(";");
        String[] agentCodeObj = agentCode.split(";");
        String[] fileNameObj = fileName.split(";");
        String[] statusObj = status.split(";");
        String[] dateObj = date.split(";");
        String[] kindObj = kind.split(";");
        int length = agentCodeObj.length;
        //根据batno查询 栏目发布历史信息
        List<PublishHistory> publishHistoryList = publishHistoryDao.getPublishHistoriesByAgentCodesAndBatchNumbers(agentCodeObj, batnoObj);
        //遍历agentCode
        for (int i = 0; i < length; i++) {
            String kindCode = kindObj[i];   //类型   0:主题   [1 8 9 10 11 12]: 栏目
            if("0".equals(kindCode) || "1".equals(kindCode) ||"8".equals(kindCode) || "9".equals(kindCode) || "10".equals(kindCode) || "11".equals(kindCode) || "12".equals(kindCode)){
                //栏目
                for (PublishHistory publishHistory : publishHistoryList) {
                    //确认agentCode 相等时才设置对应信息
                    for (int j = 0,asize = agentCodeObj.length; j < asize; j++){
                        if(agentCodeObj[j].equals(publishHistory.getAgentCode())){
                            publishHistory.setPublishStatus(Integer.parseInt(statusObj[j]));//设置下发状态
                            publishHistory.setFinishTime(DateUtils.parse(dateObj[j], DateUtils.SIMPLE_DATE_HOURS_PATTERN));//设置下发完成时间
                            if(publishHistory.getPublishRate() != 100)
                                publishHistory.setPublishRate(100);     //设置下发完成率  进度

                            if(publishHistoryDao.update(publishHistory) < 1){
                                logger.error("【处理消息】====任务接受完成消息==>更新发布历史信息失败");
                            }else {
                                logger.info("【处理消息】====任务接受完成消息==>更新发布历史信息成功");
                            }
                            break;
                        }
                    }
                }
            }
        }
        logger.info("【处理消息】====任务接受完成消息==>结束");
    }

    /**
     * @description 下发回馈信息  任务接受率
     * @param batno     批次号
     * @param agentCode 终端编号
     * @param rate  下发率
     */
    public void postFeedback(String batno, String agentCode, String rate) {
        logger.info("【处理消息】====任务接受率==>开始");
        String[] batNoObj = batno.split(";");
        String[] agentCodeObj = agentCode.split(";");
        String[] rateObj = rate.split(";");
        //根据batchNumber和agentCode查询发布历史信息
        List<PublishHistory> list = publishHistoryDao.getPublishHistoriesByAgentCodesAndBatchNumbers(agentCodeObj, batNoObj);
        int size = list.size();
        logger.info("【处理消息】====任务接受率==>准备更新发布历史记录");
        for(int i=0; i<size; i++){
            PublishHistory info = list.get(i);
            //更新发布历史的接受率
            //确认agentCode 相等时才设置对应信息
            for (int j = 0,asize = agentCodeObj.length; j < asize; j++){
                if(agentCodeObj[j].equals(info.getAgentCode())){
                    info.setPublishRate(Integer.parseInt(rateObj[j]));
                    if(publishHistoryDao.update(info) < 1){
                        logger.error("【处理消息】====任务接受率==>"+ info.getPublishHistoryId() +"更新失败");
                    }else {
                        logger.info("【处理消息】====任务接受率==>"+ info.getPublishHistoryId() +"更新成功");
                    }
                    break;
                }
            }
        }
        logger.info("【处理消息】====任务接受率==>结束");
    }

    /**
     * @description  安卓端  下发任务接受率
     * @param publishId     下发信息编号
     * @param agentCode
     * @param publishRate
     */
    public void postPublishRate(String publishId, String agentCode, int publishRate) {
        logger.info("【处理安卓消息】====任务接受率==>开始");
        logger.info("【处理安卓消息】====任务接受率==>publishId"+publishId+" agentCode"+agentCode+" publishRate"+publishRate);
        //根据publishId查询发布历史信息
        PublishHistory info = publishHistoryDao.getPublishHistoryById(Long.parseLong(publishId));
        logger.info("【处理安卓消息】====任务接受率==>准备更新发布历史记录");
        //更新发布历史的接受率
        //确认agentCode 相等时才设置对应信息
        if(info != null && agentCode.equals(info.getAgentCode())){
            //当下发状态为完成时  修改下发率为100
            if(info.getPublishStatus() != null && info.getPublishStatus().equals(1))
                info.setPublishRate(100);
            else
                info.setPublishRate(publishRate);
            if(publishHistoryDao.update(info) < 1){
                logger.error("【处理安卓消息】====任务接受率==>"+ info.getPublishHistoryId() +"更新失败");
            }else {
                logger.info("【处理安卓消息】====任务接受率==>"+ info.getPublishHistoryId() +"更新成功");
            }
        }
        logger.info("【处理安卓消息】====任务接受率==>结束");
    }


    /**
     * @description 安卓端  下发完成消息
     * @param publishId
     * @param agentCode
     * @param status
     * @param date
     */
    public void postPublishEnd(String publishId, String agentCode, String status, String date) {
        logger.info("【处理安卓消息】====任务接受完成消息==>开始");
        //根据publishId查询发布历史信息
        PublishHistory info = publishHistoryDao.getPublishHistoryById(Long.parseLong(publishId));
        //确认agentCode 相等时才设置对应信息
        if(info != null && agentCode.equals(info.getAgentCode())){
            info.setPublishStatus(Integer.parseInt(status));//设置下发状态
            info.setFinishTime(DateUtils.parse(date, DateUtils.SIMPLE_DATE_HOURS_PATTERN));//设置下发完成时间
            if(info.getPublishRate() == null || info.getPublishRate() < 100)
                info.setPublishRate(100);     //设置下发完成率  进度

            if(publishHistoryDao.update(info) < 1){
                logger.error("【处理安卓消息】====任务接受完成消息==>更新发布历史信息失败");
            }else {
                logger.info("【处理安卓消息】====任务接受完成消息==>更新发布历史信息成功");
            }
        }
        logger.info("【处理安卓消息】====任务接受完成消息==>结束");
    }

    /**
     * 任务更新完成消息
     * @param publishId
     * @param agentCode
     * @param updateStatus
     */
    public void publishUpdateStatus(String publishId, String agentCode, int updateStatus){
        logger.info("【处理安卓消息】====任务更新完成消息==>开始");
        //根据publishId查询发布历史信息
        PublishHistory info = publishHistoryDao.getPublishHistoryById(Long.parseLong(publishId));
        //确认agentCode 相等时才设置对应信息
        if(info != null && agentCode.equals(info.getAgentCode())){
            info.setUpdateStatus(updateStatus);
            if(publishHistoryDao.update(info) < 1){
                logger.error("【处理安卓消息】====任务更新完成消息==>更新发布历史信息失败");
            }else {
                logger.info("【处理安卓消息】====任务更新完成消息==>更新发布历史信息成功");
            }
        }
        logger.info("【处理安卓消息】====任务更新完成消息==>结束");
    }
}
