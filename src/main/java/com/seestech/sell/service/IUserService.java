package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.User;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Set;

/**
 * Created by idiot on 2017/6/16.
 */
public interface IUserService {
    //根据用户名查询用户信息
    User getUserByName(String userName);

    //获取所有的管理员用户
    Page<User> getAllAdminUsers(RowBounds rowBounds, User user);

    //新增用户信息
    void insert(User info);

    //修改用户信息
    void update(User info);

    //根据userId获取用户信息
    User getUserByUserId(long userId);

    //删除用户信息
    void delete(long userId);

    //获取所有的有效用户
    List<User> getAllUsers();

    //根据用户名和密码获取用户信息
    User getUserByNameAndPassword(String userName, String userPassword);

    //根据微信的openid获取用户信息
    List<User> getUserByOpenId(String openId);

    //根据企业编号获取用户信息
    List<User> getUsersByCompanyIds(List<Long> companyIds);

    //根据企业编号获取用户编号
    Set<Long> getUserIdsByCompanyIds(List<Long> ids);
}
