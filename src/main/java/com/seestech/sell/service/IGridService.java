package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Grid;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

import static java.awt.SystemColor.info;

/**
 * Created by idiot on 2017/6/20.
 */
public interface IGridService {
    //新增
    void insert(Grid info);

    //修改
    void update(Grid info);

    //删除
    void delete(Long gridId);

    //批量删除
    void deleteBatch(Long[] gridIds);

    //分页获取柜格信息
    Page<Grid> getAllGrids(RowBounds rowBounds, Grid info);

    //获取单个柜格信息
    Grid getGridByGridId(Long gridId);

    //获取柜格信息
    List<Grid> getGrids(Grid info);

    //根据商品编号获取柜格信息
    List<Grid> getGridsByGoodsIds(Long[] goodsIds);
}
