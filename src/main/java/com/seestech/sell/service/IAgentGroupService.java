package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.AgentGroup;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/26.
 */
public interface IAgentGroupService {
    //分页查询所有终端分组
    List<AgentGroup> getAllAgentGroups(RowBounds rowBounds);

    //新增
    void insert(AgentGroup info);

    //修改
    void update(AgentGroup info);

    //根据agentGroupId获取终端分组信息
    AgentGroup getAgentGroupByAgentGroupId(long agentGroupId);

    //删除
    void delete(long agentGroupId);

    //获取所有有效的终端分组
    List<AgentGroup> getAllValidAgentGroups(AgentGroup info);

    //根据用户编号 分页获取终端分组信息
    Page<AgentGroup> getAllAgentGroupsByUserId(RowBounds rowBounds, Long userId, AgentGroup info);

    //根据用户编号加载有效的终端分组信息
    List<AgentGroup> getAllValidAgentGroupsByUserId(Long userId);

    //批量删除
    void deleteBatch(Long[] agentGroupIds);

    //根据名称获取终端分组
    List<AgentGroup> getAgentGroupsByAgentGroupName(String agentGroupName);
}
