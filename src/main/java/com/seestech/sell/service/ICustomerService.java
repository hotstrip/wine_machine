package com.seestech.sell.service;

import com.seestech.sell.domain.model.Customer;

import java.util.List;

/**
 * Created by idiot on 2017/7/10.
 */
public interface ICustomerService {
    //新增
    void insert(Customer info);

    //修改
    void update(Customer info);

    //获取客户信息表
    Customer getCustomerByCustomerId(String customerId);

    //更新客户积分信息
    void modify(String customerId, double fee);

    //根据phone查询客户信息
    List<Customer> getCustomersByPhone(String phone);
}
