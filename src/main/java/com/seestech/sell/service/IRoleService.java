package com.seestech.sell.service;


import com.seestech.sell.domain.model.Role;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/13.
 */
public interface IRoleService {
    //分页查询所有角色
    List<Role> getAllRoles(RowBounds rowBounds, Role role);

    //新增
    void insert(Role info);

    //修改
    void update(Role info);

    //根据roleId获取角色信息
    Role getRoleByRoleId(long roleId);

    //删除角色信息
    void delete(long roleId);

    //根据userId获取角色信息
    List<Role> getRolesByUserId(Long userId);

    //获取有效的角色信息
    List<Role> getAllValidRoles();
}
