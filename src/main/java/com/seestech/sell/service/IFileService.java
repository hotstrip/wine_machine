package com.seestech.sell.service;


import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.FileInfo;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2016/12/22.
 */
public interface IFileService {

    //获取目录下的文件信息
    Page<FileInfo> getFilesByFileInfo(RowBounds rowBounds, FileInfo info);

    //上传文件
    void upload(FileInfo fileInfo);

    //分页获取所有有效文件信息
    List<FileInfo> getAllValidFiles(RowBounds rowBounds);

    //新增
    String insert(FileInfo fileInfo);

    //修改
    boolean update(FileInfo fileInfo);

    //根据fileId获取文件信息
    FileInfo getFileByFileId(Long fileId);

    //获取文件全路径
    String getFilePath(Long fileId);

    //根据目录编号获取有效文件信息
    List<FileInfo> getAllValidFilesByDirectoryId(Long directoryId);

    //删除
    void delete(Long fileId);

    //批量新增
    int insertBatch(List<FileInfo> list);

    //批量删除
    void deleteBatch(Long[] fileIds);

    //根据目录删除文件
    void deleteFilesByDirectoryId(Long directoryId);

}
