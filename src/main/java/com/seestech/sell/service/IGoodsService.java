package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Goods;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/17.
 */
public interface IGoodsService {
    //分页获取商品信息
    Page<Goods> getAllGoodses(RowBounds rowBounds, Goods info);

    //新增
    void insert(Goods info);

    //修改
    void update(Goods info);

    //根据goodsId 获取商品信息
    Goods getGoodsByGoodsId(Long goodsId);

    //删除
    void delete(Long goodsId);

    //批量删除
    void deleteBatch(Long[] goodsIds);

    //获取商品信息
    List<Goods> getGoodses(Goods info);

    //根据品牌编号获取商品信息
    List<Goods> getGoodsesByBrandIds(Long[] brandIds);

    //根据商品编号获取商品信息
    Goods getGoodsByCode(String code);

    //根据商品编号获取商品信息
    List<Goods> getGoodsesByCode(String code);

    //获取商品信息
    List<Goods> getGoodsByGoodsIds(String[] goodsIds);

    // 根据商品类型编号获取商品信息
    List<Goods> getGoodsesByTypeIds(Long[] typeIds);
}
