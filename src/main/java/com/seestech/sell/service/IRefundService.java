package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Refund;
import org.apache.ibatis.session.RowBounds;

/**
 * Created by idiot on 2017/7/7.
 * @description  退款服务
 */
public interface IRefundService {
    //新增
    void insert(Refund info);

    //修改
    void update(Refund info);

    //获取所有退款信息
    Page<Refund> getAllRefundes(RowBounds rowBounds, Refund info);

    //删除
    void delete(Long refundId);

    //批量删除
    void deleteBatch(Long[] refundIds);
}
