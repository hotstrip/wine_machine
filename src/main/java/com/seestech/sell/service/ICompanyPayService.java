package com.seestech.sell.service;

import com.seestech.sell.domain.model.CompanyPay;

/**
 * Created by idiot on 2017/9/9.
 */
public interface ICompanyPayService {
    // 新增
    void insert(CompanyPay info);

    // 修改
    void update(CompanyPay info);

    // 删除
    void delete(Long id);

    // 获取单个
    CompanyPay getCompanyPayById(Long id);

    // 根据企业编号获取
    CompanyPay getCompanyPayByCompanyId(Long companyId);

    // 根据订单编号获取企业支付信息
    CompanyPay getCompanyPayByOrderId(Long orderId);

    // 根据终端编号获取企业支付信息
    CompanyPay getCompanyPayByAgentId(Long agentId);
}
