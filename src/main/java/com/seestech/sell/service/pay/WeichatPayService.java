package com.seestech.sell.service.pay;

import com.lijing.wechatpay.conn.PaymentTools;
import com.lijing.wechatpay.impl.PayImpl;
import com.lijing.wechatpay.util.PayMD5;
import com.seestech.sell.common.utils.DateUtils;
import com.seestech.sell.common.utils.PayConstants;
import com.seestech.sell.domain.model.pay.*;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;


/**
 * Created by idiot on 2017/6/26.
 */
@Service
public class WeichatPayService {

    //生成订单
    public WeichatOrder preOrder(String wechat_app_id, String wechat_mch_id, String wechat_notify_url,
                                 String orderId, String goods, int price){
        WeichatOrder weichatOrder = new WeichatOrder();
        weichatOrder.setAppid(wechat_app_id);
        weichatOrder.setMch_id(wechat_mch_id); //商户号
        weichatOrder.setDevice_info("WEB");
        weichatOrder.setNonce_str(PayMD5.GetMD5nonce_str());     //随机字符串
        weichatOrder.setBody(goods);
        weichatOrder.setOut_trade_no(orderId);    //商户订单号
        weichatOrder.setFee_type("CNY");
        weichatOrder.setTotal_fee(String.valueOf(price));
        weichatOrder.setSpbill_create_ip(PaymentTools.getServerIP());    //终端id
        weichatOrder.setNotify_url(wechat_notify_url);
        weichatOrder.setTrade_type(PayConstants.Trade_type);
        weichatOrder.setTime_expire(DateUtils.getNextDayTime());        //设置失效时间  一天后
        return weichatOrder ;
    }

    /**
     * @description 下订单  获取返回结果
     * @param orderId
     * @param goods
     * @param price  单位   元 ==》》分
     * @return
     */
    public WeichatOrderResult orderResult(String wechat_app_id, String wechat_mch_id, String wechat_mch_key,
                                          String wechat_notify_url, String orderId, String goods, double price){
        //调用生成订单方法
        WeichatOrder order = preOrder(wechat_app_id, wechat_mch_id, wechat_notify_url, orderId, goods, (int) (price*100));
        String reqXML = PayImpl.generateXML(order, wechat_mch_key);
        String respXML = PayImpl.requestWechat(PayConstants.ORDER_URL, reqXML);
        WeichatOrderResult result = (WeichatOrderResult) PayImpl.turnObject(WeichatOrderResult.class, respXML);
        return result;
    }

    /**
     * @description  微信退款
     * @param wechat_app_id
     * @param wechat_mch_id
     * @param wechat_mch_key
     * @param refundId  退款编号
     * @param orderId   订单编号
     * @param price     订单交易金额
     * @param refundFee 退款金额
     * @param certPath  证书路径
     * @return
     */
    public WeichatRefundResult refundResult(String wechat_app_id, String wechat_mch_id, String wechat_mch_key, String refundId, String orderId, double price, double refundFee, String certPath){
        DecimalFormat d = new DecimalFormat("0");
        WeichatRefund weichatRefund = getRefund(wechat_app_id, wechat_mch_id, wechat_mch_key, refundId, orderId, d.format(price*100), d.format(refundFee*100));
        String reqXML = PayImpl.generateXML(weichatRefund, wechat_mch_key);
        String respXML = PayImpl.requestWechat(PayConstants.REFUND_URL, reqXML, certPath, wechat_mch_id);
        WeichatRefundResult result = (WeichatRefundResult) PayImpl.turnObject(WeichatRefundResult.class, respXML);
        return result;
    }

    //生成退款信息
    private WeichatRefund getRefund(String wechat_app_id, String wechat_mch_id, String wechat_mch_key, String refundId, String orderId, String price, String refundFee) {
        WeichatRefund info = new WeichatRefund();
        info.setAppid(wechat_app_id);
        info.setMch_id(wechat_mch_id);
        info.setRefund_account("REFUND_SOURCE_RECHARGE_FUNDS");     //退款账户   可用余额
        info.setNonce_str(PayMD5.GetMD5nonce_str());    //随机字符串
        info.setOut_refund_no(refundId);                //退款单号
        info.setOut_trade_no(orderId);                  //交易订单号
        info.setTotal_fee(String.valueOf(price));       //订单金额
        info.setRefund_fee(String.valueOf(refundFee));  //退款金额
        info.setSign(PayImpl.paySign(info, wechat_mch_key));
        return info;
    }
}
