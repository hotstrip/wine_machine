package com.seestech.sell.service.pay;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.AlipayRequest;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradePrecreateModel;
import com.alipay.api.domain.AlipayTradeRefundModel;
import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.seestech.sell.common.utils.PayConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by idiot on 2017/6/26.
 */
@Service
public class AlipayService {
    private static Logger logger = LoggerFactory.getLogger(AlipayService.class);

    /**
     * 支付宝下订单
     * @param alipay_app_id
     * @param alipay_public_key
     * @param alipay_private_key
     * @param alipay_sign_type
     * @param orderId
     * @param price
     * @param goods
     * @return
     * @throws AlipayApiException
     */
    public AlipayTradePrecreateResponse preOrder(String alipay_app_id, String alipay_public_key, String alipay_private_key,
                                                 String alipay_sign_type, String alipay_notify_url,
                                                 String orderId, double price,
                                                 String goods) throws AlipayApiException {
        AlipayClient alipayClient = new DefaultAlipayClient(PayConstants.gateway, alipay_app_id, alipay_private_key, "json", PayConstants.charset, alipay_public_key, alipay_sign_type);
        AlipayRequest alipayRequest = new AlipayTradePrecreateRequest();

        AlipayTradePrecreateModel param = new AlipayTradePrecreateModel();
        param.setOutTradeNo(orderId);
        param.setTotalAmount(String.valueOf(price));
        param.setSubject(goods);
        param.setTimeoutExpress("24h");      //支付失效时间 1m～15d。m-分钟，h-小时，d-天，1c-当天
        alipayRequest.setBizModel(param);
        alipayRequest.setNotifyUrl(alipay_notify_url); //支付回调地址
        AlipayTradePrecreateResponse res = (AlipayTradePrecreateResponse) alipayClient.execute(alipayRequest);
        return res;
    }

    /**
     * 支付宝退款
     * @param alipay_app_id
     * @param alipay_public_key
     * @param alipay_private_key
     * @param alipay_sign_type
     * @param orderId
     * @param price
     * @param reason
     * @return
     * @throws AlipayApiException
     */
    public AlipayTradeRefundResponse refundResponse(String alipay_app_id, String alipay_public_key, String alipay_private_key, String alipay_sign_type, String orderId, double price, String reason) throws AlipayApiException {
        AlipayClient alipayClient = new DefaultAlipayClient(PayConstants.gateway, alipay_app_id, alipay_private_key, "json", PayConstants.charset, alipay_public_key, alipay_sign_type);
        AlipayRequest alipayRequest = new AlipayTradeRefundRequest();

        AlipayTradeRefundModel param = new AlipayTradeRefundModel();
        param.setOutTradeNo(orderId);   //交易订单号
        param.setRefundReason(reason);  //原因
        param.setRefundAmount(String.valueOf(price));   //金额
        alipayRequest.setBizModel(param);
        AlipayTradeRefundResponse res = (AlipayTradeRefundResponse) alipayClient.execute(alipayRequest);
        return res;
    }
}
