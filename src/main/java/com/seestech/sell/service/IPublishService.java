package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Publish;
import com.seestech.sell.domain.model.ThemeColumn;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/8/1.
 */
public interface IPublishService {
    //新增
    void insert(Publish info);

    //修改
    void update(Publish info);

    //删除
    void delete(Long publishId);

    //批量删除
    void deleteBatch(Long[] publishIds);

    //发布信息列表
    Page<Publish> getPublishList(RowBounds rowBounds, Publish info);

    //新增主题相关信息
    boolean addTheme(Publish info, List<ThemeColumn> themeColumns);

    //获取单个
    Publish getPublishByPublishId(Long publishId);

    boolean addInfo(Publish info);

    boolean updateInfo(Publish info);

    //删除
    String remove(Publish info, String baseDirectory);

    //获取多个
    List<Publish> getPublishByPublishIds(Long[] publishIds);

    //根据名称获取
    List<Publish> getPublishByPublishName(String name);
}
