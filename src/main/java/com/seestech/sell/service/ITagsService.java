package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Tags;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/8/14.
 */
public interface ITagsService {
    //新增
    void insert(Tags info);

    //修改
    void update(Tags info);

    //删除
    void delete(Long id);

    //获取多个
    Page<Tags> getTagsByTags(RowBounds rowBounds, Tags info);

    //根据名称获取
    Tags getTagByTagName(String tagName);

    //获取多个
    List<Tags> getTagsByTagName(String tagName);

    //根据编号查询你标签信息
    Tags getTagsById(Long id);

    //批量删除
    void deleteBatch(Long[] ids);
}
