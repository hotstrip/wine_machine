package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Area;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * Created by idiot on 2017/6/19.
 */
public interface IAreaService {
    //分页获取区域信息
    Page<Area> getAllAreas(RowBounds rowBounds, Area info);

    //新增
    void insert(Area info);

    //修改
    void update(Area info);

    //根据areaId获取区域信息
    Area getAreaByAreaId(Long areaId);

    //删除
    void delete(Long areaId);

    //批量删除
    void deleteBatch(Long[] areaIds);

    //获取区域信息
    List<Area> getAreas(Area area);

    // 根据名称获取
    List<Area> getAreasByName(String name);
}
