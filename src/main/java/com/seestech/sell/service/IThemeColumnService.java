package com.seestech.sell.service;

import com.seestech.sell.domain.model.ThemeColumn;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by idiot on 2017/1/6.
 */
public interface IThemeColumnService {
    //根据主题id获取主题栏目信息
    List<ThemeColumn> getThemeColumnsByThemeId(long themeId);

    //根据主题栏目编号获取信息
    ThemeColumn getThemeColumnByThemeColumnId(@Param("themeColumnId") Long themeColumnId);

    //新增
    void insert(ThemeColumn info);

    //修改
    void update(ThemeColumn info);

    //删除
    void delete(Long themeColumnId);

    //根据目录编号获取主题栏目信息
    List<ThemeColumn> getThemeColumnsDirectoryId(Long directoryId);

    //根据主题编号和栏目目录编号查询主题栏目信息
    ThemeColumn getThemeColumnByIds(Long themeId, Long directoryId);

    //删除主题栏目信息
    void deleteBatch(List<ThemeColumn> list);

    //批量新增
    boolean insertBatch(List<ThemeColumn> themeColumns);
}
