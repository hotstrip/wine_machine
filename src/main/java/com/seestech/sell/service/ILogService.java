package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Log;
import org.apache.ibatis.session.RowBounds;

/**
 * Created by idiot on 2016/12/14.
 */
public interface ILogService {
    //新增日志信息
    void addloginLog(Log log);

    //分页查询所有日志信息
    Page<Log> getAllLogs(RowBounds rowBounds, Log info);

    //新增操作日志
    void addOperateLog(Log log);

    //删除
    void delete(long logId);

    //批量删除
    void deleteBatch(Long[] logIds);
}
