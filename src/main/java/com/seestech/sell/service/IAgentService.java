package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Agent;
import com.seestech.sell.domain.model.vo.AgentVoExcel;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * Created by idiot on 2017/6/21.
 */
public interface IAgentService {
    //新增
    void insert(Agent info);

    //修改
    void update(Agent info);

    //根据agentId获取终端信息
    Agent getAgentByAgentId(long agentId);

    //删除
    void delete(long agentId);

    //根据用户信息  分页加载终端信息
    Page<Agent> getAllAgentsByUserId(RowBounds rowBounds, Agent info);

    //批量删除
    void deleteBatch(Long[] agentIds);

    //根据终端分组id获取终端信息  分页
    List<Agent> getAgentsByAgentGroupId(long agentGroupId, RowBounds rowBounds);

    //根据终端分组编号获取终端
    List<Agent> getAgentsByAgentGroupIds(Long[] agentGroupIds);

    //分页加载所有终端的截图信息
    Page<Agent> getAllAgentsScreenshots(RowBounds rowBounds, Agent agent);

    //查询所有的终端信息
    List<Agent> getAllAgents(Agent info);

    //根据终端code获取终端信息
    Agent getAgentByAgentCode(String agentCode);

    //根据货架编号获取终端信息
    List<Agent> getAgentsByShelfIds(Long[] shelfIds);

    //根据终端编号获取终端信息
    List<Agent> getAgentsByAgentCode(String agentCode);

    //根据gridId获取终端信息
    List<Agent> getAgentsByGridId(Long gridId);

    //根据终端id获取终端的库存信息
    Map<String,Object> getGoodsInfo(Long agentId);

    //批量修改
    void updateBatch(List<Agent> agentList);

    //根据终端编号获取商品信息
    List<Map<String, Object>> getGoodsInfoByIds(List<Long> ids);

    //批量新增
    void insertBatch(List<AgentVoExcel> list);

    //导出数据查询
    List<AgentVoExcel> getAllExportAgentsByUserId(RowBounds rowBounds, Agent info);

    //根据终端类型编号查询终端信息
    List<Agent> getAgentsByAgentTypeIds(Long[] agentTypeIds);
}
