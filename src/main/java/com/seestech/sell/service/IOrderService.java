package com.seestech.sell.service;

import com.github.pagehelper.Page;
import com.seestech.sell.domain.model.Order;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;

/**
 * Created by idiot on 2017/6/22.
 */
public interface IOrderService {
    //新增
    void insert(Order info);

    //修改
    void update(Order info);

    //删除
    void delete(Long orderId);

    //批量删除
    void deleteBatch(Long[] orderIds);

    //分页获取订单信息
    Page<Order> getAllOrders(RowBounds rowBounds, Order info);

    //根据订单编号查询订单信息
    Order getOrderByOrderId(Long orderId);

    //根据customerId获取订单信息
    HashMap getOrdersByCustomerId(String customerId);

    //获取数据
    List<Order> getMonthlySales(Order info);

    //获取商品销售额度
    List<Order> getGoodsSales(Order info);

    //获取数据
    List<Order> getAllExportOrders(RowBounds rowBounds, Order info);

    //根据时间格式化获取销售信息
    List<Order> getSalesByDateformat(Order info);

    //根据时间格式化获取订单信息
    List<Order> getOrdersByDateformat(Order info);

    // 获取可退款订单信息
    Page<Order> getRefundAbleOrders(RowBounds rowBounds, Order info);

    // 获取商品排行榜
    Page<Order> getGoodsRank(RowBounds rowBounds, Order info);

    // 获取终端排行榜
    Page<Order> getAgentRank(RowBounds rowBounds, Order info);
}
