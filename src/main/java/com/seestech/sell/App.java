
package com.seestech.sell;

import com.seestech.sell.common.threads.AgentsDataBatchThread;
import com.seestech.sell.common.threads.LogFileThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * Hello world!
 */
/*
@SpringBootApplication
@EnableTransactionManagement
public class App extends SpringBootServletInitializer {
    private static Logger logger = LoggerFactory.getLogger(App.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        logger.info("===========>>>>>>>>>【售货机系统】项目启动.......");
        //添加监听事件
        builder.listeners(new MyApplicationPreparedEventListener());
        return builder.sources(App.class);
    }

    @Bean
    public Object transactionManagement(PlatformTransactionManager platformTransactionManager){
        logger.info(">>>>>>>>>>【事务管理器】>>>>>>>>>");
        logger.info(">>>>>>>>>>" + platformTransactionManager.getClass().getName());
        logger.info(">>>>>>>>>>【事务管理器】>>>>>>>>>");
        return new Object();
    }
}*/

@SpringBootApplication
@EnableTransactionManagement
public class App {
    private static Logger logger = LoggerFactory.getLogger(App.class);
    public static void main( String[] args ) {
        logger.info( "项目启动!" );
        SpringApplication.run(App.class, args);
        //日志写入文件线程
        new Thread(new LogFileThread()).start();
        // 终端信息批量新增线程
        new Thread(new AgentsDataBatchThread()).start();
    }

    @Bean
    public PlatformTransactionManager txManager(DataSource dataSource) {
        logger.info(">>>>>>>>>>【数据源】>>>>>>>>>");
        logger.info(">>>>>>>>>>" + dataSource.getClass().getName());
        logger.info(">>>>>>>>>>【数据源】>>>>>>>>>");
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean
    public Object transactionManagement(PlatformTransactionManager platformTransactionManager){
        logger.info(">>>>>>>>>>【事务管理器】>>>>>>>>>");
        logger.info(">>>>>>>>>>" + platformTransactionManager.getClass().getName());
        logger.info(">>>>>>>>>>【事务管理器】>>>>>>>>>");
        return new Object();
    }
}
