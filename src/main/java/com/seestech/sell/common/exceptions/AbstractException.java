/**
 * Package Name:com.zx.idiot.myexception
 * Date:2016年6月30日上午10:26:22
 * Copyright (c) 2016, www.chaincar.com All Rights Reserved.
 */

package com.seestech.sell.common.exceptions;

/**
 * ClassName:AbstractException <br/>
 * Function: TODO 自定义异常类. 作为自定义异常类的父类<br/>
 * Date:     2016年6月30日 上午10:26:22 <br/>
 * @author   idiot
 * @version  
 * @see 	 
 */
public class AbstractException extends RuntimeException{

	private static final long serialVersionUID = -895800631092994278L;

	public AbstractException(String message){
		super(message);
	}
}

