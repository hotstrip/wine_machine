package com.seestech.sell.common.exceptions;


import com.seestech.sell.common.exceptions.model.ResponseInfo;
import org.apache.shiro.ShiroException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @description 全局异常处理类
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("error");
        if(e instanceof ShiroException){
            mav.addObject("msg", "您可能没有访问权限, 请联系管理员");
            mav.addObject("url", req.getRequestURL());
        }else{
            mav.addObject("msg", e.getMessage());
            mav.addObject("url", req.getRequestURL());
        }
        logger.error("错误信息=====>" + e.getMessage());
        e.printStackTrace();
        return mav;
    }

    /**
     * @description 自定义异常处理
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = MyException.class)
    @ResponseBody
    public ResponseInfo<String> exceptionHandler(HttpServletRequest req, MyException e) throws Exception {
        ResponseInfo<String> r = new ResponseInfo<>();
        r.setMessage(e.getMessage());
        r.setCode(ResponseInfo.ERROR);
        r.setData("操作出错......");
        r.setUrl(req.getRequestURL().toString());
        logger.error("错误信息=====>" + e.getMessage());
        return r;
    }

}

