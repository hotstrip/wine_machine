package com.seestech.sell.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;


/**
 * @description  配置restTemplate
 * Created by idiot on 2017/7/26.
 */
@Configuration
public class RestTemplateConfig {

    @Bean
    public ClientHttpRequestFactory myClientHttpRequestFactory(){
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(1000 * 15);    //连接超时时间设置   ms
        factory.setReadTimeout(1000 * 5);        //读取超时          ms
        return factory;
    }

    @Bean
    public RestTemplate myRestTemplate(ClientHttpRequestFactory factory){
        return new RestTemplate(factory);
    }
}
