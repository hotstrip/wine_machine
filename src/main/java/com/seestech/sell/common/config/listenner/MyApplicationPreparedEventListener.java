package com.seestech.sell.common.config.listenner;

import com.seestech.sell.common.threads.AgentsDataBatchThread;
import com.seestech.sell.common.threads.LogFileThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.context.ApplicationListener;


/**
 * Created by idiot on 2017/3/22.
 */
public class MyApplicationPreparedEventListener implements ApplicationListener<ApplicationPreparedEvent> {
    private static Logger logger = LoggerFactory.getLogger(MyApplicationPreparedEventListener.class);

    @Override
    public void onApplicationEvent(ApplicationPreparedEvent applicationPreparedEvent) {

        //日志写入文件线程
        new Thread(new LogFileThread()).start();

        // 终端信息批量新增线程
        new Thread(new AgentsDataBatchThread()).start();
    }
}


