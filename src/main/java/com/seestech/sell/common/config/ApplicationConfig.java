package com.seestech.sell.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by idiot on 2017/6/21.
 */
@Component(value = "ApplicationConfig")
@ConfigurationProperties(prefix = "application")
public class ApplicationConfig {
    @Value("${application.uploadUrl}")
    private String uploadUrl;               //文件上传路径

    @Value("${application.baseDirectory}")
    private String baseDirectory;           //文件存储地址

    @Value("${application.version}")
    private String version;                 //版本

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    public String getBaseDirectory() {
        return baseDirectory;
    }

    public void setBaseDirectory(String baseDirectory) {
        this.baseDirectory = baseDirectory;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
