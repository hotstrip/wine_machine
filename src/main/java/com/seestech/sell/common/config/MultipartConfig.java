package com.seestech.sell.common.config;

import org.springframework.boot.autoconfigure.web.MultipartProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.MultipartConfigElement;

/**
 * Created by idiot on 2016/12/28.
 */

/**
 * @description 设置上传文件大小限制  单位kb
 */
@Configuration
public class MultipartConfig {
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartProperties multipartProperties = new MultipartProperties();
        multipartProperties.setEnabled(true);
        multipartProperties.setMaxFileSize(50 + "MB");
        multipartProperties.setMaxRequestSize(50 + "MB");
        return multipartProperties.createMultipartConfig();
    }

    /*@Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        resolver.setMaxUploadSizePerFile(50 * 1024 * 1024);
        return resolver;
    }*/
}
