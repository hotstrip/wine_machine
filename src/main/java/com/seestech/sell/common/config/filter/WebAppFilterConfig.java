package com.seestech.sell.common.config.filter;

import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idiot on 2017/4/14.
 * @description  过滤器   检测session中的用户是否失效
 */
@Configuration
public class WebAppFilterConfig {
    @Bean
    public FilterRegistrationBean adminUserFilterRegistrationBean(){
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        //设置过滤器
        filterRegistrationBean.setFilter(new AdminUserFilter());
        //设置过滤路径
        List<String> urlPatterns = new ArrayList<String>();
        urlPatterns.add("/*");
        filterRegistrationBean.setUrlPatterns(urlPatterns);
        filterRegistrationBean.setOrder(1);
        return filterRegistrationBean;
    }
}
