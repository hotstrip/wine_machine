package com.seestech.sell.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.velocity.VelocityToolboxView;
import org.springframework.web.servlet.view.velocity.VelocityViewResolver;

/**
 * Created by idiot on 2017/8/8.
 */
@Configuration
@ConfigurationProperties(prefix = "spring.velocity")
public class MvcConfiguration extends WebMvcConfigurerAdapter {

    @Value("${spring.velocity.suffix}")
    private String suffix;               //suffix

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Bean
    public ViewResolver viewResolver() {
        VelocityViewResolver resolver = new VelocityViewResolver();
        resolver.setViewClass(VelocityToolboxView.class);
        resolver.setSuffix(suffix);
        return resolver;
    }

}
