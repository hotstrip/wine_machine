package com.seestech.sell.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by idiot on 2017/6/27.
 * @description 压缩文件
 */
public class CompressedFileUtil {
    private static Logger logger = LoggerFactory.getLogger(CompressedFileUtil.class);
    /**
     * @desc 将源文件/文件夹生成指定格式的压缩文件,格式zip
     * @param resourcesPath 源文件/文件夹
     * @param targetPath  目的压缩文件路径
     * @return void
     * @throws Exception
     */
    public static void compressedFile(String resourcesPath, String targetPath) throws Exception{
        File resourcesFile = new File(resourcesPath);     //源文件
        File targetFile = new File(targetPath);           //目的
        //如果目的路径不存在，则新建
        if(!targetFile.exists()){
            targetFile.getParentFile().mkdirs();
        }

        FileOutputStream outputStream = new FileOutputStream(targetPath);
        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(outputStream));

        createCompressedFile(out, resourcesFile, "");

        out.close();
    }

    /**
     * @desc 生成压缩文件。
     *               如果是文件夹，则使用递归，进行文件遍历、压缩
     *       如果是文件，直接压缩
     * @param out  输出流
     * @param file  目标文件
     * @param dir
     * @return void
     * @throws Exception
     */
    public static void createCompressedFile(ZipOutputStream out, File file, String dir) {
        //初始化dir
        String directoryPath = dir;
        if (directoryPath == null) {
            directoryPath = "";
        }

        //如果当前的是文件夹，则进行进一步处理
        if(file.isDirectory()){
            //得到文件列表信息
            File[] files = file.listFiles();
            try {
                //将文件夹添加到下一级打包目录
                if(!org.springframework.util.StringUtils.isEmpty(directoryPath))
                    out.putNextEntry(new ZipEntry(directoryPath+"/"));
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }

            directoryPath = directoryPath.length() == 0 ? "" : directoryPath +"/";

            //循环将文件夹中的文件打包
            for(int i = 0 ; i < files.length ; i++){
                createCompressedFile(out, files[i], directoryPath + files[i].getName());         //递归处理
            }
        } else{   //当前的是文件，打包处理
            try {
                //文件输入流
                FileInputStream fis = new FileInputStream(file);

                out.putNextEntry(new ZipEntry(directoryPath));
                //进行写操作
                int j =  0;
                byte[] buffer = new byte[1024];
                while((j = fis.read(buffer)) > 0){
                    out.write(buffer,0,j);
                }
                fis.close();    //关闭输入流
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

}
