package com.seestech.sell.common.utils;

/**
 * Created by idiot on 2017/6/26.
 */
public class PayConstants {
    /********************微信二维码支付   start***********************/
    //付款方式
    public static final String Trade_type = "NATIVE";
    //微信统一下单接口
    public static final String ORDER_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
    //微信退款接口
    public static final String REFUND_URL = "https://api.mch.weixin.qq.com/secapi/pay/refund";
    /********************微信二维码支付   end**************************/

    /********************支付宝二维码支付  start***************************************/
    //支付宝网关
    public static final String gateway = "https://openapi.alipay.com/gateway.do";
    //编码
    public static final String charset = "UTF-8";
    /********************支付宝二维码支付  end*****************************************/
}
