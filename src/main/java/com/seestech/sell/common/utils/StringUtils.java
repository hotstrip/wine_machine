package com.seestech.sell.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;

import static java.util.Arrays.copyOf;

/**
 * Created by idiot on 2017/1/16.
 */
public class StringUtils {
    private static Logger logger = LoggerFactory.getLogger(StringUtils.class);

    /**
     * @description 去除掉最后一个";"
     * @param buffer
     * @return
     */
    public static String removeLastSemicolon(StringBuffer buffer){
        String str = buffer.toString();
        if(str.endsWith(";")){
            str = str.substring(0, str.lastIndexOf(';'));
        }
        return str;
    }

    public static String replaceLastChar(StringBuffer buffer){
        String str = buffer.toString();
        if(str.endsWith(";")){
            str = str.substring(0, str.lastIndexOf(';'));
            str += Constants.CsMessageSeperator.and;
        }
        return str;
    }

    /**
     * @description 拆分信息  #
     * @param src   信息数据
     * @param size  大小
     * @return
     */
    public static String[] isolate(String src, int size) {
        String[] res = src.split("#");
        if (res.length != size || size < 1) {
            logger.error("Format isolate error:" + res.length + "|" + size);
            res = null;
        }
        return res;
    }

    /**
     * @description 生成uuid
     * @return
     */
    public static String generateUUID(){
        return UUID.randomUUID().toString().replaceAll("-", "");
    }


    /**
     * @description  数组转字符串
     * @param objects       数组
     * @param option        分隔符
     * @return
     */
    public static String arrayToString(Object[] objects, String option){
        //判断分隔符是否为空    设置默认值
        if(option == null || "".equals(option))
            option = ";";
        StringBuffer buffer = new StringBuffer();
        for (Object o : objects){
            if(o != null || !"".equals(o))
                buffer.append(o).append(option);
        }
        int index = buffer.lastIndexOf(option);
        return index < 0 ? "" : buffer.substring(0, index);
    }

    public static String arrayToString(int[] objects, String option){
        //重载方法
        if(option == null || "".equals(option))
            option = ";";
        StringBuffer buffer = new StringBuffer();
        for (Object o : objects){
            if(o != null || !"".equals(o))
                buffer.append(o).append(option);
        }
        int index = buffer.lastIndexOf(option);
        return index < 0 ? "" : buffer.substring(0, index);
    }


    /**
     * @description  字符串转换成数组
     * @param objects
     * @param param
     * @param option
     * @return
     */
    public static int[] stringToArray(int[] objects, String param, String option){
        //非空判断
        if(param == null || "".equals(param)){
            return objects;
        }
        //判断分隔符是否为空    设置默认值
        if(option == null || "".equals(option))
            option = ";";
        //根据param分割字符串  转换成数组
        String[] temp = param.split(option);
        objects = toIntArray(temp, temp.length);
        return objects;
    }

    public static int[] toIntArray(String[] objects, int length){
        int[] result = new int[length];
        //判断当前的数组是否为空  以及  元素个数是否小于1
        if(objects == null || objects.length < 1){
            for (int i = 0; i < length; i++) {
                result[i] = 0;
            }
        }else {
            for (int i = 0; i < objects.length; i++) {
                if(i<length) {
                    result[i] = Integer.parseInt(objects[i]);
                }
            }
        }
        return result;
    }

    public static String[] toStringArray(int[] objects){
        String[] result = new String[objects.length];
        for (int i = 0; i < objects.length; i++) {
            result[i] = String.valueOf(objects[i]);
        }
        return result;
    }

    //随机数
    public static String randomValue(int seed){
        return String.valueOf(new Random().nextInt(seed));
    }

    //反转字符串
    public static String revert(String a) {
        char[] chars = a.toCharArray();
        int index, len = chars.length;
        for(int i = 0; i < len / 2; i++){
            index = len-i-1;
            // if(i != index){
                char temp = chars[i];
                chars[i] = chars[index];
                chars[index] = temp;
            // }
        }
        return new String(chars);
    }

}
