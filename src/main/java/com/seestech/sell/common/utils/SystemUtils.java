package com.seestech.sell.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by idiot on 2017/4/10.
 * @description  获取系统相关信息
 */
public class SystemUtils {
    private static Logger logger = LoggerFactory.getLogger(SystemUtils.class);

    private static String OS = System.getProperty("os.name").toLowerCase();

    private static String Windows = "windows";

    /**
     * @description判断当前系统是否是windows
     * @return
     */
    public static boolean isWindows(){
        return OS.indexOf(Windows) >= 0;
    }
}
