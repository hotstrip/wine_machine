package com.seestech.sell.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by yangyaofeng on 2016/5/11.
 */
public class DateUtils {
    private static Logger logger = LoggerFactory.getLogger(DateUtils.class);
    private static final SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static final String SIMPLE_DATE_PATTERN = "yyyy-MM-dd";

	public static final String SIMPLE_DATE_HOURS_PATTERN = "yyyy-MM-dd HH:mm:ss";

	public static final String SDF_YMD_PATTERN = "yyyyMMdd";

	public static final String SDF_YM = "yyyy-MM";

	public static final String SDF_MINUTS = "yyyy-MM-dd HH:mm";

	public static final String SDF_HOUR_MINUTS = "HH:mm";

	public static final String SDF_MD_PATTERN = "MM-dd";

	public static final String SDF_MD_HM_PATTERN = "MM-dd HH:mm";

	public static final String SDF_YMDHMS_PATTERN = "yyyyMMddHHmmss";


    public static Date stringToDate(String str, String format) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setLenient(Boolean.FALSE.booleanValue());
        Date date = sdf.parse(str);
        return date;
    }

    private static final long daytimes = 86400000L;
	public static final String YYMMDDHHMMSS = "yyMMddhhmmss";

    public static Date parseDatetime(String datetime, String pattern) throws ParseException {
        SimpleDateFormat format = (SimpleDateFormat) datetimeFormat.clone();
        format.applyPattern(pattern);
        return format.parse(datetime);
    }

    public static Date getDateByFormat(Date sourceDate, String format) throws ParseException {
        SimpleDateFormat f = new SimpleDateFormat(format);
        String dateString = f.format(sourceDate);
        Date date = f.parse(dateString);
        return date;
    }

    public static Date getDateByFormat(String sourceDateStr, String format) {
        SimpleDateFormat f = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = f.parse(sourceDateStr);
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }
        return date;
    }

    public static String formatDatetime(Date date) {
        return datetimeFormat.format(date);
    }

    public static String formatDatetime(Date date, String pattern) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat customFormat = (SimpleDateFormat) datetimeFormat.clone();
        customFormat.applyPattern(pattern);
        return customFormat.format(date);
    }

    /**
     * 获取当天开始时间
     * 
     * @return
     * @throws ParseException
     */
    public static Date getTodayStartTime() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return datetimeFormat.parse(format.format(new Date()) + " 00:00:00");
    }

    /**
     * 获取当天结束时间
     * 
     * @return
     * @throws ParseException
     */
    public static Date getTodayEndTime() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return datetimeFormat.parse(format.format(new Date()) + " 23:59:59");
    }

    /** 返回今天的日期，带时分秒 */
    public static Date currentDateTime() {
        return new Date();
    }

    /**
     * 格式化日期 为指定格式字符串
     * @param date
     * @param formatString
     * @return
     */
    public static String format(Date date, String formatString) {
        if (date == null) {
            return null;
        } else {
            SimpleDateFormat df = new SimpleDateFormat(formatString);
            return df.format(date);
        }
    }

    /**
     * @description 按照指定字符串格式  转换字符串时间为Date类型
     * @param dateStr
     * @param parseStr
     * @return
     */
    public static Date parse(String dateStr, String parseStr){
        SimpleDateFormat dfs = new SimpleDateFormat(parseStr);
        Date date = null;
        try {
            date = dfs.parse(dateStr);
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }
        return date;
    }


    public static boolean isEqual(Date date1, Date date2) {
        return date1.compareTo(date2) == 0;
    }


    public static long getDateSubDays(Date date1, Date date2) {
        long d1 = date1.getTime();
        long d2 = date2.getTime();
        long diff = 0L;
        if (d1 == d2)
            return diff;
        if (d1 > d2)
            diff = d1 - d2;
        else {
            diff = d2 - d1;
        }
        return diff / daytimes;
    }

    /**
     * ifNowBetweenTwoDate:判断当前时间是否在传参的开始和结束时间之间. <br/>
     *
     * @author owen.wang
     * @param startDate
     * @param endDate
     * @return
     */
    public static boolean ifNowBetweenTwoDate(Date startDate, Date endDate) {
        if (startDate == null || endDate == null) {
            return true;
        }
        Date nowDate = new Date();
        if (nowDate.compareTo(startDate) >= 0 && nowDate.compareTo(endDate) <= 0) {
            return true;
        }
        return false;
    }

    /**
     * dateToString:按指定格式化时间数据. <br/>
     *
     * @author Owen.Wang
     * @param date
     *            传输的时间数据
     * @param pattern
     *            格式化格式
     * @return
     * @throws ParseException
     */
    public static String dateToString(Date date, String pattern) {
        if (null == date) {
            return null;
        }
        SimpleDateFormat sf = new SimpleDateFormat(pattern);
        return sf.format(date);
    }

    /**
     * getTime: 获得当天时间<br/>
     * @author Len.Song
     * @param parrten
     *            输出的时间格式
     * @return
     */
    public static String getTime(String parrten) {
        String timestr;
        if (parrten == null || parrten.equals("")) {
            parrten = "yyyy-MM-dd";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(parrten);
        Date cday = new Date();
        timestr = sdf.format(cday);
        return timestr;
    }

    /**
     * @description 计算两个时间之间的差值  返回分钟数
     * @param date1
     * @param date2
     * @return
     */
    public static long diffMinutes(Date date1, Date date2){
        long diffMilliseconds = diffMilliseconds(date1, date2);
        long diffMinutes = diffMilliseconds/(60 * 1000);
        //获取余数 大于0 就加一分钟
        if(diffMilliseconds % (60 * 1000) > 0){
            diffMinutes ++;
        }
        return diffMinutes;
    }

    /**
     * @description 计算两个时间之间的差值  返回毫秒数
     * @param date1
     * @param date2
     * @return
     */
    public static long diffMilliseconds(Date date1, Date date2){
        long diffMilliseconds = Math.abs(date1.getTime() - date2.getTime());
        return diffMilliseconds;
    }

    //获取当前时间之后的24小时
    public static String getNextDayTime(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, +1); //今天的时间加一天
        Date date = calendar.getTime();
        String result = DateUtils.formatDatetime(date, DateUtils.SDF_YMDHMS_PATTERN);
        return result;
    }

    //  获取指定时间之后的24小时
    public static String getNextDayTime(Date date, String dateFormat){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, +1); //今天的时间加一天
        Date nextDate = calendar.getTime();
        if (dateFormat == null)
            return DateUtils.formatDatetime(nextDate, DateUtils.SDF_YMDHMS_PATTERN);
        else
            return DateUtils.formatDatetime(nextDate, dateFormat);
    }


}
