package com.seestech.sell.common.utils;

/**
 * Created by idiot on 2016/12/15.
 * @description 常量
 */
public class Constants {

    public static class ResponseCode {
        public static final int success = 0;
        public static final int error = 1;
        public static final int invalid_user = 11;  //无效的用户信息
        public static final int invalid_param = 12;  //无效的参数信息

        //token  失效
        public static final int token_invalid = 510;
        //token  校验失败
        public static final int token_check_false = 511;
    }

    public static class Regular {
        //后台
        public static final String onlineUser = "onlineUser";
        public static final String charset = "UTF-8";
        public static final String html_header = "text/html;charset=UTF-8";

        public static final String monitor_wall = "monitor_wall/";  //监控墙图片路径
        public static final String agent_pictures = "agent_pictures";     //终端初始化图片存储地址
        public static final String disk_path = "/people/";                  //磁盘路径
        public static final String url_ini = "url.ini";                     //url.ini
        public static final String my_themes = "storage/myThemes";            //主题存档目录
        public static final String my_columns = "storage/myColumns";            //栏目存档目录
        public static final String my_leds = "storage/myLeds";              //字幕存档目录
        public static final String my_videos = "storage/myVideos";            //视频存档目录
        public static final String my_zips = "storage/myZips";              //软件包存档目录
        public static final String logs = "logs";                            //日志存储目录
        public static final String my_head_image = "headerImage";            //头像存储地址
        public static final String my_goods_image = "storage/goods";            //头像存储地址
        public static final String theme_xml_name = "index.xml";            //主题文件存储名字

        //默认头像
        public static final String avatar = "https://simg.ws.126.net/e/img5.cache.netease.com/tie/images/yun/photo_default_62.png.39x39.100.jpg";

        //windows 下使用该路径作为存储根目录
        public static final String base_dir = "D:\\";
        public static final long agent_live_minutes = 5;  //终端在线存活时间  min

        public static final String reviewPermission = "release:publish:review";   //审核权限
    }

    //终端消息头(类型)
    public static class AgentTitile {
        public static final String one = "1:";
        public static final String two = "2:";
        public static final String three = "3:";
        public static final String four = "4:";
        public static final String five = "5:";
        public static final String six = "6:";
        public static final String seven = "7:";
        public static final String eight = "8:";
        public static final String nine = "9:";
    }

    //cs消息分隔符
    public static class CsMessageSeperator {
        public static final String semicolon = ";";
        public static final String Hashtag = "#";
        public static final String and = "&";
        public static final String equals = "=";
    }

}
