package com.seestech.sell.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by idiot on 2017/7/21.
 * @description  图片操作类
 */
public class ImageUtils {
    private static Logger logger = LoggerFactory.getLogger(ImageUtils.class);

    public static int[] getImageSize(String filePath) throws IOException {
        int size[] = new int[2];
        File file = new File(filePath);
        //获取图片输入流
        ImageInputStream iis = ImageIO.createImageInputStream(file);
        //创建ImageReader对象
        Iterator<ImageReader> readers = ImageIO.getImageReaders(iis);
        ImageReader reader = readers.next();
        reader.setInput(iis, true);
        size[0] = reader.getWidth(0);       //图片的尺寸宽
        size[1] = reader.getHeight(0);      //尺寸高

        return size;
    }
}
