package com.seestech.sell.common.utils;

import org.apache.velocity.tools.config.DefaultKey;
import org.apache.velocity.tools.generic.FormatConfig;

/**
 * Created by idiot 2017/6/7.
 */
@DefaultKey("stringTool")
public class StringTool extends FormatConfig {

    public String format(Object obj) {
        return obj.toString();
    }

    public String valueOf(Object obj) {
        return String.valueOf(obj);
    }
}
