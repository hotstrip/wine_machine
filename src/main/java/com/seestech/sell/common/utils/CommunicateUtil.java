package com.seestech.sell.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by idiot on 2017/1/2.
 * @description 通信相关
 */
public class CommunicateUtil {
    private static Logger logger = LoggerFactory.getLogger(CommunicateUtil.class);
    /**
     * @description 数组转换成字符串
     * @param strings
     * @return
     */
    public static String arrayToString(String[] strings, String seperator){
        StringBuffer message = new StringBuffer();
        if(strings.length > 0) {
            for(String str : strings){
                if(str != null){
                    message.append(str).append(seperator);
                }
            }
            message.deleteCharAt(message.length() - 1);
        }
        return message.toString();
    }

    /**
     * @description 用#连接消息
     * @param message
     * @param volume
     * @return
     */
    public static String ConnectionHashtag(String message, int volume) {
        StringBuffer stringBuffer = new StringBuffer(message);
        stringBuffer.append(Constants.CsMessageSeperator.Hashtag).append(volume);
        return stringBuffer.toString();
    }

    /**
     * @description 获取最终的消息   头（类型） + 消息内容
     * @param title
     * @param message
     * @return
     */
    public static String getFinalMessage(String title, String message){
        StringBuffer buffer = new StringBuffer();
        logger.info("消息：" + buffer.append(title).append(message).toString());
        return buffer.append(title).append(message).toString();
    }

}
