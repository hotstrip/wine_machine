package com.seestech.sell.common.kaptcha;

import java.awt.image.BufferedImage;

/**
 * Created by idiot on 2017/5/18.
 * @description 干掉图片的背景   简洁
 */
public class KapchaStyle implements com.google.code.kaptcha.GimpyEngine {

    @Override
    public BufferedImage getDistortedImage(BufferedImage bufferedImage) {
        return bufferedImage;
    }
}
