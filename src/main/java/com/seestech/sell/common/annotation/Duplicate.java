package com.seestech.sell.common.annotation;

import java.lang.annotation.*;

/**
 * Created by idiot on 2017/5/31.
 * @description 用于防止重复提交
 */
@Inherited
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Duplicate {
    boolean value() default true;
}
