package com.seestech.sell.common.annotation;

import java.lang.annotation.*;

/**
 * Created by idiot on 2017/7/15.
 * @description  检测参数值是否正确
 */
@Target(value = ElementType.PARAMETER)
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface CheckFixedParamValue {
    //参数名称
    String name() default "";
    //指定参数值
    String[] value() default {};
}
