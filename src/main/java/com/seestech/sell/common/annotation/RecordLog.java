package com.seestech.sell.common.annotation;

import java.lang.annotation.*;

/**
 * Created by idiot on 2017/5/8.
 */
@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface RecordLog {
    //记录log的内容
    public String value() default "";
}
