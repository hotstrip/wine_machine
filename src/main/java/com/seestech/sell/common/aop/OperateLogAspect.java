package com.seestech.sell.common.aop;

import com.seestech.sell.common.utils.Constants;
import com.seestech.sell.common.utils.GetRequestUtil;
import com.seestech.sell.common.annotation.RecordLog;
import com.seestech.sell.domain.model.Log;
import com.seestech.sell.domain.model.User;
import com.seestech.sell.service.ILogService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * Created by idiot on 2016/12/21.
 * @description 记录操作日志信息 切面
 */
@Aspect
@Component
public class OperateLogAspect {
    private static Logger logger = LoggerFactory.getLogger(OperateLogAspect.class);

    @Resource
    private ILogService logService;

    //定义切点  controller类下面的带有@RecordLog注解的方法
    @Around("execution(public * com.seestech.sell.web..*(..)) and @annotation(com.seestech.sell.common.annotation.RecordLog))")
    public Object doMain(ProceedingJoinPoint proceedingJoinPoint) throws NoSuchMethodException {
        logger.info("=======>开始记录操作记录");
        //初始化log
        Log log = new Log();

        //切点获取方法名
        String methodName=proceedingJoinPoint.getSignature().getName();
        Class<?> classTarget=proceedingJoinPoint.getTarget().getClass();
        Class<?>[] par=((MethodSignature) proceedingJoinPoint.getSignature()).getParameterTypes();
        Method objMethod=classTarget.getMethod(methodName, par);

        //获取指定 注解的value
        RecordLog recordLog = objMethod.getAnnotation(com.seestech.sell.common.annotation.RecordLog.class);
        //设置日志内容
        log.setLogMessage(recordLog.value());
        logger.info("当前操作=========>>>>>>"+recordLog.value());

        //获取request对象
        HttpServletRequest request = GetRequestUtil.getRequest();
        String uri = request.getRequestURI();

        // String url = request.getRequestURL().toString();
        // String method = request.getMethod();
        // String queryString = request.getQueryString();
        // logger.info("请求开始, 各个参数, url: {}, method: {}, uri: {}, params: {}", url, method, uri, queryString);

        Object obj = null;
        try {
            obj = proceedingJoinPoint.proceed();
            log.setResponseCode(String.valueOf(Constants.ResponseCode.success));
        } catch (Throwable throwable) {
            logger.error("记录操作记录===>>>"+throwable.getMessage());
            log.setResponseCode(String.valueOf(Constants.ResponseCode.error));
        }

        User user = (User) request.getSession().getAttribute(Constants.Regular.onlineUser);
        if(user != null){
            log.setUserId(user.getUserId());                    //设置操作人
            log.setCreateUser(user.getUserId());                //设置操作人
            log.setRequestUrl(uri);                             //设置请求路径
            logService.addOperateLog(log);
        }
        logger.info("=======>记录操作记录结束");
        return obj;
    }

}
