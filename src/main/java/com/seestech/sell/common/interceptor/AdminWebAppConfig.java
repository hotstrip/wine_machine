package com.seestech.sell.common.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


/**
 * Created by idiot on 2017/1/4.
 * @description 自定义拦截器配置
 */
@Configuration
public class AdminWebAppConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry){
        //添加拦截器  生效
        registry.addInterceptor(new DuplicateInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(new NotNullParamInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(new CheckFixedParamValueInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(new CheckMinMaxParamValueInterceptor()).addPathPatterns("/**");
        super.addInterceptors(registry);
    }

}
