package com.seestech.sell.common.threads;

import com.seestech.sell.common.config.SpringUtil;
import com.seestech.sell.domain.mapper.IAgentDao;
import com.seestech.sell.domain.mapper.IAgentGroupDao;
import com.seestech.sell.domain.mapper.IAreaDao;
import com.seestech.sell.domain.mapper.IShelfDao;
import com.seestech.sell.domain.model.AgentGroup;
import com.seestech.sell.domain.model.Area;
import com.seestech.sell.domain.model.Shelf;
import com.seestech.sell.domain.model.vo.AgentVoExcel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by idiot on 2017/9/1.
 */
public class AgentsDataBatchThread implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(AgentsDataBatchThread.class);

    private IAgentDao agentDao = null;
    private IShelfDao shelfDao = null;
    private IAreaDao areaDao = null;
    private IAgentGroupDao agentGroupDao = null;

    public static List<Collection> pool = new ArrayList<>();

    //添加数据
    public static void addCollectionData(Collection collection){
        synchronized (pool){
            pool.add(pool.size(), collection);
            pool.notifyAll();
        }
    }

    @Override
    public void run(){
        while (true){
            logger.debug("=====>>>批量新增终端数据信息=====.>>>>【开始】");
            Collection collection = null;
            synchronized (pool){
                while (pool.isEmpty()){
                    try {
                        pool.wait();
                        Thread.currentThread().sleep(1000 * 10);
                    } catch (InterruptedException e) {
                        logger.error("批量新增终端数据信息===>>异常信息==>"+e.getMessage());
                        Thread.currentThread().interrupt();
                    }
                }
                collection = pool.remove(0);
                logger.debug("=====>>>批量新增终端数据信息=====.>>>>【集合大小】"+collection.size());
                if(agentDao == null)
                    agentDao = (IAgentDao) SpringUtil.getBean("IAgentDao");
                if(shelfDao == null)
                    shelfDao = (IShelfDao) SpringUtil.getBean("IShelfDao");
                if(areaDao == null)
                    areaDao = (IAreaDao) SpringUtil.getBean("IAreaDao");
                if(agentGroupDao == null)
                    agentGroupDao = (IAgentGroupDao) SpringUtil.getBean("IAgentGroupDao");
            }
            List<AgentVoExcel> list = (List<AgentVoExcel>) collection;
            //数据不为空时
            if(list != null && list.size() > 0){
                //遍历集合   name -->  id
                for (int i=0; i<list.size(); i++){
                    AgentVoExcel agentVo = list.get(i);
                    List<Shelf> shelfs = shelfDao.getShelfsByName(agentVo.getShelfName().trim());
                    List<Area> areas = areaDao.getAreasByName(agentVo.getAreaName().trim());
                    List<AgentGroup> agentGroups = agentGroupDao.getAgentGroupsByAgentGroupName(agentVo.getAgentGroupName().trim());
                    if(shelfs != null && shelfs.size() > 0)
                        agentVo.setShelfId(shelfs.get(0).getShelfId());
                    if(areas != null && areas.size() > 0)
                        agentVo.setAreaId(areas.get(0).getAreaId());
                    if(agentGroups != null && agentGroups.size() > 0)
                        agentVo.setAgentGroupId(agentGroups.get(0).getAgentGroupId());
                    if(agentVo.getShelfId() == null || agentVo.getAreaId() == null || agentVo.getAgentGroupId() == null){
                        list.remove(agentVo);
                    }
                }
                agentDao.insertBatch(list);
            }
        }
    }
}
